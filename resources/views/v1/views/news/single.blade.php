@extends($template.'.layout.master')

@section('meta_title')
| {{ $entry->title}}
@endsection
@section('meta_description')
 {{strip_tags($entry->subtitle)}}
@endsection

@section('content')
        <!-- Main -->
        @if($entry->article_type == 250 && isset($entry->images[0]->files) && is_array($entry->images[0]->files) && count($entry->images[0]->files)>0)
                <!-- Image full -->
                <div id="above-the-fold" class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif featured" @if($entry->article_type == 250 && isset($entry->sections) && is_array($entry->sections) && count($entry->sections)>0) style="margin-bottom:0px;" @endif >
                    <img src="{{$entry->images[0]->files[0]->cdn}}" />
                </div>
        @else


                @if(isset($banners['featured_long']))
                  <!-- Banner space / Show if BIG banner exists -->
                   <div class="banner-space top-banner-space  @if(isset($banners['featured_small']) && count($banners['featured_small'])>0) double-banner @endif">
                      @if(isset($banners['featured_long']) && count($banners['featured_long'])>0)
                          <div class="banner_big" >
                              <?php $featured_long = $banners['featured_long'][0]; ?>
                              @if(isset($featured_long->file_cdn))
                                   <a href="{{$featured_long->target_url}}" target="{{$featured_long->target_url_window}}">
                                       <img src="{{$featured_long->file_cdn}}" alt="{{$featured_long->description}}"/>
                                   </a>
                              @else
                                   {{{$featured_long->content }}}
                              @endif
                          </div>
                      @endif

                      @if(isset($banners['featured_small']) && count($banners['featured_small'])>0)
                        <div class="banner_small">
                            <?php $featured_small = $banners['featured_small'][0]; ?>
                            @if(isset($featured_small->file_cdn))
                                 <a href="{{$featured_small->target_url}}" target="{{$featured_small->target_url_window}}">
                                     <img src="{{$featured_small->file_cdn}}" alt="{{$featured_small->description}}"/>
                                 </a>
                            @else
                                 {{{$featured_small->content }}}
                            @endif
                        </div>
                      @endif

                      <div class="clearfix"></div>
                   </div>
               @endif


        @endif

         <!-- Section -->
        <section id="section" ng-app="mainApp" ng-controller="newsSingleCtrl" ng-init="lang='<?php echo $lang;?>'">
            <div class="inner-wrapper main-side-to-single ">

                <!-- Main -->
                <div id="main" @if($entry->article_type != 250) class="left" @endif role="main">

                    <!-- Post -->
                    <article class="single-post post-70 post type-post status-publish format-standard has-post-thumbnail hentry category-culture category-lifestyle category-technology category-video tag-business-2 tag-code tag-development">
                        @if($entry->article_type == 250 && isset($entry->sections) && is_array($entry->sections) && count($entry->sections)>0)
                            <nav class="sections-primary-menu light sticky-menu" role="navigation" style="margin-bottom: 2.5em;">

                                <!-- Responsive menu -->
                                <a class="click-to-open-menu"><i class="fa fa-align-justify"></i> {{trans('application.sections_navigator_title')}}</a>
                                <!-- Main menu -->
                                <ul class="sections-menu">
                                    @foreach($entry->sections as $section)
                                        @if(!isset($section->translations->$lang->title))
                                            @continue
                                        @endif
                                        <li><a href="javascript:void(0);" class="section-url" target="#section{{$section->id}}"><span>{{isset($section->translations->$lang->title) ? $section->translations->$lang->title : current($section->translations)->title}}</span></a></li>
                                    @endforeach

                                </ul>

                            </nav>
                        @endif

                        @if($entry->article_type == 249)
                                 <div class="gallery-single section-gallery">
                                    <ul class="slides">
                                        @if(isset($entry->images[0]->files) && is_array($entry->images[0]->files) && count($entry->images[0]->files)>0)
                                            @foreach($entry->images[0]->files as $file)
                                                <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                    <img src="{{$file->cdn}}" alt="{{$file->title}}" />
                                                    <!-- / The parents article details -->
                                                    <div class="gallery-caption-body">
                                                        <h3>{{$entry->title}}</h3>
                                                        @if(isset($entry->subtitle) && $entry->subtitle!=null && $entry->subtitle!='')
                                                            <p>
                                                                <strong>{{$entry->subtitle}}</strong>
                                                            </p>
                                                        @endif
                                                        @if($lock_status == 'open')
                                                            @if(isset($entry->description) && $entry->description!=null && $entry->description!='')

                                                                    {{{$entry->description}}}

                                                            @endif
                                                        @else
                                                             @include($template.'.partials._article_locked_messages')
                                                        @endif
                                                    </div>
                                                </li>
                                            @endforeach
                                        @endif

                                        @if(isset($entry->sections) && is_array($entry->sections) && count($entry->sections)>0)
                                            @foreach($entry->sections as $section)
                                                @if(isset($section->translations->$lang->title))

                                                    <?php $section_title = isset($section->translations->$lang->title) ? $section->translations->$lang->title : null; ?>
                                                    <?php $section_subtitle = isset($section->translations->$lang->subtitle) ? $section->translations->$lang->subtitle : null; ?>
                                                    <?php $section_description = isset($section->translations->$lang->description) ? $section->translations->$lang->description : null; ?>

                                                    @if(isset($section->images) && is_array($section->images) && count($section->images)>0)
                                                        @foreach($section->images as $file)
                                                            <!-- / list of sections -->
                                                            <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                                <img src="{{$file->cdn}}" alt="{{$section_title}}" />
                                                                <div class="gallery-caption-body">
                                                                    <h3>{{$section_title}}</h3>
                                                                    @if($section_subtitle != null && $section_subtitle!='')
                                                                        <p>
                                                                            <strong>{{$section_subtitle}}</strong>
                                                                        </p>
                                                                    @endif
                                                                    @if($lock_status == 'open')
                                                                        @if($section_description != null && $section_description!='')
                                                                                {{{$section_description}}}
                                                                        @endif
                                                                    @else
                                                                         @include($template.'.partials._article_locked_messages')
                                                                    @endif
                                                                </div>
                                                            </li>
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif

                                    </ul>
                                 </div>
                        @else


                                @if($entry->article_type != 250)
                                    @if($entry->article_type == 246 || $entry->article_type == 247)
                                        <h1 class="post-title">{{$entry->title}}</h1>
                                        <h3 class="lead">{{$entry->subtitle}}</h3>
                                        <div class="post-meta">
                                            <span class="date">{{--trans('application.news_single_created_title')--}} <a href="javascript:void(0);">{{Helpers::localeDate($entry->created,'d/m/Y h:i')}}</a> @if(isset($entry->updated_at) && $entry->updated_at!=null && $entry->updated_at!='') ({{trans('application.news_single_updated_at_placeholder')}} {{Helpers::localeDate($entry->updated_at,'d/m/Y h:i')}}) @endif</span>
                                            @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
                                                <a href="{{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}"><span class="comments disqus-comment-count" data-disqus-url="{{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}"> 0 </span> {{--trans('application.news_single_comments_title')--}}</a>
                                            @endif
                                            @if(is_object($authUser))
                                               <span style="font-size:0.8em;">
                                                   <a href="javascript:void(0);"  ng-click="favorite_post('<?php echo $entry->id;?>')">
                                                        <i class="fa fa-heart heart aheart"  ng-show="favorite_posts.indexOf('<?php echo $entry->id;?>') > -1"></i>
                                                        <i class="fa fa-heart aheart" ng-show="favorite_posts.indexOf('<?php echo $entry->id;?>') < 0"></i>
                                                   </a>
                                               </span>
                                            @endif
                                        </div>
                                    @endif
                                    @if(isset($entry->images[0]->files) && is_array($entry->images[0]->files) && count($entry->images[0]->files)>1)
                                        <div class="gallery-single">

                                            <ul class="slides">
                                                @foreach($entry->images[0]->files as $file)
                                                     <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                        <img src="{{$file->cdn}}" alt="{{$file->title}}" />
                                                        <div class="gallery-caption-body">
                                                            <p>{{$file->title}}</p>
                                                        </div>
                                                     </li>
                                                @endforeach

                                            </ul>

                                        </div>
                                    @elseif(isset($entry->images[0]->files) && is_array($entry->images[0]->files) && count($entry->images[0]->files)==1)
                                        <div class="featured">
                                            <a href="javascript:void(0);">
                                                <img src="{{$entry->images[0]->files[0]->cdn}}" alt="{{$entry->images[0]->files[0]->title}}">
                                            </a>
                                        </div>
                                    @endif
                                @endif

                                @if($entry->article_type != 246 && $entry->article_type != 247)
                                    <h1 class="post-title">{{$entry->title}}</h1>
                                    <h3 class="lead">{{$entry->subtitle}}</h3>
                                    <div class="post-meta">
                                        <span class="date">{{--trans('application.news_single_created_title')--}} <a href="javascript:void(0);">{{Helpers::localeDate($entry->created,'d/m/Y h:i')}}</a> @if(isset($entry->updated_at) && $entry->updated_at!=null && $entry->updated_at!='') ({{trans('application.news_single_updated_at_placeholder')}} {{Helpers::localeDate($entry->updated_at,'d/m/Y h:i')}}) @endif</span>
                                        @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
                                            <a href="{{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}"><span class="comments disqus-comment-count" data-disqus-url="{{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}"> 0 </span> {{--trans('application.news_single_comments_title')--}}</a>
                                        @endif
                                        @if(is_object($authUser))
                                           <span style="font-size:0.8em;">
                                               <a href="javascript:void(0);"  ng-click="favorite_post('<?php echo $entry->id;?>')">
                                                   <i class="fa fa-heart heart aheart"  ng-show="favorite_posts.indexOf('<?php echo $entry->id;?>') > -1"></i>
                                                   <i class="fa fa-heart aheart" ng-show="favorite_posts.indexOf('<?php echo $entry->id;?>') < 0"></i>
                                               </a>
                                           </span>
                                        @endif
                                    </div>
                                @endif

                                <div class="post-container">
                                    @if($entry->article_type == 248 && isset($entry->sections) && count($entry->sections)>0)
                                        <div class="post-rating">
                                            <div class="rating">
                                                <ul class="rating-list">
                                                    @foreach($entry->sections as $section)
                                                        @if(!isset($section->translations->$lang->title))
                                                            @continue
                                                        @endif
                                                        <li>
                                                            <p>{{ isset($section->translations->$lang->title) ? Helpers::remove_tones($section->translations->$lang->title) : Helpers::remove_tones(current($section->translations)->title) }}</p>
                                                            <div class="rating-stars" title="Rating: 5.0">
                                                                <span style="width: 100%"></span>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            <div class="total">
                                                <span>Συνολικα</span>
                                                <span><strong>4.1</strong></span>
                                            </div>
                                        </div>
                                    @endif




                                    @if($lock_status == 'open')
                                        @if($entry->article_type == 250)
                                                 @if(isset($entry->images[0]->files) && is_array($entry->images[0]->files) && count($entry->images[0]->files)>1)
                                                        <div class="post-right">
                                                            <div class="gallery-single">
                                                                <ul class="slides">
                                                                    @foreach($entry->images[0]->files as $key=>$file)
                                                                            @if($key!=0)
                                                                             <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                                                <img src="{{$file->cdn}}" alt="{{$file->title}}" />
                                                                                <div class="gallery-caption-body">
                                                                                    <p>{{$file->title}}</p>
                                                                                </div>
                                                                             </li>
                                                                         @endif
                                                                    @endforeach

                                                                </ul>
                                                            </div>
                                                        </div>
                                                 @endif
                                        @endif
                                        @if(isset($entry->description) && $entry->description!=null && $entry->description!='')
                                            <p>{{{$entry->description}}}</p>
                                            @if($entry->article_type == 250)
                                                <div class="inner-wrapper">
                                                    @if(isset($banners['featured_long']))
                                                      <!-- Banner space / Show if BIG banner exists -->
                                                       <div class="banner-space top-banner-space">
                                                          @if(isset($banners['featured_long']) && count($banners['featured_long'])>0)
                                                              <div class="banner_big" @if(!isset($banners['featured_small']) || count($banners['featured_small'])==0) style="float:none;border:none;" @endif>
                                                                  <?php $featured_long = $banners['featured_long'][0]; ?>
                                                                  @if(isset($featured_long->file_cdn))
                                                                       <a href="{{$featured_long->target_url}}" target="{{$featured_long->target_url_window}}">
                                                                           <img src="{{$featured_long->file_cdn}}" alt="{{$featured_long->description}}"/>
                                                                       </a>
                                                                  @else
                                                                       {{{$featured_long->content }}}
                                                                  @endif
                                                              </div>
                                                          @endif
                                                          @if(isset($banners['featured_small']) && count($banners['featured_small'])>0)
                                                            <div class="banner_small">
                                                                <?php $featured_small = $banners['featured_small'][0]; ?>
                                                                @if(isset($featured_small->file_cdn))
                                                                     <a href="{{$featured_small->target_url}}" target="{{$featured_small->target_url_window}}">
                                                                         <img src="{{$featured_small->file_cdn}}" alt="{{$featured_small->description}}"/>
                                                                     </a>
                                                                @else
                                                                     {{{$featured_small->content }}}
                                                                @endif
                                                            </div>
                                                          @endif
                                                          <div class="clearfix"></div>
                                                       </div>
                                                   @endif
                                                </div><!-- /inner-wrapper -->
                                            @endif

                                            @if(isset($entry->sections) && is_array($entry->sections) && count($entry->sections)>0)

                                                @if($entry->article_type != 250 && $entry->article_type != 247  && isset($entry->sections) && is_array($entry->sections) && count($entry->sections)>0)
                                                    <nav class="sections-primary-menu light sticky-menu" role="navigation" style="margin-bottom: 2.5em;">

                                                        <!-- Responsive menu -->
                                                        <a class="click-to-open-menu"><i class="fa fa-align-justify"></i> {{trans('application.sections_navigator_title')}}</a>
                                                        <!-- Main menu -->
                                                        <ul class="sections-menu">
                                                            @foreach($entry->sections as $section)
                                                               @if(!isset($section->translations->$lang->title))
                                                                   @continue
                                                               @endif
                                                                <li><a href="javascript:void(0);" class="section-url" target="#section{{$section->id}}"><span>{{isset($section->translations->$lang->title) ? $section->translations->$lang->title : current($section->translations)->title}}</span></a></li>
                                                            @endforeach

                                                        </ul>

                                                    </nav>
                                                @endif
                                                @foreach($entry->sections as $section)

                                                    @if(!isset($section->translations->$lang->title))
                                                        @continue
                                                    @endif
                                                    <div style="clear:both"></div>
                                                    <div class="row" id="section{{$section->id}}">
                                                    <!-- / SECTION -->
                                                         @if(isset($section->images) && is_array($section->images) && count($section->images) == 1 && $entry->article_type==250)
                                                                <div class="featured">
                                                                    <img src="{{$section->images[0]->cdn}}" alt="Image" />
                                                                    @if(isset($section->images[0]->title) && $section->images[0]->title != null && $section->images[0]->title!='')
                                                                        <span class="caption">{{ $section->images[0]->title }}</span>
                                                                    @endif
                                                                </div>
                                                         @elseif(isset($section->images) && is_array($section->images) && count($section->images) > 1 && $entry->article_type==250)
                                                                <div class="featured">
                                                                    <div class="gallery-single">

                                                                        <ul class="slides">
                                                                            @foreach($section->images as $file)
                                                                                 <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                                                    <img src="{{$file->cdn}}" />
                                                                                    @if(isset($file->title) && $file->title != null && $file->title!='')
                                                                                        <div class="gallery-caption-body">
                                                                                            <p>{{$file->title}}</p>
                                                                                        </div>
                                                                                    @endif
                                                                                 </li>
                                                                            @endforeach

                                                                        </ul>

                                                                    </div>
                                                                </div>
                                                         @endif

                                                    <?php $section_subtitle = (isset($section->translations->$lang->subtitle)) ? $section->translations->$lang->subtitle : current($section->translations)->subtitle; ?>
                                                    <?php $section_description = (isset($section->translations->$lang->description)) ? $section->translations->$lang->description : current($section->translations)->description; ?>
                                                        @if($entry->article_type == 246)
                                                            <h3>{{ isset($section->translations->$lang->title) ? Helpers::remove_tones($section->translations->$lang->title) : Helpers::remove_tones(current($section->translations)->title) }}</h3>
                                                        @elseif($entry->article_type == 247)
                                                            <h4>{{ isset($section->translations->$lang->title) ? Helpers::remove_tones($section->translations->$lang->title) : Helpers::remove_tones(current($section->translations)->title) }}</h4>
                                                        @elseif($entry->article_type == 248)
                                                            <p class="title"><span>{{ isset($section->translations->$lang->title) ? Helpers::remove_tones($section->translations->$lang->title) : Helpers::remove_tones(current($section->translations)->title) }}</span></p>
                                                        @elseif($entry->article_type == 250)
                                                            <p class="title"><span>{{ isset($section->translations->$lang->title) ? Helpers::remove_tones($section->translations->$lang->title) : Helpers::remove_tones(current($section->translations)->title) }}</span></p>
                                                        @endif
                                                        @if( $section_subtitle != null && $section_subtitle != '')
                                                             @if($entry->article_type == 246)
                                                                <h3 class="lead">{{$section_subtitle}}</h3>
                                                             @elseif($entry->article_type == 247)
                                                                <p><em>{{$section_subtitle}}</em></p>
                                                             @elseif($entry->article_type == 248)
                                                                <h3 class="lead">{{$section_subtitle}}</h3>
                                                             @elseif($entry->article_type == 250)
                                                                <h3 class="lead">{{$section_subtitle}}</h3>
                                                             @endif
                                                        @endif
                                                    @if(isset($section->images) && is_array($section->images) && count($section->images) == 1 && $entry->article_type!=250)
                                                        <div class="post-left">
                                                            <img src="{{$section->images[0]->cdn}}" alt="Image" />
                                                            @if(isset($section->images[0]->title) && $section->images[0]->title != null && $section->images[0]->title!='')
                                                                <span class="caption">{{ $section->images[0]->title }}</span>
                                                            @endif
                                                        </div>
                                                    @elseif(isset($section->images) && is_array($section->images) && count($section->images) > 1 && $entry->article_type!=250)
                                                        <div class="featured">
                                                            <div class="gallery-single">

                                                                <ul class="slides">
                                                                    @foreach($section->images as $file)
                                                                         <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                                            <img src="{{$file->cdn}}" />
                                                                            @if(isset($file->title) && $file->title != null && $file->title!='')
                                                                                <div class="gallery-caption-body">
                                                                                    <p>{{$file->title}}</p>
                                                                                </div>
                                                                            @endif
                                                                         </li>
                                                                    @endforeach

                                                                </ul>

                                                            </div>
                                                        </div>
                                                    @endif


                                                    @if( $section_description != null && $section_description != '')
                                                        <p>{{{$section_description}}}</p>
                                                    @endif
                                                    </div>
                                                    <!-- / SECTION -->
                                                @endforeach
                                            @endif

                                            @if(isset($entry->albums) && is_array($entry->albums) && count($entry->albums)>0)

                                                <!-- / PARENT CUSTOM GALLERIES -->
                                                <br>
                                                <div class="gallery-content">
                                                    @foreach($entry->albums as $album)
                                                        @if(is_array($album->files) && count($album->files)>0)
                                                            <!-- Gallery album -->
                                                            <div class="gallery-album sport-gal">
                                                                <div class="gal-img">
                                                                    <a class="fancybox" rel="group<?php echo $album->id;?>" href="{{$album->thumb_cdn}}" title="{{$album->files[0]->title}}">
                                                                        <span>{{count($album->files)}} {{trans('application.custom_albums_images_label')}}</span>
                                                                        <img src="{{Helpers::squareFilePath($album->thumb_cdn)}}" alt="{{$album->title}}" title="{{$album->files[0]->title}}"/>
                                                                    </a>
                                                                </div>
                                                                <h4><a href="javascript:void(0);" >{{$album->title}}</a></h4>
                                                                <p>{{$album->subtitle}}</p>
                                                                @foreach($album->files as $key=>$file)
                                                                  @if($key > 0)
                                                                    <a class="fancybox" rel="group<?php echo $album->id;?>" href="{{$file->cdn}}" style="display:none;" title="{{$file->title}}"></a>
                                                                  @endif
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                                <!-- / PARENT CUSTOM GALLERIES -->
                                            @endif
                                        @endif
                                    @else
                                        @include($template.'.partials._article_locked_messages')
                                    @endif
                                </div>
                        @endif




                            <!-- Post info -->
                            <div class="post-info">
                                @if(isset($tags) && is_array($tags) && count($tags)>0)
                                    <span class="tags">
                                    @foreach($tags as $key=>$tag)
                                        @if($tag->title!=null && $tag->title!='')
                                            <a href="{{route('tags',[$tag->id,Helpers::urlize($tag->title)])}}">#{{Helpers::remove_tones($tag->title)}}</a>
                                            @if(count($tags)>$key+1)
                                            &nbsp;&nbsp;
                                            @endif
                                        @endif
                                    @endforeach
                                @endif

                                @if(isset($cats) && is_array($cats) && count($cats)>0)
                                    @if(isset($tags) && is_array($tags) && count($tags)>0)
                                        <br>
                                    @endif
                                    <span class="category">{{--trans('application.news_single_categories_title')--}}
                                        <?php $cat_i = 0; ?>
                                        @foreach($cats as $cat_id=>$cat_title)
                                            <?php $cat_i++; ?>
                                            <a href="{{route('news_category',[$cat_id,Helpers::urlize($cat_title)])}}">{{Helpers::remove_tones($cat_title)}}</a>
                                            @if(count($cats)>$cat_i)
                                            ,
                                            @endif
                                        @endforeach
                                    </span>
                                @endif
                                <span class="views">{{--trans('application.news_single_views_title')--}} {{$entry->views_count}}</span>
                                </span>
                            </div>


                        <div class="post-share">
                            <span class="share-text">{{trans('application.news_single_share_title')}}</span>
                            <ul>
                                <li><a  target="_blank" href="https://twitter.com/intent/tweet?text={{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}" class="twitter"><span class="socicon">a</span></a></li>
                                <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}" class="facebook"><span class="socicon">b</span></a></li>
                                <li><a  target="_blank" href="https://plus.google.com/share?url={{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}" class="google"><span class="socicon">c</span></a></li>
                                <li><a  target="_blank" href="http://pinterest.com/pin/create/button/?url={{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}&description={{$entry->title}}" class="google"><span class="socicon">d</span></a></li>
                                <li><a  target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}" class="linkedin"><span class="socicon">j</span></a></li>
                            </ul>
                        </div>

                    </article>

                    @if(isset($entry->author))
                        <!-- Author bio -->
                        <div class="post-bio" style="min-height:150px;">
                            @if(isset($entry->author->avatar))
                                <img src="{{$entry->author->avatar}}" alt="{{$entry->author->firstname}} {{$entry->author->lastname}}" width="80"/>
                            @endif
                            <div class="description">
                                <a class="bio" href="{{route('author',[$entry->author->id])}}">{{Helpers::remove_tones($entry->author->firstname)}} {{Helpers::remove_tones($entry->author->lastname)}}</a>
                                @if(isset($entry->author->subtitle) && $entry->author->subtitle!=null && $entry->author->subtitle!='')
                                     <p>{{$entry->author->subtitle}}</p>
                                @endif
                            </div>
                        </div>
                    @endif

                    @if(isset($entry->next) || isset($entry->previous))
                        <!-- Post controls -->
                        <div class="post-controls">
                            @if(isset($entry->previous))
                                <a href="{{Helpers::getRouteSingleBySystemCat($entry->previous->system_parent_cat_id,$entry->previous->id,Helpers::urlize($entry->previous->title))}}" class="prev">
                                    <span><i class="fa fa-angle-left"></i></span>
                                    <p>{{$entry->previous->title}}</p>
                                </a>
                            @endif
                            @if(isset($entry->next))
                                <a href="{{Helpers::getRouteSingleBySystemCat($entry->next->system_parent_cat_id,$entry->next->id,Helpers::urlize($entry->next->title))}}" class="next">
                                    <span><i class="fa fa-angle-right"></i></span>
                                    <p>{{$entry->next->title}}</p>
                                </a>
                            @endif
                        </div>
                    @endif
                    @if(isset($entry->related) && is_array($entry->related) && count($entry->related)>0)
                        <!-- Related products -->
                        <div class="block-layout-one">
                            {{--<p class="title"><span>{{trans('application.news_single_related_articles_title')}}</span></p>--}}

                            <div class="row">
                                <?php $i = 0; ?>
                                @foreach($entry->related as $related)
                                    @if($i>0 &&$i%3 == 0)
                                        </div>
                                        <div class="row">
                                    @endif
                                    <div class="item grid_4 ">
                                        @if(isset($related->thumb))
                                            <a href="{{Helpers::getRouteSingleBySystemCat($related->system_parent_cat_id,$related->id,Helpers::urlize($related->title))}}"><img src="{{Helpers::squareFilePath($related->thumb)}}" width="80" height="65"/></a>
                                        @endif
                                        <div @if(!isset($related->thumb)) style="margin-left:0px;"@endif>
                                            @if(isset($related->cat_id))
                                                <span><a href="{{ Helpers::getRouteCategoryBySystemCat($related->system_parent_cat_id, $related->cat_id, Helpers::urlize($related->category_title))}}">{{Helpers::remove_tones($related->category_title)}}</a></span>
                                            @endif
                                            <h3><a href="{{Helpers::getRouteSingleBySystemCat($related->system_parent_cat_id,$related->id,Helpers::urlize($related->title))}}">{{$related->title}}</a></h3>
                                            <p class="date">{{Helpers::remove_tones(Helpers::localeDate($related->created))}}</p>
                                        </div>
                                         <span class="clearfix"></span>
                                    </div>

                                    <?php $i++; ?>
                                @endforeach

                            </div>
                        </div>
                    @endif
                    @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
                        <!-- Comments -->
                        <div class="comments">
                            <p class="title"><span>Σχολια</span></p>
                            <div id="disqus_thread"></div>
                            <script type="text/javascript">
                                /* * * CONFIGURATION VARIABLES * * */
                                var disqus_shortname = '{{$basic_app->disqus_forum}}';
                                /* * * DON'T EDIT BELOW THIS LINE * * */
                                (function() {
                                    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                                })();
                            </script>
                        </div>
                    @endif
                </div>

                @if($entry->article_type != 250)
                    <!-- Aside -->
                    @include($template.'.partials.sidebar',['disable'=>true])
                @endif
            </div>
        </section>


@endsection

@section('scripts')
@javascript({{ asset('assets/'.$template.'/angular/news_single.js') }})
@javascript({{ asset('assets/'.$template.'/angular/sidebar_controller.js') }})

<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
@endsection