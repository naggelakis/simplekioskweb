@extends($template.'.layout.master')

@section('meta_title')
| {{$category->title}}
@endsection
@section('meta_description')
{{$category->description}}
@endsection
@section('content')

                <!-- Above the fold -->
                <div id="above-the-fold" class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif">
                    <div class="inner-wrapper">

                         @if(isset($banners['featured_long']))
                              <!-- Banner space / Show if BIG banner exists -->
                               <div class="banner-space top-banner-space  @if(isset($banners['featured_small']) && count($banners['featured_small'])>0) double-banner @endif">
                                  @if(isset($banners['featured_long']) && count($banners['featured_long'])>0)
                                      <div class="banner_big" >
                                          <?php $featured_long = $banners['featured_long'][0]; ?>
                                          @if(isset($featured_long->file_cdn))
                                               <a href="{{$featured_long->target_url}}" target="{{$featured_long->target_url_window}}">
                                                   <img src="{{$featured_long->file_cdn}}" alt="{{$featured_long->description}}"/>
                                               </a>
                                          @else
                                               {{{$featured_long->content }}}
                                          @endif
                                      </div>
                                  @endif

                                  @if(isset($banners['featured_small']) && count($banners['featured_small'])>0)
                                    <div class="banner_small">
                                        <?php $featured_small = $banners['featured_small'][0]; ?>
                                        @if(isset($featured_small->file_cdn))
                                             <a href="{{$featured_small->target_url}}" target="{{$featured_small->target_url_window}}">
                                                 <img src="{{$featured_small->file_cdn}}" alt="{{$featured_small->description}}"/>
                                             </a>
                                        @else
                                             {{{$featured_small->content }}}
                                        @endif
                                    </div>
                                  @endif

                                  <div class="clearfix"></div>
                               </div>
                          @endif

                       <h2 class="page-title">{{$category->title}}</h2>

                    </div><!-- /inner-wrapper -->
                </div>
                <!-- /above-the-fold -->

                <!-- Section -->
                <section id="section" ng-app="mainApp">
                    <div class="inner-wrapper">

                        <!-- Main -->
                        <div id="main" class="left" role="main">
                                @if(isset($showcase_posts) && is_array($showcase_posts) && count($showcase_posts)>0 )

                                <!-- SHOW ONLY IF CATEGORY ALLOWS TO LIST ARTICLES / SHOW FROM ALL SUBCATEGORIES AS WELL -->
                                    <?php $counter = 0; ?>
                                    <?php $show_div = 0; ?>

                                    @foreach($showcase_posts as $key=>$showcase_post)
                                        <?php $ids [] = $showcase_post->id;?>
                                        @if($key == 0)
                                                <!-- Post / Show Showcase of Category max 6 -->
                                                <article class="single-post">
                                                    @if(isset($showcase_post->thumb) && $showcase_post->thumb!=null)
                                                        <div class="featured">
                                                            <a href="{{Helpers::getRouteSingleBySystemCat($showcase_post->system_parent_cat_id,$showcase_post->id,Helpers::urlize($showcase_post->title))}}"><img src="{{$showcase_post->thumb}}" alt="{{$showcase_post->title}}"/></a>
                                                        </div>
                                                    @endif
                                                    <h1 class="post-title">{{$showcase_post->title}}</h1>
                                                    <h3 class="lead hide_on_761_1080">{{$showcase_post->subtitle}}</h3>
                                                    <div class="clearfix">&nbsp;</div>
                                                </article>
                                        @else

                                            @if($counter%2 == 0)

                                                <div class="block-layout-two row">
                                            @endif

                                                <!-- Post -->
                                                <div class="grid_6">
                                                    <div class="main-item">
                                                         @if(isset($showcase_post->thumb) && $showcase_post->thumb!=null)
                                                            <div class="post-img">
                                                                <a href="{{Helpers::getRouteSingleBySystemCat($showcase_post->system_parent_cat_id,$showcase_post->id,Helpers::urlize($showcase_post->title))}}"><img src="{{Helpers::squareFilePath($showcase_post->thumb)}}" alt="{{$showcase_post->title}}"/></a>
                                                            </div>
                                                        @endif
                                                        <h3><a href="{{Helpers::getRouteSingleBySystemCat($showcase_post->system_parent_cat_id,$showcase_post->id,Helpers::urlize($showcase_post->title))}}">{{$showcase_post->title}}</a></h3>
                                                        <div class="post-dca">
                                                            <span class="date">{{strtoupper(Helpers::remove_tones(Helpers::localeDate($showcase_post->created)))}}</span>
                                                            @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
                                                                <span class="comments"><a href="{{Helpers::getRouteSingleBySystemCat($showcase_post->system_parent_cat_id,$showcase_post->id,Helpers::urlize($showcase_post->title))}}"><span class="disqus-comment-count" data-disqus-url="{{Helpers::getRouteSingleBySystemCat($showcase_post->system_parent_cat_id,$showcase_post->id,Helpers::urlize($showcase_post->title))}}"> 0 </span> {{--trans('application.homepage_article_comments_title')--}}</a></span>
                                                            @endif
                                                            @if(isset($showcase_post->authors) && is_array($showcase_post->authors) && count($showcase_post->authors)>0)
                                                                <span class="author"><a href="{{route('author',[$showcase_post->authors[0]->user_id])}}">{{Helpers::remove_tones($showcase_post->authors[0]->profile->firstname.' '.$showcase_post->authors[0]->profile->lastname)}}</a></span>
                                                            @endif
                                                            @if(is_object($authUser))
                                                               <span style="font-size:0.8em;">
                                                                   <a href="javascript:void(0);"  ng-click="favorite_post(<?php echo $showcase_post->id;?>)">
                                                                       <i class="fa fa-heart heart aheart"  ng-show="favorite_posts.indexOf(<?php echo $showcase_post->id;?>) > -1"></i>
                                                                       <i class="fa fa-heart aheart" ng-show="favorite_posts.indexOf(<?php echo $showcase_post->id;?>) < 0"></i>
                                                                   </a>
                                                               </span>
                                                           @endif
                                                        </div>
                                                        <p class="hide_on_761_1080">{{Helpers::get_snippet($showcase_post->subtitle,40) }}</p>
                                                    </div>
                                                </div>
                                                <!-- Post -->
                                             @if($counter%2 != 0)

                                                </div>
                                            @endif
                                            <?php $counter++; ?>
                                        @endif


                                    @endforeach



                                    <!-- 2 Posts / Show posts in grid view in pairs only 2, 4 max 6 -->

                                    @if(isset($banners['list_top']) && count($banners['list_top'])>0)
                                      <div class="banner-space">
                                         <?php $list_top = $banners['list_top'][0]; ?>
                                         @if(isset($list_top->file_cdn))
                                              <a href="{{$list_top->target_url}}" target="{{$list_top->target_url_window}}">
                                                  <img src="{{$list_top->file_cdn}}" alt="{{$list_top->description}}"/>
                                              </a>
                                         @else
                                              {{{$list_top->content }}}
                                         @endif
                                     </div>
                                    @endif


                                   @if($category->view_type == 0 || $category->view_type==266)
                                       <!-- Block layout five / Show articles 15 at a time by date and load 15 each time -->
                                       <div class="block-layout-six"  ng-controller="newsListCtrl" ng-init="lang='{{strtolower($lang)}}'; type=<?php echo $type;?>; cat_id=<?php echo $category->id;?>; latest_category_posts_fetch('<?php echo implode(',',$ids);?>')">

                                           <div  ng-repeat="group in latest_posts | chunk:10:this">
                                               <div  ng-repeat="post in group" class="main-item @{{ post.render_no_image }}">
                                                   <div class="post-img" ng-show="post.thumb">
                                                       <a ng-href="@{{ post.render_url }}"><img ng-src="@{{ post.thumb_square }}" alt="@{{post.title}}"/></a>
                                                       <span ng-show="post.render_category"><a ng-href="@{{ post.render_category_url }}">@{{post.render_category}}</a></span>
                                                   </div>
                                                   <div class="post-meta">
                                                       <h3><a ng-href="@{{ post.render_url }}">@{{post.title}}</a></h3>
                                                       <div class="post-dca">
                                                           <span class="date">@{{ post.render_date }}</span>
                                                           @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
                                                               <span class="comments">
                                                                    <a href="@{{ post.render_url }}"><span class="disqus-comment-count" data-disqus-url="@{{ post.render_url }}"> 0 </span> {{--trans('application.homepage_article_comments_title')--}}</a>
                                                               </span>
                                                           @endif
                                                           <span class="author" ng-show="post.render_author_name"><a ng-href="@{{ post.render_author_url }}">@{{post.render_author_name}}</a></span>
                                                           @if(is_object($authUser))
                                                              <span style="font-size:0.8em;">
                                                                  <a href="javascript:void(0);"  ng-click="favorite_post(post.id)">
                                                                      <i class="fa fa-heart heart aheart"  ng-show="favorite_posts.indexOf(post.id) > -1"></i>
                                                                      <i class="fa fa-heart aheart" ng-show="favorite_posts.indexOf(post.id) < 0"></i>
                                                                  </a>
                                                              </span>
                                                           @endif
                                                       </div>
                                                       <p class="hide_on_761_1080" ng-show="post.render_subtitle">@{{ post.render_subtitle }}</p>
                                                   </div>

                                               </div >
                                               @if(isset($banners['list_mid']) && count($banners['list_mid'])>0)
                                                 <div class="banner-space" ng-show="group.length==10 && $index<(latest_posts.length/10)-1">
                                                    <?php $list_mid = $banners['list_mid'][0]; ?>
                                                    @if(isset($list_mid->file_cdn))
                                                         <a href="{{$list_mid->target_url}}" target="{{$list_mid->target_url_window}}">
                                                             <img src="{{$list_mid->file_cdn}}" alt="{{$list_mid->description}}"/>
                                                         </a>
                                                    @else
                                                         {{{$list_mid->content }}}
                                                    @endif
                                                 </div>
                                               @endif
                                           </div>


                                           <br>
                                           <a ng-click="latest_category_posts_fetch('<?php echo implode(',',$ids);?>')" class="btn btn-expand btn-large btn-custom btn-radius" ng-show="show_load_more==1">
                                                <i class="fa fa-spinner fa-spin" ng-show="spinner==1"></i>
                                                &nbsp;
                                                 <span ng-show="spinner==0">{{trans('application.homepage_recent_articles_load_more')}}</span>
                                                 <span ng-show="spinner==1">{{trans('application.homepage_recent_articles_loading')}}</span>
                                           </a>
                                       </div>

                                   @elseif($category->view_type == 267)
                                       <div ng-controller="newsListCtrl" ng-init="lang='{{strtolower($lang)}}'; type=<?php echo $type;?>; cat_id=<?php echo $category->id;?>; latest_category_posts_fetch()">
                                           <!-- 2 Posts / Show posts in grid view in pairs only 2, 4 max 6 -->
                                           <div class="block-layout-two row" ng-repeat="group in latest_posts | chunk:2:this" ng-show="latest_posts.length>0">

                                               <!-- Post -->
                                               <div class="grid_6"  ng-repeat="post in group">
                                                   <div class="main-item">
                                                       <div class="post-img" ng-show="post.thumb">
                                                           <a ng-href="@{{ post.render_url }}"><img ng-src="@{{ post.thumb_square }}" alt="@{{post.title}}"/></a>
                                                       </div>
                                                       <h3><a href="@{{ post.render_url }}">@{{post.title}}</a></h3>
                                                       <div class="post-dca">
                                                           <span class="date">@{{ post.render_date }}</span>
                                                           @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
                                                              <span class="comments">
                                                                   <a href="@{{ post.render_url }}"><span class="disqus-comment-count" data-disqus-url="@{{ post.render_url }}"> 0 </span> {{--trans('application.homepage_article_comments_title')--}}</a>
                                                              </span>
                                                           @endif
                                                            <span class="author" ng-show="post.render_author_name"><a ng-href="@{{ post.render_author_url }}">@{{post.render_author_name}}</a></span>
                                                            @if(is_object($authUser))
                                                               <span style="font-size:0.8em;">
                                                                   <a href="javascript:void(0);"  ng-click="favorite_post(post.id)">
                                                                       <i class="fa fa-heart heart aheart"  ng-show="favorite_posts.indexOf(post.id) > -1"></i>
                                                                       <i class="fa fa-heart aheart" ng-show="favorite_posts.indexOf(post.id) < 0"></i>
                                                                   </a>
                                                               </span>
                                                           @endif
                                                       </div>
                                                       <p class="hide_on_761_1080" ng-show="post.render_subtitle">@{{ post.render_subtitle }}</p>
                                                   </div>
                                               </div>

                                               @if(isset($banners['list_mid']) && count($banners['list_mid'])>0)
                                                    <div class="banner-space" ng-show="group.length==10 && $index<(latest_posts.length/10)-1">
                                                       <?php $list_mid = $banners['list_mid'][0]; ?>
                                                       @if(isset($list_mid->file_cdn))
                                                            <a href="{{$list_mid->target_url}}" target="{{$list_mid->target_url_window}}">
                                                                <img src="{{$list_mid->file_cdn}}" alt="{{$list_mid->description}}"/>
                                                            </a>
                                                       @else
                                                            {{{$list_mid->content }}}
                                                       @endif
                                                    </div>
                                               @endif

                                           </div>
                                           <br>
                                           <a ng-click="latest_category_posts_fetch('<?php echo implode(',',$ids);?>')" class="btn btn-expand btn-large btn-custom btn-radius" ng-show="show_load_more==1">
                                            <i class="fa fa-spinner fa-spin" ng-show="spinner==1"></i>
                                            &nbsp;
                                            <span ng-show="spinner==0">{{trans('application.homepage_recent_articles_load_more')}}</span>
                                            <span ng-show="spinner==1">{{trans('application.homepage_recent_articles_loading')}}</span>
                                           </a>

                                       </div>
                                   @endif


                                    @if(isset($banners['list_bottom']) && count($banners['list_bottom'])>0)
                                       <div class="banner-space">
                                          <?php $list_bottom = $banners['list_bottom'][0]; ?>
                                          @if(isset($list_bottom->file_cdn))
                                               <a href="{{$list_bottom->target_url}}" target="{{$list_bottom->target_url_window}}">
                                                   <img src="{{$list_bottom->file_cdn}}" alt="{{$list_bottom->description}}"/>
                                               </a>
                                          @else
                                               {{{$list_bottom->content }}}
                                          @endif
                                      </div>
                                    @endif






                            @else
                                    <div ng-controller="newsListCtrl" ng-init="lang='{{strtolower($lang)}}'; type=<?php echo $type;?>; cat_id=<?php echo $category->id;?>; latest_category_posts_fetch()">

                                        @if(isset($banners['list_top']) && count($banners['list_top'])>0)
                                            <div class="banner-space">
                                                 <?php $list_top = $banners['list_top'][0]; ?>
                                                 @if(isset($list_top->file_cdn))
                                                      <a href="{{$list_top->target_url}}" target="{{$list_top->target_url_window}}">
                                                          <img src="{{$list_top->file_cdn}}" alt="{{$list_top->description}}"/>
                                                      </a>
                                                 @else
                                                      {{{$list_top->content }}}
                                                 @endif
                                             </div>
                                        @endif

                                        <!-- Post / Show Showcase of Category max 6 -->
                                        <article class="single-post" ng-show="latest_posts.length>0">

                                                <div class="featured" ng-show="latest_posts[0].thumb">
                                                    <a ng-href="@{{ latest_posts[0].render_url }}"><img ng-src="@{{ latest_posts[0].thumb }}" alt="@{{ latest_posts[0].title }}"/></a>
                                                </div>

                                            <h1 class="post-title"><a ng-href="@{{ latest_posts[0].render_url }}">@{{ latest_posts[0].title }}</a></h1>
                                            <h3 class="lead hide_on_761_1080" ng-show="latest_posts[0].render_subtitle && latest_posts[0].render_subtitle!=null && latest_posts[0].render_subtitle!=''">@{{ latest_posts[0].render_subtitle }}</h3>
                                            <div class="clearfix">&nbsp;</div>
                                        </article>

                                        <!-- 2 Posts / Show posts in grid view in pairs only 2, 4 max 6 -->
                                        <div class="block-layout-two row" ng-repeat="group in latest_posts.slice(1,latest_posts.length) | chunk:2:this" ng-show="latest_posts.length>0">
                                            <!-- Post -->
                                            <div class="grid_6"  ng-repeat="post in group">
                                                <div class="main-item">
                                                    <div class="post-img" ng-show="post.thumb">
                                                        <a ng-href="@{{ post.render_url }}"><img ng-src="@{{ post.thumb_square }}" alt="@{{post.title}}"/></a>
                                                    </div>
                                                    <h3><a ng-href="@{{ post.render_url }}">@{{post.title}}</a></h3>
                                                    <div class="post-dca">
                                                        <span class="date">@{{ post.render_date }}</span>
                                                        @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
                                                           <span class="comments">
                                                                <a ng-href="@{{ post.render_url }}"><span class="disqus-comment-count" data-disqus-url="@{{ post.render_url }}"> 0 </span> {{--trans('application.homepage_article_comments_title')--}}</a>
                                                           </span>
                                                        @endif
                                                         <span class="author" ng-show="post.render_author_name"><a ng-href="@{{ post.render_author_url }}">@{{post.render_author_name}}</a></span>
                                                         @if(is_object($authUser))
                                                            <span style="font-size:0.8em;">
                                                                <a href="javascript:void(0);"  ng-click="favorite_post(post.id)">
                                                                    <i class="fa fa-heart heart aheart"  ng-show="favorite_posts.indexOf(post.id) > -1"></i>
                                                                    <i class="fa fa-heart aheart" ng-show="favorite_posts.indexOf(post.id) < 0"></i>
                                                                </a>
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <p class="hide_on_761_1080" ng-show="post.render_subtitle">@{{ post.render_subtitle }}</p>
                                                </div>
                                            </div>

                                            @if(isset($banners['list_mid']) && count($banners['list_mid'])>0)
                                                <div class="banner-space" ng-show="group.length==10 && $index<(latest_posts.length/10)-1">
                                                   <?php $list_mid = $banners['list_mid'][0]; ?>
                                                   @if(isset($list_mid->file_cdn))
                                                        <a href="{{$list_mid->target_url}}" target="{{$list_mid->target_url_window}}">
                                                            <img src="{{$list_mid->file_cdn}}" alt="{{$list_mid->description}}"/>
                                                        </a>
                                                   @else
                                                        {{{$list_mid->content }}}
                                                   @endif
                                                </div>
                                            @endif

                                        </div>
                                        <br>
                                        <a ng-click="latest_category_posts_fetch('<?php echo implode(',',$ids);?>')" class="btn btn-expand btn-large btn-custom btn-radius" ng-show="show_load_more==1">
                                            <span ng-show="spinner==0">{{trans('application.homepage_recent_articles_load_more')}}</span>
                                            <span ng-show="spinner==1"><i class="fa fa-spinner fa-spin"></i> {{trans('application.homepage_recent_articles_loading')}}</span>
                                           </a>
                                    </div>

                                     @if(isset($banners['list_bottom']) && count($banners['list_bottom'])>0)
                                       <div class="banner-space">
                                          <?php $list_bottom = $banners['list_bottom'][0]; ?>
                                          @if(isset($list_bottom->file_cdn))
                                               <a href="{{$list_bottom->target_url}}" target="{{$list_bottom->target_url_window}}">
                                                   <img src="{{$list_bottom->file_cdn}}" alt="{{$list_bottom->description}}"/>
                                               </a>
                                          @else
                                               {{{$list_bottom->content }}}
                                          @endif
                                      </div>
                                    @endif
                            @endif



                            </div><!-- /main -->
                             <!-- Aside -->
                            @include($template.'.partials.sidebar',['disable'=>true])


                    </div><!-- /inner-wrapper -->
                </section><!-- /section -->

@endsection

@section('scripts')
    @javascript({{ asset('assets/'.$template.'/angular/news_list.js') }})
    @javascript({{ asset('assets/'.$template.'/angular/sidebar_controller.js') }})

@endsection