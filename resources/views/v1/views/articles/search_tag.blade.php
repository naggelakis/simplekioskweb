@extends($template.'.layout.master')

@section('meta_title')
|
@endsection
@section('meta_description')

@endsection

@section('content')

         <!-- Section -->
                    <section id="section" >
                        <div class="inner-wrapper" ng-app="mainApp">

                            <!-- Main -->
                            <div id="main" class="left" role="main"  ng-controller="searchListController" ng-init="lang='{{strtolower($lang)}}'; @if(isset($only_news)) only_news=1; @endif">
                                @if(isset($tag_slug))
        						    <div class="alert yellow" ng-init="tag_id={{$tag_id}}">{{trans('application.search_tag_list_tag_placeholder')}} <strong>#{{$tag_slug}}</strong></div>
        						@endif


                                <!-- Search widget -->
                                <form class="searchform" ng-submit="submit_form()">
                                    <div class="row">
                                        <input type="text" ng-model="search" placeholder="{{trans('application.search_tag_list_search_placeholder')}}" @if(isset($search) && $search!=null && $search!='') ng-init="search='{{$search}}'" @endif/>
                                        <input type="submit" value="&#xf002;"/>
                                    </div>
                                    @if(isset($from) || isset($to))
                                        <div class="row" style="margin-top: 2%;">
                                           <div class="input-daterange input-group" id="datepicker" ng-init="from='{{$from}}';to='{{$to}}';">
                                               <label>Ημερομηνίες</label>
                                               <input type="text" class="input-sm form-control" name="from"  style="width:25%;"  value="@{{from}}" ng-model="from"/>
                                               <span class="input-group-addon">-</span>
                                               <input type="text" class="input-sm form-control" name="to"  style="width:25%;"  value="@{{to}}" ng-model="to" />
                                           </div>
                                        </div>
                                    @endif

                                </form>
        						<div class="spacer"></div>

                                <div ng-show="spinner==1">
                                        <div class="alert ">
                                            <i class="fa fa-spinner fa-spin" ng-show="spinner==1"></i>&nbsp;{{trans('application.posts_list_loading')}}
                                        </div>
                                </div>

                                <!-- Block layout five / Show articles 15 at a time by date and load 15 each time -->
                                <div class="block-layout-six" ng-init="posts_fetch()" ng-show="posts.length > 0">

                                    <div  ng-repeat="post in posts"  ng-class=" post.thumb ? 'main-item' : 'main-item no-image'">
                                        <div class="post-img" ng-show="post.thumb">
                                            <a ng-href="@{{ post.render_url }}"><img ng-src="@{{ post.thumb_square }}" alt="@{{post.title}}"/></a>
                                        </div>
                                        <div class="post-meta">
                                            <h3><a ng-href="@{{ post.render_url }}">@{{post.title}}</a></h3>
                                            <div class="post-dca">
                                                <span class="date">@{{post.render_date}}</span>
                                                @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
                                                   <span class="comments">
                                                        <a href="@{{ post.render_url }}"><span class="disqus-comment-count" data-disqus-url="@{{ post.render_url }}"> 0 </span> {{--trans('application.homepage_article_comments_title')--}}</a>
                                                   </span>
                                                @endif
                                                <span class="author" ng-show="post.render_author_name"><a ng-href="@{{ post.render_author_url }}">@{{post.render_author_name}}</a></span>
                                                 @if(is_object($authUser))
                                                       <span style="font-size:0.8em;">
                                                           <a href="javascript:void(0);"  ng-click="favorite_post(post.id)">
                                                               <i class="fa fa-heart heart aheart"  ng-show="favorite_posts.indexOf(post.id) > -1"></i>
                                                               <i class="fa fa-heart aheart" ng-show="favorite_posts.indexOf(post.id) < 0"></i>
                                                           </a>
                                                       </span>
                                                   @endif
                                            </div>
                                            <p class="hide_on_761_1080" ng-show="post.render_subtitle">@{{ post.render_subtitle }}</p>
                                        </div>
                                    </div>




                                    <br>
                                    <a ng-click="posts_fetch()" class="btn btn-expand btn-large btn-custom btn-radius" ng-show="show_load_more==1">
                                                <i class="fa fa-spinner fa-spin" ng-show="spinner==1"></i>&nbsp;
                                                <span ng-show="spinner==0">{{trans('application.homepage_recent_articles_load_more')}}</span>
                                                <span ng-show="spinner==1">{{trans('application.homepage_recent_articles_loading')}}</span>
                                    </a>
                                </div>

                                <div class="" ng-show="posts.length == 0 && spinner==0">
                                    <div class="alert "><strong>{{trans('application.posts_list_no_results_title')}}</strong> <br> {{trans('application.posts_list_no_results_placeholder')}}</div>
                                </div>

                            </div><!-- /main -->


                            <!-- Aside -->
                            @include($template.'.partials.sidebar',['disable'=>true,'disable_search'=>true])


                        </div><!-- /inner-wrapper -->
                    </section><!-- /section -->

@endsection

@section('scripts')
    @javascript({{ asset('assets/'.$template.'/angular/search_list.js') }})
    @javascript({{ asset('assets/'.$template.'/angular/sidebar_controller.js') }})
    @if(isset($from) || isset($to))
        <script type="text/javascript">
            $(document).ready(function(){
                $('.input-daterange').datepicker({
                    language: "<?php echo strtolower($lang);?>",
                    orientation: "top auto",
                    multidate: false,
                    autoclose: true,
                    todayHighlight: true
                });
            });
        </script>
    @endif
@endsection