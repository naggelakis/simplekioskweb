@extends($template.'.layout.master')
@section('content')
    <style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        /* display: none; <- Crashes Chrome on hover */
        -webkit-appearance: none;
        margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
    }
    </style>
    <section id="section">
            <div class="inner-wrapper">

                <!-- Main -->
                <div id="main" role="main">


                    <div class="row">

                        <div class="grid_2"></div>

                        <div class="grid_8">

                            <!-- / CART -->
                            <p class="title"><span>{{trans('application.cart_index_cart_title')}}</span></p>

                            @if(isset($items) && is_array($items) && count($items)>0)
                                <p>{{trans('application.cart_index_cart_subtitle')}} </p>
                                <table class="bordered checkout-table">
                                    <thead>
                                        <tr>

                                            <th colspan="2">{{trans('application.cart_index_cart_table_head_description')}}</th>
                                            <th width="30">{{trans('application.cart_index_cart_table_head_price')}}</th>
                                            <th width="70">{{trans('application.cart_index_cart_table_head_quantity')}}</th>
                                            <th width="30">{{trans('application.cart_index_cart_table_head_total')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $total = 0; ?>
                                        @foreach($items as $key=>$item)
                                            <?php $total += $item['subtotal']; ?>

                                            @if($item['options']['type'] == 'issue')
                                                <?php $url = route('issue_single',[$item['id'],Helpers::urlize($item['name'])]); ?>
                                            @elseif($item['options']['type'] == 'magazine')
                                               <?php $url = route('magazine_single',[$item['id'],Helpers::urlize($item['name'])]); ?>
                                            @elseif($item['options']['type'] == 'book')
                                               <?php $url = route('book_single',[$item['id'],Helpers::urlize($item['name'])]); ?>

                                            @endif
                                            <tr id="{{$key}}">

                                                @if(isset($item['options']['image']))
                                                    <td width="80"><a href="{{$url}}"><img src="{{$item['options']['image']}}" alt="{{$item['name']}}" /></a></td>
                                                @endif
                                                <td>
                                                    <a href="{{$url}}"><strong>{{$item['name']}}</strong></a>
                                                    @if(isset($item['options']['group_translation']) || isset($item['options']['system_translation']))
                                                        <br>
                                                        @if(isset($item['options']['system_translation']))
                                                            {{$item['options']['system_translation']}}
                                                        @endif
                                                        @if(isset($item['options']['group_translation']))
                                                            , {{trans('application.cart_index_cart_table_row_user_group')}} {{$item['options']['group_translation']}}
                                                        @endif

                                                    @endif
                                                </td>
                                                <td>{{$item['price']}}&nbsp;&euro;</td>
                                                <td style="min-width:120px;">
                                                    <div class="row">
                                                        <input type="number" min="0" value="{{$item['qty']}}" data-id="{{$key}}" data-price="{{$item['price']}}" class="quantities" style="width:40%;min-width:36px;" disabled/>
                                                    </div>
                                                </td>
                                                <td class="checkout-table-column-price"><span id="price_{{$key}}">{{$item['price']*$item['qty']}}</span>&nbsp;&euro;</td>
                                            </tr>
                                        @endforeach
                                        @foreach($shipping_methods as $key=>$shipping_method)
                                            @if(count($shipping_methods) ==1 || $key==0)
                                                <?php $total_final = $total+$shipping_method->shipping_rate; ?>
                                            @endif
                                            <tr>

                                                <td><input type="radio" name="group" class="shippingRate" id="che{{$key}}" value="{{$shipping_method->id}}" @if($key==0) checked="checked" @endif @if(count($shipping_methods) ==1) disabled @endif data-value="{{$shipping_method->shipping_rate}}"></td>
                                                <td>{{trans('application.market_checkout_publication_shipping_cost')}} {{$shipping_method->title}}</td>
                                                <td></td>
                                                <td></td>
                                                <td class="checkout-table-column-price">{{$shipping_method->shipping_rate}} &euro;</td>
                                            </tr>
                                        @endforeach

                                        <tr>
                                            <td colspan="4" align="right">{{trans('application.cart_index_cart_table_row_total')}}</td>
                                            <td class="checkout-table-column-price" data-total="{{$total}}" id="totalT"><b><span id="totalView">{{$total_final}}</span> &euro;</b></td>
                                        </tr>
                                    </tbody>
                                </table>


                                <div class="spacer"></div>

                                @if(is_object($authUser) && isset($payment_methods) && is_array($payment_methods))
                                <form id="payField">
                                    <div class="form-group">
                                        @foreach($payment_methods as $payment_method)
                                            @if($payment_method->sys_payment_id == 1)
                                                <?php $route_data = [$payment_method->id,0,0,0,'items']; ?>

                                                <a class="btn btn-blue btn-large btn-radius payButton" href="{{route('paypal_checkout',$route_data)}}" >
                                                    <i class="fa fa-paypal"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_paypal')}}
                                                </a>
                                                &nbsp;&nbsp;&nbsp;
                                            @endif
                                            @if($payment_method->sys_payment_id == 2)
                                                 <?php $route_data = [$payment_method->id,0,0,0,'items']; ?>
                                                 <a class="btn btn-blue btn-large btn-radius payButton" href="{{route('viva_checkout',$route_data)}}">
                                                    <i class="fa fa-credit-card"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_credit_card')}}
                                                 </a>
                                            @endif

                                            @if($payment_method->sys_payment_id == 3)
                                                 <?php $route_data = [$payment_method->id,0,0,0,'items']; ?>
                                                 <a class="btn btn-blue btn-large btn-radius payButton" href="{{route('bank_checkout',$route_data)}}">
                                                    <i class="fa fa-university"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_bank')}}
                                                 </a>
                                            @endif

                                            @if($payment_method->sys_payment_id == 4)
                                                 <?php $route_data = [$payment_method->id,0,0,0,'items']; ?>
                                                 <a class="btn btn-blue btn-large btn-radius payButton" href="{{route('delivery_checkout',$route_data)}}">
                                                    <i class="fa fa-money"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_delivery')}}( +{{$payment_method->pay_on_delivery_extra}}&euro; )
                                                 </a>
                                            @endif
                                        @endforeach


                                    </div>
                                </form>
                                <div class="spacer"></div>
                            @endif


                                <!-- / If user is logged in and has an address -->

                                <!-- / If user is NOT logged in or has NO address -->
                            @endif
                            <!-- / CART -->
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection


@section('scripts')
    <script type="text/javascript">
           $(document).ready(function(){

               $('.shippingRate').on('click',function(){

                      var base_price = $('#totalT').attr('data-total');
                      $('#totalView').html(base_price);
                      var id = $(this).attr('id');
                      var checked = $("#" + id + ":checked").length;

                      if(checked == 1){
                          var value = $(this).attr('data-value');

                          var total = parseFloat(base_price) + parseFloat(value);
                          $('#totalView').html(total);
                      }

                      var count = $('.shippingRate:checked').length;
                      if(count == 0){
                            $('#payField').hide();
                      }else if(count == 1){
                            $('#payField').show();
                      }


               });

               $('.payButton').on('click',function(e){
                   e.preventDefault();
                        var checked = $(".shippingRate:checked").attr('value');
                        var url = $(this).attr('href')+"?shipping_selected="+checked;
                        window.location = url;
                   return false;
               })
           });
       </script>
@endsection