@extends($template.'.layout.master')

@section('meta_title')
| {{ trans('application.checkout_success_head_title') }}
@endsection

@section('content')

        <!-- Section -->
        <section id="section">
            <div class="inner-wrapper">

                <!-- Main -->
                <div id="main" role="main">


                    <div class="row">

                        <div class="grid_2"></div>

                        <div class="grid_8">

                                @if($order_type == 'electronic')
                                    @include($template.'.views.market.partials.checkout_success_electronic_simple')
                                @elseif($order_type == 'electronic_subscription')
                                     @include($template.'.views.market.partials.checkout_success_electronic_publication')
                                @elseif($order_type == 'printed_subscription')
                                     @include($template.'.views.market.partials.checkout_success_printed_publication')
                                @elseif($order_type == 'items')
                                     @include($template.'.views.market.partials.checkout_success_items')
                                @endif

                        </div>

                        <div class="grid_2"></div>

                    </div>


                </div>

            </div>
        </section>

@endsection