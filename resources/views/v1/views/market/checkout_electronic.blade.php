@extends($template.'.layout.master')
@section('content')

        <!-- Section -->
        <section id="section">
            <div class="inner-wrapper">

                <!-- Main -->
                <div id="main" role="main">


                    <div class="row">

                        <div class="grid_2"></div>

                        <div class="grid_8">

                            <!-- / FAIL RESPONCE --
                            <div class="alert red">
                                <strong>Δεν ολοκληρώθηκε η αγορά</strong>
                                <br>Η πληρωμή σας δεν έγινε δεκτή από το πάροχο. Δοκιμάστε ξανά ή επικοινωνήστε μαζί μας στο: <strong>info@autospecialist.gr</strong>
                            </div>
                            <div class="spacer"></div>
                            <!-- / FAIL RESPONCE -->

                            <p class="title"><span>{{trans('application.market_checkout_electronic_title')}}</span></p>
                            <p>{{trans('application.market_checkout_electronic_subtitle')}}</p>
                            <table class="bordered checkout-table">
                                <thead>
                                    <tr>
                                        <th>{{trans('application.market_checkout_electronic_table_head_id')}}</th>
                                        <th>{{trans('application.market_checkout_electronic_table_head_description')}}</th>
                                        <th class="checkout-table-column-price"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td><strong>{{$item->title}}</strong>
                                        @if(isset($price->group_translation))
                                            <br>{{trans('application.market_checkout_electronic_table_row_group_placeholder')}} {{$price->group_translation}}</td>
                                        @endif
                                        <td class="checkout-table-column-price">{{$price->value}} &euro;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="spacer"></div>

                            @if(!is_object($authUser))
                                <form>
                                    <fieldset>
                                        <legend>{{trans('application.market_checkout_electronic_user_login_title')}}</legend>
                                        <p>{{trans('application.market_checkout_electronic_user_login_subtitle')}}</p>
                                        @if(isset($socialApps))
                                            @if(isset($socialApps->facebook))
                                                <div class="form-group">
                                                        <button class="btn-large btn-blue btn-expand" style="background-color: #3b5998;">
                                                            <a href="{{ route('social_login',['facebook']) }}@if(isset($redirect))?redirect={{$redirect}} @endif" style="color: #ffffff;">
                                                                <i class="fa fa-facebook-square"></i> {{trans('application.auth_login_social_facebook_button_title')}}
                                                            </a>
                                                        </button>
                                                </div>
                                            @endif
                                            @if(isset($socialApps->twitter))
                                                <div class="form-group">
                                                 <button class="btn-large btn-blue btn-expand" style="background-color: #00aced;">
                                                     <a href="{{ route('social_login',['twitter']) }}@if(isset($redirect))?redirect={{$redirect}} @endif"  style="color: #ffffff;">
                                                        <i class="fa fa-twitter-square"></i> {{trans('application.auth_login_social_twitter_button_title')}}
                                                     </a>
                                                 </button>
                                                </div>
                                            @endif
                                            @if(isset($socialApps->google))
                                                <div class="form-group">
                                                 <button class="btn-large btn-blue btn-expand" style="background-color: #dd4b39;">
                                                     <a href="{{ route('social_login',['google']) }}@if(isset($redirect))?redirect={{$redirect}} @endif" style="color: #ffffff;">
                                                        <i class="fa fa-google-plus-square"></i> {{trans('application.auth_login_social_google_button_title')}}
                                                     </a>
                                                 </button>
                                                </div>
                                            @endif
                                        @endif

                                        <div class="spacer"></div>
                                        <p class="text-center"><a href="{{route('login')}}@if(isset($redirect))?redirect={{$redirect}} @endif">{{trans('application.market_checkout_electronic_user_login_email')}}</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{route('login')}}@if(isset($redirect))?redirect={{$redirect}} @endif">{{trans('application.market_checkout_electronic_user_register_email')}}</a></p>
                                    </fieldset>
                                </form>
                                <div class="spacer"></div>


                            @endif

                            @if(is_object($authUser) && isset($payment_methods) && is_array($payment_methods))
                                <form>
                                    <div class="form-group">
                                        @foreach($payment_methods as $payment_method)
                                            @if($payment_method->sys_payment_id == 1)
                                                <?php $route_data = [$payment_method->id,$item->id,74,$group_id,'electronic']; ?>
                                                @if(isset($recurring_plan_id))
                                                   <?php $route_data[] = $recurring_plan_id; ?>
                                                @endif
                                                <a class="btn btn-blue btn-large btn-radius" href="{{route('paypal_checkout',$route_data)}}">
                                                    <i class="fa fa-paypal"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_paypal')}}
                                                </a>
                                                &nbsp;&nbsp;&nbsp;
                                            @endif
                                            @if($payment_method->sys_payment_id == 2)
                                                <?php $route_data = [$payment_method->id,$item->id,74,$group_id,'electronic']; ?>
                                                 @if(isset($recurring_plan_id))
                                                    <?php $route_data[] = $recurring_plan_id; ?>
                                                 @endif
                                                 <a class="btn btn-blue btn-large btn-radius" href="{{route('viva_checkout',$route_data)}}">
                                                    <i class="fa fa-credit-card"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_credit_card')}}
                                                 </a>
                                            @endif
                                        @endforeach


                                    </div>
                                </form>
                                <div class="spacer"></div>
                            @endif
                        </div>

                        <div class="grid_2"></div>

                    </div>


                </div>

            </div>
        </section>

@endsection