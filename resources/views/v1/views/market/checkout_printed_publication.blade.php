@extends($template.'.layout.master')
@section('content')

        <!-- Section -->
        <section id="section">
            <div class="inner-wrapper">

                <!-- Main -->
                <div id="main" role="main">


                    <div class="row">

                        <div class="grid_2"></div>

                        <div class="grid_8">

                            @if(isset($error))
                                <!-- / FAIL RESPONCE -->
                                <div class="alert red">
                                    <strong>{{trans('application.market_checkout_publication_error_title')}}</strong>
                                    <br>
                                    {{trans('application.market_checkout_publication_error_'.$error)}}
                                    </strong>
                                </div>
                                <div class="spacer"></div>
                                <!-- / FAIL RESPONCE -->
                            @endif

                            @if($action == 'step1')
                                @include($template.'.views.market.partials.checkout_printed_publication_step_1')
                            @elseif($action == 'step2')
                                @include($template.'.views.market.partials.checkout_printed_publication_step_2')
                            @endif



                        </div>

                        <div class="grid_2"></div>

                    </div>


                </div>

            </div>
        </section>

@endsection
