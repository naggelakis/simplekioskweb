@extends($template.'.layout.master')
@section('content')
     <!-- Section -->
            <section id="section">
                <div class="inner-wrapper">

                    <!-- Main -->
                    <div id="main" role="main">


                        <div class="row">

                            <div class="grid_2"></div>

                            <div class="grid_8">


                                <p class="title"><span>{{trans('application.checkout_success_update_title')}}</span></p>
                                <div class="alert green">
                                    <strong>{{trans('application.checkout_success_update_alert_title')}}</strong>
                                    <br>{{trans('application.checkout_success_update_alert_placeholder')}}
                                </div>
                                <div class="spacer"></div>
                                <table class=" checkout-table">
                                    <tbody>
                                        <tr>
                                            <td>
                                               {{trans('application.checkout_success_update_alert_placeholder_publication')}} <strong>{{$subscription->prod_title}}</strong>
                                               <br>
                                               {{trans('application.checkout_success_update_alert_placeholder_publication_expires')}} <strong>{{Helpers::localeDate($subscription->expires,'d F Y')}}</strong>
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                                <div class="spacer"></div>
                                <!-- / SUCCESS RESPONCE -->


                            </div>

                            <div class="grid_2"></div>

                        </div>


                    </div>

                </div>
            </section>
@endsection