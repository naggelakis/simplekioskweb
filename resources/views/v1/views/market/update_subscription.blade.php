@extends($template.'.layout.master')
@section('content')
     <!-- Section -->
            <section id="section">
                <div class="inner-wrapper">

                    <!-- Main -->
                    <div id="main" role="main">


                        <div class="row">

                            <div class="grid_2"></div>

                            <div class="grid_8">

                                <!-- / FAIL RESPONCE --
                                <div class="alert red">
                                    <strong>Δεν ολοκληρώθηκε η αγορά</strong>
                                    <br>Η πληρωμή σας δεν έγινε δεκτή από το πάροχο. Δοκιμάστε ξανά ή επικοινωνήστε μαζί μας στο: <strong>info@autospecialist.gr</strong>
                                </div>
                                <div class="spacer"></div>
                                <!-- / FAIL RESPONCE -->

                                <p class="title"><span>{{trans('application.market_update_electronic_title')}}</span></p>
                                <p>{{trans('application.market_update_electronic_subtitle')}}</p>
                                <table class="bordered checkout-table">
                                    <thead>
                                        <tr>
                                            <th>{{trans('application.market_checkout_electronic_table_head_id')}}</th>
                                            <th>{{trans('application.market_checkout_electronic_table_head_description')}}</th>
                                            <th class="checkout-table-column-price"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$subscription->id}}</td>
                                            <td><strong>{{$subscription->prod_title}}</strong>
                                            <td class="checkout-table-column-price">{{$price}} &euro;</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="spacer"></div>


                                @if(is_object($authUser) && isset($payment_methods) && is_array($payment_methods))
                                    <form>
                                        <div class="form-group">
                                            @foreach($payment_methods as $payment_method)
                                                @if($payment_method->sys_payment_id == 1)

                                                    <?php $route_data = [$payment_method->id,$subscription->id,74,0,'update_subscription']; ?>
                                                    @if(isset($recurring_plan_id))
                                                       <?php $route_data[] = $recurring_plan_id; ?>
                                                    @endif
                                                    <a class="btn btn-blue btn-large btn-radius" href="{{route('paypal_checkout',$route_data)}}">
                                                        <i class="fa fa-paypal"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_paypal')}}
                                                    </a>
                                                    &nbsp;&nbsp;&nbsp;
                                                @endif
                                                @if($payment_method->sys_payment_id == 2)
                                                    <?php $route_data = [$payment_method->id,$subscription->id,74,0,'update_subscription']; ?>
                                                     @if(isset($recurring_plan_id))
                                                        <?php $route_data[] = $recurring_plan_id; ?>
                                                     @endif
                                                     <a class="btn btn-blue btn-large btn-radius" href="{{route('viva_checkout',$route_data)}}">
                                                        <i class="fa fa-credit-card"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_credit_card')}}
                                                     </a>
                                                @endif
                                            @endforeach


                                        </div>
                                    </form>
                                    <div class="spacer"></div>
                                @endif
                            </div>

                            <div class="grid_2"></div>

                        </div>


                    </div>

                </div>
            </section>
@endsection