                            <p class="title"><span>{{trans('application.market_checkout_publication_title')}}</span></p>
                            <p>{{trans('application.market_checkout_publication_title_placeholder')}}</p>
                            <table class="bordered checkout-table">
                                <thead>
                                    <tr>
                                        <th>{{trans('application.market_checkout_electronic_table_head_id')}}</th>
                                        <th>{{trans('application.market_checkout_electronic_table_head_description')}}</th>
                                        <th class="checkout-table-column-price"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td><strong>{{$item->title}}</strong>
                                        @if(isset($price->group_translation))
                                            <br>{{trans('application.market_checkout_electronic_table_row_group_placeholder')}} {{$price->group_translation}}</td>
                                        @endif
                                        <td class="checkout-table-column-price">{{$price->value}} &euro;</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="spacer"></div>

                            @if(!is_object($authUser))

                                <form >
                                    <fieldset>
                                        <legend>{{trans('application.market_checkout_electronic_user_login_title')}}</legend>
                                        <p>{{trans('application.market_checkout_electronic_user_login_subtitle')}}</p>
                                        @if(isset($socialApps))
                                            @if(isset($socialApps->facebook))
                                                <div class="form-group">
                                                        <button class="btn-large btn-blue btn-expand" style="background-color: #3b5998;">
                                                            <a href="{{ route('social_login',['facebook']) }}@if(isset($redirect))?redirect={{$redirect}} @endif" style="color: #ffffff;">
                                                                <i class="fa fa-facebook-square"></i> {{trans('application.auth_login_social_facebook_button_title')}}
                                                            </a>
                                                        </button>
                                                </div>
                                            @endif
                                            @if(isset($socialApps->twitter))
                                                <div class="form-group">
                                                 <button class="btn-large btn-blue btn-expand" style="background-color: #00aced;">
                                                     <a href="{{ route('social_login',['twitter']) }}@if(isset($redirect))?redirect={{$redirect}} @endif"  style="color: #ffffff;">
                                                        <i class="fa fa-twitter-square"></i> {{trans('application.auth_login_social_twitter_button_title')}}
                                                     </a>
                                                 </button>
                                                </div>
                                            @endif
                                            @if(isset($socialApps->google))
                                                <div class="form-group">
                                                 <button class="btn-large btn-blue btn-expand" style="background-color: #dd4b39;">
                                                     <a href="{{ route('social_login',['google']) }}@if(isset($redirect))?redirect={{$redirect}} @endif" style="color: #ffffff;">
                                                        <i class="fa fa-google-plus-square"></i> {{trans('application.auth_login_social_google_button_title')}}
                                                     </a>
                                                 </button>
                                                </div>
                                            @endif
                                        @endif

                                        <div class="spacer"></div>
                                        <p class="text-center"><a href="{{route('login')}}@if(isset($redirect))?redirect={{$redirect}} @endif">{{trans('application.market_checkout_electronic_user_login_email')}}</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{route('login')}}@if(isset($redirect))?redirect={{$redirect}} @endif">{{trans('application.market_checkout_electronic_user_register_email')}}</a></p>
                                    </fieldset>
                                </form>
                                <div class="spacer"></div>

                            @elseif(is_object($authUser))

                                    <p>{{trans('application.market_checkout_publication_order_receiver_title')}}</p>
                                    <form id="postalForm" action="{{route('publication_printed_checkout_shipping',[$item->id,$recurring_plan_id,$system_cat_id,$group_id])}}" method="post">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <fieldset>
                                            <legend>{{trans('application.market_checkout_publication_subscriber_details_title')}}</legend>
                                            <div class="form-group">
                                                <label>{{trans('application.market_checkout_publication_subscriber_firstname')}}</label>
                                                <input type="text" name="billing_firstname" value="{{$authUser->firstname}}" placheholder="{{trans('application.market_checkout_publication_subscriber_firstname_placeholder')}}" class="required" required="required" />
                                            </div>
                                            <div class="form-group">
                                                <label>{{trans('application.market_checkout_publication_subscriber_lastname')}}</label>
                                                <input type="text" name="billing_lastname" value="{{$authUser->lastname}}" placeholder="{{trans('application.market_checkout_publication_subscriber_lastname_placeholder')}}" class="required" required="required"/>
                                            </div>
                                            <div class="spacer"></div>
                                            <div class="form-group">
                                                <label>{{trans('application.market_checkout_publication_subscriber_address')}}</label>
                                                <input type="text"  name="billing_address" placeholder="{{trans('application.market_checkout_publication_subscriber_address_placeholder')}}" class="required" required="required" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->line1 :null}}"/>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="billing_address2" placeholder="" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->line2 :null}}"/>
                                            </div>
                                            <div class="form-group">
                                                <select id="billingCountry" name="billing_country" placeholder="{{trans('application.market_checkout_publication_subscriber_country_placeholder')}}" class="required" required="required">
                                                    @foreach($countries as $country_id=>$country_name)
                                                        <option value="{{$country_id}}" @if( !is_object($default_location) && $country_id == 95) selected="selected" @elseif(is_object($default_location) && isset($default_location->items->address) && $default_location->items->address->country_id == $country_id ) selected="selected" @endif >{{$country_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div id="billingCityDiv">
                                                    <select id="billingCity" name="billing_city" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required">
                                                        @foreach($cities as $county=>$cities_list)
                                                            <?php $cities_list = (array)$cities_list; ?>
                                                            <optgroup label="{{{$county}}}">
                                                                @foreach($cities_list as $city_id=>$city_name)
                                                                    <option value="{{$city_id}}" @if(is_object($default_location) && isset($default_location->items->address) && $default_location->items->address->city_id == $city_id ) selected="selected" @endif>{{$city_name}}</option>
                                                                @endforeach
                                                            </optgroup>

                                                        @endforeach
                                                    </select>
                                                </div>

                                                <input style="display:none;" id="billingCityAlt" type="text" name="billing_city_alt" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->city :null}}"/>

                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="billing_zip" placeholder="{{trans('application.market_checkout_publication_subscriber_zip_placeholder')}}" class="required" required="required" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->zip :null}}"/>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="billing_phone" placeholder="{{trans('application.market_checkout_publication_subscriber_phone_placeholder')}}" value="{{ is_object($default_location) && isset($default_location->items->phone) ? $default_location->items->phone->number :null}}"/>
                                            </div>
                                            <div class="spacer"></div>
                                            <div class="form-group">
                                                <label><input type="checkbox" name="postal_same" checked="checked" id="postalSame"/> {{trans('application.market_checkout_publication_subscriber_post_details_same')}}</label>
                                            </div>
                                            <div class="form-group">
                                                <input class="btn btn-blue" type="submit" value="{{trans('application.market_checkout_publication_subscriber_continue_next_step')}}"/>
                                            </div>
                                        </fieldset>

                                        <fieldset id="postalDetails" style="display: none;">
                                            <legend>{{trans('application.market_checkout_publication_subscriber_details_shipping_title')}}</legend>
                                            <div class="form-group">
                                                <label>{{trans('application.market_checkout_publication_subscriber_firstname')}}</label>
                                                <input type="text" name="shipping_firstname" value="{{$authUser->firstname}}" placheholder="{{trans('application.market_checkout_publication_subscriber_firstname_placeholder')}}" class="required" required="required"/>
                                            </div>
                                            <div class="form-group">
                                                <label>{{trans('application.market_checkout_publication_subscriber_lastname')}}</label>
                                                <input type="text" name="shipping_lastname" value="{{$authUser->lastname}}" placeholder="{{trans('application.market_checkout_publication_subscriber_lastname_placeholder')}}" class="required" required="required"/>
                                            </div>
                                            <div class="spacer"></div>
                                            <div class="form-group">
                                                <label>{{trans('application.market_checkout_publication_subscriber_address')}}</label>
                                                <input type="text"  name="shipping_address" placeholder="{{trans('application.market_checkout_publication_subscriber_address_placeholder')}}" class="required" required="required"/>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="shipping_address2" placeholder=""/>
                                            </div>
                                            <div class="form-group">
                                                <select id="shippingCountry" name="shipping_country" placeholder="{{trans('application.market_checkout_publication_subscriber_country_placeholder')}}" class="required" required="required">
                                                    @foreach($countries as $country_id=>$country_name)
                                                        <option value="{{$country_id}}" @if($country_id == 95) selected="selected" @endif >{{$country_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div id="shippingCityDiv">
                                                    <select id="shippingCity" name="shipping_city" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required">
                                                        @foreach($cities as $county=>$cities_list)
                                                            <?php $cities_list = (array)$cities_list; ?>
                                                            <optgroup label="{{{$county}}}">
                                                                @foreach($cities_list as $city_id=>$city_name)
                                                                    <option value="{{$city_id}}">{{$city_name}}</option>
                                                                @endforeach
                                                            </optgroup>

                                                        @endforeach
                                                    </select>
                                                </div>

                                                <input style="display:none;" id="shippingCityAlt" type="text" name="shipping_city_alt" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required"/>

                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="shipping_zip" placeholder="{{trans('application.market_checkout_publication_subscriber_zip_placeholder')}}" class="required" required="required"/>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" name="shipping_phone" placeholder="{{trans('application.market_checkout_publication_subscriber_phone_placeholder')}}" />
                                            </div>
                                            <div class="spacer"></div>

                                        </fieldset>
                                    </form>
                                    <div class="spacer"></div>

                            @endif


@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#postalForm').validate();
            $('select').select2();
            $('#postalSame').on('click',function(){
                var checked = $("#postalSame:checked").length;
                if(checked == 1){
                    $('#postalDetails').hide();
                }else if(checked == 0){
                    $('#postalDetails').show();
                }
            });

            $('#billingCountry').on('change',function(){
                var country_id = $(this).val();
                if(country_id == 95){
                    $('#billingCityDiv').show();
                    $('#billingCityAlt').hide();
                }else{
                    $('#billingCityAlt').show();
                    $('#billingCityDiv').hide();
                }
            });

            $('#shippingCountry').on('change',function(){
                var country_id = $(this).val();
                if(country_id == 95){
                    $('#shippingCityDiv').show();
                    $('#shippingCityAlt').hide();
                }else{
                    $('#shippingCityAlt').show();
                    $('#shippingCityDiv').hide();
                }
            });
        });
    </script>
@endsection