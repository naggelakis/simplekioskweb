<!-- / SUCCESS RESPONCE -->
<p class="title"><span>{{trans('application.checkout_success_electronic_simple_title')}}</span></p>
<div class="alert green">
    <strong>{{trans('application.checkout_success_electronic_simple_alert_title')}}</strong>
    <br>{{trans('application.checkout_success_electronic_simple_alert_placeholder')}}
</div>
<div class="spacer"></div>
<table class=" checkout-table">
    <tbody>
        <tr>
            <td>
               {{trans('application.checkout_success_electronic_simple_alert_placeholder_publication')}} <strong>{{$product_title}}</strong>
            </td>

        </tr>
    </tbody>
</table>
<div class="spacer"></div>
<p>{{trans('application.checkout_success_electronic_simple_members_label')}} <a href="{{ route('user_profile') }}">{{trans('application.checkout_success_electronic_simple_members_label_link')}}</a>.</p>
<!-- / SUCCESS RESPONCE -->

