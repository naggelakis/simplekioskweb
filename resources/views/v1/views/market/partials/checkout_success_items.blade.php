@extends($template.'.layout.master')
@section('content')

        <!-- Section -->
        <section id="section">
            <div class="inner-wrapper">

                <!-- Main -->
                <div id="main" role="main">


                    <div class="row">

                        <div class="grid_2"></div>

                        <div class="grid_8">

                            @if(isset($error))
                                <!-- / FAIL RESPONCE -->
                                <div class="alert red">
                                    <strong>{{trans('application.market_checkout_publication_error_title')}}</strong>
                                    <br>
                                    {{trans('application.market_checkout_publication_error_'.$error)}}
                                    </strong>
                                </div>
                                <div class="spacer"></div>
                                <!-- / FAIL RESPONCE -->
                            @endif


                            @if($order_details->payment_status_id == 121)
                                <!-- / SUCCESS RESPONCE -->
                                <p class="title"><span>{{trans('application.checkout_success_items_title')}}</span></p>
                                <div class="alert green">
                                    <strong>{{trans('application.checkout_success_items_alert_title')}}</strong>
                                    <br>{{trans('application.checkout_success_items_alert_placeholder')}}
                                </div>
                                <div class="spacer"></div>
                                <table class=" checkout-table">
                                    <tbody>
                                        @foreach($order_details->items as $item)
                                        <tr>
                                            @if(isset($item->thumb))
                                                <td width="80">
                                                    <a href="{{$item->url}}" target="_blank">
                                                        <img src="{{$item->thumb}}" alt="{{$item->prod_title}}">
                                                    </a>
                                                </td>
                                            @endif
                                            <td>
                                                <a href="{{$item->url}}" target="_blank">
                                                    <strong>{{$item->prod_title}}</strong>
                                                </a>
                                                @if($item->quantity > 1)
                                                    <br> x{{$item->quantity}}
                                                @endif
                                            </td>

                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                                <div class="spacer"></div>
                                <p>{{trans('application.checkout_success_items_members_label')}} <a href="{{ route('user_profile') }}">{{trans('application.checkout_success_items_members_label_link')}}</a>.</p>
                                <!-- / SUCCESS RESPONCE -->
                            @else
                                <p class="title"><span>{{trans('application.checkout_success_items_title')}}</span></p>
                                    @if($payment_method->sys_payment_id == 3) {{-- bank --}}
                                        <div class="alert grey">
                                            <strong>{{trans('application.checkout_success_printed_subscription_bank_title')}}</strong>
                                            <br>{{trans('application.checkout_success_printed_subscription_bank_placeholder_left')}}<strong>{{ $order_details->total }} €</strong> {{trans('application.checkout_success_printed_subscription_bank_placeholder_right')}}

                                            {{{$payment_method->description}}}

                                            <p><em>{{trans('application.checkout_success_printed_subscription_bank_bottom_left')}}<strong>{{ $order_details->id }}</strong> {{{trans('application.checkout_success_printed_subscription_bank_bottom_right')}}}</em></p>
                                        </div>
                                    @elseif($payment_method->sys_payment_id == 4) {{-- delivery --}}
                                        <div class="alert yellow">
                                            <strong>{{trans('application.checkout_success_printed_subscription_delivery_title')}}</strong>
                                            <br>{{trans('application.checkout_success_printed_subscription_delivery_placeholder_left')}}<strong> {{ $order_details->total }} €</strong> {{{trans('application.checkout_success_printed_subscription_delivery_placeholder_right')}}}
                                        </div>
                                    @endif
                                <div class="spacer"></div>
                                <table class=" checkout-table">
                                    <tbody>
                                        @foreach($order_details->items as $item)
                                        <tr>
                                            @if(isset($item->thumb))
                                                <td width="80">
                                                    <a href="{{$item->url}}" target="_blank">
                                                        <img src="{{$item->thumb}}" alt="{{$item->prod_title}}">
                                                    </a>
                                                </td>
                                            @endif
                                            <td>
                                                <a href="{{$item->url}}" target="_blank">
                                                    <strong>{{$item->prod_title}}</strong>
                                                </a>
                                                @if($item->quantity > 1)
                                                    <br> x{{$item->quantity}}
                                                @endif
                                            </td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="spacer"></div>
                                <p>{{trans('application.checkout_success_items_members_label')}} <a href="{{ route('user_profile') }}">{{trans('application.checkout_success_items_members_label_link')}}</a>.</p>

                            @endif




                        </div>

                        <div class="grid_2"></div>

                    </div>


                </div>

            </div>
        </section>

@endsection
