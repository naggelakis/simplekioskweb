
@if($order_details->payment_status_id == 121)
    <!-- / SUCCESS RESPONCE -->
    <p class="title"><span>{{trans('application.checkout_success_printed_simple_title')}}</span></p>
    <div class="alert green">
        <strong>{{trans('application.checkout_success_electronic_simple_alert_title')}}</strong>
        <br>{{trans('application.checkout_success_electronic_simple_alert_placeholder')}}
    </div>
    <div class="spacer"></div>
    <table class=" checkout-table">
        <tbody>
            <tr>
                <td>
                   {{trans('application.checkout_success_printed_alert_placeholder_publication')}} <strong>{{$product_title}}</strong>
                </td>

            </tr>
        </tbody>
    </table>
    <div class="spacer"></div>
    <p>{{trans('application.checkout_success_electronic_simple_members_label')}} <a href="{{ route('user_profile') }}">{{trans('application.checkout_success_electronic_simple_members_label_link')}}</a>.</p>
    <!-- / SUCCESS RESPONCE -->
@else
    <p class="title"><span>{{trans('application.checkout_success_printed_simple_title')}}</span></p>
        @if($payment_method->sys_payment_id == 3) {{-- bank --}}
            <div class="alert grey">
                <strong>{{trans('application.checkout_success_printed_subscription_bank_title')}}</strong>
                <br>{{trans('application.checkout_success_printed_subscription_bank_placeholder_left')}}<strong>{{ $order_details->total }} €</strong> {{trans('application.checkout_success_printed_subscription_bank_placeholder_right')}}

                {{{$payment_method->description}}}

                <p><em>{{trans('application.checkout_success_printed_subscription_bank_bottom_left')}}<strong>{{ $order_details->id }}</strong> {{{trans('application.checkout_success_printed_subscription_bank_bottom_right')}}}</em></p>
            </div>
        @elseif($payment_method->sys_payment_id == 4) {{-- delivery --}}
            <div class="alert yellow">
                <strong>{{trans('application.checkout_success_printed_subscription_delivery_title')}}</strong>
                <br>{{trans('application.checkout_success_printed_subscription_delivery_placeholder_left')}}<strong> {{ $order_details->total }} €</strong> {{{trans('application.checkout_success_printed_subscription_delivery_placeholder_right')}}}
            </div>
        @endif
    <div class="spacer"></div>
    <table class=" checkout-table">
        <tbody>
            <tr>
                <td>
                   {{trans('application.checkout_success_printed_alert_placeholder_publication')}} <strong>{{$product_title}}</strong>
                </td>
                <td>
                    {{ $order_details->total }} &euro;
                </td>
                <td class="checkout-table-column-price">
                    <a href="javascript:void(0);" class="btn btn-orange btn-radius" type="submit">
                        {{trans('application.checkout_success_printed_subscription_waiting_payment_placeholder')}}
                    </a>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="spacer"></div>
    <p>{{trans('application.checkout_success_electronic_simple_members_label')}} <a href="{{ route('user_profile') }}">{{trans('application.checkout_success_electronic_simple_members_label_link')}}</a>.</p>

@endif
