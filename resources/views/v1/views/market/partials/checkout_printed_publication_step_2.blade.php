                            <p class="title"><span>{{trans('application.market_checkout_publication_shipping_title')}}</span></p>
                            <p>{{trans('application.market_checkout_publication_shipping_title_placeholder')}}</p>
                            <?php
                                $base_total = $price->value;
                                $total = $price->value;
                            ?>
                            <table class="bordered checkout-table">
                                <thead>
                                    <tr>
                                        <th>{{trans('application.market_checkout_electronic_table_head_id')}}</th>
                                        <th>{{trans('application.market_checkout_electronic_table_head_description')}}</th>
                                        <th class="checkout-table-column-price"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td><strong>{{$item->title}}</strong>
                                        @if(isset($price->group_translation))
                                            <br>{{trans('application.market_checkout_electronic_table_row_group_placeholder')}} {{$price->group_translation}}</td>
                                        @endif
                                        <td class="checkout-table-column-price">{{$price->value}} &euro;</td>
                                    </tr>
                                    @foreach($shipping_methods as $key=>$shipping_method)
                                        @if(count($shipping_methods) ==1 || $key==0)
                                            <?php $total = $total+$shipping_method->shipping_rate; ?>
                                        @endif
                                        <tr>
                                            <td><input type="radio" name="group" class="shippingRate" id="che{{$key}}" value="{{$shipping_method->id}}" @if($key==0) checked="checked" @endif @if(count($shipping_methods) ==1) disabled @endif data-value="{{$shipping_method->shipping_rate}}"></td>
                                            <td>{{trans('application.market_checkout_publication_shipping_cost')}} {{$shipping_method->title}}</td>
                                            <td class="checkout-table-column-price">{{$shipping_method->shipping_rate}} &euro;</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td class="checkout-table-column-price" data-total="{{$base_total}}" id="totalT"><b><span id="totalView">{{$total}}</span> &euro;</b></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="spacer"></div>

                            @if(is_object($authUser) && isset($payment_methods) && is_array($payment_methods))
                                <form id="payField">
                                    <div class="form-group">
                                        @foreach($payment_methods as $payment_method)
                                            @if($payment_method->sys_payment_id == 1)
                                                <?php $route_data = [$payment_method->id,$item->id,$system_cat_id,$group_id,'printed']; ?>
                                                @if(isset($recurring_plan_id))
                                                   <?php $route_data[] = $recurring_plan_id; ?>
                                                @endif
                                                <a class="btn btn-blue btn-large btn-radius payButton" href="{{route('paypal_checkout',$route_data)}}" >
                                                    <i class="fa fa-paypal"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_paypal')}}
                                                </a>
                                                &nbsp;&nbsp;&nbsp;
                                            @endif
                                            @if($payment_method->sys_payment_id == 2)
                                                <?php $route_data = [$payment_method->id,$item->id,$system_cat_id,$group_id,'printed']; ?>
                                                 @if(isset($recurring_plan_id))
                                                    <?php $route_data[] = $recurring_plan_id; ?>
                                                 @endif
                                                 <a class="btn btn-blue btn-large btn-radius payButton" href="{{route('viva_checkout',$route_data)}}">
                                                    <i class="fa fa-credit-card"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_credit_card')}}
                                                 </a>
                                            @endif

                                            @if($payment_method->sys_payment_id == 3)
                                                <?php $route_data = [$payment_method->id,$item->id,$system_cat_id,$group_id,'printed']; ?>
                                                 @if(isset($recurring_plan_id))
                                                    <?php $route_data[] = $recurring_plan_id; ?>
                                                 @endif
                                                 <a class="btn btn-blue btn-large btn-radius payButton" href="{{route('bank_checkout',$route_data)}}">
                                                    <i class="fa fa-university"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_bank')}}
                                                 </a>
                                            @endif

                                            @if($payment_method->sys_payment_id == 4)
                                                <?php $route_data = [$payment_method->id,$item->id,$system_cat_id,$group_id,'printed']; ?>
                                                 @if(isset($recurring_plan_id))
                                                    <?php $route_data[] = $recurring_plan_id; ?>
                                                 @endif
                                                 <a class="btn btn-blue btn-large btn-radius payButton" href="{{route('delivery_checkout',$route_data)}}">
                                                    <i class="fa fa-money"></i>&nbsp;{{trans('application.market_checkout_electronic_pay_delivery')}}( +{{$payment_method->pay_on_delivery_extra}}&euro; )
                                                 </a>
                                            @endif
                                        @endforeach


                                    </div>
                                </form>
                                <div class="spacer"></div>
                            @endif

@section('scripts')
<script type="text/javascript">
        $(document).ready(function(){

            $('.shippingRate').on('click',function(){
                var base_price = $('#totalT').attr('data-total');
                $('#totalView').html(base_price);
                var id = $(this).attr('id');
                var checked = $("#" + id + ":checked").length;

                if(checked == 1){
                    var value = $(this).attr('data-value');

                    var total = parseFloat(base_price) + parseFloat(value);
                    $('#totalView').html(total);
                }

                 var count = $('.shippingRate:checked').length;
                  if(count == 0){
                        $('#payField').hide();
                  }else if(count == 1){
                        $('#payField').show();
                  }
            });

            $('.payButton').on('click',function(e){
                e.preventDefault();
                     var checked = $(".shippingRate:checked").attr('value');
                     var url = $(this).attr('href')+"?shipping_selected="+checked;
                     window.location = url;
                return false;
            })
        });
    </script>
@endsection