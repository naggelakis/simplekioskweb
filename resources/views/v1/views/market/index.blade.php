@extends($template.'.layout.master')
@section('content')
    <style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        /* display: none; <- Crashes Chrome on hover */
        -webkit-appearance: none;
        margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
    }
    </style>
    <section id="section">
            <div class="inner-wrapper">

                <!-- Main -->
                <div id="main" role="main">


                    <div class="row">

                        <div class="grid_2"></div>

                        <div class="grid_8">

                            <!-- / CART -->
                            <p class="title"><span>{{trans('application.cart_index_cart_title')}}</span></p>

                            @if(isset($items) && is_array($items) && count($items)>0)
                                <p>{{trans('application.cart_index_cart_subtitle')}} </p>
                                <table class="bordered checkout-table">
                                    <thead>
                                        <tr>
                                            <th width="10"></th>
                                            <th colspan="2">{{trans('application.cart_index_cart_table_head_description')}}</th>
                                            <th width="30">{{trans('application.cart_index_cart_table_head_price')}}</th>
                                            <th width="70">{{trans('application.cart_index_cart_table_head_quantity')}}</th>
                                            <th width="30">{{trans('application.cart_index_cart_table_head_total')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $total = 0; ?>
                                        @foreach($items as $key=>$item)
                                            <?php $total += $item['subtotal']; ?>

                                            @if($item['options']['type'] == 'issue')
                                                <?php $url = route('issue_single',[$item['id'],Helpers::urlize($item['name'])]); ?>
                                            @elseif($item['options']['type'] == 'magazine')
                                               <?php $url = route('magazine_single',[$item['id'],Helpers::urlize($item['name'])]); ?>
                                            @elseif($item['options']['type'] == 'book')
                                               <?php $url = route('book_single',[$item['id'],Helpers::urlize($item['name'])]); ?>

                                            @endif
                                            <tr id="{{$key}}">
                                                <td>
                                                    <a href="javascript:void(0);" class="deleteRow" data-id="{{$key}}">
                                                        <i class="fa fa-times" id="spi{{$key}}"></i>
                                                    </a>
                                                </td>
                                                @if(isset($item['options']['image']))
                                                    <td width="80"><a href="{{$url}}"><img src="{{$item['options']['image']}}" alt="{{$item['name']}}" /></a></td>
                                                @endif
                                                <td>
                                                    <a href="{{$url}}"><strong>{{$item['name']}}</strong></a>
                                                    @if(isset($item['options']['group_translation']) || isset($item['options']['system_translation']))
                                                        <br>
                                                        @if(isset($item['options']['system_translation']))
                                                            {{$item['options']['system_translation']}}
                                                        @endif
                                                        @if(isset($item['options']['group_translation']))
                                                            , {{trans('application.cart_index_cart_table_row_user_group')}} {{$item['options']['group_translation']}}
                                                        @endif

                                                    @endif
                                                </td>
                                                <td>{{$item['price']}}&nbsp;&euro;</td>
                                                <td style="min-width:120px;">
                                                    <div class="row">
                                                        <input type="number" min="0" value="{{$item['qty']}}" data-id="{{$key}}" data-price="{{$item['price']}}" class="quantities" style="width:40%;min-width:36px;"/>
                                                    </div>
                                                    <div class="row">
                                                        <a class="updateQuantity" href="javascript:void(0);" style="font-size: 12px;" data-id="{{$key}}" id="update{{$key}}">
                                                            <span class="saveButton" >{{trans('application.cart_index_cart_table_row_save_quantity')}}</span>
                                                            <span class="loadingButton" style="display: none;"> <i class="fa fa-spinner fa-spin" ></i> {{trans('application.cart_index_cart_table_row_save_quantity_saving')}}</span>
                                                        </a>
                                                    </div>

                                                </td>
                                                <td class="checkout-table-column-price"><span id="price_{{$key}}">{{$item['price']*$item['qty']}}</span>&nbsp;&euro;</td>
                                            </tr>
                                        @endforeach

                                        <tr>
                                            <td></td>
                                            <td colspan="4" align="right">{{trans('application.cart_index_cart_table_row_total')}}</td>
                                            <td class="checkout-table-column-price"><strong><span id="total">{{$total}}</span>&nbsp;&euro;</strong></td>
                                        </tr>
                                    </tbody>
                                </table>


                                <div class="spacer"></div>
                                <!-- / If user is logged in and has an address -->

                                <!-- / If user is NOT logged in or has NO address -->
                                @if(!is_object($authUser))

                                    <form >
                                        <fieldset>
                                            <legend>{{trans('application.market_checkout_electronic_user_login_title')}}</legend>
                                            <p>{{trans('application.market_checkout_electronic_user_login_subtitle')}}</p>
                                            @if(isset($socialApps))
                                                @if(isset($socialApps->facebook))
                                                    <div class="form-group">
                                                            <button class="btn-large btn-blue btn-expand" style="background-color: #3b5998;">
                                                                <a href="{{ route('social_login',['facebook']) }}@if(isset($redirect))?redirect={{$redirect}} @endif" style="color: #ffffff;">
                                                                    <i class="fa fa-facebook-square"></i> {{trans('application.auth_login_social_facebook_button_title')}}
                                                                </a>
                                                            </button>
                                                    </div>
                                                @endif
                                                @if(isset($socialApps->twitter))
                                                    <div class="form-group">
                                                     <button class="btn-large btn-blue btn-expand" style="background-color: #00aced;">
                                                         <a href="{{ route('social_login',['twitter']) }}@if(isset($redirect))?redirect={{$redirect}} @endif"  style="color: #ffffff;">
                                                            <i class="fa fa-twitter-square"></i> {{trans('application.auth_login_social_twitter_button_title')}}
                                                         </a>
                                                     </button>
                                                    </div>
                                                @endif
                                                @if(isset($socialApps->google))
                                                    <div class="form-group">
                                                     <button class="btn-large btn-blue btn-expand" style="background-color: #dd4b39;">
                                                         <a href="{{ route('social_login',['google']) }}@if(isset($redirect))?redirect={{$redirect}} @endif" style="color: #ffffff;">
                                                            <i class="fa fa-google-plus-square"></i> {{trans('application.auth_login_social_google_button_title')}}
                                                         </a>
                                                     </button>
                                                    </div>
                                                @endif
                                            @endif

                                            <div class="spacer"></div>
                                            <p class="text-center"><a href="{{route('login')}}@if(isset($redirect))?redirect={{$redirect}} @endif">{{trans('application.market_checkout_electronic_user_login_email')}}</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{route('login')}}@if(isset($redirect))?redirect={{$redirect}} @endif">{{trans('application.market_checkout_electronic_user_register_email')}}</a></p>
                                        </fieldset>
                                    </form>
                                    <div class="spacer"></div>

                                @elseif(is_object($authUser))

                                        <p>{{trans('application.market_checkout_publication_order_receiver_title')}}</p>
                                        <form id="postalForm" action="{{route('cart_shipping')}}" method="post">
                                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <fieldset>
                                                <legend>{{trans('application.market_checkout_publication_subscriber_details_title')}}</legend>
                                                <div class="form-group">
                                                    <label>{{trans('application.market_checkout_publication_subscriber_firstname')}}</label>
                                                    <input type="text" name="billing_firstname" value="{{$authUser->firstname}}" placheholder="{{trans('application.market_checkout_publication_subscriber_firstname_placeholder')}}" class="required" required="required"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>{{trans('application.market_checkout_publication_subscriber_lastname')}}</label>
                                                    <input type="text" name="billing_lastname" value="{{$authUser->lastname}}" placeholder="{{trans('application.market_checkout_publication_subscriber_lastname_placeholder')}}" class="required" required="required"/>
                                                </div>
                                                <div class="spacer"></div>
                                                <div class="form-group">
                                                    <label>{{trans('application.market_checkout_publication_subscriber_address')}}</label>
                                                    <input type="text"  name="billing_address" placeholder="{{trans('application.market_checkout_publication_subscriber_address_placeholder')}}" class="required" required="required" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->line1 :null}}"/>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="billing_address2" placeholder="" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->line2 :null}}"/>
                                                </div>
                                                <div class="form-group">
                                                    <select id="billingCountry" name="billing_country" placeholder="{{trans('application.market_checkout_publication_subscriber_country_placeholder')}}" class="required" required="required" >
                                                        @foreach($countries as $country_id=>$country_name)
                                                            <option value="{{$country_id}}" @if( !is_object($default_location) && $country_id == 95) selected="selected" @elseif(is_object($default_location) && isset($default_location->items->address) && $default_location->items->address->country_id == $country_id ) selected="selected" @endif >{{$country_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div id="billingCityDiv">
                                                        <select id="billingCity" name="billing_city" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required">
                                                            @foreach($cities as $county=>$cities_list)
                                                                <?php $cities_list = (array)$cities_list; ?>
                                                                <optgroup label="{{{$county}}}">
                                                                    @foreach($cities_list as $city_id=>$city_name)
                                                                        <option value="{{$city_id}}" @if(is_object($default_location) && isset($default_location->items->address) && $default_location->items->address->city_id == $city_id ) selected="selected" @endif >{{$city_name}}</option>
                                                                    @endforeach
                                                                </optgroup>

                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <input style="display:none;" id="billingCityAlt" type="text" name="billing_city_alt" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->city :null}}"/>

                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="billing_zip" placeholder="{{trans('application.market_checkout_publication_subscriber_zip_placeholder')}}" class="required" required="required" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->zip :null}}"/>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="billing_phone" placeholder="{{trans('application.market_checkout_publication_subscriber_phone_placeholder')}}" value="{{ is_object($default_location) && isset($default_location->items->phone) ? $default_location->items->phone->number :null}}"/>
                                                </div>
                                                <div class="spacer"></div>
                                                <div class="form-group">
                                                    <label><input type="checkbox" name="postal_same" checked="checked" id="postalSame"/> {{trans('application.market_checkout_publication_subscriber_post_details_same')}}</label>
                                                </div>

                                            </fieldset>

                                            <fieldset id="postalDetails" style="display: none;">
                                                <legend>{{trans('application.market_checkout_publication_subscriber_details_shipping_title')}}</legend>
                                                <div class="form-group">
                                                    <label>{{trans('application.market_checkout_publication_subscriber_firstname')}}</label>
                                                    <input type="text" name="shipping_firstname" value="{{$authUser->firstname}}" placheholder="{{trans('application.market_checkout_publication_subscriber_firstname_placeholder')}}" class="required" required="required"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>{{trans('application.market_checkout_publication_subscriber_lastname')}}</label>
                                                    <input type="text" name="shipping_lastname" value="{{$authUser->lastname}}" placeholder="{{trans('application.market_checkout_publication_subscriber_lastname_placeholder')}}" class="required" required="required"/>
                                                </div>
                                                <div class="spacer"></div>
                                                <div class="form-group">
                                                    <label>{{trans('application.market_checkout_publication_subscriber_address')}}</label>
                                                    <input type="text"  name="shipping_address" placeholder="{{trans('application.market_checkout_publication_subscriber_address_placeholder')}}" class="required" required="required"/>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="shipping_address2" placeholder=""/>
                                                </div>
                                                <div class="form-group">
                                                    <select id="shippingCountry" name="shipping_country" placeholder="{{trans('application.market_checkout_publication_subscriber_country_placeholder')}}" class="required" required="required">
                                                        @foreach($countries as $country_id=>$country_name)
                                                            <option value="{{$country_id}}" @if($country_id == 95) selected="selected" @endif >{{$country_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div id="shippingCityDiv">
                                                        <select id="shippingCity" name="shipping_city" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required">
                                                            @foreach($cities as $county=>$cities_list)
                                                                <?php $cities_list = (array)$cities_list; ?>
                                                                <optgroup label="{{{$county}}}">
                                                                    @foreach($cities_list as $city_id=>$city_name)
                                                                        <option value="{{$city_id}}">{{$city_name}}</option>
                                                                    @endforeach
                                                                </optgroup>

                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <input style="display:none;" id="shippingCityAlt" type="text" name="shipping_city_alt" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required"/>

                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="shipping_zip" placeholder="{{trans('application.market_checkout_publication_subscriber_zip_placeholder')}}" class="required" required="required"/>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="shipping_phone" placeholder="{{trans('application.market_checkout_publication_subscriber_phone_placeholder')}}" />
                                                </div>
                                                <div class="spacer"></div>

                                            </fieldset>

                                         <div class="spacer"></div>
                                         <div class="form-group">
                                            <!--<a id="refresh" class="btn btn-orange btn-radius ">{{trans('application.cart_index_refresh')}}</a>&nbsp;&nbsp;&nbsp;-->
                                            <button class="btn btn-grey btn-radius" type="submit">{{trans('application.cart_index_continue_shopping')}}</button>&nbsp;&nbsp;&nbsp;
                                            @if(is_object($authUser))
                                                <button class="btn btn-blue btn-radius btn-large" type="submit">{{trans('application.cart_index_checkout')}}</button>
                                            @endif
                                        </div>

                                        </form>
                                @endif

                            @else

                                <p>{{trans('application.cart_index_cart_no_items')}} </p>

                            @endif
                            <div class="spacer"></div>
                            <!-- / If user is NOT logged in or has NO address -->
                            @if(!is_object($authUser))
                                <form>
                                    <div class="form-group">
                                        <!--<a id="refresh" class="btn btn-orange btn-radius ">{{trans('application.cart_index_refresh')}}</a>&nbsp;&nbsp;&nbsp;-->
                                        <button class="btn btn-grey btn-radius" type="submit">{{trans('application.cart_index_continue_shopping')}}</button>&nbsp;&nbsp;&nbsp;
                                        @if(is_object($authUser))
                                            <button class="btn btn-blue btn-radius btn-large" type="submit">{{trans('application.cart_index_checkout')}}</button>
                                        @endif
                                    </div>
                                </form>
                            @endif
                            <!-- / CART -->
                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection


@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            var _token = '{{ csrf_token() }}';
            function computeTotal(){
                var total = 0;
                $('.quantities').each(function(){
                    var price = parseFloat($(this).attr('data-price'));
                    var quantity = $(this).val();
                    total = total + (price*quantity);
                });
                $('#total').html(total);
                 var count = $('.quantities').length;
                 if(count == 0){
                    window.location.reload();
                 }
            }

            $('.updateQuantity').on('click',function(){
                var id = $(this).attr('id');
                var key = $(this).attr('data-id');
                $('#update'+key+' .saveButton').hide();
                $('#update'+key+' .loadingButton').show();

                var data = [];
                $('.quantities').each(function(){
                    var id = $(this).attr('data-id');
                    var value = $(this).val();
                    var item = {
                        id: id,
                        quantity: value
                    };
                    data.push(item);
                });

                var endpoint = '/<?php echo strtolower($lang);?>/cart/update';
                $.post(endpoint,{data:data,_token:_token},function(data){
                    data = $.parseJSON(data);
                    $('#update'+key+' .saveButton').show();
                    $('#update'+key+' .loadingButton').hide();
                });





            });

            $('.deleteRow').on('click',function(){
                var id = $(this).attr('data-id');
                $('#spi'+id).removeClass('fa-times');
                $('#spi'+id).addClass('fa-spinner fa-spin');

                var endpoint = '/<?php echo strtolower($lang);?>/cart/remove/'+id;
                $.get(endpoint,function(data){
                   if(data){
                           $('#'+id).html('');
                           $('#'+id).hide();
                           computeTotal();
                   }else{
                            $('#spi'+id).removeClass('fa-spinner fa-spin');
                            $('#spi'+id).addClass('fa-times')
                   }
                });

            });

            $('.quantities').bind("keyup change",function(){
                var price = parseFloat($(this).attr('data-price'));
                var quantity = $(this).val();
                var id = $(this).attr('data-id');
                $('#price_'+id).html(price*quantity);
                computeTotal();
            });


            $('#refresh').on('click',function(){
                var data = [];
                $('.quantities').each(function(){
                    var id = $(this).attr('data-id');
                    var value = $(this).val();
                    var item = {
                        id: id,
                        quantity: value
                    };
                    data.push(item);
                });
                console.log(data);
            });


            $('#postalForm').validate();
            $('select').select2();
            $('#postalSame').on('click',function(){
                var checked = $("#postalSame:checked").length;
                if(checked == 1){
                    $('#postalDetails').hide();
                }else if(checked == 0){
                    $('#postalDetails').show();
                }
            });

            $('#billingCountry').on('change',function(){
                var country_id = $(this).val();
                if(country_id == 95){
                    $('#billingCityDiv').show();
                    $('#billingCityAlt').hide();
                }else{
                    $('#billingCityAlt').show();
                    $('#billingCityDiv').hide();
                }
            });

            $('#shippingCountry').on('change',function(){
                var country_id = $(this).val();
                if(country_id == 95){
                    $('#shippingCityDiv').show();
                    $('#shippingCityAlt').hide();
                }else{
                    $('#shippingCityAlt').show();
                    $('#shippingCityDiv').hide();
                }
            });
        });
    </script>
@endsection