@extends($template.'.layout.master')

@section('meta_title')
| {{ $entry->title}}
@endsection
@section('meta_description')
 {{ $entry->subtitle}}
@endsection

@section('content')
        <!-- Main -->
        @if($entry->article_type == 253 && isset($entry->images[0]->files) && is_array($entry->images[0]->files) && count($entry->images[0]->files)>0)
                <!-- Image full -->
                <div id="above-the-fold" class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif featured" @if($entry->article_type == 253 && isset($entry->sections) && is_array($entry->sections) && count($entry->sections)>0) style="margin-bottom:0px;" @endif   >
                    <img src="{{$entry->images[0]->files[0]->cdn}}" />
                </div>
        @else
                <!-- Above the fold -->
                <div id="above-the-fold" class="above-the-fold light">
                    <div class="inner-wrapper main-side-to-single">

                         @if(isset($banners['featured_long']))
                              <!-- Banner space / Show if BIG banner exists -->
                               <div class="banner-space top-banner-space  @if(isset($banners['featured_small']) && count($banners['featured_small'])>0) double-banner @endif">
                                  @if(isset($banners['featured_long']) && count($banners['featured_long'])>0)
                                      <div class="banner_big" >
                                          <?php $featured_long = $banners['featured_long'][0]; ?>
                                          @if(isset($featured_long->file_cdn))
                                               <a href="{{$featured_long->target_url}}" target="{{$featured_long->target_url_window}}">
                                                   <img src="{{$featured_long->file_cdn}}" alt="{{$featured_long->description}}"/>
                                               </a>
                                          @else
                                               {{{$featured_long->content }}}
                                          @endif
                                      </div>
                                  @endif

                                  @if(isset($banners['featured_small']) && count($banners['featured_small'])>0)
                                    <div class="banner_small">
                                        <?php $featured_small = $banners['featured_small'][0]; ?>
                                        @if(isset($featured_small->file_cdn))
                                             <a href="{{$featured_small->target_url}}" target="{{$featured_small->target_url_window}}">
                                                 <img src="{{$featured_small->file_cdn}}" alt="{{$featured_small->description}}"/>
                                             </a>
                                        @else
                                             {{{$featured_small->content }}}
                                        @endif
                                    </div>
                                  @endif

                                  <div class="clearfix"></div>
                               </div>
                         @endif

                    </div><!-- /inner-wrapper -->
                </div>
                <!-- /above-the-fold -->
        @endif



         <!-- Section -->
        <section id="section">
            <div class="inner-wrapper main-side-to-single ">

                <!-- Main -->
                <div id="main"  class="left"  role="main">

                    <!-- Post -->
                    <article class="single-post">

                                @if($entry->article_type == 253 && isset($entry->sections) && is_array($entry->sections) && count($entry->sections)>0)
                                    <nav class="sections-primary-menu light sticky-menu" role="navigation" style="margin-bottom: 2.5em;">

                                        <!-- Responsive menu -->
                                        <a class="click-to-open-menu"><i class="fa fa-align-justify"></i> {{trans('application.sections_navigator_title')}}</a>
                                        <!-- Main menu -->
                                        <ul class="sections-menu">
                                            @foreach($entry->sections as $section)
                                                @if(!isset($section->translations->$lang->title))
                                                    @continue
                                                @endif
                                                <li><a href="javascript:void(0);" class="section-url" target="#section{{$section->id}}"><span>{{isset($section->translations->lang->title) ? $section->translations->lang->title : current($section->translations)->title}}</span></a></li>
                                            @endforeach

                                        </ul>

                                    </nav>
                                @endif

                                @if($entry->article_type != 253)
                                    @if(isset($entry->images[0]->files) && is_array($entry->images[0]->files) && count($entry->images[0]->files)>1)
                                        <div class="gallery-single">

                                            <ul class="slides">
                                                @foreach($entry->images[0]->files as $file)
                                                     <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                        <img src="{{$file->cdn}}" alt="{{$file->title}}" />
                                                        <div class="gallery-caption-body">
                                                            <p>{{$file->title}}</p>
                                                        </div>
                                                     </li>
                                                @endforeach

                                            </ul>

                                        </div>
                                    @elseif(isset($entry->images[0]->files) && is_array($entry->images[0]->files) && count($entry->images[0]->files)==1)
                                        <div class="featured">
                                            <a href="javascript:void(0);">
                                                <img src="{{$entry->images[0]->files[0]->cdn}}" alt="{{$entry->images[0]->files[0]->title}}">
                                            </a>
                                        </div>
                                    @endif
                                @endif


                                <h1 class="post-title">{{$entry->title}}</h1>
                                <h3 class="lead">{{$entry->subtitle}}</h3>



                                <div class="post-container">

                                    @if($lock_status == 'open')

                                        @if($entry->article_type == 253)
                                                 @if(isset($entry->images[0]->files) && is_array($entry->images[0]->files) && count($entry->images[0]->files)>1)
                                                        <div class="post-right">
                                                            <div class="gallery-single">
                                                                <ul class="slides">
                                                                    @foreach($entry->images[0]->files as $key=>$file)
                                                                            @if($key!=0)
                                                                             <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                                                <img src="{{$file->cdn}}" alt="{{$file->title}}" />
                                                                                <div class="gallery-caption-body">
                                                                                    <p>{{$file->title}}</p>
                                                                                </div>
                                                                             </li>
                                                                         @endif
                                                                    @endforeach

                                                                </ul>
                                                            </div>
                                                        </div>
                                                 @endif
                                        @endif

                                        @if(isset($entry->description) && $entry->description!=null && $entry->description!='')
                                            <p>{{{$entry->description}}}</p>
                                        @endif

                                            @if(isset($entry->sections) && is_array($entry->sections) && count($entry->sections)>0)



                                                @if($entry->article_type == 252)
                                                    <div class="spacer"></div>
                                                    <div class="accordion">
                                                @endif

                                                @if($entry->article_type != 253 && $entry->article_type != 252  && isset($entry->sections) && is_array($entry->sections) && count($entry->sections)>0)
                                                    <nav class="sections-primary-menu light sticky-menu" role="navigation" style="margin-bottom: 2.5em;">

                                                        <!-- Responsive menu -->
                                                        <a class="click-to-open-menu"><i class="fa fa-align-justify"></i> {{trans('application.sections_navigator_title')}}</a>
                                                        <!-- Main menu -->
                                                        <ul class="sections-menu">
                                                            @foreach($entry->sections as $section)
                                                                @if(!isset($section->translations->$lang->title))
                                                                    @continue
                                                                @endif
                                                                <li><a href="javascript:void(0);" class="section-url" target="#section{{$section->id}}"><span>{{isset($section->translations->lang->title) ? $section->translations->lang->title : current($section->translations)->title}}</span></a></li>
                                                            @endforeach

                                                        </ul>

                                                    </nav>
                                                @endif


                                                @foreach($entry->sections as $section)
                                                    @if(isset($section->translations->$lang)){



                                                        <!-- / SECTION -->
                                                        <?php $section_subtitle = (isset($section->translations->$lang->subtitle)) ? $section->translations->$lang->subtitle : null; ?>
                                                        <?php $section_description = (isset($section->translations->$lang->description)) ? $section->translations->$lang->description : null; ?>

                                                        @if($entry->article_type == 251)
                                                              <div style="clear:both"></div>
                                                              <div class="row" id="section{{$section->id}}">
                                                                <h2>{{ isset($section->translations->$lang->title) ? Helpers::remove_tones($section->translations->$lang->title) : Helpers::remove_tones(current($section->translations)->title) }}</h2>
                                                                <h3 class="lead">{{$section_subtitle}}</h3>
                                                                @if(isset($section->images) && is_array($section->images) && count($section->images) == 1)
                                                                    <div class="post-left">
                                                                        <img src="{{$section->images[0]->cdn}}" alt="Image" />
                                                                        @if(isset($section->images[0]->title) && $section->images[0]->title != null && $section->images[0]->title!='')
                                                                            <span class="caption">{{ $section->images[0]->title }}</span>
                                                                        @endif
                                                                    </div>
                                                                @elseif(isset($section->images) && is_array($section->images) && count($section->images) > 1)
                                                                    <div class="post-left">
                                                                        <div class="gallery-single">

                                                                            <ul class="slides">
                                                                                @foreach($section->images as $file)
                                                                                     <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                                                        <img src="{{$file->cdn}}" />
                                                                                        @if(isset($file->title) && $file->title != null && $file->title!='')
                                                                                            <div class="gallery-caption-body">
                                                                                                <p>{{$file->title}}</p>
                                                                                            </div>
                                                                                        @endif
                                                                                     </li>
                                                                                @endforeach
                                                                                @if(count($entry->images)>1)
                                                                                    @foreach($entry->images as $file)
                                                                                        <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}"><img src="{{$file->cdn}}" alt="{{$file->title}}" /></li>
                                                                                    @endforeach
                                                                                @endif
                                                                            </ul>

                                                                        </div>
                                                                    </div>
                                                                @endif


                                                                @if( $section_description != null && $section_description != '')
                                                                    <p>{{{$section_description}}}</p>
                                                                @endif
                                                             </div>
                                                        @elseif($entry->article_type == 252)


                                                                    <!-- SECTION -->
                                                                    <div class="title" id="section{{$section->id}}">{{ isset($section->translations->$lang->title) ? Helpers::remove_tones($section->translations->$lang->title) : Helpers::remove_tones(current($section->translations)->title) }}</div>
                                                                    <div class="content" style="padding: 20px;">
                                                                        <h3 class="lead">{{$section_subtitle}}</h3>

                                                                        @if(isset($section->images) && is_array($section->images) && count($section->images) == 1)
                                                                            <div class="post-left">
                                                                                <img src="{{$section->images[0]->cdn}}" alt="Image" />
                                                                                @if(isset($section->images[0]->title) && $section->images[0]->title != null && $section->images[0]->title!='')
                                                                                    <span class="caption">{{ $section->images[0]->title }}</span>
                                                                                @endif
                                                                            </div>
                                                                        @elseif(isset($section->images) && is_array($section->images) && count($section->images) > 1)
                                                                            <div class="post-left">
                                                                                <div class="gallery-single">

                                                                                    <ul class="slides">
                                                                                        @foreach($section->images as $file)
                                                                                             <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                                                                <img src="{{$file->cdn}}" />
                                                                                                @if(isset($file->title) && $file->title != null && $file->title!='')
                                                                                                    <div class="gallery-caption-body">
                                                                                                        <p>{{$file->title}}</p>
                                                                                                    </div>
                                                                                                @endif
                                                                                             </li>
                                                                                        @endforeach
                                                                                        @if(count($entry->images)>1)
                                                                                            @foreach($entry->images as $file)
                                                                                                <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}"><img src="{{$file->cdn}}" alt="{{$file->title}}" /></li>
                                                                                            @endforeach
                                                                                        @endif
                                                                                    </ul>

                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                        @if( $section_description != null && $section_description != '')
                                                                            <p>{{{$section_description}}}</p>
                                                                        @endif
                                                                    </div>
                                                                    <!-- SECTION -->
                                                        @elseif($entry->article_type == 253)
                                                                  <div style="clear:both"></div>
                                                                  <div class="row" id="section{{$section->id}}">
                                                                    <p class="title"><span>{{ isset($section->translations->$lang->title) ? Helpers::remove_tones($section->translations->$lang->title) : Helpers::remove_tones(current($section->translations)->title) }}</span></p>
                                                                    @if(isset($section->images) && is_array($section->images) && count($section->images) == 1)
                                                                        <div class="featured">
                                                                            <img src="{{$section->images[0]->cdn}}" alt="Image" />
                                                                            @if(isset($section->images[0]->title) && $section->images[0]->title != null && $section->images[0]->title!='')
                                                                                <span class="caption">{{ $section->images[0]->title }}</span>
                                                                            @endif
                                                                        </div>
                                                                    @elseif(isset($section->images) && is_array($section->images) && count($section->images) > 1)

                                                                            <div class="gallery-single">

                                                                                <ul class="slides">
                                                                                    @foreach($section->images as $file)
                                                                                         <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                                                            <img src="{{$file->cdn}}" />
                                                                                            @if(isset($file->title) && $file->title != null && $file->title!='')
                                                                                                <div class="gallery-caption-body">
                                                                                                    <p>{{$file->title}}</p>
                                                                                                </div>
                                                                                            @endif
                                                                                         </li>
                                                                                    @endforeach
                                                                                    @if(count($entry->images)>1)
                                                                                        @foreach($entry->images as $file)
                                                                                            <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}"><img src="{{$file->cdn}}" alt="{{$file->title}}" /></li>
                                                                                        @endforeach
                                                                                    @endif
                                                                                </ul>

                                                                            </div>

                                                                    @endif
                                                                    <h3 class="lead">{{$section_subtitle}}</h3>
                                                                    @if( $section_description != null && $section_description != '')
                                                                        <p>{{{$section_description}}}</p>
                                                                    @endif
                                                                 </div>
                                                        @endif
                                                    @endif
                                                @endforeach
                                                @if($entry->article_type == 252)
                                                    </div>
                                                @endif
                                            @endif

                                            @if(isset($entry->albums) && is_array($entry->albums) && count($entry->albums)>0)

                                                <!-- / PARENT CUSTOM GALLERIES -->
                                                <br>
                                                <div class="gallery-content">
                                                    @foreach($entry->albums as $album)
                                                        @if(is_array($album->files) && count($album->files)>0)
                                                            <!-- Gallery album -->
                                                            <div class="gallery-album sport-gal">
                                                                <div class="gal-img">
                                                                    <a class="fancybox" rel="group<?php echo $album->id;?>" href="{{$album->thumb_cdn}}" title="{{$album->files[0]->title}}">
                                                                        <span>{{count($album->files)}} {{trans('application.custom_albums_images_label')}}</span>
                                                                        <img src="{{Helpers::squareFilePath($album->thumb_cdn)}}" alt="{{$album->title}}" title="{{$album->files[0]->title}}"/>
                                                                    </a>
                                                                </div>
                                                                <h4><a href="javascript:void(0);" >{{$album->title}}</a></h4>
                                                                <p>{{$album->subtitle}}</p>
                                                                @foreach($album->files as $key=>$file)
                                                                  @if($key > 0)
                                                                    <a class="fancybox" rel="group<?php echo $album->id;?>" href="{{$file->cdn}}" style="display:none;" title="{{$file->title}}"></a>
                                                                  @endif
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                                <!-- / PARENT CUSTOM GALLERIES -->
                                            @endif

                                    @else
                                        @include($template.'.partials._article_locked_messages')
                                    @endif
                                </div>





                        <!-- Post info -->
                        <div class="post-info">

                            @if(isset($cats) && is_array($cats) && count($cats))
                                <span class="category">{{trans('application.news_single_categories_title')}}
                                    <?php $cat_i = 0; ?>
                                    @foreach($cats as $cat_id=>$cat_title)
                                        <?php $cat_i++; ?>
                                        <a href="{{route('pages_category',[$cat_id,Helpers::urlize($cat_title)])}}">{{Helpers::remove_tones($cat_title)}}</a>
                                        @if(count($cats)>$cat_i)
                                        ,
                                        @endif
                                    @endforeach
                                </span>
                            @endif
                        </div>

                        <div class="post-share">
                            <span class="share-text">{{trans('application.news_single_share_title')}}</span>
                            <ul>
                                <li><a data-tip="Share on Twitter!" target="_blank" href="https://twitter.com/intent/tweet?text={{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}" class="twitter"><span class="socicon">a</span></a></li>
                                <li><a data-tip="Share on Facebook!" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}" class="facebook"><span class="socicon">b</span></a></li>
                                <li><a data-tip="Share on Google+!" target="_blank" href="https://plus.google.com/share?url={{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}" class="google"><span class="socicon">c</span></a></li>
                                <li><a data-tip="Share on Pinterest!" target="_blank" href="http://pinterest.com/pin/create/button/?url={{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}&description={{$entry->title}}" class="google"><span class="socicon">d</span></a></li>
                                <li><a data-tip="Share on LinkedIn!" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title))}}" class="linkedin"><span class="socicon">j</span></a></li>
                            </ul>
                        </div>

                    </article>
                    @if(isset($other_posts) && is_array($other_posts) && count($other_posts)>0)
                     <!-- Related articles -->
                    <div class="block-layout-one">
                        <p class="title"><span>{{ trans('application.page_single_related_articles_title') }}</span></p>
                        <div class="row">

                        <?php $i = 0; ?>
                        @foreach($other_posts as $related)
                            @if( $i>0 && $i%3 == 0)
                                </div>
                                <div class="row">
                            @endif
                            <div class="item grid_4 ">
                                @if(isset($related->thumb))
                                    <a href="{{Helpers::getRouteSingleBySystemCat($related->system_parent_cat_id,$related->id,Helpers::urlize($related->title))}}"><img src="{{Helpers::squareFilePath($related->thumb)}}" width="80" height="65"/></a>
                                @endif
                                <div @if(!isset($related->thumb)) style="margin-left:0px;"@endif>
                                    @if(isset($related->cat_id))
                                        <span><a href="{{ Helpers::getRouteCategoryBySystemCat($related->system_parent_cat_id, $related->cat_id, Helpers::urlize($related->category_title))}}">{{Helpers::remove_tones($related->category_title)}}</a></span>
                                    @endif
                                    <h3><a href="{{Helpers::getRouteSingleBySystemCat($related->system_parent_cat_id,$related->id,Helpers::urlize($related->title))}}">{{$related->title}}</a></h3>
                                    <p class="date">{{Helpers::remove_tones(Helpers::localeDate($related->created))}}</p>
                                </div>
                                <span class="clearfix"></span>
                            </div>
                            <?php $i++; ?>
                        @endforeach

                    </div>
                        </div>
                        @endif

                </div>


                 <!-- Aside -->
                    <aside id="sidebar" role="complementary">
                         <!-- Banner 300x250 / Show if Image Sidebar Banner exists / Show only the first in line -->
                           @if(isset($banners['sidebar_top']) && count($banners['sidebar_top'])>0)
                              <div class="widget">
                                  <div class="ad-banner-300x250">
                                      <?php $sidebar_top = $banners['sidebar_top'][0]; ?>
                                      @if(isset($sidebar_top->file_cdn))
                                           <a href="{{$sidebar_top->target_url}}" target="{{$sidebar_top->target_url_window}}">
                                               <img src="{{$sidebar_top->file_cdn}}" alt="{{$sidebar_top->description}}"/>
                                           </a>
                                      @else
                                           {{{$sidebar_top->content }}}
                                      @endif
                                  </div>
                              </div>
                            @endif

                        @if(isset($category_siblings) && is_array($category_siblings) && count($category_siblings)>0)
                            <!-- Category widget / Show if Subcategories exists -->
                            <div class="widget">
                                <h3 class="widget-title">{{trans('application.pages_category_list_sidebar_categories_title')}}</h3>
                                <ul class="widget-categories">
                                    @foreach($category_siblings as $category)
                                        <li><a href="{{route('pages_category',[$category->id,Helpers::urlize($category->title)])}}">{{$category->title}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(isset($banners['sidebar_bottom']) && count($banners['sidebar_bottom'])>0)
                            <div class="widget">
                                <h3 class="widget-title">{{trans('application.homepage_sidebar_bottom_banners_title')}}</h3>
                                @foreach(($banners['sidebar_bottom']) as $sidebar_bottom)
                                <div class="ad-banner-300x250" style="margin-bottom:20px;">
                                    @if(isset($sidebar_bottom->file_cdn))
                                         <a href="{{$sidebar_bottom->target_url}}" target="{{$sidebar_bottom->target_url_window}}">
                                             <img src="{{$sidebar_bottom->file_cdn}}" alt="{{$sidebar_bottom->description}}"/>
                                         </a>
                                    @else
                                         {{{$sidebar_bottom->content }}}
                                    @endif
                                </div>
                                 @endforeach
                            </div>
                        @endif


                    </aside>
            </div>
        </section>


@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
        });
    </script>
@endsection