@extends($template.'.layout.master')

@if(isset($category->title))
    @section('meta_title')
    | {{ $category->title }}
    @endsection
@endif

@if(isset($category->description))
    @section('meta_description')
    {{$category->description}}
    @endsection
@endif
@section('content')

                @if(isset($category->title))
                    <!-- Above the fold -->
                    <div id="above-the-fold" class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif">
                        <div class="inner-wrapper">

                            <h2 class="page-title">{{$category->title}}</h2>

                        </div><!-- /inner-wrapper -->
                    </div><!-- /above-the-fold -->
                @endif
                <!-- Section -->
                <section id="section" ng-app="mainApp" ng-controller="pagesListCtrl">
                    <div class="inner-wrapper">




                        <!-- Main -->
                        <div id="main" class="left" role="main"  @if($category->hide_entries == 0) ng-init="lang='{{strtolower($lang)}}'; type=<?php echo $type;?>; cat_id=<?php echo $category->id;?>; latest_category_posts_fetch()" @endif>

                            @if($category->hide_entries == 0)
                                 @if(isset($banners['list_top']) && count($banners['list_top'])>0)
                                        <div class="banner-space">
                                             <?php $list_top = $banners['list_top'][0]; ?>
                                             @if(isset($list_top->file_cdn))
                                                  <a href="{{$list_top->target_url}}" target="{{$list_top->target_url_window}}">
                                                      <img src="{{$list_top->file_cdn}}" alt="{{$list_top->description}}"/>
                                                  </a>
                                             @else
                                                  {{{$list_top->content }}}
                                             @endif
                                         </div>
                                @endif
                                <!-- SHOW ONLY IF CATEGORY ALLOWS TO LIST ARTICLES / SHOW FROM ALL SUBCATEGORIES AS WELL -->
                                @if($category->view_type == 266 || $category->view_type == 0)
                                    <!-- Posts / Show posts in sort order / Show as list if category view is list -->
                                    <div ng-show="latest_posts.length>0" >
                                        <article class="single-post" ng-repeat="post in latest_posts">
                                            <div class="featured" ng-show="post.thumb">
                                                <a ng-href="@{{ post.render_url  }}"><img ng-src="@{{post.thumb_square}}" alt="@{{post.title}}"/></a>
                                            </div>
                                            <h1 class="post-title"><a ng-href="@{{ post.render_url  }}">@{{ post.title  }}</a></h1>
                                            <h3 class="lead hide_on_761_1080" ng-show="post.subtitle && post.subtitle!=''">@{{ post.subtitle }}</h3>
                                            <hr>
                                            <div class="clearfix">&nbsp;</div>
                                        </article>
                                    </div>
                                     <br>
                                     <a ng-click="latest_category_posts_fetch()" class="btn btn-expand btn-large btn-custom btn-radius" ng-show="show_load_more==1">
                                        <i class="fa fa-spinner fa-spin" ng-show="spinner==1"></i>
                                        &nbsp;
                                        <span ng-show="spinner==0">{{trans('application.homepage_recent_articles_load_more')}}</span>
                                        <span ng-show="spinner==1">{{trans('application.homepage_recent_articles_loading')}}</span>
                                     </a>
                                @endif

                                <!-- Posts / Show posts in sort order / Show as thumbs if category view is thumbs -->
                                @if($category->view_type == 267)
                                    <div ng-show="latest_posts.length>0">
                                        <div class="block-layout-two row" ng-repeat="group in latest_posts | chunk:2:this">
                                            <!-- Post -->
                                            <div class="grid_6" ng-repeat="post in group">
                                                <div class="main-item">
                                                    <div class="post-img" ng-show="post.thumb">
                                                        <a ng-href="@{{ post.render_url  }}"><img ng-src="@{{post.thumb_square}}" alt="@{{post.title}}"/></a>
                                                    </div>
                                                    <h3><a ng-href="@{{ post.render_url  }}">@{{ post.title  }}</a></h3>
                                                    <p class="hide_on_761_1080" ng-show="post.subtitle && post.subtitle!=''">@{{ post.subtitle }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                     <a ng-click="latest_category_posts_fetch()" class="btn btn-expand btn-large btn-custom btn-radius" ng-show="show_load_more==1">
                                          <i class="fa fa-spinner fa-spin" ng-show="spinner==1"></i>
                                          &nbsp;
                                           <span ng-show="spinner==0">{{trans('application.homepage_recent_articles_load_more')}}</span>
                                           <span ng-show="spinner==1">{{trans('application.homepage_recent_articles_loading')}}</span>
                                     </a>
                                @endif
                             <!-- SHOW ONLY IF CATEGORY ALLOWS TO LIST ARTICLES / SHOW FROM ALL SUBCATEGORIES AS WELL -->
                            @else
                                 @if(isset($category->subcategories) && is_array($category->subcategories) && count($category->subcategories)>0)
                                        @if(isset($banners['list_top']) && count($banners['list_top'])>0)
                                            <div class="banner-space">
                                                 <?php $list_top = $banners['list_top'][0]; ?>
                                                 @if(isset($list_top->file_cdn))
                                                      <a href="{{$list_top->target_url}}" target="{{$list_top->target_url_window}}">
                                                          <img src="{{$list_top->file_cdn}}" alt="{{$list_top->description}}"/>
                                                      </a>
                                                 @else
                                                      {{{$list_top->content }}}
                                                 @endif
                                             </div>
                                        @endif
                                        <div class="block-layout-two row" >
                                            @foreach($category->subcategories as $key=>$subcategory)

                                                @if($key > 0 && $key%2 == 0)
                                                    </div>
                                                    <div class="block-layout-two row" >
                                                @endif
                                                <!-- Post -->
                                                <div class="grid_6" >
                                                    <div class="main-item">
                                                        @if(isset($subcategory->thumb))
                                                            <div class="post-img">
                                                                <a href="{{ route('pages_category',[$subcategory->id,Helpers::urlize($subcategory->title)]) }}"><img src="{{$subcategory->thumb}}" alt="{{$subcategory->title}}"/></a>
                                                            </div>
                                                        @endif
                                                        <h3><a href="{{ route('pages_category',[$subcategory->id,Helpers::urlize($subcategory->title)]) }}">{{ $subcategory->title  }}</a></h3>
                                                        @if(isset($subcategory->subtitle) && $subcategory->subtitle!=null && $subcategory->subtitle!='')
                                                            <p class="hide_on_761_1080" >{{ Helpers::get_snippet($subcategory->subtitle,20) }}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                 @endif
                            @endif
                             @if(isset($banners['list_bottom']) && count($banners['list_bottom'])>0)
                                   <div class="banner-space">
                                      <?php $list_bottom = $banners['list_bottom'][0]; ?>
                                      @if(isset($list_bottom->file_cdn))
                                           <a href="{{$list_bottom->target_url}}" target="{{$list_bottom->target_url_window}}">
                                               <img src="{{$list_bottom->file_cdn}}" alt="{{$list_bottom->description}}"/>
                                           </a>
                                      @else
                                           {{{$list_bottom->content }}}
                                      @endif
                                  </div>
                             @endif
                        </div><!-- /main -->


                        <!-- Aside -->
                        <aside id="sidebar" role="complementary">
                            @if(isset($category->subcategories) && is_array($category->subcategories) && count($category->subcategories)>0)
                                <!-- Category widget / Show if Subcategories exists -->
                                <div class="widget">
                                    <h3 class="widget-title">{{trans('application.pages_category_list_sidebar_categories_title')}}</h3>
                                    <ul class="widget-categories">
                                        @foreach($category->subcategories as $category)
                                            <li><a href="{{route('pages_category',[$category->id,Helpers::urlize($category->title)])}}">{{$category->title}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            @elseif(isset($category->siblings) && is_array($category->siblings) && count($category->siblings)>0)
                                <!-- Category widget / Show if Subcategories exists -->
                                <div class="widget">
                                    <h3 class="widget-title">{{trans('application.pages_category_list_sidebar_categories_title')}}</h3>
                                    <ul class="widget-categories">
                                        @foreach($category->siblings as $category)
                                            <li><a href="{{route('pages_category',[$category->id,Helpers::urlize($category->title)])}}">{{$category->title}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(isset($banners['sidebar_bottom']) && count($banners['sidebar_bottom'])>0)
                                <div class="widget">
                                    <h3 class="widget-title">{{trans('application.homepage_sidebar_bottom_banners_title')}}</h3>
                                    @foreach(($banners['sidebar_bottom']) as $sidebar_bottom)
                                    <div class="ad-banner-300x250" style="margin-bottom:20px;">
                                        @if(isset($sidebar_bottom->file_cdn))
                                             <a href="{{$sidebar_bottom->target_url}}" target="{{$sidebar_bottom->target_url_window}}">
                                                 <img src="{{$sidebar_bottom->file_cdn}}" alt="{{$sidebar_bottom->description}}"/>
                                             </a>
                                        @else
                                             {{{$sidebar_bottom->content }}}
                                        @endif
                                    </div>
                                     @endforeach
                                </div>
                            @endif


                        </aside>


                    </div><!-- /inner-wrapper -->
                </section><!-- /section -->

@endsection


@section('scripts')
    @javascript({{ asset('assets/'.$template.'/angular/pages_list.js') }})
@endsection