<aside id="sidebar" role="complementary">

    <!-- User menu widget / Show if Store app exists -->
    <div class="widget">
        <h3 class="widget-title">{{trans('application.user_sidebar_menu_group_one_title')}}</h3>
        <ul class="widget-categories">
            <li><a href="{{route('user_profile')}}">{{trans('application.user_sidebar_menu_group_one_link_homepage')}}</a></li>
            <li><a href="{{route('user_subscriptions')}}">{{trans('application.user_sidebar_menu_group_one_link_subscriptions')}}</a></li>
            <li><a href="{{route('user_orders')}}">{{trans('application.user_sidebar_menu_group_one_link_orders')}}</a></li>
        </ul>
    </div>
    <div class="widget">
        <h3 class="widget-title">{{trans('application.user_sidebar_menu_group_two_title')}}</h3>
        <ul class="widget-categories">
            <li><a href="{{route('user_favorites')}}">{{trans('application.user_sidebar_menu_group_two_link_favorites')}}</a></li>
            <li><a href="{{route('user_issues')}}">{{trans('application.user_sidebar_menu_group_two_link_issues')}}</a></li>
        </ul>
    </div>
    <div class="widget">
        <h3 class="widget-title">{{trans('application.user_sidebar_menu_group_three_title')}}</h3>
        <ul class="widget-categories">
            <li><a href="{{route('user_edit')}}">{{trans('application.user_sidebar_menu_group_three_link_profile')}}</a></li>
            <li><a href="{{route('user_locations')}}">{{trans('application.user_sidebar_menu_group_three_link_locations')}}</a></li>
            <li><a href="{{route('user_logout')}}">{{trans('application.user_sidebar_menu_group_three_link_logout')}}</a></li>
        </ul>
    </div>

</aside>