@extends($template.'.layout.master')

@section('content')
                <!-- Above the fold -->
                <div id="above-the-fold" class="above-the-fold light">
                    <div class="inner-wrapper">

                        <h2 class="page-title">{{trans('application.user_home_title')}}</h2>

                    </div><!-- /inner-wrapper -->
                </div><!-- /above-the-fold -->

                <!-- Section -->
                <section id="section" ng-app="mainApp" ng-controller="userIssuesListController" ng-init="lang='<?php echo strtolower($lang);?>'">
                    <div class="inner-wrapper">

                        <!-- Main -->
                        <div id="main" class="left" role="main" ng-init="fetchIssues()">


                        	<div class="block-layout-one">
                                <p class="title"><span>{{trans('application.user_issues_title')}}</span></p>

                                @if($userIssuesAppPurchases == null)
                                    <div class="alert yellow" id="alertDiv" ng-show="message_hide == 0">
                                        <strong>{{trans('application.user_issues_alert_title')}}</strong>
                                        <br>{{trans('application.user_issues_alert_placeholder')}}
                                        <br>
                                        <br>
                                        <!--<a class="btn btn-grey btn-radius" href="#" style="margin-bottom: 0px;">{{trans('application.user_issues_alert_button_why_not')}}</a>&nbsp;&nbsp;-->
                                        <a class="btn btn-blue btn-radius" href="javascript:void(0);" ng-click="hideMessage()" style="margin-bottom: 0px;">{{trans('application.user_issues_alert_button_yes')}}</a>
                                    </div>
                                    <div class="spacer" ng-show="message_hide == 0"></div>
                                @endif
                                <!-- Form -->
                                <form class="woocommerce-ordering" ng-show="posts.length > 0 && spinner==0">
                                    <select name="orderby" class="orderby" ng-model="sort" ng-change="sort_fetch()">
                                        <option ng-value="0" ng-selected="selected">{{trans('application.user_issues_sort_publication_date')}} </option>
                                        @if(isset($magazines) && is_array($magazines) && count($magazines)>0)
                                            @foreach($magazines as $magazine)
                                                <option ng-value="{{$magazine->id}}">{{trans('application.user_issues_sort_issue')}} {{$magazine->title}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </form>
                                <!-- Products -->
                                <div class="row"  ng-show="posts.length >0 && spinner==0" ng-repeat="group in posts | chunk:4:this">
                                    <ul class="products" >
                                        <li ng-class="{ 'product first': $index == 0,'product last': $index == 3, 'product': $index >0 && $index <3 }" ng-repeat="post in group">
                                            <a ng-href="@{{ post.render_url }}">
                                                <img src="@{{ post.thumb.cdn }}" alt="@{{ post.render_title }}" ng-show="post.render_image"/>
                                                <h3>@{{ post.render_title }}</h3>
                                                <a ng-href="@{{ post.render_magazine_url }}" ng-show="post.magazine">@{{ post.magazine.title }}</a>
                                                <span class="price"></span>
                                            </a>
                                            <a href="@{{ post.render_url }}" class="btn btn-grey">{{trans('application.user_issues_read')}}</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="row">
                                   <div class="spacer"></div>
                                   <a ng-click="fetchIssues()" class="btn btn-expand btn-large btn-custom btn-radius" ng-show="show_load_more==1">
                                        <i class="fa fa-spinner fa-spin" ng-show="spinner==1"></i>
                                        &nbsp;
                                        <span ng-show="spinner==0">{{trans('application.homepage_recent_articles_load_more')}}</span>
                                        <span ng-show="spinner==1">{{trans('application.homepage_recent_articles_loading')}}</span>
                                   </a>
                                </div>
                                <!-- / Show if page has at least one mobile app of kiosk with a subscription and the force register is NOT selected -->

                                <span ng-show="posts.length == 0 && spinner==0">
                                    {{trans('application.user_issues_no_issues')}}
                                </span>

                            </div>

                        </div><!-- /main -->

                        <!-- Aside -->
                        @include($template.'.views.user.partials.sidebar')



                    </div><!-- /inner-wrapper -->
                </section><!-- /section -->

@endsection


@section('scripts')
    @javascript({{ asset('assets/'.$template.'/angular/user_issues_list.js') }})
@endsection