@extends($template.'.layout.master')

@section('content')
                <!-- Above the fold -->
                <div id="above-the-fold" class="above-the-fold light">
                    <div class="inner-wrapper">

                        <h2 class="page-title">{{trans('application.user_home_title')}}</h2>

                    </div><!-- /inner-wrapper -->
                </div><!-- /above-the-fold -->

                <!-- Section -->
                <section id="section" ng-app="mainApp" ng-controller="userFavoritesListController" ng-init="lang='<?php echo strtolower($lang);?>'">
                    <div class="inner-wrapper">

                        <!-- Main -->
                        <div id="main" class="left" role="main" ng-init="posts_fetch()">


                        	<div class="block-layout-one">
                                <p class="title"><span>{{trans('application.user_favorites_title')}}</span></p>


                                <div class="row" ng-show="posts.length >0 " ng-repeat="group in posts | chunk:3:this">
                                    <div class="item grid_4" ng-repeat="post in group">
                                        <a ng-href="@{{ post.render_url }}" ng-show="post.thumb"><img ng-src="@{{ post.thumb_square }}" alt="@{{ post.title }}" width="80"></a>
                                        <div>
                                            <span ng-show="post.render_category"><a ng-href="@{{ post.render_category_url }}">@{{ post.render_category }}</a></span>
                                            <h3><a ng-href="@{{ post.render_url }}">@{{ post.title }}</a></h3>
                                            <a href="javascript:void(0);" style="font-size:0.8em;" ng-click="favorite_post(post.id)"> {{trans('application.user_favorites_delete')}}</a>
                                            <p style="clear:both;"></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                   <div class="spacer"></div>
                                   <a ng-click="posts_fetch()" class="btn btn-expand btn-large btn-custom btn-radius" ng-show="show_load_more==1">
                                        <i class="fa fa-spinner fa-spin" ng-show="spinner==1"></i>
                                        &nbsp;
                                        <span ng-show="spinner==0">{{trans('application.homepage_recent_articles_load_more')}}</span>
                                        <span ng-show="spinner==1">{{trans('application.homepage_recent_articles_loading')}}</span>
                                   </a>
                                </div>
                                <!-- / Show if page has at least one mobile app of kiosk with a subscription and the force register is NOT selected -->

                                <span ng-show="posts.length == 0 && spinner==0">
                                    {{trans('application.user_favorites_no_favorites')}}
                                </span>

                            </div>

                        </div><!-- /main -->

                        <!-- Aside -->
                        @include($template.'.views.user.partials.sidebar')



                    </div><!-- /inner-wrapper -->
                </section><!-- /section -->

@endsection


@section('scripts')
    @javascript({{ asset('assets/'.$template.'/angular/user_favorites_list.js') }})
@endsection