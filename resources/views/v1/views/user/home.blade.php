@extends($template.'.layout.master')

@section('content')
                <!-- Above the fold -->
                <div id="above-the-fold" class="above-the-fold light">
                    <div class="inner-wrapper">

                        <h2 class="page-title">{{trans('application.user_home_title')}}</h2>

                    </div><!-- /inner-wrapper -->
                </div><!-- /above-the-fold -->

                <!-- Section -->
                <section id="section">
                    <div class="inner-wrapper">
                         @if(Session::has('success'))
                            <?php $status = Session::get('success'); ?>
                            <div class="alert green">
                                <i class="fa fa-check"></i> {{{ trans('application.'.$status) }}}
                            </div>
                         @endif
                         @if(Session::has('error'))
                            <?php $status = Session::get('error'); ?>
                            <div class="alert red">
                                <i class="fa fa-times"></i> {{{ trans('application.'.$status) }}}
                            </div>
                        @endif
                        @if($authUser->confirmed_email== null)
                            <!-- / EMAIL CONFIRMATION -->
                            <div class="alert yellow">
                                <strong>{{trans('application.user_home_email_confirmation_title')}}</strong>
                                <br><br>{{trans('application.user_home_email_confirmation_placeholder_left')}} <strong>{{$authUser->email}}</strong> {{{trans('application.user_home_email_confirmation_placeholder_right')}}}
                                <br><br>
                                <!--<a class="btn btn-grey btn-radius" href="#" style="margin-bottom: 0px;">{{trans('application.user_home_email_confirmation_change_button')}}</a>
                                &nbsp;&nbsp;-->
                                <a class="btn btn-blue btn-radius" href="{{route('generate_confirmation_code')}}" id="confirmEmail"  style="margin-bottom: 0px;">
                                    <span id="confButton">{{trans('application.user_home_email_confirmation_resend_button')}}</span>
                                    <span id="loading" style="display:none;"><i class="fa fa-spin fa-spinner"></i> {{trans('application.user_home_email_confirmation_email_sending')}}</span>
                                </a>
                                <span id="confirmMessage" style="display:none;">
                                    <i class="fa fa-check " ></i> {{trans('application.user_home_email_confirmation_email_sent')}}
                                </span>
                            </div>
                            <div class="spacer"></div>
                            <!-- / EMAIL CONFIRMATION -->
                        @endif

                        @if(Session::has('email_confirmed'))
                            <!-- / EMAIL CONFIRMATION -->
                            <div class="alert green">
                                <strong>{{trans('application.user_home_email_confirmed_title')}}</strong>
                                <br>{{trans('application.user_home_email_confirmed_placeholder')}}
                            </div>
                            <div class="spacer"></div>
                            <!-- / EMAIL CONFIRMATION -->
                        @endif
                        <!-- Main -->
                        <div id="main" class="left" role="main">

                            @if(isset($expiring_subscriptions) && count($expiring_subscriptions)>0)
                                @foreach($expiring_subscriptions as $expiring_subscription)
                                    <!-- / SUBSCRIPTION NEAR END (1 month) -->
                                    <div class="alert red">
                                        <strong>{{trans('application.user_home_subscription_expire_title')}}</strong>
                                        <br><br>{{trans('application.user_home_subscription_expire_placeholder_left')}} <strong>{{$expiring_subscription->prod_title}}</strong> {{trans('application.user_home_subscription_expire_placeholder_center')}} <strong>{{$expiring_subscription->expiring_in}} {{trans('application.user_home_subscription_expire_placeholder_days')}}</strong>. {{trans('application.user_home_subscription_expire_placeholder_right')}}
                                        <br><br><a class="btn btn-blue btn-radius" href="{{route('update_subscription',[$expiring_subscription->subscription_id])}}" style="margin-bottom: 0px;">{{trans('application.user_home_subscription_expire_refresh_button')}}</a>
                                    </div>
                                    <div class="spacer"></div>
                                    <!-- / SUBSCRIPTION NEAR END (1 month) -->
    	                        @endforeach
                            @endif

                        	<!-- CONTENT DASHBOARD -->
                        	<div class="block-layout-one">

    	                    	<!-- Quick buttons / Show if Store app NOT exists -->
    	                    	<div class="row">
    		                    	<a class="grid_6 alert grey" style="min-height:150px;" href="{{route('user_favorites')}}">
    									<h3>{{trans('application.user_home_subscription_favorites_title')}}</h3>
    									<p>{{trans('application.user_home_subscription_favorites_placeholder')}}</p>
    		                    	</a>
    		                    	<a class="grid_6 alert grey" style="min-height:150px;" href="{{route('user_edit')}}">
    									<h3>{{trans('application.user_home_subscription_profile_title')}}</h3>
    									<p>{{trans('application.user_home_subscription_profile_placeholder')}}</p>
    		                    	</a>
    	                    	</div>

                                @if(isset($socialApps) && count($socialApps)>0 && (isset($socialApps->facebook) || isset($socialApps->google) || isset($socialApps->twitter) ))
                                    <!-- Social connect buttons / Show if Store app NOT exists -->
                                    <p class="title"><span>{{trans('application.user_home_social_media_title')}}</span></p>
                                    <p>{{trans('application.user_home_social_media_placeholder')}}</p>
                                    <div class="row">
                                        @if(isset($socialApps->facebook))
                                            <div class="form-group grid_4">
                                                <button class="btn-large btn-grey btn-expand white" onclick="@if($authUser->fb_id == null) window.location.href ='{{route('user_social_connect',['facebook'])}}'  @endif " @if($authUser->fb_id!=null) style="background-color: #3b5998;" @endif ><i class="fa fa-facebook-square"></i> {{ $authUser->fb_id == null ? trans('application.user_home_social_media_connect') : trans('application.user_home_social_media_connected') }} Facebook</button>
                                            </div>
                                        @endif
                                        @if(isset($socialApps->twitter))
                                            <div class="form-group grid_4">
                                                <button class="btn-large btn-grey btn-expand white" onclick="@if($authUser->twitter_id == null) window.location.href ='{{route('user_social_connect',['twitter'])}}'  @endif" @if($authUser->twitter_id!=null) style="background-color: #00aced;" @endif ><i class="fa fa-twitter-square"></i> {{ $authUser->fb_id == null ? trans('application.user_home_social_media_connect') : trans('application.user_home_social_media_connected') }} Twitter</button>
                                            </div>
                                        @endif
                                        @if(isset($socialApps->google))
                                            <div class="form-group grid_4">
                                                <button class="btn-large btn-grey btn-expand white" onclick="@if($authUser->google_id == null) window.location.href ='{{route('user_social_connect',['google'])}}'  @endif" @if($authUser->google_id!=null) style="background-color: #D62D20;" @endif ><i class="fa fa-google-plus-square"></i> {{ $authUser->fb_id == null ? trans('application.user_home_social_media_connect') : trans('application.user_home_social_media_connected') }} Google</button>
                                            </div>
                                        @endif
                                    </div>
                                @endif
    	                    	<!-- Quick buttons / Show if Store app exists -->
    	                    	<div class="row">
    		                    	<a class="grid_4 alert grey" href="{{route('user_favorites')}}">
    									<h3>{{trans('application.user_home_aricles_title')}}</h3>
    									<p>{{trans('application.user_home_aricles_placeholder')}}</p>
    		                    	</a>
    		                    	<a class="grid_4 alert grey"  href="{{route('user_issues')}}">
    									<h3>{{trans('application.user_home_issues_title')}}</h3>
    									<p>{{trans('application.user_home_issues_placeholder')}}</p>
    		                    	</a>
    		                    	<a class="grid_4 alert grey" href="{{route('user_subscriptions')}}">
    									<h3>{{trans('application.user_home_subscriptions_title')}}</h3>
    									<p>{{trans('application.user_home_subscriptions_placeholder')}}</p>
    		                    	</a>
    	                    	</div>

                        	</div><!-- /CONTENT DASHBOARD -->

                        </div><!-- /main -->

                        <!-- Aside -->
                        @include($template.'.views.user.partials.sidebar')



                    </div><!-- /inner-wrapper -->
                </section><!-- /section -->

@endsection


@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#confirmEmail').on('click',function(e){
                e.stopPropagation();

                var url = $(this).attr('href');
                    $('#confButton').hide();
                    $('#loading').show();
                    $.get(url,function(){
                    }).complete(function(){
                        $('#confirmEmail').hide();
                        $('#confirmMessage').show();
                    });
                 //alert(url);
                return false;
            });
        });
    </script>
@endsection