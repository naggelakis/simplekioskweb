@extends($template.'.layout.master')

@section('content')
                <!-- Above the fold -->
                <div id="above-the-fold" class="above-the-fold light">
                    <div class="inner-wrapper">

                        <h2 class="page-title">{{trans('application.user_home_title')}}</h2>

                    </div><!-- /inner-wrapper -->
                </div><!-- /above-the-fold -->

                <!-- Section -->
                <section id="section">
                    <div class="inner-wrapper">

                        <!-- Main -->
                        <div id="main" class="left" role="main" >

                        	<div class="block-layout-one">
                                <p class="title"><span>{{trans('application.user_orders_title')}}</span></p>
                                @if(isset($orders) && is_array($orders) && count($orders)>0)
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{trans('application.user_orders_table_head_date')}}</th>
                                                <th>{{trans('application.user_orders_table_head_total')}}</th>
                                                <th>{{trans('application.user_orders_table_head_status')}}</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($orders as $order)
                                                <tr>
                                                    <td>{{$order->id}}</td>
                                                    <td>{{Helpers::localeDate($order->created,'d/m/Y')}}</td>
                                                    <td>{{$order->total}} €</td>
                                                    <td>
                                                        <span class="btn btn-blue btn-expand btn-radius" style="margin-bottom: 0px; cursor: default;">{{ trans('application.payment_status_id_'.$order->status_id)}}</span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="row">
                                        <span >{{trans('application.user_orders_no_orders_found')}}</span>
                                    </div>
                                @endif

                            </div>

                        </div><!-- /main -->

                        <!-- Aside -->
                        @include($template.'.views.user.partials.sidebar')



                    </div><!-- /inner-wrapper -->
                </section><!-- /section -->

@endsection
