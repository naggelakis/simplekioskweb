@extends($template.'.layout.master')

@section('head_styles')
		<link rel='stylesheet' href='{{asset('assets/'.$template.'/plugins/nifty/lib/nifty.css')}}'>
@endsection
@section('head_scripts')
        <script src="{{asset('assets/'.$template.'/plugins/nifty/lib/nifty.js')}}"></script>
@endsection
@section('content')
                <!-- Above the fold -->
                <div id="above-the-fold" class="above-the-fold light">
                    <div class="inner-wrapper">

                        <h2 class="page-title">{{trans('application.user_locations_title')}}</h2>

                    </div><!-- /inner-wrapper -->
                </div><!-- /above-the-fold -->

                <!-- Section -->
                <section id="section">
                    <div class="inner-wrapper">

                        <!-- Main -->
                        <div id="main" class="left" role="main" >
                                @if(Session::has('success'))
                                    <?php $status = Session::get('success'); ?>
                                    <div class="alert green">
                                        <i class="fa fa-check"></i> {{{ trans('application.'.$status) }}}
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <?php $status = Session::get('error'); ?>
                                    <div class="alert red">
                                        <i class="fa fa-times"></i> {{{ trans('application.'.$status) }}}
                                    </div>
                                @endif
                                @if(is_array($locations) && count($locations)>0)
                                    <div class="row">
                                        <div class="grid_3" style="float: right;">
                                            <a class="btn btn-green btn-expand btn-radius " href="javascript:void(0);" style="margin-bottom: 0px; " data-trigger='modal' data-target='#modal-1'>
                                                {{trans('application.user_location_create')}}
                                            </a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="accordion ui-accordion ui-widget ui-helper-reset" role="tablist">
                                                @foreach($locations as $location)
                                                    <div class="title ui-accordion-header ui-helper-reset ui-state-default ui-accordion-icons ui-corner-all" role="tab" id="ui-accordion-1-header-{{$location->id}}" aria-controls="ui-accordion-1-panel-0" aria-selected="false" aria-expanded="true" tabindex="0">
                                                        <span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>
                                                        {{$location->title}}
                                                        @if($location->is_default == 1)
                                                            <a class="btn btn-grey btn-small  btn-radius" href="javascript:void(0);" style="margin-bottom: 0px;float:right;">
                                                                {{trans('application.user_location_default')}}
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <div class="content ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" id="ui-accordion-1-panel-0" aria-labelledby="ui-accordion-1-header-{{$location->id}}" role="tabpanel" aria-hidden="true" style="display: none;">
                                                         <div class="row" style="margin-top:15px;padding-left: 10px;padding-right: 10px;">
                                                            <div class="grid_8">
                                                                <span>
                                                                    {{$location->items->address->line1}} <br>
                                                                    @if($location->items->address->line2!=null && $location->items->address->line2!='') {{$location->items->address->line2}} <br> @endif
                                                                    @if($location->items->address->city_id!=null && is_numeric($location->items->address->city_id)) {{$cities_e[$location->items->address->city_id]}} @else  {{ $location->items->address->city }} @endif {{$location->items->address->zip}}<br>
                                                                    {{ $countries->{$location->items->address->country_id} }}
                                                                    @if(isset($location->items->phone))
                                                                        <br>
                                                                        T: +{{$location->items->phone->country_code.$location->items->phone->number}}
                                                                    @endif
                                                                </span>
                                                            </div>
                                                             <div class="grid_3">
                                                                <div class="row">
                                                                     <div class="grid_12">
                                                                         <a class="btn btn-green btn-expand btn-radius editModal" href="javascript:void(0);" style="margin-bottom: 0px;" data-title="{{$location->title}}" data-zip="{{$location->items->address->zip}}" data-city_alt="{{$location->items->address->city}}" data-phone="@if(isset($location->items->phone)) {{$location->items->phone->number}} @endif" data-country_id="{{$location->items->address->country_id}}" data-id="{{$location->id}}" data-line1="{{$location->items->address->line1}}" data-line2="@if($location->items->address->line2!=null && $location->items->address->line2!='') {{$location->items->address->line2}} @endif" data-city_id="{{$location->items->address->city_id}}" data-address_id="@if($location->items->address!=null ) {{$location->items->address->id}} @endif" data-phone_id="@if(isset($location->items->phone)) {{$location->items->phone->id}} @endif" data-trigger='modal' data-target='#modal-edit'>
                                                                             {{trans('application.user_location_edit')}}
                                                                         </a>
                                                                     </div>
                                                                </div>
                                                                @if($location->is_default == 1)
                                                                    <!--<div class="row">
                                                                        <div class="grid_12">
                                                                            <a class="btn btn-grey btn-expand btn-radius" href="javascript:void(0);" style="margin-bottom: 0px;">
                                                                                {{trans('application.user_location_default')}}
                                                                            </a>
                                                                        </div>
                                                                    </div>-->
                                                                @else
                                                                    <div class="row">
                                                                        <div class="grid_12">
                                                                            <a class="btn btn-blue btn-expand btn-radius" href="{{route('make_location_default',[$location->id])}}" style="margin-bottom: 0px;">
                                                                                {{trans('application.user_location_set_default')}}
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="grid_12">
                                                                            <a class="btn btn-red btn-expand btn-radius" href="{{route('delete_location',[$location->id])}}" style="margin-bottom: 0px; ">
                                                                                {{trans('application.user_location_delete')}}
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                              </div>
                                                         </div>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="alert yellow">
                                        <strong>{{trans('application.user_locations_no_location_alert_title')}}</strong>
                                        <br>{{trans('application.user_locations_no_location_alert_placeholder')}}
                                        <br><br>
                                        <a class="btn btn-blue btn-radius" href="javascript:void(0);" style="margin-bottom: 0px;" data-trigger='modal' data-target='#modal-1'>{{trans('application.user_locations_no_location_alert_click_here')}}</a>
                                    </div>
                                @endif

                        </div><!-- /main -->

                        <!-- Aside -->
                        @include($template.'.views.user.partials.sidebar')



                    </div><!-- /inner-wrapper -->
                </section><!-- /section -->
                    <div class="nifty-modal fade-in-scale" id="modal-1">
               			<div class="md-content">
               				<div class='md-title' style="@if($basic_app->app_theme_base_color!=null ) {{'background:#'.$basic_app->app_theme_base_color.';'}} @endif ">
               					<h3 style="color:#fff;">{{trans('application.user_locations_modal_create_title')}}</h3>
               				</div>
               				<div class='md-body'>
                                <form id="postalForm" action="{{route('user_locations_post')}}" method="post">
                                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <fieldset>
                                        <div class="form-group">
                                            <label>{{trans('application.market_checkout_publication_subscriber_address')}}</label>
                                            <input type="text"  name="address1" placeholder="{{trans('application.market_checkout_publication_subscriber_address_placeholder')}}" class="required" required="required" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="address2" placeholder="" />
                                        </div>
                                        <div class="form-group">
                                            <select id="billingCountry" name="country_id" placeholder="{{trans('application.market_checkout_publication_subscriber_country_placeholder')}}" class="required" required="required">
                                                @foreach($countries as $country_id=>$country_name)
                                                    <option value="{{$country_id}}" @if($country_id == 95) selected="selected" @endif >{{$country_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div id="billingCityDiv">
                                                <select id="billingCity" name="city_id" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required">
                                                    @foreach($cities as $county=>$cities_list)
                                                        <?php $cities_list = (array)$cities_list; ?>
                                                        <optgroup label="{{{$county}}}">
                                                            @foreach($cities_list as $city_id=>$city_name)
                                                                <option value="{{$city_id}}" >{{$city_name}}</option>
                                                            @endforeach
                                                        </optgroup>

                                                    @endforeach
                                                </select>
                                            </div>

                                            <input style="display:none;" id="billingCityAlt" type="text" name="city_name" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required" />

                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="zip" placeholder="{{trans('application.market_checkout_publication_subscriber_zip_placeholder')}}" class="required" required="required" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="phone" placeholder="{{trans('application.market_checkout_publication_subscriber_phone_placeholder')}}"  />
                                        </div>
                                        <div class="spacer"></div>

                                        <div class="form-group">
                                            <input class="btn btn-blue" type="submit" value="{{trans('application.user_locations_modal_save')}}"/>
                                        </div>
                                    </fieldset>


                                </form>

               					<button class="btn btn-primary md-close">{{trans('application.user_locations_modal_close')}}</button>
               				</div>
               			</div>
               		</div>

               		<div class="md-overlay"></div>



               		<div class="nifty-modal fade-in-scale" id="modal-edit">
                        <div class="md-content">
                            <div class='md-title' style="@if($basic_app->app_theme_base_color!=null ) {{'background:#'.$basic_app->app_theme_base_color.';'}} @endif ">
                                <h3 style="color:#fff;">{{trans('application.user_locations_modal_edit_title')}}</h3>
                            </div>
                            <div class='md-body'>
                                <form id="postalEditForm" action="{{route('user_locations_edit')}}" method="post">
                                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <fieldset>
                                        <div class="form-group">
                                            <label>{{trans('application.market_checkout_publication_subscriber_title')}}</label>
                                            <input type="text"  id="editTitle" name="title" placeholder="{{trans('application.market_checkout_publication_subscriber_title_placeholder')}}" class="required" required="required" />
                                        </div>
                                        <div class="form-group">
                                            <label>{{trans('application.market_checkout_publication_subscriber_address')}}</label>
                                            <input type="text"  id="editAddress1" name="address1" placeholder="{{trans('application.market_checkout_publication_subscriber_address_placeholder')}}" class="required" required="required" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" id="editAddress2" name="address2" placeholder="" />
                                        </div>
                                        <div class="form-group">
                                            <select id="billingCountry" name="country_id" placeholder="{{trans('application.market_checkout_publication_subscriber_country_placeholder')}}" class="required country" required="required">
                                                @foreach($countries as $country_id=>$country_name)
                                                    <option value="{{$country_id}}" @if($country_id == 95) selected="selected" @endif >{{$country_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div id="editCityDiv">
                                                <select id="billingCity" name="city_id" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required city" required="required">
                                                    @foreach($cities as $county=>$cities_list)
                                                        <?php $cities_list = (array)$cities_list; ?>
                                                        <optgroup label="{{{$county}}}">
                                                            @foreach($cities_list as $city_id=>$city_name)
                                                                <option value="{{$city_id}}" >{{$city_name}}</option>
                                                            @endforeach
                                                        </optgroup>

                                                    @endforeach
                                                </select>
                                            </div>

                                            <input style="display:none;" id="editCityAlt" type="text" name="city_name" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required" />

                                        </div>
                                        <div class="form-group">
                                            <input type="text" id="editZip" name="zip" placeholder="{{trans('application.market_checkout_publication_subscriber_zip_placeholder')}}" class="required" required="required" />
                                        </div>
                                        <div class="form-group">
                                            <input type="text" id="editPhone" name="phone" placeholder="{{trans('application.market_checkout_publication_subscriber_phone_placeholder')}}"  />
                                        </div>
                                        <div class="spacer"></div>

                                        <div class="form-group">
                                            <input class="btn btn-blue" type="submit" value="{{trans('application.user_locations_modal_save')}}"/>
                                        </div>
                                    </fieldset>

                                    <input id="location_id" name="location_id" type="hidden"/>
                                    <input id="address_id" name="address_id" type="hidden"/>
                                    <input id="phone_id" name="phone_id" type="hidden"/>
                                </form>

                                <button class="btn btn-primary md-close">{{trans('application.user_locations_modal_close')}}</button>
                            </div>
                        </div>
                    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $('#postalForm').validate();
            $('#postalEditForm').validate();
            $('#modal-1 #billingCountry').on('change',function(){
                var country_id = $(this).val();
                if(country_id == 95){
                    $('#billingCityDiv').show();
                    $('#billingCityAlt').hide();

                }else{
                    $('#billingCityAlt').show();
                    $('#billingCityDiv').hide();

                }
            });


            function handleNiftyEvent(event) {
                $('#location_id').val($(this).attr('data-id'));
                $('#phone_id').val($(this).attr('data-phone_id'));
                $('#address_id').val($(this).attr('data-address_id'));
                $('#editAddress1').val($(this).attr('data-line1'));
                $('#editAddress2').val($(this).attr('data-line2'));
                $('#editZip').val($(this).attr('data-zip'));
                $('#editPhone').val($(this).attr('data-phone'));
                $('#editTitle').val($(this).attr('data-title'));
                $('#modal-edit .country').val($(this).attr('data-country_id'));
                $('#modal-edit .city').val($(this).attr('data-city_id'));
                $('#editCityAlt').val($(this).attr('data-city'));
                if($(this).attr('data-country_id')!=95){
                    $('#modal-edit .city').hide();
                    $('#editCityAlt').show();
                }
            }

            $('#modal-edit #billingCountry').on('change',function(){
                var country_id = $(this).val();
                if(country_id == 95){
                    $('#editCityDiv').show();
                    $('#editCityAlt').hide();
                }else{
                    $('#editCityDiv').hide();
                    $('#editCityAlt').show();
                }
            });
            $(".editModal").on("click", handleNiftyEvent)

        });
    </script>
@endsection