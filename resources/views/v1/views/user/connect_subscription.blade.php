@extends($template.'.layout.master')

@section('meta_title')
   | {{ trans('application.connect_subscription_head_title') }}
@endsection

@section('content')

        <!-- Section -->
        <section id="section">
            <div class="inner-wrapper">

                <!-- Main -->
                <div id="main" role="main">


                    <div class="row">

                        <div class="grid_2"></div>

                        <div class="grid_8">

                            @if(isset($error))
                                <!-- / FAIL RESPONCE -->
                                <div class="alert red">
                                    <strong>{{trans('application.market_checkout_publication_error_title')}}</strong>
                                    <br>
                                    {{trans('application.market_checkout_publication_error_'.$error)}}
                                    </strong>
                                </div>
                                <div class="spacer"></div>
                                <!-- / FAIL RESPONCE -->
                            @endif


                            <p>{{trans('application.connect_subscription_title')}}</p>
                            <form id="subForm" action="{{route('connect_subscription_post')}}" method="post">
                               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <fieldset>
                                    <legend>{{trans('application.connect_subscription_details_title')}}</legend>

                                    <div class="form-group">
                                        <label>{{trans('application.connect_subscription_details_subscriber_recurring_plan_title')}}</label>
                                        <select id="recurring_pland_id" name="recurring_plan_id"  class="required" required="required">
                                            @foreach($recurring_plans as $key=>$recurring_plan)
                                                <option value="{{$recurring_plan->id}}" @if($key == 0) selected="selected" @endif >{{$recurring_plan->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>{{trans('application.connect_subscription_details_subscriber_firstname')}}</label>
                                        <input type="text" name="billing_firstname" value="{{$authUser->firstname}}" placheholder="{{trans('application.market_checkout_publication_subscriber_firstname_placeholder')}}" class="required" required="required" />
                                    </div>
                                    <div class="form-group">
                                        <label>{{trans('application.connect_subscription_details_subscriber_lastname')}}</label>
                                        <input type="text" name="billing_lastname" value="{{$authUser->lastname}}" placeholder="{{trans('application.market_checkout_publication_subscriber_lastname_placeholder')}}" class="required" required="required"/>
                                    </div>
                                    <div class="spacer"></div>
                                    <div class="form-group">
                                        <label>{{trans('application.connect_subscription_details_subscriber_address')}}</label>
                                        <input type="text"  name="billing_address" placeholder="{{trans('application.market_checkout_publication_subscriber_address_placeholder')}}" class="required" required="required" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->line1 :null}}"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="billing_address2" placeholder="" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->line2 :null}}"/>
                                    </div>
                                    <div class="form-group">
                                        <select id="billingCountry" name="billing_country" placeholder="{{trans('application.market_checkout_publication_subscriber_country_placeholder')}}" class="required" required="required">
                                            @foreach($countries as $country_id=>$country_name)
                                                <option value="{{$country_id}}" @if( !is_object($default_location) && $country_id == 95) selected="selected" @elseif(is_object($default_location) && isset($default_location->items->address) && $default_location->items->address->country_id == $country_id ) selected="selected" @endif >{{$country_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <div id="billingCityDiv">
                                            <select id="billingCity" name="billing_city" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required">
                                                @foreach($cities as $county=>$cities_list)
                                                    <?php $cities_list = (array)$cities_list; ?>
                                                    <optgroup label="{{{$county}}}">
                                                        @foreach($cities_list as $city_id=>$city_name)
                                                            <option value="{{$city_id}}" @if(is_object($default_location) && isset($default_location->items->address) && $default_location->items->address->city_id == $city_id ) selected="selected" @endif>{{$city_name}}</option>
                                                        @endforeach
                                                    </optgroup>

                                                @endforeach
                                            </select>
                                        </div>

                                        <input style="display:none;" id="billingCityAlt" type="text" name="billing_city_alt" placeholder="{{trans('application.market_checkout_publication_subscriber_city_placeholder')}}" class="required" required="required" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->city :null}}"/>

                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="billing_zip" placeholder="{{trans('application.market_checkout_publication_subscriber_zip_placeholder')}}" class="required" required="required" value="{{ is_object($default_location) && isset($default_location->items->address) ? $default_location->items->address->zip :null}}"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="billing_phone" placeholder="{{trans('application.market_checkout_publication_subscriber_phone_placeholder')}}" value="{{ is_object($default_location) && isset($default_location->items->phone) ? $default_location->items->phone->number :null}}"/>
                                    </div>
                                    <div class="spacer"></div>

                                    <div class="form-group">
                                        <input class="btn btn-blue" type="submit" value="{{trans('application.connect_subscription_details_subscriber_save')}}"/>
                                    </div>
                                </fieldset>


                            </form>
                            <div class="spacer"></div>

                        </div>

                        <div class="grid_2"></div>

                    </div>


                </div>

            </div>
        </section>

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('select').select2();
            $('#subForm').validate();

            $('#billingCountry').on('change',function(){
                var country_id = $(this).val();
                if(country_id == 95){
                    $('#billingCityDiv').show();
                    $('#billingCityAlt').hide();
                }else{
                    $('#billingCityAlt').show();
                    $('#billingCityDiv').hide();
                }
            });

        });
    </script>
@endsection