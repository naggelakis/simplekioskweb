@extends($template.'.layout.master')

@section('content')
                <!-- Above the fold -->
                <div id="above-the-fold" class="above-the-fold light">
                    <div class="inner-wrapper">

                        <h2 class="page-title">{{trans('application.user_home_title')}}</h2>

                    </div><!-- /inner-wrapper -->
                </div><!-- /above-the-fold -->

                <!-- Section -->
                <section id="section">
                    <div class="inner-wrapper">

                        <!-- Main -->
                        <div id="main" class="left" role="main">


                        	<div class="block-layout-one">
                                <p class="title"><span>{{trans('application.user_subscriptions_title')}}</span></p>
                                @if(Session::has('success'))
                                    <?php $status = Session::get('success'); ?>
                                    <div class="alert green">
                                        <i class="fa fa-check"></i> {{{ trans('application.'.$status) }}}
                                    </div>
                                @endif
                                @if(Session::has('error'))
                                    <?php $status = Session::get('error'); ?>
                                    <div class="alert red">
                                        <i class="fa fa-times"></i> {{{ trans('application.'.$status) }}}
                                    </div>
                                @endif
                                @if($clickedSubscriptions == null)
                                    <!-- / Show if page offers print subscriptions -->
                                    <div class="alert yellow">
                                        <strong>{{trans('application.user_subscriptions_connect_printed_title')}}</strong>
                                        <br>{{trans('application.user_subscriptions_connect_printed_placeholder')}}
                                        <br><br>
                                        <a class="btn btn-grey btn-radius" href="{{route('connect_subscription')}}?noSubscription=true" style="margin-bottom: 0px;">{{trans('application.user_subscriptions_connect_printed_button_not_have')}}</a>
                                        &nbsp;&nbsp;
                                        <a class="btn btn-blue btn-radius" href="{{route('connect_subscription')}}" style="margin-bottom: 0px;">{{trans('application.user_subscriptions_connect_printed_button_have')}}</a>
                                    </div>
                                    <!-- / Show if page offers print subscriptions -->
                                @endif
                                <!-- / Show if page has at least one mobile app of kiosk with a subscription and the force register is NOT selected -->
                                <div class="alert yellow">
                                    <strong>{{trans('application.user_subscriptions_connect_inapp_title')}}</strong>
                                    <br>{{trans('application.user_subscriptions_connect_inapp_placeholder')}}
                                    <br><br><a class="btn btn-grey btn-radius" href="#" style="margin-bottom: 0px;">{{trans('application.user_subscriptions_connect_inapp_button_not')}}</a>&nbsp;&nbsp;<a class="btn btn-blue btn-radius" href="#" style="margin-bottom: 0px;">{{trans('application.user_subscriptions_connect_inapp_button_yes')}}</a>
                                </div>
                                <div class="spacer"></div>
                                <!-- / Show if page has at least one mobile app of kiosk with a subscription and the force register is NOT selected -->
                                @if(isset($subscriptions) && is_array($subscriptions) && count($subscriptions)>0)
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{trans('application.user_subscriptions_table_description')}}</th>
                                                <th></th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($subscriptions as $subscription)
                                                <tr>
                                                    <td>{{$subscription->id}}</td>
                                                    <td>{{trans('application.user_subscriptions_table_type_'.$subscription->system_type_id)}} ({{$subscription->prod_title}})</td>
                                                    <td>
                                                        <span class="btn btn-blue btn-expand btn-radius" style="margin-bottom: 0px; cursor: default;">
                                                            {{trans('application.user_subscriptions_table_expire_title')}} {{Helpers::localeDate($subscription->expires,'d/m/Y')}}
                                                        </span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <span>
                                        {{trans('application.user_subscriptions_no_subscriptions')}}
                                    </span>
                                @endif
                            </div>

                        </div><!-- /main -->

                        <!-- Aside -->
                        @include($template.'.views.user.partials.sidebar')



                    </div><!-- /inner-wrapper -->
                </section><!-- /section -->

@endsection

