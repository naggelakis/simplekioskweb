@extends($template.'.layout.master')

@section('content')
                <!-- Above the fold -->
                <div id="above-the-fold" class="above-the-fold light">
                    <div class="inner-wrapper">

                        <h2 class="page-title">{{trans('application.user_home_title')}}</h2>

                    </div><!-- /inner-wrapper -->
                </div><!-- /above-the-fold -->

                <!-- Section -->
                <section id="section">
                    <div class="inner-wrapper">

                        <!-- Main -->
                        <div id="main" class="left" role="main" >

                            @if(Session::has('update_profile_status'))
                                <?php $status = Session::get('update_profile_status'); ?>
                                <div class="alert {{ $status == 'success' ? 'green' : 'red' }}">
                                    <i class="fa fa-{{ $status == 'success' ? 'check' : 'times' }}"></i> {{{ trans('application.update_profile_'.$status) }}}
                                </div>
                                <div class="spacer"></div>
                            @endif
                            @if(Session::has('update_password_status'))
                                <?php $status = Session::get('update_password_status'); ?>
                                <div class="alert {{ $status == 'success' ? 'green' : 'red' }}">
                                    <i class="fa fa-{{ $status == 'success' ? 'check' : 'times' }}"></i> {{{ trans('application.update_password_'.$status) }}}
                                </div>
                                <div class="spacer"></div>
                            @endif

                        	<div class="block-layout-one">
                                <p class="title"><span>{{trans('application.user_profile_title')}}</span></p>
                                 <div class="grid_12">
                                    <form action="{{route('update_user_profile')}}" id="registerForm" method="post" >

                                        <fieldset>
                                            <legend>{{trans('application.user_profile_panel_title')}}</legend>
                                            @if(isset($form_errors['register']) && isset($form_errors['register']['message']))
                                                <div class="row">
                                                    <div class="grid_12">
                                                        <div class="alert  red">{{ trans('application.'.$form_errors['register']['message'])}}</div>
                                                    </div>
                                                </div>
                                            @endif
                                            <div class="form-group">
                                                <label id="forregEmail" for="regEmail">{{trans('application.auth_login_panel_register_email_label')}} <i class="fa " style="display:none;"></i><span></span></label>
                                                <input type="email" placeholder="{{trans('application.auth_login_panel_register_email_placeholder')}}" name="email" required="required" class="required" id="regEmail" value="{{$authUser->email}}"  />
                                            </div>
                                            <div class="form-group">
                                                <label id="forregFirstname" for="regFirstname">{{trans('application.auth_login_panel_register_firstname_label')}} <i class="fa " style="display:none;"></i></label>
                                                <input type="text" placeholder="{{trans('application.auth_login_panel_register_firstname_placeholder')}}" name="firstname" required="required" class="required" id="regFirstname"  value="{{$authUser->firstname}}" />
                                            </div>
                                            <div class="form-group">
                                                <label id="forregLastname" for="regLastname">{{trans('application.auth_login_panel_register_lastname_label')}} <i class="fa " style="display:none;"></i></label>
                                                <input type="text" placeholder="{{trans('application.auth_login_panel_register_lastname_placeholder')}}" name="lastname" required="required" class="required" id="regLastname"  value="{{$authUser->lastname}}" />
                                            </div>

                                            <div class="form-group">
                                                <label id="forregBirthdate" for="regBirthdate">{{trans('application.auth_login_panel_register_birthdate_label')}} <i class="fa " style="display:none;"></i></label>
                                                <input type="text" name="birthdate"   id="regBirthdate" placeholder="(DD)-(MM)-(YYYY)" value="@if($authUser->birth_date != null ) {{date('d/m/Y',strtotime($authUser->birth_date))}} @endif" />
                                            </div>
                                            <div class="form-group">
                                                <label id="forregGender" for="regGender">{{trans('application.auth_login_panel_register_gender_label')}} <i class="fa " style="display:none;"></i></label>
                                                @if(isset($form_errors['register']) && isset($form_errors['register']['data']['gender']))
                                                    <?php $gender = $form_errors['register']['data']['gender']; ?>
                                                @else
                                                    <?php $gender = 244; ?>
                                                @endif
                                                <label><input type="radio" name="gender" value="244" name="gender"  id="regGender" @if($authUser->gender == 244) checked="checked" @endif /> {{trans('application.auth_login_panel_register_gender_value_man')}}</label>
                                                <label><input type="radio" name="gender" value="245" name="gender"   id="regGender" @if($authUser->gender == 245) checked="checked" @endif /> {{trans('application.auth_login_panel_register_gender_value_woman')}}</label>
                                            </div>
                                            <div class="spacer"></div>

                                            <div class="form-group">
                                                <input class="btn btn-custom btn-expand" type="submit" value="{{trans('application.user_profile_panel_button_label')}}"/>
                                            </div>
                                        </fieldset>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                     <form action="{{route('update_user_password')}}" id="passwordForm" method="post" >
                                            <fieldset>
                                                <legend>{{trans('application.user_password_panel_title')}}</legend>
                                                @if($authUser->registered_social != 1)
                                                    <div class="form-group">
                                                        <label>{{trans('application.auth_login_panel_login_password_old_label')}}</label>
                                                        <input type="password" placeholder="{{trans('application.auth_login_panel_login_password_old_placeholder')}}"  name="old_password" required="required" class="required" id="old_password"/>
                                                    </div>
                                                @endif
                                                <div class="form-group">
                                                    <label>{{trans('application.auth_login_panel_login_password_new_label')}}</label>
                                                    <input type="password" placeholder="{{trans('application.auth_login_panel_login_password_new_placeholder')}}"  name="password" required="required" class="required" id="password"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>{{trans('application.auth_login_panel_login_password_confirm_label')}}</label>
                                                    <input type="password" placeholder="{{trans('application.auth_login_panel_login_password_confirm_placeholder')}}"  name="password_confirm" required="required" class="required" id="password_confirm"/>
                                                </div>
                                                <div class="spacer"></div>

                                                <div class="form-group">
                                                    <input class="btn btn-custom btn-expand" type="submit" value="{{trans('application.user_profile_panel_button_label')}}"/>
                                                </div>
                                            </fieldset>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </form>
                                </div>

                            </div>

                        </div><!-- /main -->

                        <!-- Aside -->
                        @include($template.'.views.user.partials.sidebar')



                    </div><!-- /inner-wrapper -->
                </section><!-- /section -->

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $('#passwordForm').validate(
                            {
                                submitHandler: function(form) {
                                    // do other things for a valid form
                                    form.submit();
                                  },
                                  rules: {
                                      old_password: {
                                          required: true,
                                          minlength: 6
                                      },
                                      password: {
                                          required: true,
                                          minlength: 6
                                      },
                                      password_confirm: {
                                          required: true,
                                          equalTo: "#password",
                                          minlength: 6
                                      }
                                  },
                                      highlight: function(element) {
                                          var valid = $(element).attr('aria-invalid');
                                          var id = $(element).attr('id');
                                          $('#for'+id+' i').removeClass('fa fa-times');
                                          $('#for'+id+' i').removeClass('fa fa-check');
                                          if(valid == 'false'){
                                            $('#for'+id+' i').addClass('fa fa-check').css('color','green').show();
                                          }else{
                                             $('#for'+id+' i').addClass('fa fa-times').css('color','red').show();
                                          }

                                      },
                                      unhighlight: function(element) {
                                            var valid = $(element).attr('aria-invalid');
                                            var id = $(element).attr('id');
                                            $('#for'+id+' i').removeClass('fa fa-times');
                                            $('#for'+id+' i').removeClass('fa fa-check');
                                            if(valid == 'false'){
                                                $('#for'+id+' i').addClass('fa fa-check').css('color','green').show();
                                            }else{
                                                $('#for'+id+' i').addClass('fa fa-times').css('color','red').show();
                                            }
                                      }
                            }
                        );



            $('#registerForm').validate(
                {
                    submitHandler: function(form) {
                        // do other things for a valid form
                        form.submit();
                      },
                      rules: {

                          password: {
                              required: true,
                              minlength: 6
                          },
                          firstname: {
                              required: true,
                              minlength: 3
                          },
                          lastname: {
                            required: true,
                            minlength: 3
                          },
                          password_confirm: {
                              required: true,
                              equalTo: "#regPassword",
                              minlength: 6
                          }
                      },
                          highlight: function(element) {
                              var valid = $(element).attr('aria-invalid');
                              var id = $(element).attr('id');
                              $('#for'+id+' i').removeClass('fa fa-times');
                              $('#for'+id+' i').removeClass('fa fa-check');
                              if(valid == 'false'){
                                $('#for'+id+' i').addClass('fa fa-check').css('color','green').show();
                              }else{
                                 $('#for'+id+' i').addClass('fa fa-times').css('color','red').show();
                              }

                          },
                          unhighlight: function(element) {
                                var valid = $(element).attr('aria-invalid');
                                var id = $(element).attr('id');
                                $('#for'+id+' i').removeClass('fa fa-times');
                                $('#for'+id+' i').removeClass('fa fa-check');
                                if(valid == 'false'){
                                    $('#for'+id+' i').addClass('fa fa-check').css('color','green').show();
                                }else{
                                    $('#for'+id+' i').addClass('fa fa-times').css('color','red').show();
                                }
                          }
                }
            );

            $('#regBirthdate').datepicker({
                format: 'dd/mm/yyyy'
            });

            function validateEmail(email) {
              var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
              if(!emailReg.test(email)) {
                 return false;
              }else{
                 return true;
              }
            }
            var crsf = '{{ csrf_token() }}';
            var running = 0;
            $('#regEmail').bind('keydown keyup',function(e){
                if(e.type == "keydown"){
                    return;
                }else{
                    if(running == 0){
                        var email =  $(this).val();
                        if(validateEmail(email)){
                            running = 1;
                             $('#forregEmail i').removeClass('fa fa-times');
                             $('#forregEmail i').removeClass('fa fa-check');
                             $('#forregEmail span').html('');
                            setTimeout( function(){
                                $('#regEmail').attr('disabled','disabled');
                                $.post('/el/endpoint/validate_email',{ email: email,_token:crsf },function(data){
                                }).complete(function(data){
                                    var response = data.responseText;

                                    if(response == 0){
                                         $('#forregEmail i').addClass('fa fa-check').css('color','green').show();
                                    }else if(response == 1){
                                         $('#forregEmail i').addClass('fa fa-times').css('color','red').show();
                                         $('#forreg span').html(' {{{trans("application.email_in_use_left")}}} '+email+' {{{trans("application.email_in_use_right")}}}');
                                         $('#regEmail').val('');
                                    }
                                    running = 0;
                                    $('#regEmail').removeAttr('disabled');
                                });
                          },1000);
                        }
                    }
                }

            });



            $('#loginForm').validate(
                {
                    submitHandler: function(form) {
                        // do other things for a valid form
                        form.submit();
                      },
                      rules: {

                          password: {
                              required: true,
                              minlength: 6
                          }
                      }
                }
            );

        });


    </script>
@endsection