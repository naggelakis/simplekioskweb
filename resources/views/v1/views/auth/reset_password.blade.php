@extends($template.'.layout.master')
@section('content')

    <!-- Above the fold -->
                <div id="above-the-fold" class="above-the-fold light">
                    <div class="inner-wrapper">

                        <h2 class="page-title">{{trans('application.auth_login_head_title')}}</h2>

                    </div><!-- /inner-wrapper -->
                </div><!-- /above-the-fold -->

                <!-- Section -->
                <section id="section">
                    <div class="inner-wrapper">

                        <!-- Main -->
                        <div id="main" role="main">



                        	<div class="row">
                                <div class="grid_6">

    		                        <form >

    		                            <fieldset>
    		                                <legend>{{trans('application.auth_reset_password_panel_left_title')}}</legend>
                                            <p>
                                                    {{trans('application.auth_reset_password_panel_left_placeholder')}}
                                            </p>
    		                            </fieldset>
    		                        </form>
                                </div>
                               <div class="grid_6">
                                    <form action="{{route('password_reset_post',[$id,$code])}}" id="resetForm" method="post" >

                                        <fieldset>
                                             @if($valid == false && !isset($updated))
                                                <div class="row">
                                                    <div class="grid_12">
                                                        <div class="alert  red">
                                                          <i class="fa fa-times"></i>  {{ trans('application.auth_reset_password_panel_right_invalid_code')}}
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif(isset($updated))
                                                <div class="row">
                                                    <div class="grid_12">
                                                        <div class="alert  green">
                                                          <i class="fa fa-check"></i>  {{ trans('application.auth_reset_password_panel_right_password_updated')}}

                                                        </div>
                                                        <br>
                                                        <a href="{{route('login')}}">{{ trans('application.auth_reset_password_panel_right_password_updated_link')}}</a>
                                                    </div>
                                                </div>
                                            @else
                                                <legend>{{trans('application.auth_reset_password_panel_right_title')}}</legend>
                                                @if(isset($form_errors['login']) && isset($form_errors['login']['message']))
                                                       <div class="row">
                                                           <div class="grid_12">
                                                               <div class="alert  red">{{ trans('application.'.$form_errors['login']['message'])}}</div>
                                                           </div>
                                                       </div>
                                                   @endif
                                                <div class="form-group">
                                                    <label>{{trans('application.auth_reset_password_panel_right_password_label')}}</label>
                                                    <input type="password" placeholder="{{trans('application.auth_reset_password_panel_right_password_placeholder')}}"  name="password" required="required" class="required" id="resPassword"/>
                                                </div>
                                                <div class="form-group">
                                                    <label>{{trans('application.auth_reset_password_panel_right_confirm_password_label')}}</label>
                                                    <input type="password" placeholder="{{trans('application.auth_reset_password_panel_right_confirm_password_placeholder')}}"  name="confirm_password" required="required" class="required" />
                                                </div>

                                                <div class="form-group">
                                                    <input class="btn btn-custom btn-expand" type="submit" value="{{trans('application.auth_reset_password_panel_right_submit_label')}}"/>
                                                </div>
                                            @endif
                                            <div class="spacer"></div>
                                        </fieldset>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                   </div>
                        	</div>

                        </div><!-- /main -->

                    </div><!-- /inner-wrapper -->
                </section><!-- /section -->

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){

            $('#resetForm').validate({
                rules: {
                      password: {
                          required: true,
                          minlength: 6
                      },
                      confirm_password: {
                          required: true,
                          equalTo: "#resPassword",
                          minlength: 6
                      }
                  }
            });

        });
    </script>
@endsection