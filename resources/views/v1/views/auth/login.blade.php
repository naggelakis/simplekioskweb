@extends($template.'.layout.master')
@section('content')

    <!-- Above the fold -->
                <div id="above-the-fold" class="above-the-fold light">
                    <div class="inner-wrapper">

                        <h2 class="page-title">{{trans('application.auth_login_head_title')}}</h2>

                    </div><!-- /inner-wrapper -->
                </div><!-- /above-the-fold -->

                <!-- Section -->
                <section id="section">
                    <div class="inner-wrapper">

                        <!-- Main -->
                        <div id="main" role="main">



                        	<div class="row">
                                <div class="grid_6">
                                    @if(isset($socialApps) && count($socialApps)>0 && (isset($socialApps->facebook) || isset($socialApps->google) || isset($socialApps->twitter) ))
                                        <form>
                                            <fieldset>
                                                <legend>{{trans('application.auth_login_social_media_title')}} <span class="alert green txt-small">{{trans('application.auth_login_social_media_title_helper_message')}}</span></legend>
                                                <p>{{trans('application.auth_login_social_media_placeholder')}}</p>
                                                @if(isset($socialApps->facebook))
                                                    <div class="form-group">
                                                            <button class="btn-large btn-blue btn-expand" style="background-color: #3b5998;">
                                                                <a href="{{ route('social_login',['facebook']) }}@if(isset($redirect))?redirect={{$redirect}} @endif" style="color: #ffffff;">
                                                                    <i class="fa fa-facebook-square"></i> {{trans('application.auth_login_social_facebook_button_title')}}
                                                                </a>
                                                            </button>
                                                    </div>
                                                @endif
                                                @if(isset($socialApps->twitter))
                                                    <div class="form-group">
                                                     <button class="btn-large btn-blue btn-expand" style="background-color: #00aced;">
                                                         <a href="{{ route('social_login',['twitter']) }}@if(isset($redirect))?redirect={{$redirect}} @endif"  style="color: #ffffff;">
                                                            <i class="fa fa-twitter-square"></i> {{trans('application.auth_login_social_twitter_button_title')}}
                                                         </a>
                                                     </button>
                                                    </div>
                                                @endif
                                                @if(isset($socialApps->google))
                                                    <div class="form-group">
                                                     <button class="btn-large btn-blue btn-expand" style="background-color: #dd4b39;">
                                                         <a href="{{ route('social_login',['google']) }}@if(isset($redirect))?redirect={{$redirect}} @endif" style="color: #ffffff;">
                                                            <i class="fa fa-google-plus-square"></i> {{trans('application.auth_login_social_google_button_title')}}
                                                         </a>
                                                     </button>
                                                    </div>
                                                @endif
                                            </fieldset>
                                        </form>
                                        <div class="spacer"></div>
                                    @endif
                                    <form action="{{route('forgot_password')}}" id="forgotForm" method="post" style="display:none;">

                                        <fieldset>
                                            <legend>{{trans('application.auth_login_panel_forgot_title')}}</legend>

                                                <div class="row">
                                                    <div class="grid_12">
                                                        <div class="alert grey forgotStatus" id="forgotLoading" style="display: none;">
                                                            <i class="fa fa-spinner fa-spin"></i> {{trans('application.auth_login_panel_forgot_alert_loading')}}
                                                        </div>
                                                        <div class="alert red forgotStatus" id="forgotEmail" style="display: none;">
                                                            <i class="fa fa-times"></i> {{trans('application.auth_login_panel_forgot_alert_email_error')}}
                                                        </div>
                                                        <div class="alert red forgotStatus" id="forgotError" style="display: none;">
                                                            <i class="fa fa-times"></i> {{trans('application.auth_login_panel_forgot_alert_error')}}
                                                        </div>
                                                        <div class="alert green forgotStatus" id="forgotSuccess" style="display: none;">
                                                            <i class="fa fa-check-circle-o"></i> {{trans('application.auth_login_panel_forgot_alert_success')}}
                                                        </div>
                                                    </div>
                                                </div>

                                            <div class="form-group">
                                                <label>{{trans('application.auth_login_panel_forgot_email_label')}}</label>
                                                <input type="email" placeholder="{{trans('application.auth_login_panel_forgot_email_placeholder')}}" name="email" required="required" class="required" id="femail" />
                                            </div>
                                            <div class="form-group">
                                                <input class="btn btn-custom btn-expand" type="submit" value="{{trans('application.auth_login_panel_forgot_submit_label')}}"/>
                                            </div>
                                            <div class="spacer"></div>
                                            <p><a href="javascript:void(0);" id="showLogin">{{trans('application.auth_forgot_panel_login_sign_password_submit')}}</a></p>
                                        </fieldset>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="ftoken">
                                    </form>

    		                        <form action="{{route('login')}}@if(isset($redirect))?redirect={{$redirect}} @endif" id="loginForm" method="post" >

    		                            <fieldset>
    		                                <legend>{{trans('application.auth_login_panel_login_title')}}</legend>
    		                                @if(isset($form_errors['login']) && isset($form_errors['login']['message']))
                                                <div class="row">
                                                    <div class="grid_12">
                                                        <div class="alert  red">{{ trans('application.'.$form_errors['login']['message'])}}</div>
                                                    </div>
                                                </div>
                                            @endif
    		                                <div class="form-group">
    		                                    <label>{{trans('application.auth_login_panel_login_email_label')}}</label>
    		                                    <input type="email" placeholder="{{trans('application.auth_login_panel_login_email_placeholder')}}" name="email" required="required" class="required" @if(isset($form_errors['login']) && isset($form_errors['login']['email'])) value="{{$form_errors['login']['email']}}" @endif />
    		                                </div>
    		                                <div class="form-group">
    		                                    <label>{{trans('application.auth_login_panel_login_password_label')}}</label>
    		                                    <input type="password" placeholder="{{trans('application.auth_login_panel_login_password_placeholder')}}"  name="password" required="required" class="required"/>
    		                                </div>
    		                                <div class="form-group">
    		                                    <label><input type="checkbox" name="keep"/> {{trans('application.auth_login_panel_login_remember_me_label')}}</label>
    		                                </div>
    		                                <div class="form-group">
    		                                	<input class="btn btn-custom btn-expand" type="submit" value="{{trans('application.auth_login_panel_login_submit_label')}}"/>
    		                                </div>
    		                                <div class="spacer"></div>
    		                                <p><a href="javascript:void(0);" id="showForgot">{{trans('application.auth_login_panel_login_forgot_password_label')}}</a></p>
    		                            </fieldset>
    		                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
    		                        </form>
                                </div>
                                <div class="grid_6">
    		                        <form action="{{route('register')}}@if(isset($redirect))?redirect={{$redirect}} @endif" id="registerForm" method="post" >

    		                            <fieldset>
    		                                <legend>{{trans('application.auth_login_panel_register_title')}}</legend>
    		                                @if(isset($form_errors['register']) && isset($form_errors['register']['message']))
                                                <div class="row">
                                                    <div class="grid_12">
                                                        <div class="alert  red">{{ trans('application.'.$form_errors['register']['message'])}}</div>
                                                    </div>
                                                </div>
                                            @endif
    		                                <div class="form-group">
    		                                    <label id="forregEmail" for="regEmail">{{trans('application.auth_login_panel_register_email_label')}} <i class="fa " style="display:none;"></i></label>
    		                                    <input type="email" placeholder="{{trans('application.auth_login_panel_register_email_placeholder')}}" name="email" required="required" class="required" id="regEmail" @if(isset($form_errors['register']) && isset($form_errors['register']['data']['email'])) value="{{$form_errors['register']['data']['email']}}" @endif />

    		                                </div>
    		                                <div class="form-group">
    		                                    <label id="forregPassword" for="regPassword">{{trans('application.auth_login_panel_register_password_label')}} <i class="fa " style="display:none;"></i></label>
    		                                    <input type="password" placeholder="{{trans('application.auth_login_panel_register_password_placeholder')}}" name="password" required="required" class="required" id="regPassword"/>
    		                                </div>
    		                                <div class="form-group">
    		                                    <label id="forregPasswordConfirm" for="regPasswordConfirm">{{trans('application.auth_login_panel_register_password_confirm_label')}} <i class="fa " style="display:none;"></i></label>
    		                                    <input type="password" placeholder="{{trans('application.auth_login_panel_register_password_confirm_placeholder')}}" name="password_confirm" required="required" class="required" id="regPasswordConfirm"/>
    		                                </div>
    		                                <div class="spacer"></div>
    		                                <div class="form-group">
    		                                    <label id="forregFirstname" for="regFirstname">{{trans('application.auth_login_panel_register_firstname_label')}} <i class="fa " style="display:none;"></i></label>
    		                                    <input type="text" placeholder="{{trans('application.auth_login_panel_register_firstname_placeholder')}}" name="firstname" required="required" class="required" id="regFirstname" @if(isset($form_errors['register']) && isset($form_errors['register']['data']['firstname'])) value="{{$form_errors['register']['data']['firstname']}}" @endif/>
    		                                </div>
    		                                <div class="form-group">
    		                                    <label id="forregLastname" for="regLastname">{{trans('application.auth_login_panel_register_lastname_label')}} <i class="fa " style="display:none;"></i></label>
    		                                    <input type="text" placeholder="{{trans('application.auth_login_panel_register_lastname_placeholder')}}" name="lastname" required="required" class="required" id="regLastname" @if(isset($form_errors['register']) && isset($form_errors['register']['data']['lastname'])) value="{{$form_errors['register']['data']['lastname']}}" @endif/>
    		                                </div>
    		                                <div class="spacer"></div>
    		                                <div class="form-group">
    		                                    <label id="forregBirthdate" for="regBirthdate">{{trans('application.auth_login_panel_register_birthdate_label')}} <i class="fa " style="display:none;"></i></label>
    		                                    <input type="text" name="birthdate"   id="regBirthdate" placeholder="(DD)-(MM)-(YYYY)" @if(isset($form_errors['register']) && isset($form_errors['register']['data']['birthdate'])) value="{{$form_errors['register']['data']['birthdate']}}" @endif/>
    		                                </div>
    		                                <div class="form-group">
    		                                    <label id="forregGender" for="regGender">{{trans('application.auth_login_panel_register_gender_label')}} <i class="fa " style="display:none;"></i></label>
    		                                    @if(isset($form_errors['register']) && isset($form_errors['register']['data']['gender']))
    		                                        <?php $gender = $form_errors['register']['data']['gender']; ?>
    		                                    @else
    		                                        <?php $gender = 244; ?>
    		                                    @endif
    		                                    <label><input type="radio" name="gender" value="244" name="gender"  id="regGender" @if($gender == 244) checked="checked" @endif /> {{trans('application.auth_login_panel_register_gender_value_man')}}</label>
    		                                    <label><input type="radio" name="gender" value="245" name="gender"   id="regGender" @if($gender == 245) checked="checked" @endif /> {{trans('application.auth_login_panel_register_gender_value_woman')}}</label>
    		                                </div>
    		                                <div class="spacer"></div>
    		                                <div class="form-group">
    		                                    <label><input type="checkbox" name="terms" name="terms" required="required" class="required" id="regTerms" checked="checked"/> <a href="{{ route('terms') }}">{{trans('application.auth_login_panel_register_terms_label')}}</a></label>
    		                                </div>
    		                                <div class="form-group">
    		                                	<input class="btn btn-custom btn-expand" type="submit" value="{{trans('application.auth_login_panel_register_submit_button_label')}}"/>
    		                                </div>
    		                            </fieldset>
    		                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
    		                        </form>
                                </div>
                        	</div>

                        </div><!-- /main -->

                    </div><!-- /inner-wrapper -->
                </section><!-- /section -->

@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#showForgot').on('click',function(){
                $('#loginForm').hide();
                $('#forgotForm').show();
            });

            $('#showLogin').on('click',function(){
                $('#loginForm').show();
                $('#forgotForm').hide();
            });

            $('#forgotForm').validate();
            $('#forgotForm').on('submit',function(e){
                $('.forgotStatus').hide();

                if($(this).valid()){
                    e.stopPropagation();
                    var email = $('#femail').val();
                    var token = $('#ftoken').val();
                    var route = "{{route('forgot_password')}}";
                    $('#forgotLoading').show();

                    $.ajax({
                      type: "POST",
                      url: route,
                      data: { email: email, _token: token}
                    }).done(function(data){
                        var response = JSON.parse(data);
                        if(response.response){
                            $('.forgotStatus').hide();
                            if(response.response == 0){
                                $('#forgotError').show();
                            }else if(response.response == -1){
                                $('#forgotEmail').show();
                            }else if(response.response == 1){
                                 $('#forgotSuccess').show();
                                 $('#femail').val('');
                             }
                        }
                    });



                    return false;

                }
            });

            $('#registerForm').validate(
                {
                    submitHandler: function(form) {
                        // do other things for a valid form
                        form.submit();
                      },
                      rules: {

                          password: {
                              required: true,
                              minlength: 6
                          },
                          firstname: {
                              required: true,
                              minlength: 3
                          },
                          lastname: {
                            required: true,
                            minlength: 3
                          },
                          password_confirm: {
                              required: true,
                              equalTo: "#regPassword",
                              minlength: 6
                          }
                      },
                          highlight: function(element) {
                              var valid = $(element).attr('aria-invalid');
                              var id = $(element).attr('id');
                              $('#for'+id+' i').removeClass('fa fa-times');
                              $('#for'+id+' i').removeClass('fa fa-check');
                              if(valid == 'false'){
                                $('#for'+id+' i').addClass('fa fa-check').css('color','green').show();
                              }else{
                                 $('#for'+id+' i').addClass('fa fa-times').css('color','red').show();
                              }

                          },
                          unhighlight: function(element) {
                                var valid = $(element).attr('aria-invalid');
                                var id = $(element).attr('id');
                                $('#for'+id+' i').removeClass('fa fa-times');
                                $('#for'+id+' i').removeClass('fa fa-check');
                                if(valid == 'false'){
                                    $('#for'+id+' i').addClass('fa fa-check').css('color','green').show();
                                }else{
                                    $('#for'+id+' i').addClass('fa fa-times').css('color','red').show();
                                }
                          }
                }
            );

            $('#regBirthdate').datepicker({
                format: 'mm/dd/yyyy'
            })

            $('#loginForm').validate(
                            {
                                submitHandler: function(form) {
                                    // do other things for a valid form
                                    form.submit();
                                  },
                                  rules: {

                                      password: {
                                          required: true,
                                          minlength: 6
                                      }
                                  }
                            }
                        );

        });


    </script>
@endsection