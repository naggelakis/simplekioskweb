@extends($template.'.layout.master')
@section('content')
            @if(isset($basic_app->location_address) && isset($basic_app->location_address->lat) && isset($basic_app->location_address->lon) &&  $basic_app->location_address->lat!=null && $basic_app->location_address->lon != null)
                <style>
                      #map-canvas { height: 600px }
                </style>
                <!-- Canvas Google map -->
                <div class="above-the-fold light featured map" style="margin-top: -2.5em;padding-top: 0px;">
                    <div id="map-canvas"></div>
                </div>
            @endif
            <!-- Page title -->
            <div class="above-the-fold">
                <div class="inner-wrapper">
                    <h2 class="page-title">{{trans('application.contact_form_title')}}</h2>
                </div>
            </div>

            <!-- Section -->
            <section id="section">
                <div class="inner-wrapper">

                    <!-- Main -->
                    <div id="main" class="left col-md-8" role="main">

                        <!-- Form validation -->
                        <div class="alert green " id="form-success" style="display:none;"><i class="fa fa-check"></i> {{trans('application.contact_form_message_success')}}</div>
                        <div class="alert red" id="form-error" style="display:none;"><i class="fa fa-times"></i> {{trans('application.contact_form_message_error')}}</div>

                        <div id="form-container">
                            <h4>{{trans('application.contact_form_subtitle')}}</h4>
                            <p>{{trans('application.contact_form_placeholder')}}</p>

                            <form action="{{route('contact_send')}}" id="contact-form" method="post">
                                <div class="form-group">
                                    <label for="name">{{trans('application.contact_form_name_title')}}</label>
                                    <input class="inpi" type="text" name="name" placeholder="{{trans('application.contact_form_name_placeholder')}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="email">{{trans('application.contact_form_email_title')}}</label>
                                    <input  class="inpi" type="email" name="email" placeholder="{{trans('application.contact_form_email_placeholder')}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="message">{{trans('application.contact_form_message_title')}}</label>
                                    <textarea  class="inpi" name="message" placeholder="{{trans('application.contact_form_message_placeholder')}}" required></textarea>
                                </div>
                                <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                <div class="form-group">
                                    <button  id="form_submit" type="submit" class="btn btn-blue">
                                        <span class="idle">{{trans('application.contact_form_submit_label')}}</span>
                                        <span class="sending" style="display: none;"><i class="fa fa-spin fa-spinner"></i> {{trans('application.contact_form_submit_sending_label')}}</span>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- Aside -->
                    <aside class="col-md-4">

                        <!-- Text widget -->
                        <div class="widget">
                            <ul class="recent-comments">

                              @if(count($addresses)>0 || isset($appPhone) || isset($appEmail))
                                  <li>
                                      <p class="author" style="margin-bottom: 30px;">{{Helpers::remove_tones(trans('application.footer_contact_details_title'))}}</p>


                                           @if(isset($addresses) && count($addresses)>0)
                                               @foreach($addresses as $address)
                                                  @if($address->id == $basic_app->location_address->item_id)
                                                      <div class="row" style="margin-bottom:20px;">
                                                            <h3>{{$basic_app->location_address->title}}</h3>
                                                            <span>{{$address->line1.' '.$address->line2}}, {{$address->city.' '.$address->zip.', '.$address->country}}</span><br>
                                                          @if(isset($appPhone) && $appPhone!='')
                                                             T.  <a href="tel:{{$appPhone}};">{{$appPhone}}</a><br>
                                                          @endif
                                                          @if(isset($appEmail) && $appEmail!='')
                                                             E. <a href="mail:{{$appEmail}}"> {{$appEmail}}</a><br>
                                                          @endif
                                                      </div>
                                                  @endif
                                               @endforeach
                                           @endif

                                           @if(isset($basic_app->sublocations) && count($basic_app->sublocations)>0)
                                              @foreach($basic_app->sublocations as $address)
                                                 <div class="row" style="margin-bottom:20px;">
                                                       <h3>{{$address->location->title}}</h3>
                                                       <span>{{$address->location->line1.' '.$address->location->line2}}, {{$address->location->city_full.' '.$address->location->zip.', '.$address->location->country_full}}</span><br>
                                                     @if(isset($address->emails) && is_array($address->emails) && count($address->emails)>0)
                                                        @foreach($address->emails as $email)
                                                              E. <a href="mail:{{$email->link}}"> {{$email->link}}</a><br>
                                                        @endforeach
                                                     @endif
                                                      @if(isset($address->phones) && is_array($address->phones) && count($address->phones)>0)
                                                           @foreach($address->phones as $phone)
                                                               <?php $phone->number = $phone->country_code == 95 ? '+31'.$phone->number: $phone->number; ?>
                                                               T.  <a href="tel:{{$phone->number}}">{{$phone->number}}</a><br>
                                                           @endforeach
                                                     @endif
                                                 </div>
                                              @endforeach
                                          @endif

                                  </li>
                                  @if(isset($socialMedia) && count($socialMedia))
                                     <li>
                                         <p class="author">{{Helpers::remove_tones(trans('application.footer_social_profiles_title'))}}</p>
                                         <h3>
                                              @foreach($socialMedia as $social)
                                                  @if($social->social_id==1)
                                                      <?php $icon = 'fa-facebook-square'; ?>
                                                  @elseif($social->social_id==2)
                                                      <?php $icon = 'fa-twitter-square'; ?>
                                                  @elseif($social->social_id==3)
                                                      <?php $icon = 'fa-google-plus-square'; ?>
                                                  @elseif($social->social_id==4)
                                                      <?php $icon = 'fa-pinterest-square'; ?>
                                                  @elseif($social->social_id==5)
                                                      <?php $icon = 'fa-instagram'; ?>
                                                  @elseif($social->social_id==6)
                                                      <?php $icon = 'fa-foursquare'; ?>
                                                  @elseif($social->social_id==7)
                                                      <?php $icon = 'fa-linkedin'; ?>
                                                  @endif
                                                  <a style="margin-right:10px;" title="Follow" target="_blank" data-title="Follow" href="<?php echo $social->follow_link;?>">
                                                      <i class="fa {{$icon}} fa-2x"></i>
                                                  </a>
                                              @endforeach
                                          </h3>
                                     </li>
                                     @endif
                              @endif
                            </ul>
                        </div>


                    </aside>
                </div>
            </section>
@endsection

    @section('scripts')
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#contact-form').validate();

                        $('#contact-form').on('submit',function(event){
                            if($('#contact-form').valid()){
                                event.stopPropagation();
                                var endpoint = $('#contact-form').attr('action');
                                var data = $('#contact-form').serialize();

                                $('#contact-form .inpi').attr('disabled','disabled');
                                $('#form_submit .idle').hide();
                                $('#form_submit .sending').show();
                                $('.alert').hide();
                                $.post(endpoint,data,function(response){

                                }).complete(function(response){

                                    $('#contact-form .inpi').removeAttr('disabled');
                                    $('#form_submit .sending').hide();
                                    $('#form_submit .idle').show();

                                    if(response.responseText == 1 || response.responseText == '1'){
                                        $('#form-container').hide();
                                        $('#form-success').show();
                                        $("#contact-form").trigger('reset');
                                    }else{
                                        $('#form-error').show();
                                    }
                                });

                                return false;

                            }
                        });

                    });
                </script>

                @if(isset($basic_app->location_address) && isset($basic_app->location_address->lat) && isset($basic_app->location_address->lon) &&  $basic_app->location_address->lat!=null && $basic_app->location_address->lon != null)
                <script src="https://maps.googleapis.com/maps/api/js"></script>
                <script>
                        var lat= {{$basic_app->location_address->lat}};
                        var lon= {{$basic_app->location_address->lon}};
                        var label = '{{$basic_app->location_address->title}}';

                        var locations = [
                                    [label, lat, lon, 4]
                                ];
                        @if(isset($addresses) && is_array($addresses) && count($addresses)>1)
                            @foreach($addresses as $address)
                                @if($basic_app->location_address->item_id != $address->id && isset($address->lat) && isset($address->lon) &&  $address->lat!=null && $address->lon != null)
                                    var address_lat =  {{ $address->lat}};
                                    var address_lon =  {{ $address->lon}};
                                    var address_label =  '{{ $address->line1}}';
                                    var array = [address_label,address_lat,address_lon,4];
                                    locations.push(array);
                                @endif
                            @endforeach
                        @endif

                          $(document).ready(function() {
                              function initialize() {
                              var styles = [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}];
                              var mapCenter = new google.maps.LatLng(lat, lon);
                              var mapOptions = {
                                  zoom: 13,
                                  center: mapCenter,
                                  disableDefaultUI: false,
                                  scrollwheel: false,
                                  styles: styles
                              };
                              var mapElement = document.getElementById('map-canvas');
                              var map = new google.maps.Map(mapElement, mapOptions);


                              var infowindow = new google.maps.InfoWindow();

                              var marker, i;
                              var markers = [];
                              var bounds = new google.maps.LatLngBounds();

                              for (i = 0; i < locations.length; i++) {
                                  marker = new google.maps.Marker({
                                      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                      map: map,
                                      animation: google.maps.Animation.DROP,
                                      icon: '/assets/map/office-building.png'
                                  });

                                  google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                      return function () {
                                          infowindow.setContent(locations[i][0]);
                                          infowindow.open(map, marker);
                                          toggleBounce(marker);
                                      }
                                  })(marker, i));

                                  function toggleBounce(marker) {
                                      if (marker.getAnimation() !== null) {
                                        marker.setAnimation(null);
                                      } else {
                                        marker.setAnimation(google.maps.Animation.BOUNCE);
                                      }
                                    }


                                  if(locations.length>1) {
                                      bounds.extend(new google.maps.LatLng(locations[i][1], locations[i][2]));
                                  }
                                  markers.push(marker);
                              }
                                  if(locations.length>1) {
                                      map.fitBounds(bounds);
                                  }

                              }


                              google.maps.event.addDomListener(window, 'load', initialize);

                         });

                </script>
                @endif
    @endsection
