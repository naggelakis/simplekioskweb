@extends($template.'.layout.master')
@section('content')
            <div ng-app="mainApp" ng-controller="HomepageCtrl"  ng-init="lang='{{strtolower($lang)}}';fetch_slider();fetch_featured();fetch_showcase_categories();">
                @include($template.'.views.main.partials.homepage_featured')
                @include($template.'.views.main.partials.homepage_showcase_categories')



                <div  ng-controller="HomepageCtrl" ng-init="fetch_authors()" class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif" ng-show="authors.length > 0">
                         <div class="inner-wrapper">

                           <!-- Block layout one / Showcase Author - At least 3 or 6 or 9 max 12 -->
                              <div class="block-layout-one"  ng-show="authors.length > 0 " >
                                  <!--<p class="title text-center"><span>{{trans('application.homepage_authors_title')}}</span></p>-->
                                  <div class="row"  ng-repeat="group in authors | chunk:3:this">
                                      <div class="item grid_4" ng-repeat="author in group">
                                          <a ng-href="@{{ author.render_url }}"><img ng-src="@{{ author.thumb }}" alt="@{{ author.render_name }}" width="50"/></a>
                                          <div>
                                              <span><a ng-href="@{{ author.render_url }}">@{{ author.render_name }}</a></span>
                                              <h3 ng-show="author.entries && author.entries.length>0">
                                                    <a ng-href="@{{ author.entry.url }}">@{{ author.entry.title }}</a>
                                              </h3>
                                              <p class="date" ng-show="author.entries && author.entries.length>0">@{{ author.entry.render_date }}</p>
                                          </div>
                                      </div >
                                  </div>
                              </div>
                         </div>
                </div>

                 @include($template.'.views.main.partials.homepage_publications')



                <!-- Section -->
                <section id="section" ng-controller="HomepageCtrl" ng-init="latest_posts_fetch()" ng-show="latest_posts.length > 0">
                           <div class="inner-wrapper">
                            <!-- Main -->
                            <div id="main" class="left" role="main">
                                @include($template.'.views.main.partials.homepage_latest_posts')
                            </div><!-- /main -->

                               <!-- Aside -->
                               @include($template.'.partials.sidebar',['disable'=>true,'hide_month_log'=>true])


                           </div><!-- /inner-wrapper -->
                 </section><!-- /section -->
            </div>
@endsection

@section('scripts')
    @javascript({{ asset('assets/'.$template.'/angular/main.js') }})
    @javascript({{ asset('assets/'.$template.'/angular/sidebar_controller.js') }})

    <script type="text/javascript">
        $(document).ready(function(){
            //console.log(DISQUSWIDGETS);
        });
    </script>
@endsection