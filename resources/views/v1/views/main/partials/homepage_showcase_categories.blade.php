<div  class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif" ng-show="showcase_categories.length>0 " style="background-color:#fbfbfb;">
       <div class="inner-wrapper" >
            <div class="block-layout-three row"   ng-repeat="category in showcase_categories" ng-show="showcase_categories.length>0 && category.entries.length>1">
               <p class="title"><a ng-href="#"><span>@{{ category.render_title }}</span></a></p>
               <div class="grid_6" ng-show="category.entries.length>0">
                <div class="main-item" style="margin-left: 10px;">
                    <div class="post-img" ng-show="category.entries[0].thumb">
                        <a ng-href="@{{ category.entries[0].render_url }}">
                            <img ng-src="@{{ category.entries[0].thumb }}" alt="@{{ category.entries[0].title }}"/>
                        </a>
                    </div>
                    <h3><a ng-href="@{{ category.entries[0].render_url }}">@{{ category.entries[0].title }} &nbsp;<i class="fa fa-lock" ng-show="category_entries[0].render_lock"></i></a></h3>
                    <div class="post-dca">
                        <span class="date">@{{ category.entries[0].render_date }}</span>
                        @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
                           <span class="comments">
                                <a href="@{{ category.entries[0].render_url }}"><span class="disqus-comment-count" data-disqus-url="@{{ category.entries[0].render_url }}"> 0 </span> {{--trans('application.homepage_article_comments_title')--}}</a>
                           </span>
                        @endif
                        <span class="author" ng-show="category.entries[0].render_author_name"><a ng-href="@{{ category.entries[0].render_author_url }}">@{{category.entries[0].render_author_name}}</a></span>
                        @if(is_object($authUser))
                            <span style="font-size:0.8em;">
                                <a href="javascript:void(0);"  ng-click="favorite_post(category.entries[0].id)">
                                    <i class="fa fa-heart heart aheart"  ng-show="favorite_posts.indexOf(category.entries[0].id) > -1"></i>
                                    <i class="fa fa-heart aheart" ng-show="favorite_posts.indexOf(category.entries[0].id) < 0"></i>
                                </a>
                            </span>
                        @endif
                    </div>
                    <p ng-show="featured_post.subtitle && featured_post.subtitle!=null">@{{ featured_post.subtitle }}</p>
                </div>
               </div><!-- /grid_6 -->
               <div class="grid_6" ng-show="category.entries.length>1">
                    <div class="small-items">
                        <div class="item" ng-repeat="post in category.entries" ng-show="$index>0">
                                <a ng-href="@{{ post.render_url }}" ng-show="post.thumb">
                                    <img ng-src="@{{ post.thumb_square }}" alt="@{{ post.title }}" width="50"/>
                                </a>
                                <div>
                                    <h3>
                                        <a ng-href="@{{ post.render_url }}">
                                            @{{ post.title }}
                                            &nbsp;<i class="fa fa-lock" ng-show="post.render_lock"></i>
                                        </a>
                                    </h3>
                                    <p class="date">@{{ post.render_date }}

                                     @if(is_object($authUser))
                                        &nbsp; &nbsp;
                                        <span style="font-size:0.8em;">
                                            <a href="javascript:void(0);"  ng-click="favorite_post(post.id)">
                                                <i class="fa fa-heart heart aheart"  ng-show="favorite_posts.indexOf(post.id) > -1"></i>
                                                <i class="fa fa-heart aheart" ng-show="favorite_posts.indexOf(post.id) < 0"></i>
                                            </a>
                                        </span>
                                    @endif
                                    </p>

                                </div>
                        </div>
                    </div>
               </div><!-- /grid_6 -->
           </div>
       </div>
       <div class="clearfix"></div>
 </div>