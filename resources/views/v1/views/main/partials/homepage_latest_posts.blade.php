@if(isset($banners['list_top']) && count($banners['list_top'])>0)
  <div class="banner-space">
     <?php $list_top = $banners['list_top'][0]; ?>
     @if(isset($list_top->file_cdn))
          <a href="{{$list_top->target_url}}" target="{{$list_top->target_url_window}}">
              <img src="{{$list_top->file_cdn}}" alt="{{$list_top->description}}"/>
          </a>
     @else
          {{{$list_top->content }}}
     @endif
 </div>
@endif



<!-- Block layout five / Show articles 15 at a time by date and load 15 each time -->
<div class="block-layout-six"  ng-show="latest_posts.length>0">

   <div  ng-repeat="group in latest_posts | chunk:10:this">
       <div  ng-repeat="post in group" class="main-item @{{ post.render_no_image }}">
           <div class="post-img" ng-show="post.thumb">
               <a ng-href="@{{ post.render_url }}"><img ng-src="@{{ post.thumb_square }}" alt="@{{post.title}}"/></a>
               <span ng-show="post.render_category"><a ng-href="@{{ post.render_category_url }}">@{{post.render_category}}</a></span>
           </div>
           <div class="post-meta">
               <h3><a ng-href="@{{ post.render_url }}">@{{post.title}}</a></h3>
               <div class="post-dca">
                   <span class="date">@{{ post.render_date }}</span>
                   @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
                       <span class="comments">
                            <a href="@{{ post.render_url }}"><span class="disqus-comment-count" data-disqus-url="@{{ post.render_url }}"> 0 </span> {{--trans('application.homepage_article_comments_title')--}}</a>
                       </span>
                   @endif
                   <span class="author" ng-show="post.render_author_name"><a ng-href="@{{ post.render_author_url }}">@{{post.render_author_name}}</a></span>
                   @if(is_object($authUser))
                       <span style="font-size:0.8em;">
                           <a href="javascript:void(0);"  ng-click="favorite_post(post.id)">
                               <i class="fa fa-heart heart aheart"  ng-show="favorite_posts.indexOf(post.id) > -1"></i>
                               <i class="fa fa-heart aheart" ng-show="favorite_posts.indexOf(post.id) < 0"></i>
                           </a>
                       </span>
                   @endif
               </div>
               <p class="hide_on_761_1080" ng-show="post.render_subtitle">@{{ post.render_subtitle }}</p>
           </div>

       </div >
       @if(isset($banners['list_mid']) && count($banners['list_mid'])>0)
         <div class="banner-space" ng-show="group.length==10 && $index<(latest_posts.length/10)-1">
            <?php $list_mid = $banners['list_mid'][0]; ?>
            @if(isset($list_mid->file_cdn))
                 <a href="{{$list_mid->target_url}}" target="{{$list_mid->target_url_window}}">
                     <img src="{{$list_mid->file_cdn}}" alt="{{$list_mid->description}}"/>
                 </a>
            @else
                 {{{$list_mid->content }}}
            @endif
         </div>
       @endif
   </div>


   <br>
   <a ng-click="latest_posts_fetch()" class="btn btn-expand btn-large btn-custom btn-radius" ng-show="show_load_more==1">
        <i class="fa fa-spinner fa-spin" ng-show="spinner==1"></i>
        &nbsp;
        <span ng-show="spinner==0">{{trans('application.homepage_recent_articles_load_more')}}</span>
        <span ng-show="spinner==1">{{trans('application.homepage_recent_articles_loading')}}</span>
   </a>
</div>



@if(isset($banners['list_bottom']) && count($banners['list_bottom'])>0)
   <div class="banner-space">
      <?php $list_bottom = $banners['list_bottom'][0]; ?>
      @if(isset($list_bottom->file_cdn))
           <a href="{{$list_bottom->target_url}}" target="{{$list_bottom->target_url_window}}">
               <img src="{{$list_bottom->file_cdn}}" alt="{{$list_bottom->description}}"/>
           </a>
      @else
           {{{$list_bottom->content }}}
      @endif
  </div>
@endif