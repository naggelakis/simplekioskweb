<!-- Above the fold / Show the class dark or light OPPOSITE to the app settings -->
                <div id="above-the-fold" class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif" ng-show="slider_posts.length>0 || featured_posts.length > 0 || showcase_categories.length>0">
                           <div class="inner-wrapper" >

                                <div class="home" ng-show="slider_posts.length>0">
                                    <div  ng-class="{ 'gallery-single gallery-only-one':slider_posts.length == 1, 'gallery-single gallery-no-thumbs':slider_posts.length >= 2 && slider_posts.length <= 6,'gallery-single': slider_posts.length > 6 }">
                                        <ul class="slides">

                                            <li  ng-repeat="slider_post in slider_posts" ng-show="slider_post.thumb" data-thumb="@{{ slider_post.thumb_square }}">
                                                <img ng-src="@{{ slider_post.thumb }}" alt="@{{ slider_post.title }}" />
                                                <div class="post-box-text">
                                                    <span ng-show="slide_post.render_category_url"><a ng-href="@{{ slider_post.render_category_url }}">@{{ slider_post.render_category }}</a></span>
                                                    <h3><a ng-href="@{{ slider_post.render_url }}">@{{ slider_post.title }}</a>&nbsp;<i class="fa fa-lock"  ng-show="slider_post.render_lock"></i></h3>
                                                    <h4 ng-show="slider_post.subtitle">@{{ slider_post.subtitle }}</h4>
                                                    <p>@{{ slider_post.render_date }}</p>
                                                </div>
                                            </li>

                                        </ul>
                                    </div>
                                    <div class="clearfix">&nbsp;</div>
                                </div>



                                @if(isset($banners['featured_long']))
                                      <!-- Banner space / Show if BIG banner exists -->
                                       <div class="banner-space top-banner-space  @if(isset($banners['featured_small']) && count($banners['featured_small'])>0) double-banner @endif">
                                          @if(isset($banners['featured_long']) && count($banners['featured_long'])>0)
                                              <div class="banner_big" >
                                                  <?php $featured_long = $banners['featured_long'][0]; ?>
                                                  @if(isset($featured_long->file_cdn))
                                                       <a href="{{$featured_long->target_url}}" target="{{$featured_long->target_url_window}}">
                                                           <img src="{{$featured_long->file_cdn}}" alt="{{$featured_long->description}}"/>
                                                       </a>
                                                  @else
                                                       {{{$featured_long->content }}}
                                                  @endif
                                              </div>
                                          @endif

                                          @if(isset($banners['featured_small']) && count($banners['featured_small'])>0)
                                            <div class="banner_small">
                                                <?php $featured_small = $banners['featured_small'][0]; ?>
                                                @if(isset($featured_small->file_cdn))
                                                     <a href="{{$featured_small->target_url}}" target="{{$featured_small->target_url_window}}">
                                                         <img src="{{$featured_small->file_cdn}}" alt="{{$featured_small->description}}"/>
                                                     </a>
                                                @else
                                                     {{{$featured_small->content }}}
                                                @endif
                                            </div>
                                          @endif

                                          <div class="clearfix"></div>
                                       </div>
                                @endif

                                  <div >
                                    <!-- Block layout three / Showcase Home 3 - At least 3 or 6 or 9 limit 12  -->
                                    <div class="block-layout-four row"  ng-show="featured_posts.length > 0"  ng-repeat="group in featured_posts | chunk:3:this">

                                       <div class="grid_4" ng-repeat="featured_post in group">
                                           <div class="main-item">
                                               <div class="post-img">
                                                   <a ng-href="@{{ featured_post.render_url }}" ng-show="featured_post.thumb"><img src="@{{ featured_post.thumb_square }}" alt="@{{ featured_post.title }}"/></a>
                                                   <span><a ng-href="@{{ featured_post.render_category_url }}">@{{ featured_post.render_category }}</a></span>
                                               </div>
                                               <h3>
                                                    <a ng-href="@{{ featured_post.render_url }}">@{{ featured_post.title }}
                                                    &nbsp;<i class="fa fa-lock" ng-show="featured_post.render_lock"></i>
                                                    </a>
                                               </h3>
                                               <div class="post-dca">
                                                <span class="date">@{{ featured_post.render_date }}</span>
                                                @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
                                                   <span class="comments">
                                                        <a href="@{{ featured_post.render_url }}"><span class="disqus-comment-count" data-disqus-url="@{{ featured_post.render_url }}"> 0 </span> {{-- trans('application.homepage_article_comments_title') --}}</a>
                                                   </span>
                                                @endif
                                                <span class="author" ng-show="featured_post.render_author_name"><a ng-href="@{{ featured_post.render_author_url }}">@{{featured_post.render_author_name}}</a></span>
                                                @if(is_object($authUser))
                                                    <span style="font-size:0.8em;">
                                                        <a href="javascript:void(0);"  ng-click="favorite_post(featured_post.id)">
                                                            <i class="fa fa-heart heart aheart"  ng-show="favorite_posts.indexOf(featured_post.id) > -1"></i>
                                                            <i class="fa fa-heart aheart" ng-show="favorite_posts.indexOf(featured_post.id) < 0"></i>
                                                        </a>
                                                    </span>
                                                @endif
                                               </div>
                                               <p class="hide_on_761_1080" ng-show="featured_post.subtitle && featured_post.subtitle!=null">@{{ featured_post.subtitle }}</p>
                                           </div>
                                       </div>
                                   </div>
                                  </div>
                           </div>
                </div>
