                <div   ng-init="latest_publications_fetch()" class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif" ng-show="latest_magazines.length > 0" style="background-color:#fbfbfb;">
                     <div class="inner-wrapper" style="margin-top:20px;">

                         <div id="main" @if(isset($banners['publications_sidebar']) && count($banners['publications_sidebar'])>0) class="left"  @endif role="main" >

                           <!-- Products / Show the four latest published issues -->
                           <div class="row" ng-show="latest_issues.length>0 && latest_magazines.length>2" >
                               <ul class="products" style="border-bottom:none;">
                                   <li ng-class="{'product first': $index == 0 ,'product': $index > 0 && $index < latest_issues.length-1,'product last': $index == latest_issues.length-1  } "  ng-repeat="issue in latest_issues">
                                       <a ng-href="@{{issue.render_url}}">
                                           <img ng-src="@{{ issue.thumb.cdn }}" alt="@{{ issue.title }} - @{{ issue.issue_number }}"/>
                                           <h3>@{{ issue.title }}</h3>
                                           <h4 ng-show="issue.parent_magazine && issue.parent_magazine.title && latest_magazines.length>1" style="font-size: 14px;">@{{ issue.parent_magazine.title }}</h4>
                                           <span class="price">
                                               <span class="amount">No.@{{ issue.issue_number }} - @{{ issue.render_date }}</span>
                                           </span>
                                       </a>
                                   </li>
                                </ul>
                            </div>

                            <div class="row" ng-show="latest_issues.length>0 && latest_magazines.length>2">
                                <!-- / Show button if more than 2 publications exists -->
                                <a class="btn btn-small btn-custom" ng-href="{{route('publications')}}" ng-show="latest_issues.length>0 && latest_magazines.length>2"><i class="fa fa-newspaper-o"></i>&nbsp;{{trans('application.homepage_see_all_publications_title')}}</a>
                            </div>
                           <!--<div class="row" ng-show="latest_issues.length>0 && latest_magazines.length>2" >

                                <ul class="products">
                                     <!-- / Show button if more than 4 issues exists -->

                                    <!-- <li ng-show="latest_issues.length>0" ><a class="btn btn-small btn-custom" ng-href="{{route('issues')}}"><i class="fa fa-newspaper-o"></i>&nbsp;{{trans('application.homepage_see_all_issues_title')}}</a></li>
                                </ul>
                           </div>-->


                            <!-- Block layout two / Show if more than 1 publication exists -->
                           <div class="block-layout-two row" ng-show="latest_magazines.length==2">

                                <div class="grid_6" ng-repeat="magazine in latest_magazines">
                                    <div class="main-item">
                                        <div class="post-img" ng-show="magazine.last_issue_cover.cdn">
                                            <a ng-href="@{{ magazine.render_url }}"><img ng-src="@{{ magazine.last_issue_cover.cdn }}" alt="@{{ magazine.title }}" /></a>
                                            <span ng-show="magazine.min_price != null"><a href="@{{ magazine.render_url }}">{{trans('application.homepage_subscribe_to_publication_from')}} @{{ magazine.min_price }} &euro;</a></span>
                                        </div>
                                        <h3><a href="@{{ magazine.render_url }}">@{{ magazine.title }}</a></h3>
                                        <p>@{{ magazine.subtitle }}</p>
                                    </div>
                                </div>

                           </div>

                            <!--<div class="row" ng-show="latest_magazines.length>1">
                                <!-- / Show button if more than 2 publications exists -->
                                <!--<a class="btn btn-small btn-custom" ng-href="{{route('publications')}}" ng-show="latest_magazines.length>1"><i class="fa fa-newspaper-o"></i>&nbsp;{{trans('application.homepage_see_all_publications_title')}}</a>
                            </div>-->

                            <!-- Block layout two / IF 1 MAGAZINES - Show the latest issue of the magazine -->
                            <div class="block-layout-two row" ng-show="latest_magazines.length==1">
                                <div class="grid_6">
                                    <div class="main-item">
                                        <div class="post-img" ng-show="latest_magazines[0].last_issue_cover.cdn">
                                            <a ng-href="@{{ latest_magazines[0].render_url }}"><img ng-src="@{{ latest_magazines[0].last_issue_cover.cdn }}" alt="@{{ latest_magazines[0].title }}"/></a>
                                            <span ng-show="magazine.min_price != null"><a ng-href="@{{ latest_magazines[0].render_url }}">{{trans('application.homepage_subscribe_to_publication_from')}} @{{ latest_magazines[0].min_price }} &euro;</a></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid_6">
                                    <div class="main-item">
                                        <h3><a href="@{{ latest_magazines[0].render_url }}">@{{ latest_magazines[0].title }}</a></h3>
                                        <div class="spacer"></div>
                                        <p ng-show="latest_magazines[0].last_issue_cover &&  latest_magazines[0].last_issue_cover.title"><strong>@{{ latest_magazines[0].last_issue_cover.title }}</strong></p>
                                        <p ng-show="latest_magazines[0].subtitle">@{{ latest_magazines[0].subtitle }}</p>
                                        <div class="spacer"></div>
                                    </div>
                                </div>
                            </div>

                         </div>

                         @if(isset($banners['publications_sidebar']) && count($banners['publications_sidebar'])>0)
                                <aside class="col_sidebar">
                                     <div class="widget">
                                          <div class="ad-banner-300x250">
                                              <?php $publications_sidebar = $banners['publications_sidebar'][0]; ?>
                                              @if(isset($publications_sidebar->file_cdn))
                                                   <a href="{{$publications_sidebar->target_url}}" target="{{$publications_sidebar->target_url_window}}">
                                                       <img src="{{$publications_sidebar->file_cdn}}" alt="{{$publications_sidebar->description}}"/>
                                                   </a>
                                              @else
                                                   {{{$publications_sidebar->content }}}
                                              @endif
                                          </div>
                                     </div>
                                </aside>
                         @endif
                      </div>


                </div>
