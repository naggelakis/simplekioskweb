@extends($template.'.layout.master')

@section('meta_title')
| {{$post->title}}
@endsection
@section('meta_description')
{{strip_tags($post->subtitle)}}
@endsection
@section('content')

                @if(isset($post->image))
                    <!-- Main Article Image -->
                    <div id="above-the-fold" class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif featured" @if($post->article_type != 252 && $post->article_type != 253 && isset($post->sections) && is_array($post->sections) && count($post->sections)>0) style="margin-bottom:0px;" @endif >
                        <img src="{{$post->image}}" alt="{{$post->title}}"/>
                    </div>
                @endif

                <!-- Main Article -->
                <section id="section">
                    <div class="inner-wrapper">

                        <!-- Main -->
                        <div id="main" role="main">

                            <!-- Post -->
                            <article class="single-post">

                                @if( ($post->article_type != 252 && $post->article_type != 253 && $post->article_type!=246) && isset($post->sections) && is_array($post->sections) && count($post->sections)>0)
                                    <nav class="sections-primary-menu light sticky-menu" role="navigation" style="margin-bottom: 2.5em;">

                                        <!-- Responsive menu -->
                                        <a class="click-to-open-menu"><i class="fa fa-align-justify"></i> {{trans('application.sections_navigator_title')}}</a>
                                        <!-- Main menu -->
                                        <ul class="sections-menu">
                                            @foreach($post->sections as $section)
                                                <li><a href="javascript:void(0);" class="section-url" target="#section{{$section->id}}"><span>{{isset($section->translations->lang->title) ? $section->translations->lang->title : current($section->translations)->title}}</span></a></li>
                                            @endforeach

                                        </ul>

                                    </nav>
                                @endif

                                <h1 class="post-title">{{$post->title}}</h1>
                                @if(isset($post->subtitle) && $post->subtitle!= null && $post->subtitle!='')
                                    <h3 class="lead">{{$post->subtitle}}</h3>
                                @endif

                                <div class="post-container">


    	                                @if(isset($post->images[0]->files) && is_array($post->images[0]->files) && count($post->images[0]->files) >1 && count($post->images[0]->files) == 2)
                                            <div class="post-right">
                                                <img src="{{$post->images[0]->files[1]->cdn}}" alt="Image" />
                                                @if(isset($post->images[0]->files[1]->title) && $post->images[0]->files[1]->title != null && $post->images[0]->files[1]->title!='')
                                                    <span class="caption">{{ $post->images[0]->files[1]->title }}</span>
                                                @endif
                                            </div>
                                        @elseif(isset($post->images[0]->files) && is_array($post->images[0]->files) && count($post->images[0]->files) > 2)
                                            <div class="post-right">
                                                <div class="gallery-single">

                                                    <ul class="slides">
                                                        @foreach($post->images[0]->files as $key=>$file)
                                                            @if($key>0)
                                                                 <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                                    <img src="{{$file->cdn}}" />
                                                                    @if(isset($file->title) && $file->title != null && $file->title!='')
                                                                        <div class="gallery-caption-body">
                                                                            <p>{{$file->title}}</p>
                                                                        </div>
                                                                    @endif
                                                                 </li>
                                                             @endif
                                                        @endforeach

                                                    </ul>

                                                </div>
                                            </div>
                                        @endif



    	                             @if(isset($post->description) && $post->description!= null && $post->description!='')
                                        <p>{{{$post->description}}}</p>
                                     @endif
                                </div>

                            </article>

                        </div>

                    </div>
                </section>


                <div class="clearfix"></div>
                @if(isset($post->sections) && is_array($post->sections) && count($post->sections)>0 )



                    @foreach($post->sections as $section)
                        @if(isset($section->translations->$lang->title))
                            <div class="spacer" id="section{{$section->id}}"></div>
                            <?php $section->title = (isset($section->translations->$lang->title)) ? $section->translations->$lang->title :null; ?>
                            <?php $section->subtitle = (isset($section->translations->$lang->subtitle)) ? $section->translations->$lang->subtitle : null; ?>
                            <?php $section->description = (isset($section->translations->$lang->description)) ? $section->translations->$lang->description : null; ?>
                            <!-- Section Image / If exists -->
                            @if(isset($section->images) && is_array($section->images))
                                <div id="" class="above-the-fold light featured" style="margin-top:0px;">
                                    <img src="{{$section->images[0]->cdn}}" alt="{{$section->title}}"/>
                                </div>

                            @endif
                            <section id="">
                                  <div class="inner-wrapper">
                                      <div id="" role="main">
                                          <article class="single-post">
                                             <h1 class="post-title">{{$section->title}}</h1>
                                             @if(isset($section->subtitle) && $section->subtitle!= null && $section->subtitle!='')
                                                 <h3 class="lead">{{$section->subtitle}}</h3>
                                             @endif
                                             <div class="post-container">
                                                    @if(isset($section->images) && is_array($section->images) && count($section->images) >1 && count($section->images) == 2)
                                                        <div class="post-right">
                                                            <img src="{{$section->images[0]->cdn}}" alt="Image" />
                                                            @if(isset($section->images[0]->title) && $section->images[0]->title != null && $section->images[0]->title!='')
                                                                <span class="caption">{{ $section->images[0]->title }}</span>
                                                            @endif
                                                        </div>
                                                    @elseif(isset($section->images) && is_array($section->images) && count($section->images) > 2)
                                                        <div class="post-right">
                                                            <div class="gallery-single">

                                                                <ul class="slides">
                                                                    @foreach($section->images as $file)
                                                                         <li data-thumb="{{Helpers::squareFilePath($file->cdn)}}">
                                                                            <img src="{{$file->cdn}}" />
                                                                            @if(isset($file->title) && $file->title != null && $file->title!=''  && $file->title!=' ')
                                                                                <div class="gallery-caption-body">
                                                                                    <p>{{$file->title}}</p>
                                                                                </div>
                                                                            @endif
                                                                         </li>
                                                                    @endforeach

                                                                </ul>

                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if(isset($section->description) && $section->description!= null && $section->description!='')
                                                      <p>{{{$section->description}}}</p>
                                                    @endif
                                             </div>
                                          </article>
                                          @if(isset($section->albums) && is_array($section->albums) && count($section->albums)>0)
                                              <br>
                                              <div class="gallery-content">
                                                  @foreach($section->albums as $album)
                                                      @if(is_array($album->files) && count($album->files)>0)
                                                          <!-- Gallery album -->
                                                          <div class="gallery-album sport-gal">
                                                              <div class="gal-img">
                                                                  <a class="fancybox" rel="group<?php echo $album->id;?>" href="{{$album->thumb_cdn}}" title="{{$album->files[0]->title}}">
                                                                      <span>{{count($album->files)}} {{trans('application.custom_albums_images_label')}}</span>
                                                                      <img src="{{Helpers::squareFilePath($album->thumb_cdn)}}" alt="{{$album->title}}" title="{{$album->files[0]->title}}"/>
                                                                  </a>
                                                              </div>
                                                              <h4><a href="javascript:void(0);" >{{$album->title}}</a></h4>
                                                              <p>{{$album->subtitle}}</p>
                                                              @foreach($album->files as $key=>$file)
                                                                @if($key > 0)
                                                                  <a class="fancybox" rel="group<?php echo $album->id;?>" href="{{$file->cdn}}" style="display:none;" title="{{$file->title}}"></a>
                                                                @endif
                                                              @endforeach
                                                          </div>
                                                      @endif
                                                  @endforeach
                                              </div>
                                          @endif
                                      </div>
                                  </div>
                                   <div class="clearfix"></div>
                            </section>
                        @endif
                    @endforeach
                @endif


                @if(isset($post->albums) && is_array($post->albums) && count($post->albums)>0)
                     <section id="">
                        <div class="inner-wrapper">
                            <div id="" role="main">
                                <article class="single-post">
                                    <div class="post-container">
                                        <br>
                                        <div class="gallery-content">
                                            @foreach($post->albums as $album)
                                                @if(is_array($album->files) && count($album->files)>0)
                                                    <!-- Gallery album -->
                                                    <div class="gallery-album sport-gal">
                                                        <div class="gal-img">
                                                            <a class="fancybox" rel="group<?php echo $album->id;?>" href="{{$album->thumb_cdn}}" title="{{$album->files[0]->title}}">
                                                                <span>{{count($album->files)}} {{trans('application.custom_albums_images_label')}}</span>
                                                                <img src="{{Helpers::squareFilePath($album->thumb_cdn)}}" alt="{{$album->title}}" title="{{$album->files[0]->title}}"/>
                                                            </a>
                                                        </div>
                                                        <h4><a href="javascript:void(0);" >{{$album->title}}</a></h4>
                                                        <p>{{$album->subtitle}}</p>
                                                        @foreach($album->files as $key=>$file)
                                                          @if($key > 0)
                                                            <a class="fancybox" rel="group<?php echo $album->id;?>" href="{{$file->cdn}}" style="display:none;" title="{{$file->title}}"></a>
                                                          @endif
                                                        @endforeach
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                        <!-- / SECTION CUSTOM GALLERIES -->
                                    </div>
                                </article>
                            </div>
                        </div>
                    </section>
                @endif
    			<!-- Parent Article Share and related articles -->
                <section id="">
                    <div class="inner-wrapper">
                        <div id="" role="main">

                            <article class="single-post">
                                <div class="post-share">
                                   <span class="share-text">{{trans('application.news_single_share_title')}}</span>
                                   <ul>
                                       <li><a data-tip="Share on Twitter!" target="_blank" href="https://twitter.com/intent/tweet?text={{Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,Helpers::urlize($post->title))}}" class="twitter"><span class="socicon">a</span></a></li>
                                       <li><a data-tip="Share on Facebook!" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,Helpers::urlize($post->title))}}" class="facebook"><span class="socicon">b</span></a></li>
                                       <li><a data-tip="Share on Google+!" target="_blank" href="https://plus.google.com/share?url={{Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,Helpers::urlize($post->title))}}" class="google"><span class="socicon">c</span></a></li>
                                       <li><a data-tip="Share on Pinterest!" target="_blank" href="http://pinterest.com/pin/create/button/?url={{Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,Helpers::urlize($post->title))}}&description={{$post->title}}" class="google"><span class="socicon">d</span></a></li>
                                       <li><a data-tip="Share on LinkedIn!" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,Helpers::urlize($post->title))}}" class="linkedin"><span class="socicon">j</span></a></li>
                                   </ul>
                                </div>
                            </article>


                            @if(isset($other_posts) && is_array($other_posts) && count($other_posts)>0)
                                <!-- Related articles -->
                                <div class="block-layout-one">
                                    <p class="title"><span>{{trans('application.profile_single_other_posts_title')}}</span></p>
                                    <div class="row">
                                        <?php $i = 0; ?>
                                        @foreach($other_posts as $other_post)
                                            @if( $i>0 && $i%3 == 0)
                                                </div>
                                                <div class="row">
                                            @endif
                                            <div class="item grid_4 ">
                                                @if(isset($other_post->thumb))
                                                    <a href="{{route('profile_single',[$other_post->id,Helpers::urlize($other_post->title)])}}"><img src="{{Helpers::squareFilePath($other_post->thumb)}}" width="80"/></a>
                                                @endif
                                                <div>
                                                    <h3><a href="{{route('profile_single',[$other_post->id,Helpers::urlize($other_post->title)])}}">{{$other_post->title}}</a></h3>
                                                    <p>&nbsp;</p>
                                                </div>
                                                <span class="clearfix"></span>
                                            </div>
                                            <?php $i++; ?>
                                        @endforeach

                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </section>
@endsection
<div class="clearfix"></div>

@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
		$(".fancybox").fancybox();
	});
</script>
@endsection