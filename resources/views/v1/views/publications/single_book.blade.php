@extends($template.'.layout.master')

@section('meta_title')
| {{ isset($publication->translations->$lang->title) ? $publication->translations->$lang->title : current($publication->translations)->title }}
@endsection

@section('meta_description')
{{ isset($publication->translations->$lang->description) ? $publication->translations->$lang->description : current($publication->translations)->description }}
@endsection

@section('content')

    <!-- / The title of the category or magazine placeholder -->
                <div id="above-the-fold" class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif">
                    <div class="inner-wrapper">
                        <h2 class="page-title">
                            @if(isset($publication->category_title))
                                {{$publication->category_title}}
                            @else
                                {{trans('application.publications_single_book_above_the_fold_placeholder')}}
                            @endif
                        </h2>
                    </div>
                </div>

                <!-- Section -->
                <section id="section">
                    <div class="inner-wrapper" ng-app="mainApp">

                        <!-- Main -->
                        <div id="main" class="left" role="main"  ng-controller="publicationCtrl" >

                              <!-- Product type Issue -->
                              <div class="product">
                                  @if(isset($publication->thumb->cdn))
                                      <div class="images">
                                          <img src="{{$publication->thumb->cdn}}" alt="{{ isset($publication->translations->$lang->title) ? $publication->translations->$lang->title : current($publication->translations)->title }}"/>
                                      </div>
                                  @endif
                                  <div class="summary">
                                      <h1>{{ isset($publication->translations->$lang->title) ? $publication->translations->$lang->title : current($publication->translations)->title }}</h1>
                                      <div class="product_meta">
                                          @if(isset($publication->sku) && $publication->sku!=null)
                                            <span class="sku_wrapper">SKU: <span class="sku"><strong>{{$publication->sku}}</strong></span>.</span>
                                          @endif

                                          @if(isset($publication->all_categories) && is_array($publication->all_categories) && count($publication->all_categories)>0)
                                            <span class="posted_in">{{trans('application.publications_single_book_category_placeholder')}}
                                                @foreach($publication->all_categories as $key=>$category)
                                                    <a href="{{route('publications_category',[$publication->id,Helpers::urlize($category->title)])}}">{{$category->title}}</a>
                                                    @if($key<count($publication->all_categories)-1), @endif
                                                @endforeach

                                          @endif
                                      </div>

                                      <div class="woocommerce-product-rating">
                                       @if(isset($publication->publishers_details) && is_object($publication->publishers_details))
                                        {{trans('application.publications_single_book_publisher_placeholder')}} {{$publication->publishers_details->firstname}} {{$publication->publishers_details->lastname}}/
                                       @endif
                                       @if(isset($publication->publication_date) && $publication->publication_date!=null)
                                         {{trans('application.publications_single_book_publication_date_placeholder')}} {{Helpers::localeDate($publication->publication_date,'d F Y')}}
                                       @endif
                                       </div>
                                      <div><p>&nbsp;</p></div>
                                      @if((isset($prices) && is_array($prices) && count($prices)>0) || $publication->file_sample!=null)
                                          <form class="cart">
                                                @if(isset($prices) && is_array($prices) && count($prices)>1)
                                                   <div class="form-group">
                                                       <label>{{trans('application.publications_single_issue_placeholder_select_group')}}</label>
                                                       <select class="groupOptions" id="groupSelect">
                                                           @foreach($prices as $key=>$group)
                                                               <option value="{{$key}}">{{$group['title']}}</option>
                                                           @endforeach
                                                       </select>
                                                   </div>
                                                   <div class="spacer"></div>
                                                @endif
                                              @if($publication->file_sample!=null)
                                                  <a href="{{$publication->file_sample}}" target="_blank" class="btn btn-grey">{{trans('application.publications_single_issue_placeholder_sample')}}</a><br>
                                              @endif
                                              @if(isset($prices) && is_array($prices) && count($prices)>0)
                                                <?php
                                                    reset($prices);
                                                    $first = key($prices);
                                                ?>
                                                @foreach($prices as $key=>$group)
                                                    <div class="row groups" id="group{{$key}}" @if($first != $key) style="display:none;" @endif>
                                                      @foreach($group['options'] as $option)
                                                          @if(Helpers::check_if_bought($authUser,$publication,null,$option->system_type_id,$key))
                                                              <a href="{{route('view_publication',[$publication->id])}}" class="btn btn-green" target="_blank">{{trans('application.open_pdf_for_reading')}}</a>
                                                           @else
                                                              <a href="{{route('add_to_cart',[$publication->id,$key,$option->system_type_id])}}" class="btn btn-blue">{{$option->system_type_id_translation}} {{$option->price}} &euro;</a>
                                                           @endif
                                                      @endforeach
                                                    </div>
                                                @endforeach
                                              @elseif(isset($publication->base_price ) && $publication->base_price!=null)
                                                    <a href="{{route('add_to_cart',[$publication->id,0,0])}}" class="btn btn-blue">{{$option->price}} &euro;</a>
                                              @endif
                                          </form>
                                      @elseif(isset($publication->base_price ) && $publication->base_price!=null)
                                          <a href="{{route('add_to_cart',[$publication->id,0,0])}}" class="btn btn-blue">{{$publication->base_price}} &euro;</a>
                                      @endif

                                      @if($publication->ibooks_link!=null && $publication->ibooks_link!='' || $publication->google_play_link!=null && $publication->google_play_link!= '' || $publication->amazon_link!=null && $publication->amazon_link!= '')
                                          <div class="product_meta">
                                              <span class="sku_wrapper">{{trans('application.publications_single_magazine_tab_recurring_plans_also_on_stores_placeholder')}}</span><br>

                                              <p>
                                              @if($publication->ibooks_link!=null && $publication->ibooks_link!='')
                                                  <a href="{{$publication->ibooks_link}}" target="_blank"><img class="img_badge" src="{{asset('assets/'.$template.'/images/appstore.png')}}" alt="{{trans('application.publications_single_magazine_tab_recurring_plans_also_on_stores_download_apple_placeholder')}}"></a>
                                              @endif
                                              @if($publication->google_play_link!=null && $publication->google_play_link!='')
                                                  <a href="{{$publication->google_play_link}}" target="_blank"><img class="img_badge" src="{{asset('assets/'.$template.'/images/googleplay.png')}}" alt="{{trans('application.publications_single_magazine_tab_recurring_plans_also_on_stores_download_google_placeholder')}}"></a>
                                              @endif
                                              @if($publication->amazon_link!=null && $publication->amazon_link!='')
                                                  <a href="{{$publication->amazon_link}}" target="_blank"><img class="img_badge" src="{{asset('assets/'.$template.'/images/amazon_books.jpg')}}" alt="{{trans('application.publications_single_magazine_tab_recurring_plans_also_on_stores_download_amazon_placeholder')}}"></a>
                                              @endif
                                              </p>

                                          </div>
                                      @endif
                                  </div>

                                  <!-- Tabs -->
                                  <div class="woocommerce-tabs tabs">
                                      <ul>
                                          <li><a href="#tab-description">{{trans('application.publications_single_book_details_tab_title')}}</a></li>
                                      </ul>
                                      <div id="tab-description">
                                        <div class="row">
                                            <div class="grid_12">
                                                @if(isset($publication->translations->$lang->subtitle) && $publication->translations->$lang->subtitle != null && $publication->translations->$lang->subtitle!='')
                                                    <h2>{{$publication->translations->$lang->subtitle}}</h2>
                                                @endif
                                                @if(isset($publication->translations->$lang->quick_desc) && $publication->translations->$lang->quick_desc != null && $publication->translations->$lang->quick_desc!='')
                                                    <p>{{$publication->translations->$lang->quick_desc}}</p>
                                                @endif
                                                <table class="product-table">
                                                    @if(isset($publication->publication_lang) && $publication->publication_lang != null && $publication->publication_lang!='')
                                                        <tr>
                                                            <th>{{trans('application.publications_single_magazine_first_publication_lang_placeholder')}}</th>
                                                            <td>{{trans('application.publications_single_magazine_first_publication_lang_'.strtolower($publication->publication_lang))}}</td>
                                                        </tr>
                                                    @endif
                                                    @if(isset($publication->publisher_details) && $publication->publisher_details != null && is_object($publication->publisher_details))
                                                        <tr>
                                                            <th>{{trans('application.publications_single_magazine_first_publication_publisher_placeholder')}}</th>
                                                            <td>{{$publication->publisher_details->firstname}} {{$publication->publisher_details->lastname}}</td>
                                                        </tr>
                                                    @endif

                                                    <tr>
                                                        <th>{{trans('application.publications_single_magazine_first_publication_date_placeholder')}}</th>
                                                        <td>{{ Helpers::localeDate($publication->publication_date,'d F Y') }}</td>
                                                    </tr>

                                                    @if(isset($publication->pages) && $publication->pages != null && $publication->pages!='')
                                                        <tr>
                                                            <th>{{trans('application.publications_single_magazine_first_publication_pages_placeholder')}}</th>
                                                            <td>{{$publication->pages}}</td>
                                                        </tr>
                                                    @endif
                                                    @if(isset($publication->circulation) && $publication->circulation != null && $publication->circulation!='')
                                                        <tr>
                                                            <th>{{trans('application.publications_single_magazine_first_publication_circulation_placeholder')}}</th>
                                                            <td>{{$publication->circulation}}</td>
                                                        </tr>
                                                    @endif
                                                    @if(isset($publication->int_code) && $publication->int_code != null && $publication->int_code!='')
                                                        <tr>
                                                            <th>ISSN</th>
                                                            <td>{{$publication->int_code}}</td>
                                                        </tr>
                                                    @endif
                                                    @if(isset($publication->cover_type_id) && $publication->cover_type_id != null && $publication->cover_type_id!='' && $publication->cover_type_id > 0)
                                                        <tr>
                                                            <th>{{trans('application.publications_single_magazine_first_publication_cover_type_placeholder')}}</th>
                                                            @if($publication->cover_type_id == 87)
                                                                <td>{{trans('application.publications_single_magazine_first_publication_cover_type_soft')}}</td>
                                                            @elseif($publication->cover_type_id == 88)
                                                                <td>{{trans('application.publications_single_magazine_first_publication_cover_type_hard')}}</td>
                                                            @endif
                                                        </tr>
                                                    @endif
                                                </table>
                                            </div>
                                        </div>
                                      </div>

                                  </div>

                                  @if(isset($publication->other_books) && is_array($publication->other_books) && count($publication->other_books)>0)
                                  <!-- Related products -->
                                  <p class="title"><span>{{trans('application.publications_single_book_other_books_title')}}</span></p>
                                  <ul class="products">
                                      @foreach($publication->other_books as $key=>$book )
                                          <li class="product @if($key==0) first @endif">
                                              <a href="{{route('book_single',[$book->id,Helpers::urlize($book->slug)])}}">
                                                  @if(isset($book->thumb->cdn))
                                                    <img src="{{Helpers::squareFilePath($book->thumb->cdn)}}" alt="{{ $book->title }}"/>
                                                  @endif
                                                  <h3>{{$book->title}}</h3>
                                                  @if(isset($book->base_price) && $book->base_price!=null)
                                                      <span class="price">
                                                          <span class="amount">Από {{$book->base_price}} &euro;</span>
                                                      </span>
                                                  @endif
                                              </a>
                                          </li>
                                      @endforeach

                                  </ul>
                                  @endif
                              </div>



                        </div>

                        <!-- Aside -->

                            @include($template.'.partials.sidebar_publication')


                    </div>
                </section>


@endsection

@section('scripts')
    @javascript({{ asset('assets/'.$template.'/angular/publication_single_magazine.js') }})
    @javascript({{ asset('assets/'.$template.'/angular/publication_sidebar_controller.js') }})
    <script type="text/javascript">
        $(document).ready(function(){
            $('#groupSelect').on('change',function(){
                var id = $(this).val();
                $('.groups').hide();
                $('#group'+id).show();
            });
        });
    </script>
@endsection
