@extends($template.'.layout.master')

@section('meta_title')
| {{ isset($publication->translations->$lang->title) ? $publication->translations->$lang->title : current($publication->translations)->title }}
@endsection

@section('meta_description')
{{ isset($publication->translations->$lang->description) ? $publication->translations->$lang->description : current($publication->translations)->description }}
@endsection

@section('content')

    <!-- / The title of the category or magazine placeholder -->
                <div id="above-the-fold" class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif">
                    <div class="inner-wrapper">
                        <h2 class="page-title">{{ isset($publication->magazine->translations->$lang->title) ? $publication->magazine->translations->$lang->title : current($publication->magazine->translations)->title }}</h2>
                    </div>
                </div>

                <!-- Section -->
                <section id="section">
                    <div class="inner-wrapper" ng-app="mainApp">

                        <!-- Main -->
                        <div id="main" class="left" role="main"  ng-controller="publicationCtrl" >

                                <!-- Product type Issue -->
                                <div class="product">
                                    @if(isset($publication->thumb->cdn))
                                        <div class="images">
                                           <img src="{{$publication->thumb->cdn}}" alt="{{ isset($publication->translations->$lang->title) ? $publication->translations->$lang->title : current($publication->translations)->title }}"/>
                                        </div>
                                    @endif

                                    <div class="summary">
                                        <h1>
                                            @if(isset($publication->translations))
                                                {{ isset($publication->translations->$lang->title) ? $publication->translations->$lang->title : current($publication->translations)->title }}
                                            @elseif(isset($publication->issue_number) && $publication->issue_number!=null)
                                                #{{$publication->issue_number}}
                                            @elseif($publication->publication_date!=null)
                                                {{Helpers::localeDate($publication->publication_date,'F Y')}}
                                            @else
                                                {{Helpers::localeDate($publication->created,'F Y')}}
                                            @endif
                                        </h1>
                                        @if( (isset($publication->issue_number) && $publication->issue_number!=null) || (isset($publication->publication_date) && $publication->publication_date!=null) )
                                            <div class="woocommerce-product-rating">
                                            @if(isset($publication->issue_number) && $publication->issue_number!=null)
                                            {{trans('application.publications_single_issue_placeholder_issue_number')}} {{$publication->issue_number}}
                                            /
                                            @endif
                                            @if(isset($publication->publication_date) && $publication->publication_date!=null)
                                               {{trans('application.publications_single_issue_placeholder_publish_date')}} {{Helpers::localeDate($publication->publication_date,'d F Y')}}
                                            @endif
                                             </div>
                                        @endif
                                        @if(isset($publication->translations->$lang->subtitle))
                                            <div>
                                                <p>{{$publication->translations->$lang->subtitle}}</p>
                                            </div>
                                        @endif

                                        @if((isset($prices) && is_array($prices) && count($prices)>0) || $publication->file_sample!=null)
                                            <form class="cart">
                                                  @if(isset($prices) && is_array($prices) && count($prices)>1)
                                                     <div class="form-group">
                                                         <label>{{trans('application.publications_single_issue_placeholder_select_group')}}</label>
                                                         <select class="groupOptions" id="groupSelect">
                                                             @foreach($prices as $key=>$group)
                                                                 <option value="{{$key}}">{{$group['title']}}</option>
                                                             @endforeach
                                                         </select>
                                                     </div>
                                                     <div class="spacer"></div>
                                                  @endif
                                                @if($publication->file_sample!=null)
                                                    <a href="{{$publication->file_sample}}" target="_blank" class="btn btn-grey">{{trans('application.publications_single_issue_placeholder_sample')}}</a><br>
                                                @endif

                                                {{-- prices list --}}
                                                @if(isset($prices) && is_array($prices) && count($prices)>0)
                                                      <?php
                                                          reset($prices);
                                                          $first = key($prices);
                                                      ?>
                                                      @foreach($prices as $key=>$group)
                                                          <div class="row groups" id="group{{$key}}" @if($first != $key) style="display:none;" @endif>
                                                            @foreach($group['options'] as $option)
                                                                 @if(Helpers::check_if_bought($authUser,$publication,$publication->magazine,$option->system_type_id,$key))
                                                                    <a href="{{route('view_publication',[$publication->id])}}" class="btn btn-green" target="_blank">{{trans('application.open_pdf_for_reading')}}</a>
                                                                 @else
                                                                    <a href="{{route('add_to_cart',[$publication->id,$key,$option->system_type_id])}}" class="btn btn-blue">{{$option->system_type_id_translation}} {{$option->price}} &euro;</a>
                                                                 @endif
                                                            @endforeach
                                                          </div>
                                                      @endforeach
                                                @elseif(isset($publication->base_price ) && $publication->base_price!=null)
                                                      <a href="{{route('add_to_cart',[$publication->id,0,0])}}" class="btn btn-blue">{{$option->price}} &euro;</a>
                                                @endif
                                            </form>
                                        @elseif(isset($base_price ) && $base_price!=null)
                                            <a href="{{route('add_to_cart',[$publication->id,0,0])}}" class="btn btn-blue">{{$base_price}} &euro;</a>
                                        @endif


                                        @if(isset($publication->settings_appstore_link) && $publication->settings_appstore_link!=null && $publication->settings_appstore_link!='' || isset($publication->settings_googleplay_link) && $publication->settings_googleplay_link!=null && $publication->settings_googleplay_link!= '' )
                                            <div class="product_meta">
                                                <span class="sku_wrapper">{{trans('application.publications_single_magazine_tab_recurring_plans_also_on_stores_placeholder')}}</span><br>

                                                <p>
                                                @if(isset($publication->settings_appstore_link) && $publication->settings_appstore_link!=null && $publication->settings_appstore_link!='')
                                                    <a href="{{$publication->settings_appstore_link}}" target="_blank"><img class="img_badge" src="{{asset('assets/'.$template.'/images/appstore.png')}}" alt="{{trans('application.publications_single_magazine_tab_recurring_plans_also_on_stores_download_apple_placeholder')}}"></a>
                                                @endif
                                                @if(isset($publication->settings_googleplay_link) && $publication->settings_googleplay_link!=null && $publication->settings_googleplay_link!='')
                                                    <a href="{{$publication->settings_googleplay_link}}" target="_blank"><img class="img_badge" src="{{asset('assets/'.$template.'/images/googleplay.png')}}" alt="{{trans('application.publications_single_magazine_tab_recurring_plans_also_on_stores_download_google_placeholder')}}"></a>
                                                @endif
                                                </p>

                                            </div>
                                        @endif

                                    </div>

                                    <!-- Tabs -->
                                    <div class="woocommerce-tabs tabs">
                                        <ul>
                                            <li><a href="#tab-description">{{trans('application.publications_single_issue_placeholder_tab_details_title')}}</a></li>
                                            @if(isset($articles))
                                                <li><a href="#tab-index">{{trans('application.publications_single_issue_placeholder_tab_content_title')}}</a></li>
                                            @endif
                                        </ul>
                                        <div id="tab-description">

                                            <!--<div class="featured">
                                                <iframe width="560" height="315" src="https://www.youtube.com/embed/D5Y-pJDgJT8" frameborder="0" allowfullscreen></iframe>
                                                <div class="spacer"></div>
                                            </div>-->

                                            <div class="row">
                                              @if(isset($publication->screenshots) && is_array($publication->screenshots) && count($publication->screenshots)>0)
                                                <div class="grid_6">
                                              @else
                                                <div class="grid_12">
                                              @endif
                                                    @if(isset($publication->translations->$lang->subtitle) && $publication->translations->$lang->subtitle != null && $publication->translations->$lang->subtitle!='')
                                                        <h2>{{$publication->translations->$lang->subtitle}}</h2>
                                                    @endif
                                                    @if(isset($publication->translations->$lang->quick_desc) && $publication->translations->$lang->quick_desc != null && $publication->translations->$lang->quick_desc!='')
                                                        <p>{{$publication->translations->$lang->quick_desc}}</p>
                                                    @endif
                                                    <table class="product-table">
                                                        @if(isset($publication->publication_lang) && $publication->publication_lang != null && $publication->publication_lang!='')
                                                            <tr>
                                                                <th>{{trans('application.publications_single_magazine_first_publication_lang_placeholder')}}</th>
                                                                <td>{{trans('application.publications_single_magazine_first_publication_lang_'.strtolower($publication->publication_lang))}}</td>
                                                            </tr>
                                                        @endif
                                                        @if(isset($publication->publisher_details) && $publication->publisher_details != null && is_object($publication->publisher_details))
                                                            <tr>
                                                                <th>{{trans('application.publications_single_magazine_first_publication_publisher_placeholder')}}</th>
                                                                <td>{{$publication->publisher_details->firstname}} {{$publication->publisher_details->lastname}}</td>
                                                            </tr>
                                                        @endif

                                                        <tr>
                                                            <th>{{trans('application.publications_single_magazine_first_publication_date_placeholder')}}</th>
                                                            <td>{{ Helpers::localeDate($publication->publication_date,'d F Y') }}</td>
                                                        </tr>

                                                        @if(isset($publication->pages) && $publication->pages != null && $publication->pages!='')
                                                            <tr>
                                                                <th>{{trans('application.publications_single_magazine_first_publication_pages_placeholder')}}</th>
                                                                <td>{{$publication->pages}}</td>
                                                            </tr>
                                                        @endif
                                                        @if(isset($publication->circulation) && $publication->circulation != null && $publication->circulation!='')
                                                            <tr>
                                                                <th>{{trans('application.publications_single_magazine_first_publication_circulation_placeholder')}}</th>
                                                                <td>{{$publication->circulation}}</td>
                                                            </tr>
                                                        @endif
                                                        @if(isset($publication->int_code) && $publication->int_code != null && $publication->int_code!='')
                                                            <tr>
                                                                <th>ISSN</th>
                                                                <td>{{$publication->int_code}}</td>
                                                            </tr>
                                                        @endif
                                                        @if(isset($publication->cover_type_id) && $publication->cover_type_id != null && $publication->cover_type_id!='' && $publication->cover_type_id > 0)
                                                            <tr>
                                                                <th>{{trans('application.publications_single_magazine_first_publication_cover_type_placeholder')}}</th>
                                                                @if($publication->cover_type_id == 87)
                                                                    <td>{{trans('application.publications_single_magazine_first_publication_cover_type_soft')}}</td>
                                                                @elseif($publication->cover_type_id == 88)
                                                                    <td>{{trans('application.publications_single_magazine_first_publication_cover_type_hard')}}</td>
                                                                @endif
                                                            </tr>
                                                        @endif
                                                    </table>
                                                </div>
                                                 @if(isset($publication->screenshots) && is_array($publication->screenshots) && count($publication->screenshots)>0)
                                                        <div class="grid_6">
                                                            <div class="gallery-single">
                                                                <ul class="slides">
                                                                    @foreach($publication->screenshots as $screenshot)
                                                                        <li data-thumb="{{Helpers::squareFilePath($screenshot->cdn)}}">
                                                                            <img src="{{$screenshot->cdn}}" alt="{{$screenshot->title}}" />
                                                                            @if($screenshot->title!=null && $screenshot != '')
                                                                                <div class="gallery-caption-body">
                                                                                    <p>{{$screenshot->title}}</p>
                                                                                </div>
                                                                            @endif
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    @endif
                                            </div>

                                        </div>
                                        @if(isset($articles))
                                            <div id="tab-index">
                                            <h2>{{trans('application.publications_single_issue_placeholder_tab_content_inner_title')}}</h2>
                                            <div class="spacer"></div>
                                            <div class="block-layout-five">
                                                @foreach($articles as $article)
                                                    <div class="main-item">
                                                        @if(isset($article->thumb_square))
                                                        <div class="post-img">
                                                            <a href="{{Helpers::getRouteSingleBySystemCat($article->system_cat_parent,$article->id,$article->title)}}"><img src="{{$article->thumb_square}}" alt="{{$article->title}}"/></a>
                                                        </div>
                                                        @endif
                                                        <div class="post-meta">
                                                            <h3><a href="{{Helpers::getRouteSingleBySystemCat($article->system_cat_parent,$article->id,$article->title)}}">{{$article->title}}</a></h3>
                                                            @if(isset($article->subtitle) && $article->subtitle!=null && $article->subtitle!='')
                                                                <p class="hide_on_761_1080">{{Helpers::get_snippet($article->subtitle,40).' [...]'}}</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>

                                        </div>
                                        @endif
                                    </div>

                                </div>

                                @if(isset($plans) && is_array($plans) && count($plans)>0)
                                <div class="block-layout-one">
                                    <p class="title"><span>{{trans('application.publication_recurring_plans_title')}}</span></p>
                                    <div class="alert green"><strong>{{trans('application.publication_recurring_plans_placeholder_strong')}}</strong> {{trans('application.publication_recurring_plans_placeholder')}}</div>
                                    <div class="accordion">
                                        @foreach($plans as $recurring_plan)
                                            <div class="title">{{$recurring_plan['title']}}: {{$recurring_plan['period_translation']}}</div>
                                            <div class="content">
                                            <div class="subscription-table">
                                                <table>

                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th class="subscription-table-column">{{trans('application.syst_class_market_publication_type_all')}} </th>
                                                            <th class="subscription-table-column">{{trans('application.syst_class_market_publication_type_print')}} </th>
                                                            <th class="subscription-table-column">{{trans('application.syst_class_market_publication_type_electronic')}} </th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach($recurring_plan['prices'] as $key=>$price)
                                                            <tr>
                                                                <td><strong>{{$price['group_translation']}}</strong></td>

                                                                <td>
                                                                    @if(isset($price['items'][72]))
                                                                        @if(is_object($authUser) && isset($authUser->magazine_subscriptions->{72}->{$publication->magazine->id}->{$key}) && $authUser->magazine_subscriptions->{72}->{$publication->magazine->id}->{$key} == $price['items'][72]->plan_id)
                                                                            <a class="btn btn-blue btn-expand btn-radius f " style="background-color: #edfdd3;border: 1px solid #c4dba0;color: #657e3c;">{{trans('application.publication_bought')}}</a>
                                                                        @else
                                                                            <a class="btn btn-blue btn-expand btn-radius f" href="{{route('publication_checkout',[$publication->magazine->id,$price['items'][72]->id,$price['items'][72]->system_type_id,$key])}}" style="margin-bottom: 0px;">{{$price['items'][72]->price}} &euro;</a>
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(isset($price['items'][73]))
                                                                        @if(is_object($authUser) && isset($authUser->magazine_subscriptions->{73}->{$publication->magazine->id}->{$key}) && $authUser->magazine_subscriptions->{73}->{$publication->magazine->id}->{$key} == $price['items'][73]->plan_id)
                                                                            <a class="btn btn-blue btn-expand btn-radius f green" style="background-color: #edfdd3;border: 1px solid #c4dba0;color: #657e3c;">{{trans('application.publication_bought')}}</a>
                                                                        @else
                                                                            <a class="btn btn-blue btn-expand btn-radius a" href="{{route('publication_checkout',[$publication->magazine->id,$price['items'][73]->id,$price['items'][73]->system_type_id,$key])}}" style="margin-bottom: 0px;">{{$price['items'][73]->price}} &euro;</a>
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(isset($price['items'][74]))

                                                                        @if(is_object($authUser) && isset($authUser->magazine_subscriptions->{74}->{$publication->magazine->id}->{$key}) && $authUser->magazine_subscriptions->{74}->{$publication->magazine->id}->{$key} == $price['items'][74]->plan_id)
                                                                            <a class="btn btn-blue btn-expand btn-radius f green" style="background-color: #edfdd3;border: 1px solid #c4dba0;color: #657e3c;">{{trans('application.publication_bought')}}</a>
                                                                        @else
                                                                            <a class="btn btn-blue btn-expand btn-radius b" href="{{route('publication_checkout',[$publication->magazine->id,$price['items'][74]->id,$price['items'][74]->system_type_id,$key])}}" style="margin-bottom: 0px;">{{$price['items'][74]->price}} &euro;</a>
                                                                        @endif
                                                                    @endif
                                                                </td>

                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        @endforeach

                                    </div>
                                </div>
                                @endif

                                @if(isset($publication->magazine))
                                    <!-- Author bio -->
                                    <div class="post-bio">
                                        @if(isset($publication->magazine->thumb->cdn))
                                            <img src="{{Helpers::squareFilePath($publication->magazine->thumb->cdn)}}" alt="{{$publication->magazine->translations->$lang->title}}" width="100"/>
                                        @endif
                                        <div class="description">
                                            <a class="bio" href="{{route('publication_single',[$publication->magazine->id,Helpers::urlize($publication->magazine->translations->$lang->title)])}}">{{ isset($publication->magazine->translations->$lang->title) ? Helpers::remove_tones($publication->magazine->translations->$lang->title) : Helpers::remove_tones(current($publication->magazine->translations)->title) }}</a>
                                            @if(isset($publication->magazine->translations->$lang->quick_desc))
                                                <p>{{$publication->magazine->translations->$lang->quick_desc}}</p>
                                            @endif
                                            <div style="clear:both;"></div>
                                        </div>
                                    </div>
                                @endif
                        </div>

                        <!-- Aside -->

                        @include($template.'.partials.sidebar_publication')


                    </div>
                </section>


@endsection

@section('scripts')
    @javascript({{ asset('assets/'.$template.'/angular/publication_single_magazine.js') }})
    @javascript({{ asset('assets/'.$template.'/angular/publication_sidebar_controller.js') }})
    <script type="text/javascript">
            $(document).ready(function(){
                $('#groupSelect').on('change',function(){
                    var id = $(this).val();
                    $('.groups').hide();
                    $('#group'+id).show();
                });
            });
    </script>
@endsection
