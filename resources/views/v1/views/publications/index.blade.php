@extends($template.'.layout.master')

@section('meta_title')
|
@endsection

@section('meta_description')

@endsection

@section('content')
    <!-- Above the fold -->
                    <div id="above-the-fold" class="above-the-fold light">
                        <div class="inner-wrapper">

                             <h2 class="page-title">{{$category->title}}</h2>

                        </div><!-- /inner-wrapper -->
                    </div><!-- /above-the-fold -->

                    <!-- Section -->
                    <section id="section" ng-app="mainApp" ng-controller="userIssuesListController" ng-init="lang='<?php echo strtolower($lang);?>';category=<?php echo $cat_id;?>">
                        <div class="inner-wrapper">

                            <!-- Main -->
                            <div id="main" class="left" role="main" ng-init="fetchIssues()">


                            	<div class="block-layout-one">



                                    <!-- Form -->
                                    <form class="woocommerce-ordering">
                                        <select name="orderby" class="orderby" ng-model="sort" ng-change="sort_fetch()">
                                            <option ng-value="0" ng-selected="selected">{{trans('application.user_issues_sort_publication_date')}} </option>
                                            <option ng-value="1" >{{trans('application.user_issues_sort_title')}} </option>
                                            <option ng-value="2" >{{trans('application.user_issues_sort_price_asc')}}</option>
                                            <option ng-value="3" >{{trans('application.user_issues_sort_price_desc')}}</option>
                                        </select>
                                    </form>



                                    <!-- Block layout two / IF 1 MAGAZINES - Show the latest issue of the magazine -->
                                    <div class="block-layout-two row" ng-show="posts.length==1">
                                        <div class="grid_6">
                                            <div class="main-item">
                                                <div class="post-img" ng-show="posts[0].last_issue_cover.cdn">
                                                    <a ng-href="@{{ posts[0].render_url }}"><img ng-src="@{{ posts[0].last_issue_cover.cdn }}" alt="@{{ posts[0].title }}"/></a>
                                                    <span ng-show="posts[0].min_price != null"><a ng-href="@{{ posts[0].render_url }}">{{trans('application.homepage_subscribe_to_publication_from')}} @{{ posts[0].min_price }} &euro;</a></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid_6">
                                            <div class="main-item">
                                                <h3><a href="@{{ posts[0].render_url }}">@{{ posts[0].title }}</a></h3>
                                                <div class="spacer"></div>
                                                <p ng-show="posts[0].last_issue_cover &&  posts[0].last_issue_cover.title"><strong>@{{ posts[0].last_issue_cover.title }}</strong></p>
                                                <p ng-show="posts[0].subtitle">@{{ posts[0].subtitle }}</p>
                                                <div class="spacer"></div>
                                            </div>
                                        </div>
                                    </div>



                                   <!-- Block layout two / Show if more than 1 publication exists -->
                                   <div class="block-layout-two row" ng-show="posts.length<=4">
                                       <div class="row"  ng-repeat="group in posts | chunk:2:this">
                                            <div class="grid_6" ng-repeat="magazine in posts">
                                                <div class="main-item">
                                                    <div class="post-img" ng-show="magazine.last_issue_cover.cdn">
                                                        <a ng-href="@{{ magazine.render_url }}"><img ng-src="@{{ magazine.last_issue_cover.cdn }}" alt="@{{ magazine.title }}" /></a>
                                                        <span ng-show="magazine.min_price != null"><a href="@{{ magazine.render_url }}">{{trans('application.homepage_subscribe_to_publication_from')}} @{{ magazine.min_price }} &euro;</a></span>
                                                    </div>
                                                    <h3><a href="@{{ magazine.render_url }}">@{{ magazine.title }}</a></h3>
                                                    <p>@{{ magazine.subtitle }}</p>
                                                </div>
                                            </div>
                                        </div>
                                   </div>



                                    <!-- Products -->
                                    <div class="row"  ng-show="posts.length >4 " ng-repeat="group in posts | chunk:4:this">
                                        <ul class="products" >
                                            <li ng-class="{ 'product first': $index == 0,'product last': $index == 3, 'product': $index >0 && $index <3 }" ng-repeat="post in group">
                                                <a ng-href="@{{ post.render_url }}">
                                                    <img src="@{{ post.render_image }}" alt="@{{ post.render_title }}" ng-show="post.render_image"/>
                                                    <h3>@{{ post.render_title }}</h3>
                                                    <span class="price" ng-show="post.base_price!=null">
                                                        <span class="amount">Από @{{ post.base_price }} €</span>
                                                    </span>
                                                </a>

                                            </li>
                                        </ul>
                                    </div>






                                    <div class="row">
                                       <div class="spacer"></div>
                                       <a ng-click="fetchIssues()" class="btn btn-expand btn-large btn-custom btn-radius" ng-show="show_load_more==1">
                                            <i class="fa fa-spinner fa-spin" ng-show="spinner==1"></i>
                                            &nbsp;
                                            <span ng-show="spinner==0">{{trans('application.homepage_recent_articles_load_more')}}</span>
                                            <span ng-show="spinner==1">{{trans('application.homepage_recent_articles_loading')}}</span>
                                       </a>
                                    </div>
                                    <!-- / Show if page has at least one mobile app of kiosk with a subscription and the force register is NOT selected -->

                                    <span ng-show="posts.length == 0 && spinner==0">
                                        {{trans('application.user_issues_no_issues')}}
                                    </span>

                                </div>

                            </div><!-- /main -->

                            @include($template.'.partials.sidebar',['disable'=>true,'hide_month_log'=>true])




                        </div><!-- /inner-wrapper -->
                    </section><!-- /section -->
@endsection


@section('scripts')
    @javascript({{ asset('assets/'.$template.'/angular/publications_index.js') }})
@endsection