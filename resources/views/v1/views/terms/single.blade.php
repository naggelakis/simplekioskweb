@extends($template.'.layout.master')

@section('meta_title')
| {{$post->title}}
@endsection
@section('meta_description')
{{$post->description}}
@endsection
@section('content')
                    <!-- Above the fold -->
                    <div id="above-the-fold" class="above-the-fold @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') dark @else light @endif">
                        <div class="inner-wrapper">

                            <h2 class="page-title">{{$post->title}}</h2>

                        </div><!-- /inner-wrapper -->
                    </div><!-- /above-the-fold -->

                    <!-- Section -->
                    <section id="section">
                        <div class="inner-wrapper">

        					<!-- Main -->
                            <div id="main" role="main">

                                <!-- Post -->
                                <article class="single-post">
                                    @if(isset($post->subtitle) && $post->subtitle!=null && $post->subtitle!='' )
                                        <h3 class="lead">{{$post->subtitle}}</h3>
                                   @endif
                                    <div class="post-meta">
                                        <span class="date">
                                            {{trans('application.terms_single_last_updated_placeholder')}}
                                            <a href="javascript:void(0);">
                                                @if(isset($post->updated_at) && $post->updated_at!=null)
                                                    {{Helpers::localeDate($post->updated_at,'d/m/Y')}}
                                                @else
                                                     {{Helpers::localeDate($post->created,'d/m/Y')}}
                                                @endif
                                            </a>
                                        </span>
                                    </div>
                                    <div class="post-container">
                                        @if(isset($terms_posts) && is_array($terms_posts) && count($terms_posts)>0)
                                            <!-- / Show if terms articles are more that one -->
                                            <div class="post-left-list">
                                                <div class="widget">
                                                        <h3 class="widget-title">{{trans('application.terms_single_terms_list_title')}}</h3>
                                                        <ul class="widget-categories">
                                                            @foreach($terms_posts as $term)
                                                                <li><a href="{{route('terms_single',[$term->id,Helpers::urlize($term->title)])}}">{{$term->title}}</a></li>
                                                            @endforeach
                                                        </ul>
                                                </div>
                                            </div>
        	                            @endif
                                        <!-- / Show if terms articles are more that one -->
                                        @if(isset($post->description) && $post->description!=null && $post->description!='' )
                                            <p>
                                                {{{$post->description}}}
                                            </p>
                                        @endif
                                    </div>
                                </article>
                                <div class="spacer"></div>
                                <!-- App Description -->
                                <div class="post-bio" style="min-height:200px;">
                                    @if($app_details->icons_favicon && $app_details->icons_favicon!='' && $app_details->icons_favicon != null)
                                        <img src="{{$basic_app->app_cdn_folder.'images/'.$app_details->icons_favicon }}" alt="Author" width="100" height="100"/>
                                    @endif
                                    <div class="description">
                                        <a class="bio" href="javascript:void(0)">{{$app_details->title}}</a>
                                         @if(isset($app_details->seo_meta_desc) && $app_details->seo_meta_desc!=null && $app_details->seo_meta_desc!='')
                                                <p>{{$app_details->seo_meta_desc}}</p>
                                         @endif
                                    </div>
                                </div>

                                @if( isset($post->next) || isset($post->previous))
                                    <!-- Post controls / Show if terms articles are more that one -->
                                    <div class="post-controls">
                                        @if( isset($post->previous) )
                                            <a href="{{route('terms_single',[$post->previous->id,Helpers::urlize($post->previous->title)])}}" class="prev">
                                                <span><i class="fa fa-angle-left"></i></span>
                                                <p>{{$post->previous->title}}</p>
                                            </a>
                                        @endif
                                        @if( isset($post->next) )
                                            <a href="{{route('terms_single',[$post->next->id,Helpers::urlize($post->next->title)])}}" class="next">
                                                <span><i class="fa fa-angle-right"></i></span>
                                                <p>{{$post->next->title}}</p>
                                            </a>
                                        @endif

                                    </div>
                                @endif
                                @if(isset($profile_posts) && is_array($profile_posts) && count($profile_posts)>0)
                                <!-- Related products -->
                                <div class="block-layout-one">
                                    <p class="title"><span>Autospecialist</span></p>
                                    <div class="row">
                                         <?php $i = 0; ?>
                                        @foreach($profile_posts as $key=>$other_post)
                                            @if( $i>0 && $i%3 == 0)
                                                </div>
                                                <div class="row">
                                            @endif
                                             <div class="item grid_4 ">
                                                @if(isset($other_post->thumb))
                                                    <a href="{{route('profile_single',[$other_post->id,Helpers::urlize($other_post->title)])}}"><img src="{{Helpers::squareFilePath($other_post->thumb)}}" width="80"/></a>
                                                @endif
                                                <div>
                                                    <h3><a href="{{route('profile_single',[$other_post->id,Helpers::urlize($other_post->title)])}}">{{$other_post->title}}</a></h3>
                                                    <p>&nbsp;</p>
                                                </div>
                                                <span class="clearfix"></span>
                                            </div>
                                            <?php $i++; ?>
                                        @endforeach
                                    </div>
                                </div>
                                @endif

                            </div>


                        </div><!-- /inner-wrapper -->
                    </section><!-- /section -->

@endsection