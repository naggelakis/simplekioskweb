@if(isset($basic_app->menus))
    <?php $menus = Helpers::object_to_array($basic_app->menus); ?>
@endif
                   <!-- Header / Show the class dark or light according to the app settings -->
                   <header id="header" class=" @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='') {{  $basic_app->app_theme_skin }} @else dark  @endif @if(isset($menus[3])) header-with-menu @endif @if(!isset($banners['header_top']) || count($banners['header_top'])==0) header-without-banner @endif" role="banner">

                       @include($template.'/partials/top_menu')


                       <div class="inner-wrapper row">

                           <!-- Logo -->
                           <div id="logo">
                            @if($basic_app->icons_logo_light && $basic_app->icons_logo_light!='')
                               <h1 id="site-logo">
                                   <a href="{{route('homepage')}}">
                                       <img src="{{$basic_app->app_cdn_folder.'images/'.$basic_app->icons_logo_light}}" alt="{{$app_details->title}}">
                                   </a>
                               </h1>
                             @else
                                    <h1 id="site-logo">
                                     <a href="{{route('homepage')}}">
                                            <span>{{$app_details->title}}</span>
                                        </a>
                                    </h1>
                                    @if(isset($app_details->subtitle) && $app_details->subtitle!=null && $app_details->subtitle!='')
                                        <h2 id="site-description">{{ Helpers::get_snippet($app_details->subtitle)}}</h2>
                                    @endif

                                <span style="">{{$app_details->title}}</span>
                             @endif
                           </div>

                           @if(isset($banners['header_top']) && count($banners['header_top'])>0)
                               <!-- Ad banner / Show HEADER BANNER Image Banner (if exists - with priority) OR Code Banner (if exists) -->
                               <div class="ad-banner-728x90">
                                   <?php $head_banner = $banners['header_top'][0]; ?>
                                   @if(isset($head_banner->file_cdn))
                                        <a href="{{$head_banner->target_url}}" target="{{$head_banner->target_url_window}}">
                                            <img src="{{$head_banner->file_cdn}}" alt="{{$head_banner->description}}"/>
                                        </a>
                                   @else
                                        {{{$head_banner->content }}}
                                   @endif
                               </div>
                           @endif

                       </div>




                   </header>
                  @if(isset($menus[3]))
                        <!-- Primary navigation / Show if MAIN MENU EXISTS (no default) Show the class dark or light OPPOSITE to the app settings -->
                          @include($template.'/partials/main_menu')
                        <!-- END primary-menu -->
                  @endif