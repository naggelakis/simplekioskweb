<script src="{{asset('assets/'.$template.'/js/modernizr.min.js')}}"></script>
<script src="{{asset('assets/'.$template.'/js/easing.min.js')}}"></script>
<script src="{{asset('assets/'.$template.'/js/stickykit.min.js')}}"></script>
<script src="{{asset('assets/'.$template.'/js/flexslider.min.js')}}"></script>
<script src="{{asset('assets/'.$template.'/js/isotope.js')}}"></script>
<script src="{{asset('assets/'.$template.'/js/fitvids.min.js')}}"></script>
<!--<script src="{{asset('asssets/'.$template.'/js/contact.form.js')}}"></script>-->
<script src="{{asset('assets/'.$template.'/js/init.js')}}"></script>
<script src="{{asset('assets/'.$template.'/js/validator/jquery.validate.min.js')}}"></script>
<script src="{{asset('assets/'.$template.'/js/validator/localization/messages_'.strtolower($lang).'.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/'.$template.'/plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.5')}}"></script>
<script type="text/javascript" src="{{asset('assets/'.$template.'/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/'.$template.'/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.'.strtolower($lang).'.min.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $('#searchForm').on('submit',function(e){
                e.stopPropagation();
                var search = $('#searchField').val();
                var action = $(this).attr('action');
                if(search.length > 0){
                    var url = action.replace("QUERY", search);
                    window.location = url;
                }
                return false;
            });

            $('.section-url').on('click',function(){
                var target = $(this).attr('target');
                $('.open-close-wrapper').hide();
                $('html,body').animate({
                   scrollTop: $(target).offset().top
                });

            });

             var checkImage = function(){
                $("img").each(function() {
                    if($(this).attr("src")){
                        var image = $(this).attr("src");
                        if(image.indexOf("Ʒ") > -1 || image.indexOf("%C6%B7") > -1  ){
                            image = image.replace(new RegExp('Ʒ', 'g'),'3');
                            image = image.replace(new RegExp('%C6%B7', 'g'),'3');
                            $(this).attr("src",image);
                        }
                    }
                    if($(this).attr("ng-src")){
                        var image = $(this).attr("ng-src");
                        if(image.indexOf("Ʒ") > -1 || image.indexOf("%C6%B7") > -1  ){
                            image = image.replace(new RegExp('Ʒ', 'g'),'3');
                            image = image.replace(new RegExp('%C6%B7', 'g'),'3');
                            $(this).attr("ng-src",image);
                        }
                    }
                });
            };
            setTimeout(checkImage,500);



        });
    </script>
