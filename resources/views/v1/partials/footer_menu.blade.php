@foreach ($menus[2] as $level_1)

   @if($level_1['menu_type']==1 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile))
       <li>
           <a href="{{ route('homepage') }}">
                {{$level_1['title']}}
           </a>
       </li>
   @elseif($level_1['menu_type']==2 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- contact --}}
       <li>
            <a href="{{ route('contact') }}">
                {{$level_1['title']}}
            </a>

       </li>
   @elseif($level_1['menu_type']==3 && $level_1['class_id']==17  && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- url --}}
       <li>
            <a href="{{ $level_1['value'] }}" target="_blank">
                {{$level_1['title']}}
            </a>

       </li>
   @elseif($level_1['menu_type']==4 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- type view --}}
       <li>
            <a href="{{ url($level_1['value']) }}" >
                {{$level_1['title']}}
            </a>

       </li>
   @elseif($level_1['menu_type']==5 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- article category --}}
       <li>
            <a href="{{ route('article_category',[$level_1['value'], Helpers::urlize($level_1['category']['title'])]) }}" >
                {{$level_1['title']}}
            </a>

       </li>
   @elseif($level_1['menu_type']==6 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{--  article single--}}
       <li>
            <a href="{{ route('article_single',[$level_1['value'], Helpers::urlize($level_1['title'])]) }}" >
                {{$level_1['title']}}
            </a>

       </li>
   @elseif($level_1['menu_type']==8 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- profile index --}}
       <li>
            <a href="{{route('profile')}}">
                {{$level_1['title']}}
            </a>

       </li>
   @elseif($level_1['menu_type']==9 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- market category --}}
       <li>
            <a href="{{route('market_category',[$level_1['value'], Helpers::urlize($level_1['title'])] )}}">
                {{$level_1['title']}}
            </a>

       </li>
   @elseif($level_1['menu_type']==10 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- market single --}}
       <li>
            <a href="{{route('market_single',[$level_1['value'], Helpers::urlize($level_1['title'])] )}}">
                {{$level_1['title']}}
            </a>

       </li>
   @elseif($level_1['menu_type']==11 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- market single --}}
        <li>
            <a href="{{route('author',[$level_1['value']]) }}">
                {{$level_1['title']}}
            </a>
        </li>
   @elseif($level_1['menu_type']==12 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- profile single--}}
       <li>
            <a href="{{route('profile_single',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                {{$level_1['title']}}
            </a>
       </li>
   @elseif($level_1['menu_type']==13 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- pages single --}}
       <li >
            <a href="{{route('pages_single',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                {{$level_1['title']}}
            </a>
       </li>
   @elseif($level_1['menu_type']==14 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- pages index --}}
       <li >
            <a href="{{ route('pages_category',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                {{$level_1['title']}}
            </a>
       </li>
   @elseif($level_1['menu_type']==15 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- pages index --}}
       <li >
            <a href="{{ route('news_single',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                {{$level_1['title']}}
            </a>
       </li>
   @elseif($level_1['menu_type']==16 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- pages index --}}
       <li >
            <a href="{{ route('news_category',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                {{$level_1['title']}}
            </a>
       </li>
   @elseif(Helpers::check_group_permission($level_1['user_groups'],$user_profile) && $level_1['parent']==0 )
       <li>
            <a href="<?php echo url($level_1['value']); ?>" >
                {{$level_1['title']}}
            </a>
       </li>
   @endif

@endforeach