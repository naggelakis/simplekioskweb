                    <ul class="sub-menu">
                       @foreach($level_1['children'] as $level_2)

                           @if($level_2['menu_type']==1 && $level_2['class_id']==17 &&  Helpers::check_group_permission($level_2['user_groups'],$user_profile))
                               <li>
                                   <a href="{{ route('homepage') }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif($level_2['menu_type']==2 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- contact --}}
                               <li>
                                    <a href="{{ route('contact') }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif($level_2['menu_type']==3 && $level_2['class_id']==17   && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- url --}}
                               <li>
                                    <a href="{{ $level_2['value'] }}" target="_blank">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif($level_2['menu_type']==4 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- type view --}}
                               <li>
                                    <a href="{{ url($level_2['value']) }}" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif($level_2['menu_type']==5 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- article category --}}
                               <li>
                                    <a href="{{ route('article_category',[$level_2['value'], Helpers::urlize($level_2['category']['title'])]) }}" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif($level_2['menu_type']==6 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{--  article single--}}
                               <li>
                                    <a href="{{ route('article_single',[$level_2['value'], Helpers::urlize($level_2['title'])]) }}" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif($level_2['menu_type']==8 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- profile index --}}
                               <li>
                                    <a href="{{route('profile')}}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif($level_2['menu_type']==9 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- market category --}}
                               <li>
                                    <a href="{{route('market_category',[$level_2['value'], Helpers::urlize($level_2['title'])] )}}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif($level_2['menu_type']==10 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- market single --}}
                               <li>
                                    <a href="{{route('market_single',[$level_2['value'], Helpers::urlize($level_2['title'])] )}}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif($level_2['menu_type']==11 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- market single --}}
                                <li>
                                    <a href="{{route('author',[$level_2['value']]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                </li>
                           @elseif($level_2['menu_type']==12 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- profile single--}}
                               <li>
                                    <a href="{{route('profile_single',[$level_2['value'],Helpers::urlize($level_2['title'])]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif($level_2['menu_type']==13 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- pages single --}}
                               <li >
                                    <a href="{{route('pages_single',[$level_2['value'],Helpers::urlize($level_2['title'])]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif($level_2['menu_type']==14 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- pages index --}}
                               <li >
                                    <a href="{{ route('pages',[$level_2['value']]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @elseif(Helpers::check_group_permission($level_2['user_groups'],$user_profile)  )
                               <li>
                                    <a href="<?php echo url($level_2['value']); ?>" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                               </li>
                           @endif
                       @endforeach
                   </ul>