    @if(isset($authUser) && is_object($authUser))
        <!-- / If logged in -->
        <li><a href="javascript:void(0);"><span><i class="fa fa-user"></i>&nbsp;&nbsp;{{ Helpers::remove_tones($authUser->firstname.' '.$authUser->lastname)}}</span></a>
            <ul class="sub-menu">
                <!-- Favourites like wishlists -->
                <li><a href="{{route('user_edit')}}">{{ Helpers::remove_tones(trans('application.user_sidebar_menu_group_three_link_profile'))}}</a></li>
                <!-- Commented articles by user -->
                <li><a href="{{route('user_favorites')}}">{{ Helpers::remove_tones(trans('application.user_sidebar_menu_group_two_link_favorites'))}}</a></li>
                <!-- If Page offers subscriptions -->
                <li><a href="{{route('user_subscriptions')}}">{{ Helpers::remove_tones(trans('application.user_sidebar_menu_group_one_link_subscriptions'))}}</a></li>

                <li><a href="{{route('user_logout')}}">{{ Helpers::remove_tones(trans('application.user_sidebar_menu_group_three_link_logout'))}}</a></li>
            </ul>
        </li>
    @else
        <!-- / If not logged in -->
        <li><a href="{{route('login')}}"><i class="fa fa-user"></i>&nbsp;&nbsp;{{trans('application.top_menu_li_login_register_title')}}</a></li>
    @endif
    @if($basic_app->retail_close_purchases == 0)
    <li>
        <a href="{{route('cart')}}">
            <i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;{{trans('application.top_menu_li_cart_title')}}
       </a>
    </li>
    @endif