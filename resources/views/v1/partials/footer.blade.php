                    <footer id="footer" role="contentinfo">
                        @if(isset($basic_app->menus))
                            <?php $menus = Helpers::object_to_array($basic_app->menus); ?>
                            @if(isset($menus[2]))
                               <div class="inner-wrapper footer-quicklinks">
                                   @if($basic_app->page_info->description && $basic_app->page_info->description!='')
                                       <!-- Showcase 1 -->
                                       <div class="widget">
                                           <h3 class="widget-title">{{ Helpers::remove_tones(trans('application.footer_about_title'))}}</h3>
                                           <p>
                                             {{{$basic_app->page_info->description}}}
                                           </p>
                                       </div>
                                   @endif

                                   @if(isset($footer_featured_posts) && is_array($footer_featured_posts) && count($footer_featured_posts)>0)
                                       <!-- Subscriptions -->
                                       <div class="widget">
                                           <h3 class="widget-title">{{trans('application.footer_featured_posts_title')}}</h3>
                                           <ul class="recent-posts">
                                               @foreach($footer_featured_posts as $post)
                                                   <li>
                                                       @if(isset($post->thumb))
                                                           <div class="image">
                                                               <a href="{{$post->render_url}}"><img src="{{$post->thumb_square}}" alt="{{$post->title}}" width="80"/></a>
                                                           </div>
                                                       @endif
                                                       <div class="text">
                                                           <h3><a href="{{$post->render_url}}">{{$post->title}}</a></h3>
                                                           <p class="date">{{$post->render_date}}</p>
                                                           <br>
                                                           <p>
                                                                @if(isset($post->subtitle) && $post->subtitle!=null && $post->subtitle!='')
                                                                    {{Helpers::get_snippet($post->subtitle)}}[...]
                                                                @endif
                                                           </p>
                                                       </div>
                                                   </li>
                                               @endforeach
                                           </ul>
                                       </div>
                                   @endif
                                   <!-- Contact -->
                                   <div class="widget">
                                       <h3 class="widget-title">{{ Helpers::remove_tones(trans('application.footer_contact_title'))}}</h3>
                                       <ul class="recent-comments">

                                           @if(count($addresses)>0 || isset($appPhone) || isset($appEmail))
                                               <li>
                                                   <p class="author">{{Helpers::remove_tones(trans('application.footer_contact_details_title'))}}</p>
                                                   <h3>
                                                    @if(isset($addresses) && count($addresses)>0)
                                                        <?php $adress=$addresses;?>
                                                        <a href="#">{{$adress[0]->line1.' '.$adress[0]->line2}}, {{$adress[0]->city.' '.$adress[0]->zip.', '.$adress[0]->country}}</a><br>
                                                    @endif
                                                    @if(isset($appPhone) && $appPhone!='')
                                                        <a href="tel:{{$appPhone}};">T. {{$appPhone}}</a><br>
                                                    @endif
                                                    @if(isset($appEmail) && $appEmail!='')
                                                        <a href="mail:{{$appEmail}}">E. {{$appEmail}}</a><br>
                                                    @endif
                                                </h3>
                                               </li>
                                           @endif

                                           <?php if(isset($this->socialMedia) && count($this->socialMedia)){ ?>
                                                        <div class="col-xs-6 text-right">
                                                            <ul class="list-inline social-list">
                                                                <?php foreach($this->socialMedia as $social): ?>
                                                                    <li>
                                                                        <a data-toggle="tooltip" data-placement="top" title="Follow" data-title="Follow" href="<?php echo $social->follow_link;?>">
                                                                            <?php if($social->social_id==1):?>
                                                                                <i class="fa fa-facebook"></i>
                                                                            <?php elseif($social->social_id==2):?>
                                                                                <i class="fa fa-twitter"></i>
                                                                            <?php elseif($social->social_id==4):?>
                                                                                <i class="fa fa-pinterest-square"></i>
                                                                            <?php endif; ?>
                                                                        </a>
                                                                    </li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        </div>
                                                    <?php } ?>

                                           @if(isset($socialMedia) && count($socialMedia))
                                           <li>
                                               <p class="author">{{Helpers::remove_tones(trans('application.footer_social_profiles_title'))}}</p>
                                               <h3>
                                                    @foreach($socialMedia as $social)
                                                        @if($social->social_id==1)
                                                            <?php $icon = 'fa-facebook-square'; ?>
                                                        @elseif($social->social_id==2)
                                                            <?php $icon = 'fa-twitter-square'; ?>
                                                        @elseif($social->social_id==3)
                                                            <?php $icon = 'fa-google-plus-square'; ?>
                                                        @elseif($social->social_id==4)
                                                            <?php $icon = 'fa-pinterest-square'; ?>
                                                        @elseif($social->social_id==5)
                                                            <?php $icon = 'fa-instagram'; ?>
                                                        @elseif($social->social_id==6)
                                                            <?php $icon = 'fa-foursquare'; ?>
                                                        @elseif($social->social_id==7)
                                                            <?php $icon = 'fa-linkedin'; ?>
                                                        @endif
                                                        <a style="margin-right:10px;" title="Follow" target="_blank" data-title="Follow" href="<?php echo $social->follow_link;?>">
                                                            <i class="fa {{$icon}} fa-2x"></i>
                                                        </a>
                                                    @endforeach
                                                </h3>
                                           </li>
                                           @endif
                                       </ul>
                                   </div>

                                   @if(isset($basic_app->menus))
                                        <?php $menus = Helpers::object_to_array($basic_app->menus); ?>
                                        @if(isset($menus[2]))
                                           <!-- Footer Menu -->
                                           <div class="widget">
                                               <h3 class="widget-title">{{ trans('application.footer_menu_title') }}</h3>
                                               <ul class="widget-categories">
                                                   @include($template.'/partials/footer_menu')
                                               </ul>
                                           </div>
                                        @endif
                                   @endif
                               </div>
                            @endif
                        @endif

                       <!-- Copyright -->
                       <div id="copyright">
                           <div class="inner-wrapper">
                               <div class="row">
                                   <div class="grid_6">
                                        @if(isset($socialMedia) && count($socialMedia))
                                        <ul class="icon-socials">

                                              @foreach($socialMedia as $social)
                                                    @if($social->social_id==1)
                                                        <?php $icon = 'fa-facebook'; ?>
                                                        <?php $text = 'Facebook'; ?>
                                                        <?php $class = 'icon-socials-facebook'; ?>
                                                    @elseif($social->social_id==2)
                                                        <?php $text = 'Twitter'; ?>
                                                        <?php $icon = 'fa-twitter'; ?>
                                                        <?php $class = 'icon-socials-twitter'; ?>
                                                    @elseif($social->social_id==3)
                                                        <?php $text = 'Google'; ?>
                                                        <?php $icon = 'fa-google'; ?>
                                                        <?php $class = 'icon-socials-google'; ?>
                                                    @elseif($social->social_id==4)
                                                        <?php $text = 'Pinterest'; ?>
                                                        <?php $icon = 'fa-pinterest-square'; ?>
                                                        <?php $class = 'icon-socials-pinterest'; ?>
                                                    @elseif($social->social_id==5)
                                                        <?php $text = 'Instagram'; ?>
                                                        <?php $icon = 'fa-instagram'; ?>
                                                        <?php $class = 'icon-socials-instagram'; ?>
                                                    @elseif($social->social_id==6)
                                                        <?php $icon = 'fa-foursquare'; ?>
                                                        <?php $text = 'Foursquare'; ?>
                                                        <?php $class = 'icon-socials-foursquare'; ?>
                                                    @elseif($social->social_id==7)
                                                        <?php $icon = 'fa-linkedin'; ?>
                                                        <?php $text = 'Linkedin'; ?>
                                                        <?php $class = 'icon-socials-linkedin'; ?>
                                                    @endif
                                                    <li><a href="<?php echo $social->follow_link;?>"  class="{{$class}}" target="_blank"><i class="fa {{$icon}}"></i><span>{{$text}}</span></a></li>
                                                @endforeach
                                        </ul>
                                        @endif
                                         <div class="clearfix"></div>
                                         <p class="copyright-notice"> &copy; Copyright {{ date("Y") }} @if($basic_app->page_info->official_name && $basic_app->page_info->official_name!='') {{$basic_app->page_info->official_name}} @else {{ $basic_app->page_info->title }} @endif {{ trans('application.footer_legal_placeholder') }}</p>
                                         <ul class="copyright-menu">
                                            <li><a href="{{route('homepage')}}">{{trans('application.profile_footer_homepage')}}</a></li>
                                            <li><a href="{{route('profile')}}">{{trans('application.profile_footer_title')}}</a></li>
                                            <li><a href="{{route('contact')}}">{{trans('application.contact_footer_title')}}</a></li>
                                            <li><a href="#">Site Map</a></li>
                                            @if(isset($terms) && count($terms)>0) <li><a href="<?php echo route('terms');?>"><?php echo trans('application.terms_footer_title');?></a></li> @endif
                                         </ul>
                                   </div>
                                   <div class="grid_6">
                                        <ul class="icon-badges">
                                            @if(isset($basic_app->settings_googleplay_link) && $basic_app->settings_googleplay_link!=null)
                                                <li><a href="{{$basic_app->settings_googleplay_link}}" target="_blank"><img class="img_badge" src="{{asset('assets/'.$template.'/images/googleplay.png')}}" alt="{{trans('application.footer_third_party_google')}}"></a></li>
                                            @endif
                                            @if(isset($basic_app->settings_appstore_link) && $basic_app->settings_appstore_link!=null)
                                                <li><a href="{{$basic_app->settings_appstore_link}}" target="_blank"><img class="img_badge" src="{{asset('assets/'.$template.'/images/appstore.png')}}" alt="{{trans('application.footer_third_party_apple')}}"></a></li>
                                            @endif
                                        </ul>
                                        <div class="clearfix"></div>
                                        <p class="copyright-thirdparty">{{trans('application.footer_third_party_declaimer')}}</p>
                                        @if(isset($basic_app->payment_methods) && is_array($basic_app->payment_methods) && count($basic_app->payment_methods)>0)
                                            @foreach($basic_app->payment_methods as $method)
                                                @if($method->sys_payment_id == 1 || $method->sys_payment_id == 2)
                                                    <ul class="icon-payments">
                                                        <li><i class="fa fa-cc-visa fa-3x"></i></li>
                                                        <li><i class="fa fa-cc-mastercard fa-3x"></i></li>
                                                        <li><i class="fa fa-cc-amex fa-3x"></i></li>
                                                        <li><i class="fa fa-cc-paypal fa-3x"></i></li>
                                                    </ul>
                                                    @break;
                                                @endif
                                            @endforeach

                                        @endif
                                        <div class="spacer"></div>
                                   </div>
                               </div>
                               <div class="row">
                                    <a href="http://www.loggia.gr/simplekiosk/index.html" target="_blank" class="text-center simplekiosk-img"><img src="{{asset('assets/'.$template.'/images/simplekiosk.png')}}" alt="{{trans('application.footer_simple_kiosk_alt')}}"></a>
                                    <p class="simplekiosk-notice">{{trans('application.footer_simple_kiosk_placeholder')}}</p>
                                  </div>
                           </div>
                       </div>
                   </footer>