<div class="post-subscription">
    @if($lock_status == 'users')
        <div class="alert yellow"><strong>{{trans('application.article_locked_message_users_registered_title')}}</strong><br>{{trans('application.article_locked_message_users_registered_placeholder')}}  <br><br><a class="btn btn-blue btn-expand btn-radius" href="{{route('login')}}/?redirect={{Request::url()}}" style="margin-bottom: 0px;">{{trans('application.article_locked_message_users_registered_login')}} </a></div>
    @elseif($lock_status == 'locked')
    <div class="alert red"><strong>{{trans('application.article_locked_message_locked_title')}}</strong> {{trans('application.article_locked_message_locked_placeholder')}}</div>
    @else
    <div class="alert red"><strong>{{trans('application.article_locked_message_subscribers_title')}}</strong> {{trans('application.article_locked_message_subscribers_placeholder')}}</div>

                                        @if(isset($plans) && is_array($plans) && count($plans)>0)
        			                        <div class="accordion">
                                                    @foreach($plans as $recurring_plan)
                                                        <div class="title">{{$recurring_plan['title']}}: {{$recurring_plan['period_translation']}}</div>
                                                        <div class="content">
                                                        <div class="subscription-table">
                                                            <table>

                                                                <thead>
                                                                    <tr>
                                                                        <th></th>
                                                                        <th class="subscription-table-column">{{trans('application.syst_class_market_publication_type_all')}} </th>
                                                                        <th class="subscription-table-column">{{trans('application.syst_class_market_publication_type_print')}} </th>
                                                                        <th class="subscription-table-column">{{trans('application.syst_class_market_publication_type_electronic')}} </th>
                                                                    </tr>
                                                                </thead>

                                                                <tbody>
                                                                    @foreach($recurring_plan['prices'] as $key=>$price)
                                                                        <tr>
                                                                            <td><strong>{{$price['group_translation']}}</strong></td>

                                                                            <td>
                                                                                @if(isset($price['items'][72]))
                                                                                    <a class="btn btn-blue btn-expand btn-radius" href="{{ route('add_to_cart_recurring',[$key,$price['items'][72]->id])}}" style="margin-bottom: 0px;">{{$price['items'][72]->price}} &euro;</a>
                                                                                @endif
                                                                            </td>
                                                                            <td>
                                                                                @if(isset($price['items'][73]))
                                                                                    <a class="btn btn-blue btn-expand btn-radius" href="{{route('add_to_cart_recurring',[$key,$price['items'][73]->id])}}" style="margin-bottom: 0px;">{{$price['items'][73]->price}} &euro;</a>
                                                                                @endif
                                                                            </td>
                                                                            <td>
                                                                                @if(isset($price['items'][74]))
                                                                                    <a class="btn btn-blue btn-expand btn-radius" href="{{route('add_to_cart_recurring',[$key,$price['items'][74]->id])}}" style="margin-bottom: 0px;">{{$price['items'][74]->price}} &euro;</a>
                                                                                @endif
                                                                            </td>

                                                                        </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    @endforeach

                                            @endif
    @endif
</div>