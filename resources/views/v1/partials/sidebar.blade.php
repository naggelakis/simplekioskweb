<aside id="sidebar" role="complementary" @if(!isset($disable)) ng-app="mainApp" @endif ng-controller="sidebarCtrl" ng-init="lang='{{strtolower($lang)}}'">
   @if(!isset($disable_search))
       <div class="widget">
           <h3 class="widget-title">{{trans('application.sidebar_search_widget_title')}}</h3>
           <form class="searchform" id="searchForm" action="{{route('search',['QUERY'])}}">
               <input type="text" name="search" placeholder="{{trans('application.sidebar_search_widget_placeholder')}}" id="searchField">
               <input type="submit" value="">
           </form>
       </div>
   @endif

   <!-- Banner 300x250 / Show if Image Sidebar Banner exists / Show only the first in line -->
   @if(isset($banners['sidebar_top']) && count($banners['sidebar_top'])>0)
      <div class="widget">
          <div class="ad-banner-300x250">
              <?php $sidebar_top = $banners['sidebar_top'][0]; ?>
              @if(isset($sidebar_top->file_cdn))
                   <a href="{{$sidebar_top->target_url}}" target="{{$sidebar_top->target_url_window}}">
                       <img src="{{$sidebar_top->file_cdn}}" alt="{{$sidebar_top->description}}"/>
                   </a>
              @else
                   {{{$sidebar_top->content }}}
              @endif
          </div>
      </div>
    @endif



   <!-- Tabs / Show tabs with Video articles and Gallery articles / If none is active the show most popular -->
   <div class="widget"  ng-init="sidebar_tabs()" ng-show=" gallery_posts.length > 0">
       <div class="tabs">
           <ul>
               <!--<li ><a href="#tab-videos">Videos</a></li>-->
               <li ng-show="gallery_posts.length > 0"><a href="#tab-galleries">{{ trans('application.homepage_sidebar_tabs_galleries_title') }}</a></li>
               <li ng-show="comment_posts.length > 0"><a href="#tab-comments">{{ trans('application.homepage_sidebar_tabs_comments_title') }}</a></li>
           </ul>
           <!--<div id="tab-videos">
               <ul class="recent-posts">
                   <li>
                       <div class="image">
                           <a href="#"><img src="http://placehold.it/80x65" alt="Post"/></a>
                       </div>
                       <div class="text">
                           <h3><a href="#">The Giro d'Italia comes to Northern Ireland</a></h3>
                           <p class="date">September 16, 2104</p>
                       </div>
                   </li>
                   <li>
                       <div class="image">
                           <a href="#"><img src="http://placehold.it/80x65" alt="Post"/></a>
                       </div>
                       <div class="text">
                           <h3><a href="#">The Giro d'Italia comes to Northern Ireland</a></h3>
                           <p class="date">September 16, 2104</p>
                       </div>
                   </li>
                   <li>
                       <div class="image">
                           <a href="#"><img src="http://placehold.it/80x65" alt="Post"/></a>
                       </div>
                       <div class="text">
                           <h3><a href="#">The Giro d'Italia comes to Northern Ireland</a></h3>
                           <p class="date">September 16, 2104</p>
                       </div>
                   </li>
               </ul>
               <br>
               <a class="btn btn-small btn-custom" href="#"><i class="fa fa-video-camera"></i>&nbsp;See all videos</a>
           </div>-->
           <div id="tab-galleries" ng-show="gallery_posts.length > 0">
               <ul class="recent-posts">
                   <li ng-repeat="galleryPost in gallery_posts">
                       <div class="image" ng-show="galleryPost.thumb">
                           <a ng-href="@{{ galleryPost.render_url }}"><img ng-src="@{{ galleryPost.thumb_square }}" alt="@{{ galleryPost.title }}" width="50"/></a>
                       </div>
                       <div class="text">
                           <h3><a ng-href="@{{ galleryPost.render_url }}">@{{ galleryPost.title }}</a></h3>
                           <p class="date">@{{ galleryPost.render_date }}</p>
                       </div>
                   </li>

               </ul>

           </div>
           <div id="tab-comments" ng-show="comment_posts.length > 0">
               <ul class="recent-comments">
                   <li ng-repeat="commentPost in comment_posts | limitTo: 5">
                       <p class="author">@{{ commentPost.authors[0].name }}</p>
                       <h3><a ng-href="@{{ commentPost.permalink }}">@{{ commentPost.content | htmlToPlaintext | limitTo: 20 }} @{{ (commentPost.content | htmlToPlaintext).length > 20 ? '...' : ''}} </a></h3>
                   </li>

               </ul>
           </div>
       </div>
   </div>

    @if(!isset($hide_month_log))
       <!-- Archive widget / Show this if articles on page are more than 100 and span across more than 5 months / Show last five months that has articles -->
       <div class="widget" ng-show="month_log.length>0" ng-init="log_month_fetch()">
           <h3 class="widget-title">{{trans('application.homepage_month_log_title')}}</h3>
           <ul class="widget-archive">
               <li ng-repeat="log in month_log" ng-show="log.count>0">
                    <a ng-href="@{{ log.url }}">@{{ log.name }}</a> (@{{ log.count }})
               </li>
           </ul>
       </div>
   @endif

   @if(isset($banners['sidebar_mid']) && count($banners['sidebar_mid'])>0)
        <div class="widget">
            <h3 class="widget-title">{{trans('application.homepage_sidebar_mid_banners_title')}}</h3>
            @foreach(($banners['sidebar_mid']) as $sidebar_mid)
            <div class="ad-banner-300x250"  style="margin-bottom:20px;">
                @if(isset($sidebar_mid->file_cdn))
                     <a href="{{$sidebar_mid->target_url}}" target="{{$sidebar_mid->target_url_window}}">
                         <img src="{{$sidebar_mid->file_cdn}}" alt="{{$sidebar_mid->description}}"/>
                     </a>
                @else
                     {{{$sidebar_mid->content }}}
                @endif
            </div>
             @endforeach
        </div>
   @endif



   <!-- Tag cloud widget / Show the 20 most used tags --
   <div class="widget">
       <h3 class="widget-title">Ετικετες</h3>
       <div class="tagcloud">
           <div><a href="#">design</a><span>3</span></div>
           <div><a href="#">photography</a><span>16</span></div>
           <div><a href="#">tutorial</a><span>23</span></div>
           <div><a href="#">css3</a><span>1</span></div>
           <div><a href="#">photoshop</a><span>6</span></div>
           <div><a href="#">show</a><span>14</span></div>
           <div><a href="#">business</a><span>63</span></div>
           <div><a href="#">code</a><span>43</span></div>
           <div><a href="#">magazine</a><span>22</span></div>
           <div><a href="#">premium</a><span>10</span></div>
           <div><a href="#">theme</a><span>2</span></div>
           <div><a href="#">development</a><span>9</span></div>
           <div><a href="#">illustration</a><span>17</span></div>
           <div><a href="#">school</a><span>8</span></div>
           <div><a href="#">culture</a><span>1</span></div>
           <div><a href="#">fashion</a><span>5</span></div>
           <div><a href="#">music</a><span>63</span></div>
           <div><a href="#">technology</a><span>1</span></div>
       </div>
   </div>

   <!-- Banner 300x250 / Show if Google Sidebar Banner exists -->

   @if(isset($banners['sidebar_bottom']) && count($banners['sidebar_bottom'])>0)
        <div class="widget">
            <h3 class="widget-title">{{trans('application.homepage_sidebar_bottom_banners_title')}}</h3>
            @foreach(($banners['sidebar_bottom']) as $sidebar_bottom)
            <div class="ad-banner-300x250" style="margin-bottom:20px;">
                @if(isset($sidebar_bottom->file_cdn))
                     <a href="{{$sidebar_bottom->target_url}}" target="{{$sidebar_bottom->target_url_window}}">
                         <img src="{{$sidebar_bottom->file_cdn}}" alt="{{$sidebar_bottom->description}}"/>
                     </a>
                @else
                     {{{$sidebar_bottom->content }}}
                @endif
            </div>
             @endforeach
        </div>
   @endif

</aside>

@if(!isset($disable))
    @javascript({{ asset('assets/'.$template.'/angular/sidebar.js') }})
@endif
