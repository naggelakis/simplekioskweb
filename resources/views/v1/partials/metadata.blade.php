        <!-- Site title / The App title for home page and then Category, Article, Product, Subscription or static page -->
        <title>{{$app_details->title}} | {{$basic_app->page_info->subtitle}} @yield('meta_title')</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <!-- Meta tags / Meta description and keywords from the app and then if exists in Category, Article, Product, Subscription or static page -->
        <meta name="description" content="{{$app_details->seo_meta_desc}}  @yield('meta_description')">

        <meta name="tags" content="{{$app_details->seo_meta_tags}}  @yield('meta_tags')">


        @if($app_details->icons_favicon && $app_details->icons_favicon!='')
	        <link rel="shortcut icon" href="{{$basic_app->app_cdn_folder.'images/'.$app_details->icons_favicon }}">
        @endif

        @if($app_details->icons_phone && $app_details->icons_phone!='')
                <!-- For non-Retina iPhone, iPod Touch, and Android 2.2+ devices: -->
            <link rel="apple-touch-icon" href="{{ $basic_app->app_cdn_folder.'images/'.$app_details->icons_phone }}">
            <!-- For first-generation iPad: -->
            <link rel="apple-touch-icon" sizes="72x72" href="{{ Helpers::extendedFilePath($basic_app->app_cdn_folder.'images/'.$app_details->icons_phone,'_72') }} ">
            <!-- For iPhone 4 with high-resolution Retina display: -->
            <link rel="apple-touch-icon" sizes="114x114" href="{{ Helpers::extendedFilePath($basic_app->app_cdn_folder.'images/'.$app_details->icons_phone,'_114') }}">
            <link rel="apple-touch-icon" sizes="144x144" href="{{ Helpers::extendedFilePath($basic_app->app_cdn_folder.'images/'.$app_details->icons_phone,'_144') }}">
        @endif


