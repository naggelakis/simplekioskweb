@if(isset($basic_app->menus))
    <?php $menus = Helpers::object_to_array($basic_app->menus); ?>
    {{--Helpers::die_pre($menus)--}}

            <nav class="primary-menu dark sticky-menu" role="navigation">
               <div class="inner-wrapper">

                   <!-- Responsive menu -->
                   <a class="click-to-open-menu"><i class="fa fa-align-justify" style="line-height: 55px;"></i></a>

                   <!-- Main menu -->
                   <ul class="main-menu">
                    @if(isset($menus[3]))
                                    @foreach ($menus[3] as $level_1)
                                       @if($level_1['menu_type']==1 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile))
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                               <a href="{{ route('homepage') }}">
                                                     {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                               </a>
                                               @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                               @endif
                                           </li>
                                       @elseif($level_1['menu_type']==2 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- contact --}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{ route('contact') }}">
                                                    {{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @elseif($level_1['menu_type']==3 && $level_1['class_id']==17  && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- url --}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{ $level_1['value'] }}" target="_blank">
                                                     {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @elseif($level_1['menu_type']==4 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- type view --}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{ url($level_1['value']) }}" >
                                                    {{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @elseif($level_1['menu_type']==5 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- article category --}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{ route('article_category',[$level_1['value'], Helpers::urlize($level_1['category']['title'])]) }}" >
                                                     {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @elseif($level_1['menu_type']==6 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{--  article single--}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{ route('article_single',[$level_1['value'], Helpers::urlize($level_1['title'])]) }}" >
                                                     {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @elseif($level_1['menu_type']==8 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- profile index --}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{route('profile')}}">
                                                     {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @elseif($level_1['menu_type']==9 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- market category --}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{route('market_category',[$level_1['value'], Helpers::urlize($level_1['title'])] )}}">
                                                     {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @elseif($level_1['menu_type']==10 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- market single --}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{route('market_single',[$level_1['value'], Helpers::urlize($level_1['title'])] )}}">
                                                     {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @elseif($level_1['menu_type']==11 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- market single --}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{route('author',[$level_1['value']]) }}">
                                                     {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                            </li>
                                       @elseif($level_1['menu_type']==12 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- profile single--}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{route('profile_single',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                                                     {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @elseif($level_1['menu_type']==13 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- pages single --}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{route('pages_single',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                                                    {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @elseif($level_1['menu_type']==14 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- pages index --}}
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="{{ route('pages_category',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                                                    {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @elseif($level_1['menu_type']==15 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- news single --}}
                                          <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                               <a href="{{ route('news_single',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                                                   {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                               </a>
                                               @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                   @include($template.'/partials/main_menu_subcategories')
                                               @endif
                                          </li>
                                       @elseif($level_1['menu_type']==16 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- news single --}}
                                         <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                              <a href="{{ route('news_category',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                                                  {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                              </a>
                                              @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                  @include($template.'/partials/main_menu_subcategories')
                                              @endif
                                         </li>
                                       @elseif(Helpers::check_group_permission($level_1['user_groups'],$user_profile) && $level_1['parent']==0 )
                                           <li @if($level_1['style']=='megamenu')class="mega-menu-full"@endif>
                                                <a href="<?php echo url($level_1['value']); ?>" >
                                                     {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                                                </a>
                                                @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                                    @include($template.'/partials/main_menu_subcategories')
                                                @endif
                                           </li>
                                       @endif

                                    @endforeach
                    @else
                            <li >
                                 <a href="{{route('homepage')}}">{{Helpers::remove_tones(strtoupper( trans('application.menu_homepage_title') ))}}</a>
                            </li>
                            <li >
                                 <a href="{{route('contact')}}">{{Helpers::remove_tones(strtoupper(trans('application.menu_contact_title')))}}</a>
                            </li>
                    @endif
                    <!--


                    <li class="mega-menu-full">
                        <a href="#"><span><i class="fa fa-th"></i> Ενοτητες</span></a>
                        <ul class="menu-blocks row">
                            <li class="grid_4">
                                <ul class="menu-content category-menu">
                                    <li><a href="#">Advertising</a></li>
                                    <li><a href="#">Creativity</a></li>
                                    <li><a href="#">Design</a></li>
                                    <li><a href="#">Interactive Marketing</a></li>
                                    <li><a href="#">Marketing</a></li>
                                    <li><a href="#">Media</a></li>
                                    <li><a href="#">Public Relations</a></li>
                                    <li><a href="#">Awards</a></li>
                                    <li><a href="#">Market Statistics</a></li>
                                </ul>
                            </li>
                            <li class="grid_4">
                                <ul class="menu-content featured-post">
                                    <li>
                                        <div class="block-layout-two">
                                            <div class="main-item">
                                                <div class="post-img">
                                                    <a href="#"><img src="{{asset('assets/'.$template.'/demo/nav_mega_featured_01.jpg')}}" alt="Θετική πορεία για τα media shops"/></a>
                                                    <span><a href="#">Προτεινομενα</a></span>
                                                </div>
                                                <h3><a href="#">Θετική πορεία για τα media shops</a></h3>
                                                <p>Trending topic η Θεσσαλονίκη, o Όλυμπος και το Discover Greece μέσα από τις εικόνες και ιστορίες που αναμεταδίδουν 45 bloggers σε όλο τον κόσμο …</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            <li class="grid_4">
                                <ul class="menu-content article-list">
                                    <li>
                                        <ul class="recent-posts">
                                            <li>
                                                <div class="image">
                                                    <a href="#"><img src="{{asset('assets/'.$template.'/demo/80x65.gif')}}" alt="Post"/></a>
                                                </div>
                                                <div class="text">
                                                    <h3><a href="#">Etiam luctus neque vel enim molestie pretium</a></h3>
                                                    <p class="date">September 16, 2104</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="image">
                                                    <a href="#"><img src="{{asset('assets/'.$template.'/demo/80x65.gif')}}" alt="Post"/></a>
                                                </div>
                                                <div class="text">
                                                    <h3><a href="#">Etiam luctus neque vel enim molestie pretium</a></h3>
                                                    <p class="date">September 16, 2104</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="image">
                                                    <a href="#"><img src="{{asset('assets/'.$template.'/demo/80x65.gif')}}" alt="Post"/></a>
                                                </div>
                                                <div class="text">
                                                    <h3><a href="#">Etiam luctus neque vel enim molestie pretium</a></h3>
                                                    <p class="date">September 16, 2104</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="image">
                                                    <a href="#"><img src="{{asset('assets/'.$template.'/demo/80x65.gif')}}" alt="Post"/></a>
                                                </div>
                                                <div class="text">
                                                    <h3><a href="#">Etiam luctus neque vel enim molestie pretium</a></h3>
                                                    <p class="date">September 16, 2104</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="image">
                                                    <a href="#"><img src="{{asset('assets/'.$template.'/demo/80x65.gif')}}" alt="Post"/></a>
                                                </div>
                                                <div class="text">
                                                    <h3><a href="#">Etiam luctus neque vel enim molestie pretium</a></h3>
                                                    <p class="date">September 16, 2104</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    -->
                   </ul>
               </div>
            </nav>
@endif