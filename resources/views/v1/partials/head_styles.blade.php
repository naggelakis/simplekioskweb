  <!-- Css Styles -->
        <link rel="stylesheet" href="{{asset('assets/'.$template.'/css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('assets/'.$template.'/css/style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/'.$template.'/css/layout.css')}}">

        <!-- Colors style / Link to a PHP File -->
        @if(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light')
                <link rel="stylesheet" href="{{asset('assets/'.$template.'/css/colors_light.css')}}">
        @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='dark') light @elseif(isset($basic_app->app_theme_skin) && $basic_app->app_theme_skin!=null && $basic_app->app_theme_skin!='light')
                <link rel="stylesheet" href="{{asset('assets/'.$template.'/css/colors.css')}}">
        @else
                <link rel="stylesheet" href="{{asset('assets/'.$template.'/css/colors.css')}}">
        @endif

        @if(isset($basic_app->app_theme_base_color) && $basic_app->app_theme_base_color!=null && $basic_app->app_theme_base_color!='')
                @include($template.'.partials.custom_css')
        @endif
        <!-- Retina icons -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <!-- Responsive style -->
        <link rel="stylesheet" media="(min-width:0px) and (max-width:760px)"  href="{{asset('assets/'.$template.'/css/mobile.css')}}">
        <link rel="stylesheet" media="(min-width:761px) and (max-width:1080px)" href="{{asset('assets/'.$template.'/css/720.css')}}">
        <link rel="stylesheet" media="(min-width:1081px) and (max-width:1300px)" href="{{asset('assets/'.$template.'/css/960.css')}}">
        <link rel="stylesheet" media="(min-width:1301px)" href="{{asset('assets/'.$template.'/css/1200.css')}}">
        <link rel="stylesheet" href="{{asset('assets/'.$template.'/plugins/bootstrap-datepicker/css/bootstrap-datepicker.standalone.css')}}" type="text/css" media="screen" />

        <!-- Overide style / Link to a PHP File -->
        <link rel="stylesheet" href="{{asset('assets/'.$template.'/css/loggia.css')}}">

        <!-- Fonts library / Link to a PHP File -->
        <link rel="stylesheet" href="{{asset('assets/'.$template.'/css/fonts.css')}}">
        <link rel="stylesheet" href="{{asset('assets/'.$template.'/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5')}}" type="text/css" media="screen" />
        <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
        <!-- / IF BACKGROUND BANNER IS ACTIVE -->
        <style>
	    	@media (min-width:1081px) {
				#wrapper.boxed_add {
					background-color: #82CBB8;
					background-image:   url('{{asset('assets/'.$template.'/demo/add_center.jpg')}}');
				}
			}

			.heart { color: <?php echo  $basic_app->app_theme_base_color!=null ? '#'.$basic_app->app_theme_base_color : 'red';?>!important;}
            .aheart{color:#AAA;}
	    </style>