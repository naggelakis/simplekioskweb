@if(isset($basic_app->menus))
    <?php $menus = Helpers::object_to_array($basic_app->menus); ?>
    {{--Helpers::die_pre($menus)--}}
    @if(isset($menus[1]))
        <!-- Top bar / Show if App allows for users register or has a top menu / Show the class dark or light according to the app settings -->
        <div class="top-bar dark">
           <div class="inner-wrapper">

           <!-- Responsive menu -->
           <a class="click-to-open-menu"><i class="fa fa-align-justify" style="line-height: 41px;"></i></a>
           <!-- Top menu -->
           <nav class="top-menu" role="navigation">
               <ul class="top-menu">
                @foreach ($menus[1] as $level_1)

                   @if($level_1['menu_type']==1 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile))
                       <li>
                           <a href="{{ route('homepage') }}">
                                 {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                           </a>
                           @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                           @endif
                       </li>
                   @elseif($level_1['menu_type']==2 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- contact --}}
                       <li>
                            <a href="{{ route('contact') }}">
                                {{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @elseif($level_1['menu_type']==3 && $level_1['class_id']==17  && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- url --}}
                       <li>
                            <a href="{{ $level_1['value'] }}" target="_blank">
                                 {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @elseif($level_1['menu_type']==4 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- type view --}}
                       <li>
                            <a href="{{ url($level_1['value']) }}" >
                                {{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @elseif($level_1['menu_type']==5 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- article category --}}
                       <li>
                            <a href="{{ route('article_category',[$level_1['value'], Helpers::urlize($level_1['category']['title'])]) }}" >
                                 {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @elseif($level_1['menu_type']==6 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{--  article single--}}
                       <li>
                            <a href="{{ route('article_single',[$level_1['value'], Helpers::urlize($level_1['title'])]) }}" >
                                 {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @elseif($level_1['menu_type']==8 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- profile index --}}
                       <li>
                            <a href="{{route('profile')}}">
                                 {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @elseif($level_1['menu_type']==9 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- market category --}}
                       <li>
                            <a href="{{route('market_category',[$level_1['value'], Helpers::urlize($level_1['title'])] )}}">
                                 {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @elseif($level_1['menu_type']==10 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- market single --}}
                       <li>
                            <a href="{{route('market_single',[$level_1['value'], Helpers::urlize($level_1['title'])] )}}">
                                 {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @elseif($level_1['menu_type']==11 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- market single --}}
                        <li>
                            <a href="{{route('author',[$level_1['value']]) }}">
                                 {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                        </li>
                   @elseif($level_1['menu_type']==12 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- profile single--}}
                       <li>
                            <a href="{{route('profile_single',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                                 {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @elseif($level_1['menu_type']==13 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- pages single --}}

                       <li >
                            <a href="{{route('pages_single',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                                {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @elseif($level_1['menu_type']==14 && $level_1['class_id']==17 && $level_1['parent']==0 && Helpers::check_group_permission($level_1['user_groups'],$user_profile)) {{-- pages index --}}
                       <li >
                            <a href="{{ route('pages_category',[$level_1['value'],Helpers::urlize($level_1['title'])]) }}">
                                {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @elseif(Helpers::check_group_permission($level_1['user_groups'],$user_profile) && $level_1['parent']==0 )
                       <li>
                            <a href="<?php echo url($level_1['value']); ?>" >
                                 {{{ isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children']) ? '<span>'. Helpers::remove_tones(strtoupper($level_1['title'])).'</span>': Helpers::remove_tones(strtoupper($level_1['title'])) }}}
                            </a>
                            @if(isset($level_1['children']) && is_array($level_1['children']) && count($level_1['children'])>0)
                                @include($template.'/partials/top_menu_subcategories')
                            @endif
                       </li>
                   @endif

                @endforeach

                @if($app_details->settings_allowregister==1)
                    @include($template.'/partials/top_user_li')
                @endif
               </ul>
           </nav>

       </div>
        </div>
    <!-- END top-bar -->
    @endif
@endif