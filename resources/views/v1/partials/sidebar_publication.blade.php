    <aside id="sidebar" role="complementary"  ng-controller="sidebarCtrl" ng-init="setVars('{{strtolower($lang)}}')">
       <div class="widget">
           <h3 class="widget-title">{{trans('application.sidebar_search_widget_title')}}</h3>
           <form class="searchform" id="searchForm" action="{{route('search',['QUERY'])}}">
               <input type="text" name="search" placeholder="{{trans('application.sidebar_search_widget_placeholder')}}" id="searchField">
               <input type="submit" value="">
           </form>
       </div>

       <!-- Banner 300x250 / Show if Image Sidebar Banner exists / Show only the first in line -->
       @if(isset($banners['sidebar_top']) && count($banners['sidebar_top'])>0)
          <div class="widget">
              <div class="ad-banner-300x250">
                  <?php $sidebar_top = $banners['sidebar_top'][0]; ?>
                  @if(isset($sidebar_top->file_cdn))
                       <a href="{{$sidebar_top->target_url}}" target="{{$sidebar_top->target_url_window}}">
                           <img src="{{$sidebar_top->file_cdn}}" alt="{{$sidebar_top->description}}"/>
                       </a>
                  @else
                       {{{$sidebar_top->content }}}
                  @endif
              </div>
          </div>
        @endif

        <!-- Category widget -->
        <div class="widget" ng-show="categories.length>1">
            <h3 class="widget-title">{{trans('application.publication_sidebar_categories_widget_placeholder')}}</h3>
            <ul class="widget-categories">
                <li ng-repeat="category in categories"><a ng-href="@{{ category.render_url }}">@{{ category.render_title }}</a></li>
            </ul>
        </div>

        <!-- Tabs -->
        <div class="widget" ng-show="latest_magazines.length>0 || latest_issues.length>0">
            <div class="tabs">
                <ul>
                    <li><a href="#tab-side-issues" ng-show="latest_issues.length>0">{{trans('application.publication_sidebar_latest_issues_widget_placeholder')}}</a></li>
                    <li><a href="#tab-side-magazines" ng-show="latest_magazines.length>0">{{trans('application.publication_sidebar_latest_magazines_widget_placeholder')}}</a></li>
                </ul>
                <div id="tab-side-issues" ng-show="latest_issues.length>0">
                    <ul class="recent-posts" >
                        <li ng-repeat="issue in latest_issues">
                            <div class="image" ng-show="issue.render_image">
                                <a ng-href="@{{ issue.render_url }}"><img ng-src="@{{ issue.cdn }}" alt="@{{ issue.render_title }}" width="80"/></a>
                            </div>
                            <div class="text">
                                <h3><a ng-href="@{{ issue.render_url }}">@{{ issue.render_title }}</a></h3>
                                <p class="date" ng-show="issue.render_date">@{{ issue.render_date }}</p>
                            </div>
                        </li>
                    </ul>
                    <br>
                    <a class="btn btn-small btn-custom" href="{{route('issues')}}">{{trans('application.publication_sidebar_latest_issues_load_more_widget_placeholder')}}</a>
                </div>

                <div id="tab-side-magazines" ng-show="latest_magazines.length>0">
                    <ul class="recent-posts">
                       <li ng-repeat="magazine in latest_magazines">
                           <div class="image" ng-show="magazine.render_image">
                               <a ng-href="@{{ magazine.render_url }}"><img ng-src="@{{ magazine.cdn }}" alt="@{{ magazine.render_title }}" width="80"/></a>
                           </div>
                           <div class="text">
                               <h3><a ng-href="@{{ magazine.render_url }}">@{{ magazine.render_title }}</a></h3>
                               <p class="date" ng-show="magazine.render_date">@{{ magazine.render_date }}</p>
                           </div>
                       </li>
                    </ul>
                    <br>
                    <a class="btn btn-small btn-custom" href="{{route('publications')}}">{{trans('application.publication_sidebar_latest_magazines_load_more_widget_placeholder')}}</a>
                </div>

            </div>
        </div>

        <!-- Shop products -->
        <div class="widget" ng-show="latest_featured.length>0">
            <h3 class="widget-title">{{trans('application.publication_sidebar_featured_widget_placeholder')}}</h3>
            <ul class="product_list_widget">
                <li ng-repeat="featured in latest_featured">
                    <a href="@{{ featured.render_url }}" >
                        <img src="@{{featured.render_image}}" alt="@{{featured.render_title}}" ng-show="featured.render_image"/>
                        @{{featured.render_title}}
                    </a>
                    <ins ng-show="featured.render_price">{{trans('application.publication_sidebar_featured_from_widget_placeholder')}} <span class="amount">@{{featured.render_price}} &euro;</span></ins>
                </li>

            </ul>
        </div>
       </aside>