<style>
/*==============================================================================
    Color (Secondary)
===============================================================================*/
a,
h1#site-logo,
h1#site-logo a,
.weather-report .report,
.tabs .ui-tabs-nav li.ui-state-active a,
article .post-meta span:before,
.post-info span:before,
.post-controls a:hover,
.post-controls a span i,
.accordion .title.ui-state-active,
.above-the-fold h2.page-title,
.page404 h2 {color: #<?php echo $basic_app->app_theme_base_color;?>} /*custom color */

/*==============================================================================
    Background Color (Secondary)
===============================================================================*/
.primary-menu.dark, .primary-menu.light {
	background-color: #<?php echo $basic_app->app_theme_base_color;?>; /*custom color */
}

/*==============================================================================
    Background color (Secondary)
===============================================================================*/
.tagcloud a:hover,
#footer .tagcloud a:hover,
ul.page-numbers li span.current,
ul.page-numbers li a:hover,
.onsale,
.price_slider_wrapper .ui-slider-range,
a.selected-gallery-filter,
.block-layout-one .item span,
.post-box-text span,
.block-layout-two .main-item .post-img span,
.block-layout-three .main-item .post-img span,
.block-layout-four .main-item .post-img span,
.block-layout-five .main-item .post-img span,
.block-layout-six .main-item .post-img span,
.btn-custom, #footer .btn-custom:hover,
.primary-menu.light .click-to-open-menu, .primary-menu.dark .click-to-open-menu {background-color: #<?php echo $basic_app->app_theme_base_color;?>} /*custom color */

/*==============================================================================
    Border color (Secondary)
===============================================================================*/
.price_slider_wrapper .ui-slider-handle,
.main-menu > li > a,
.block-layout-three .item img,
.block-layout-two .item img,
.block-layout-one .item img {border-color: #<?php echo $basic_app->app_theme_base_color;?>} /*custom color */

</style>