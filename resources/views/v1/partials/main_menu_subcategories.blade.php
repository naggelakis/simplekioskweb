                    @if($level_1['style'] != 'megamenu')
                           <ul class="sub-menu">
                           @foreach($level_1['children'] as $level_2)
                               @if($level_1['id'] == $level_2['parent'])
                                   @if($level_2['menu_type']==1 && $level_2['class_id']==17 &&  Helpers::check_group_permission($level_2['user_groups'],$user_profile))
                                       <li>
                                           <a href="{{ route('homepage') }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==2 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- contact --}}
                                       <li>
                                            <a href="{{ route('contact') }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==3 && $level_2['class_id']==17   && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- url --}}
                                       <li>
                                            <a href="{{ $level_2['value'] }}" target="_blank">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==4 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- type view --}}
                                       <li>
                                            <a href="{{ url($level_2['value']) }}" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==5 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- article category --}}
                                       <li>
                                            <a href="{{ route('article_category',[$level_2['value'], Helpers::urlize($level_2['title'])]) }}" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==6 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{--  article single--}}
                                       <li>
                                            <a href="{{ route('article_single',[$level_2['value'], Helpers::urlize($level_2['title'])]) }}" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==8 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- profile index --}}
                                       <li>
                                            <a href="{{route('profile')}}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==9 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- market category --}}
                                       <li>
                                            <a href="{{route('market_category',[$level_2['value'], Helpers::urlize($level_2['title'])] )}}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==10 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- market single --}}
                                       <li>
                                            <a href="{{route('market_single',[$level_2['value'], Helpers::urlize($level_2['title'])] )}}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==11 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- market single --}}
                                        <li>
                                            <a href="{{route('author',[$level_2['value']]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                        </li>
                                   @elseif($level_2['menu_type']==12 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- profile single--}}
                                       <li>
                                            <a href="{{route('profile_single',[$level_2['value'],Helpers::urlize($level_2['title'])]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==13 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- pages single --}}
                                       <li >
                                            <a href="{{route('pages_single',[$level_2['value'],Helpers::urlize($level_2['title'])]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==14 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- pages index --}}
                                       <li >
                                            <a href="{{ route('pages',[$level_2['value']]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @elseif($level_2['menu_type']==15 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- news single --}}
                                      <li >
                                           <a href="{{ route('news_single',[$level_2['value'],Helpers::urlize($level_2['title'])]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                      </li>
                                   @elseif($level_2['menu_type']==16 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- news single --}}
                                      <li >
                                           <a href="{{ route('news_category',[ $level_2['value'],Helpers::urlize($level_2['title']) ]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                      </li>
                                   @elseif(Helpers::check_group_permission($level_2['user_groups'],$user_profile)  )
                                       <li>
                                            <a href="<?php echo url($level_2['value']); ?>" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                       </li>
                                   @endif
                               @endif
                           @endforeach
                       </ul>
                    @else
                        {{--Helpers::die_pre($level_1)--}}
                        <ul class="menu-blocks row">
                            @if(isset($level_1['entries']) && is_array($level_1['entries']) && count($level_1['entries'])>1)
                                <?php $grid = 'grid_4'; ?>
                            @elseif(isset($level_1['entries']) && is_array($level_1['entries']) && count($level_1['entries'])==1)
                                <?php $grid = 'grid_6'; ?>
                            @else
                                <?php $grid = 'grid_12'; ?>
                            @endif
                            <li class="grid_4">
                                <ul class="menu-content category-menu">
                                    @foreach($level_1['children'] as $level_2)
                                       @if($level_1['id'] == $level_2['parent'])
                                           @if($level_2['menu_type']==1 && $level_2['class_id']==17 &&  Helpers::check_group_permission($level_2['user_groups'],$user_profile))
                                               <li>
                                                   <a href="{{ route('homepage') }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==2 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- contact --}}
                                               <li>
                                                    <a href="{{ route('contact') }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==3 && $level_2['class_id']==17   && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- url --}}
                                               <li>
                                                    <a href="{{ $level_2['value'] }}" target="_blank">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==4 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- type view --}}
                                               <li>
                                                    <a href="{{ url($level_2['value']) }}" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==5 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- article category --}}
                                               <li>
                                                    <a href="{{ route('article_category',[$level_2['value'], Helpers::urlize($level_2['title'])]) }}" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==6 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{--  article single--}}
                                               <li>
                                                    <a href="{{ route('article_single',[$level_2['value'], Helpers::urlize($level_2['title'])]) }}" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==8 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- profile index --}}
                                               <li>
                                                    <a href="{{route('profile')}}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==9 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- market category --}}
                                               <li>
                                                    <a href="{{route('market_category',[$level_2['value'], Helpers::urlize($level_2['title'])] )}}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==10 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- market single --}}
                                               <li>
                                                    <a href="{{route('market_single',[$level_2['value'], Helpers::urlize($level_2['title'])] )}}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==11 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- market single --}}
                                                <li>
                                                    <a href="{{route('author',[$level_2['value']]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                                </li>
                                           @elseif($level_2['menu_type']==12 && $level_2['class_id']==17  && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- profile single--}}
                                               <li>
                                                    <a href="{{route('profile_single',[$level_2['value'],Helpers::urlize($level_2['title'])]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==13 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- pages single --}}
                                               <li >
                                                    <a href="{{route('pages_single',[$level_2['value'],Helpers::urlize($level_2['title'])]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==14 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- pages index --}}
                                               <li >
                                                    <a href="{{ route('pages',[$level_2['value']]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==15 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- pages index --}}
                                               <li >
                                                    <a href="{{ route('news_single',[$level_2['value'],Helpers::urlize($level_2['title'])]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @elseif($level_2['menu_type']==16 && $level_2['class_id']==17 && Helpers::check_group_permission($level_2['user_groups'],$user_profile)) {{-- pages index --}}
                                                <li >
                                                     <a href="{{ route('news_category',[$level_2['value'],Helpers::urlize($level_2['title'])]) }}">{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                                </li>
                                           @elseif(Helpers::check_group_permission($level_2['user_groups'],$user_profile)  )
                                               <li>
                                                    <a href="<?php echo url($level_2['value']); ?>" >{{ Helpers::remove_tones(strtoupper($level_2['title']))}}</a>
                                               </li>
                                           @endif
                                       @endif
                                   @endforeach
                                </ul>
                            </li>
                            @if($grid != 'grid_12')
                                <li class="grid_4">
                                    <ul class="menu-content featured-post">
                                        <li>
                                            <?php $article = $level_1['entries'][0]; ?>
                                            {{-- Helpers::die_pre($article) --}}
                                            <div class="block-layout-two">
                                                <div class="main-item">
                                                    <div class="post-img">
                                                        @if(isset($article['thumb']))
                                                            <a href="{{Helpers::getRouteSingleBySystemCat($article['system_cat_parent'],$article['id'],$article['title'])}}"><img src="{{Helpers::squareFilePath($article['thumb'])}}" alt="{{$article['title']}}"/></a>
                                                        @endif
                                                        @if(isset($article['category_id']))
                                                            <span>
                                                                <a href="{{Helpers::getRouteCategoryBySystemCat($article['system_cat_parent'],$article['category_id'],$article['category_title'])}}">{{Helpers::remove_tones($article['category_title'])}}</a>
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <h3><a href="{{Helpers::getRouteSingleBySystemCat($article['system_cat_parent'],$article['id'],$article['title'])}}">{{$article['title']}}</a></h3>
                                                    <p>@if(isset($article['subtitle'])) {{ $article['subtitle']}} @endif</p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                            @if($grid == 'grid_4')
                                <li class="grid_4">
                                <ul class="menu-content article-list">
                                    <li>
                                        <ul class="recent-posts">
                                            @for($i=1;$i<count($level_1['entries']);$i++)
                                                @if($i>4) @break @endif
                                                <?php $article = $level_1['entries'][$i]; ?>
                                                <li>
                                                    <div class="image">
                                                        @if(isset($article['thumb']))
                                                            <a href="{{Helpers::getRouteSingleBySystemCat($article['system_cat_parent'],$article['id'],$article['title'])}}">
                                                                <img src="{{Helpers::squareFilePath($article['thumb'])}}" alt="{{$article['title']}}" width="80" height="65"/>
                                                            </a>
                                                        @endif
                                                    </div>

                                                    <div class="text ">
                                                        <h3><a href="{{Helpers::getRouteSingleBySystemCat($article['system_cat_parent'],$article['id'],$article['title'])}}">{{$article['title']}}</a></h3>
                                                        <p class="date">{{Helpers::remove_tones(Helpers::localeDate($article['created']))}}</p>
                                                    </div>
                                                </li>
                                            @endfor

                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            @endif
                        </ul>
                    @endif