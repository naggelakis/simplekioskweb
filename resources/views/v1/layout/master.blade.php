<!DOCTYPE html>
<html lang="@if(strtolower($lang)=='el') el-GR @else en-US @endif">
    <head>
        @include($template.'.partials.metadata')
        @yield('metadata' )

        @include($template.'.partials.head_icons')
        @yield('head_icons' )

        @include($template.'.partials.head_styles')
        @yield('head_styles' )

        @include($template.'.partials.head_scripts')
        @yield('head_scripts' )

    </head>
    <body id="body">



               <!-- Wrapper / Show wide by default exept if banner background exists so change to boxed_add -->
               <div id="wrapper" class="@if(isset($basic_app->app_theme_style) && $basic_app->app_theme_style!=null && $basic_app->app_theme_style!='') {{  $basic_app->app_theme_style }} @else wide @endif" >


                   <!-- Header -->
                   @include($template.'.partials.header')
                   @yield('header' )

                        @yield('content')

                   <!-- Footer / Set background as Apps primary color if exists or leave #333 -->
                   @include($template.'.partials.footer')
                   @yield('footer' )


               </div>

        <!-- Load scripts -->
            @include($template.'.partials.scripts')
            @yield('scripts' )
        <!-- Load scripts -->



        @if(isset($basic_app->disqus_forum) && $basic_app->disqus_forum!=null && $basic_app->disqus_forum!='')
            <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES * * */
            var disqus_shortname = '{{$basic_app->disqus_forum}}';
            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function () {
                var s = document.createElement('script'); s.async = true;
                s.type = 'text/javascript';
                s.src = '//'+disqus_shortname+'.disqus.com/count.js';
                (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
            }());
        </script>

        @endif

    </body>
</html>


