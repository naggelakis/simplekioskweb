<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap.min.css" rel="stylesheet">
		<link href="{{asset('assets/viewer/css/main.css')}}" rel="stylesheet">
		<script src="{{asset('assets/viewer/pdf.js')}}"></script>
		<script src="{{asset('assets/viewer/pdf.compat.js')}}"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.min.js"></script>
		<script src="https://code.angularjs.org/1.4.7/angular-animate.js"></script>
		<script src="https://code.angularjs.org/1.4.7/angular-touch.js"></script>		
		<script src="https://code.angularjs.org/1.4.7/angular-sanitize.js"></script>
		<script src="{{asset('assets/viewer/ng-pdfviewer.js')}}"></script>
		<script src="{{asset('assets/viewer/test.js')}}"></script>
		<title>{{ isset($publication->magazine) ? $publication->magazine->translations->{$lang}->title.' - ':null}} {{$publication->translations->{$lang}->title}}</title>

	</head>
	<body ng-app="testApp" ng-controller="TestController" ng-keydown="callFunction($event)" ng-swipe-right="prevPage()" ng-swipe-left="nextPage()">
		<div  class="text-center container" style="width:100%;">
			<div class="row" ng-show="loaded==1">
				<div class="col-md-8">		
					<div class="btn-group">
						<button class="btn" ng-click="gotoPage(1)" title="{{trans('application.pdf_go_to_first')}}">|&lt;</button>
						<button class="btn" ng-click="prevPage()" title="{{trans('application.pdf_go_to_previous')}}">&lt;</button>
						<button class="btn" ng-click="nextPage()" title="{{trans('application.pdf_go_to_next')}}">&gt;</button>
						<button class="btn" ng-click="gotoPage(totalPages)" title="{{trans('application.pdf_go_to_last')}}">&gt;|</button>
						<button class="btn" ng-click="setScale(0.7)" title="{{trans('application.pdf_zoom_out')}}">-</button>
						<button class="btn" ng-click="setScale(1)">{{trans('application.pdf_normal_zoom')}}</button>
						<button class="btn" ng-click="setScale(1.8)" title="{{trans('application.pdf_zoom_in')}}">+</button>
					</div>	
				</div>
				<div class="col-md-4">
					<span class="label" ng-show="totalPages">@{{currentPage}}/@{{totalPages}}</span>
				</div>
				
			</div>

			<div class="row" >
				<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 pdf" ng-init="id={{$publication->id}};lang='{{strtolower($lang)}}';initUrl()">
					<pdfviewer src="@{{ trustSrc(pdfURL) }}" scale="@{{ scale }}" pages-to-show="@{{ pages }}" on-page-load='pageLoaded(page,total)' id="viewer" load-progress='loadProgress(loaded, total, state)'></pdfviewer>
				</div>
			</div>
			<div class="row-fluid pdf" ng-show="loaded==0" style="margin-top:2%;min-height:400px;padding-top:20%;">
                <div class="span6 offset3">
                    <div class="progress">
                      <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        <span>{{trans('application.pdf_loading')}}</span>
                      </div>
                    </div>
                </div>
            </div>
		</div>
	</body>
</html>
