<h2>{{trans('application.email_confirm_title')}}</h2>

<p>{{trans('application.email_confirm_placeholder').$app_details->title}}</p>

<p>
    <a href="{{$url}}">{{trans('application.email_confirm_click_here')}}</a>
    <br>
</p>
<p>
    {{trans('application.email_confirm_link_alternative')}}
    <br>
    {{$url}}
</p>

<p>
    {{trans('application.email_confirm_no_action')}}
</p>

<p>
    {{trans('application.email_confirm_thanks')}},
    <br>
    {{$app_details->title}}
</p>