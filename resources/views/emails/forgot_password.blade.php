<h2>{{trans('application.email_forgot_pass_title')}}</h2>

<p>{{trans('application.email_forgot_pass_placeholder').$app_details->title}}</p>

<p>
    <a href="{{$url}}">{{trans('application.email_forgot_pass_click_here')}}</a>
    <br>
</p>
<p>
    {{trans('application.email_forgot_pass_link_alternative')}}
    <br>
    {{$url}}
</p>

<p>
    {{trans('application.email_forgot_pass_no_action')}}
</p>

<p>
    {{trans('application.email_forgot_pass_thanks')}},
    <br>
    {{$app_details->title}}
</p>