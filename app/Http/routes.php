<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/404/not_found',['as' => '404', 'uses' => 'Frontend\MainController@not_found']);

/** Auth Controller */
Route::get('/auth/login',['as' => 'login', 'uses' => 'Frontend\AuthController@login']);
Route::post('/auth/register',['as' => 'register', 'uses' => 'Frontend\AuthController@register']);
Route::post('/auth/login',['as' => 'login', 'uses' => 'Frontend\AuthController@login']);
Route::get('/auth/social/{method}',['as' => 'social_login', 'uses' => 'Frontend\AuthController@social_login']);
Route::get('/auth/social/authenticated/{method}',['as' => 'social_login_user', 'uses' => 'Frontend\AuthController@social_login_user']);
Route::get('/auth/social/reset_password/{id}/{code}',['as' => 'password_reset', 'uses' => 'Frontend\AuthController@reset_password']);
Route::post('/auth/forgot_password',['as' => 'forgot_password', 'uses' => 'Frontend\AuthController@forgot_password']);
Route::post('/auth/social/reset_password/{id}/{code}',['as' => 'password_reset_post', 'uses' => 'Frontend\AuthController@reset_password']);
Route::get('/auth/confirm/{code}',['as' => 'confirm_email', 'uses' => 'Frontend\UserController@confirm_email']);

/** User Controller */
Route::get('/me',['as' => 'user_profile', 'uses' => 'Frontend\UserController@home']);
Route::get('/logout',['as' => 'user_logout', 'uses' => 'Frontend\UserController@logout']);
Route::get('/me/subscriptions',['as' => 'user_subscriptions', 'uses' => 'Frontend\UserController@subscriptions']);
Route::get('/me/favorites',['as' => 'user_favorites', 'uses' => 'Frontend\UserController@favorites']);
Route::get('/me/orders',['as' => 'user_orders', 'uses' => 'Frontend\UserController@orders']);
Route::get('/me/issues',['as' => 'user_issues', 'uses' => 'Frontend\UserController@issues']);
Route::get('/me/profile',['as' => 'user_edit', 'uses' => 'Frontend\UserController@edit']);
Route::get('/me/locations',['as' => 'user_locations', 'uses' => 'Frontend\UserController@locations']);
Route::post('/me/locations',['as' => 'user_locations_post', 'uses' => 'Frontend\UserController@locations']);
Route::post('/me/locations/edit',['as' => 'user_locations_edit', 'uses' => 'Frontend\UserController@edit_location']);
Route::get('/me/locations/{location_id}/default',['as' => 'make_location_default', 'uses' => 'Frontend\UserController@make_location_default']);
Route::get('/me/locations/{location_id}/delete',['as' => 'delete_location', 'uses' => 'Frontend\UserController@delete_location']);
Route::post('/me/profile/update',['as' => 'update_user_profile', 'uses' => 'Frontend\UserController@update']);
Route::post('/me/profile/update/password',['as' => 'update_user_password', 'uses' => 'Frontend\UserController@update_password']);
Route::get('/me/profile/social/connect/{media}',['as' => 'user_social_connect', 'uses' => 'Frontend\UserController@user_social_connect']);
Route::get('/me/profile/social/connect/user_social_connect_success/{id}/{method}',['as' => 'user_social_connect_success', 'uses' => 'Frontend\UserController@user_social_connect_success']);
Route::get('/me/connect_subscription',['as' => 'connect_subscription', 'uses' => 'Frontend\UserController@connect_subscription']);
Route::post('/me/connect_subscription',['as' => 'connect_subscription_post', 'uses' => 'Frontend\MarketController@connect_subscription']);


/** Main Controller */
Route::get('/',['as' => 'homepage', 'uses' => 'Frontend\MainController@homepage']);
Route::get('/contact',['as' => 'contact', 'uses' => 'Frontend\MainController@contact']);
Route::post('/contact',['as' => 'contact_send', 'uses' => 'Frontend\MainController@contact']);

/** News Controller */
Route::get('/news/category/{cat_id}/{slug}',['as' => 'news_category', 'uses' => 'Frontend\NewsController@category']);
Route::get('/news/{id}/{slug}',['as' => 'news_single', 'uses' => 'Frontend\NewsController@single']);
Route::get('/author/{id}',['as' => 'author', 'uses' => 'Frontend\NewsController@author']);
Route::get('/news/feed/{from?}/{to?}',['as' => 'feed_list', 'uses' => 'Frontend\NewsController@feed']);
Route::get('/tags/{tag_id}/{tag_slug}',['as' => 'tags', 'uses' => 'Frontend\NewsController@tags']);
Route::get('/search/{search?}',['as' => 'search', 'uses' => 'Frontend\NewsController@search']);
//Route::post('/search',['as' => 'search', 'uses' => 'Frontend\NewsController@search']);


/** Profile Controller */
Route::get('/profile',['as' => 'profile', 'uses' => 'Frontend\ProfileController@index']);
Route::get('/profile/{id}/{slug}',['as' => 'profile_single', 'uses' => 'Frontend\ProfileController@single']);
Route::get('/profile/category/{cat_id}/{slug}',['as' => 'profile_category', 'uses' => 'Frontend\ProfileController@category']);


/** Articles Controller */
Route::get('/articles',['as' => 'articles', 'uses' => 'Frontend\ArticlesController@index']);
Route::get('/articles/{id}/{slug}',['as' => 'article_single', 'uses' => 'Frontend\ArticlesController@single']);
Route::get('/articles/category/{cat_id}/{slug}',['as' => 'article_category', 'uses' => 'Frontend\ArticlesController@category']);
///Route::get('/news/feed/{from}/{to}',['as' => 'article_list', 'uses' => 'Frontend\ArticlesController@feed']);


/** Pages Controller */
Route::get('/page',['as' => 'pages', 'uses' => 'Frontend\PagesController@index']);
Route::get('/page/{id}/{slug}',['as' => 'pages_single', 'uses' => 'Frontend\PagesController@single']);
Route::get('/page/category/{cat_id}/{slug}',['as' => 'pages_category', 'uses' => 'Frontend\PagesController@category']);


/** Market Controller */
Route::get('/cart',['as' => 'cart', 'uses' => 'Frontend\MarketController@index']);
Route::get('/market/category/{cat_id}/{slug}',['as' => 'market_category', 'uses' => 'Frontend\PublicationsController@index']);
Route::get('/market/{id}/{slug}',['as' => 'market_single', 'uses' => 'Frontend\MarketController@single']);
Route::get('/market/cart/publication/checkout/{publication_id}/{recurring_plan_id}/{system_cat_id}/{group_id?}',['as' => 'publication_checkout', 'uses' => 'Frontend\MarketController@checkout_publication']);
Route::get('/market/cart/recurring/add/{recuring_plan_id}/{recuring_plan_price_id}',['as' => 'add_to_cart_recurring', 'uses' => 'Frontend\MarketController@add_cart_recurring_plan']);
Route::get('/market/cart/checkout/electronic/{publication_id}/{group_id}',['as' => 'electronic_checkout', 'uses' => 'Frontend\MarketController@electronic_checkout']);
Route::get('/checkout/viva/{payment_method_id}/{item_id}/{system_id}/{group_id}/{type}/{recurring_plan_id?}',['as' => 'viva_checkout', 'uses' => 'Frontend\MarketController@viva_checkout']);
Route::get('/checkout/bank/{payment_method_id}/{item_id}/{system_id}/{group_id}/{type}/{recurring_plan_id?}',['as' => 'bank_checkout', 'uses' => 'Frontend\MarketController@bank_checkout']);
Route::get('/checkout/delivery/{payment_method_id}/{item_id}/{system_id}/{group_id}/{type}/{recurring_plan_id?}',['as' => 'delivery_checkout', 'uses' => 'Frontend\MarketController@delivery_checkout']);
Route::get('/checkout/paypal/{payment_method_id}/{item_id}/{system_id}/{group_id}/{type}/{recurring_plan_id?}',['as' => 'paypal_checkout', 'uses' => 'Frontend\MarketController@paypal_checkout']);
Route::get('/checkout/completed/{payment_service}',['as' => 'checkout_success', 'uses' => 'Frontend\MarketController@checkout_complete']);
Route::get('/market/cart/add/{publication_id}/{group_id}/{system_cat_id}',['as' => 'add_to_cart', 'uses' => 'Frontend\MarketController@add_cart']);
Route::post('/market/cart/publication/confirm/checkout/{publication_id}/{recurring_plan_id}/{system_cat_id}/{group_id?}',['as' => 'publication_printed_checkout_shipping', 'uses' => 'Frontend\MarketController@publication_checkout_shipping']);
Route::get('/cart/remove/{cart_item_id}',['as' => 'cart_item_remove', 'uses' => 'Frontend\MarketController@removeCartItem']);
Route::post('/cart/update',['as' => 'cart_update', 'uses' => 'Frontend\MarketController@updateCart']);
Route::post('/cart/shipping',['as' => 'cart_shipping', 'uses' => 'Frontend\MarketController@cart_checkout_shipping']);
Route::get('/subscription/update/{subscription_id}',['as' => 'update_subscription', 'uses' => 'Frontend\MarketController@update_subscription']);


/** Terms Controller */
Route::get('/terms',['as' => 'terms', 'uses' => 'Frontend\TermsController@index']);
Route::get('/terms/{id}/{slug}',['as' => 'terms_single', 'uses' => 'Frontend\TermsController@single']);
Route::get('/terms/category/{cat_id}/{slug}',['as' => 'terms_category', 'uses' => 'Frontend\TermsController@category']);

/** Publications Controller */
Route::get('/issues',['as' => 'issues', 'uses' => 'Frontend\PublicationsController@index']);
Route::get('/issue/{id}/{slug}',['as' => 'issue_single', 'uses' => 'Frontend\PublicationsController@single']);
Route::get('/magazines',['as' => 'publications', 'uses' => 'Frontend\PublicationsController@index']);
Route::get('/magazine/{id}/{slug}',['as' => 'publication_single', 'uses' => 'Frontend\PublicationsController@single']);
Route::get('/books',['as' => 'books', 'uses' => 'Frontend\PublicationsController@index']);
Route::get('/book/{id}/{slug}',['as' => 'book_single', 'uses' => 'Frontend\PublicationsController@single']);
Route::get('/publications/category/{id}/{slug}',['as' => 'publications_category', 'uses' => 'Frontend\PublicationsController@category']);
Route::get('/publications/view/{id}',['as' => 'view_publication', 'uses' => 'Frontend\PublicationsController@viewer']);
Route::get('/publications/view/pdf/{id}',['as' => 'serve_pdf', 'uses' => 'Frontend\PublicationsController@serve_pdf']);


/** Ajax requests */

Route::get('/endpoint/news/latest/{limit}/{offset}', 'Ajax\EndpointsController@latest_articles');
Route::get('/endpoint/news/latest_publications', 'Ajax\EndpointsController@latest_publications');
Route::get('/endpoint/news/month_log', 'Ajax\EndpointsController@month_log');
Route::get('/endpoint/news/recent_homepage_tabs', 'Ajax\EndpointsController@recent_tabs_homepage');
Route::get('/endpoint/news/authors_showcase', 'Ajax\EndpointsController@authors_showcase_homepage');
Route::get('/endpoint/news/showcase_entries/{showcase_id}/{limit}/{offset}', 'Ajax\EndpointsController@showcase_entries');
Route::get('/endpoint/news/showcase_categories/{showcase_id}/{limit}/{offset}', 'Ajax\EndpointsController@showcase_categories');
Route::get('/endpoint/category/latest/{limit}/{offset}/{type}/{cat_id}/{ids}', 'Ajax\EndpointsController@latest_category_articles');
Route::get('/endpoint/news/tag_search/{limit}/{offset}/{only_news}/{tag_id}/{search}/{from?}/{to?}', 'Ajax\EndpointsController@tag_search');
Route::get('/endpoint/publications/issues_list/{publication_id}/{limit}/{offset}', 'Ajax\EndpointsController@publication_issues');
Route::get('/endpoint/market/categories/fetch', 'Ajax\EndpointsController@market_categories');
Route::get('/endpoint/fetch/latest_magazines/{limit}', 'Ajax\EndpointsController@fetch_magazines');
Route::get('/endpoint/fetch/latest_issues/{limit}', 'Ajax\EndpointsController@fetch_issues');
Route::get('/endpoint/fetch/featured_publications', 'Ajax\EndpointsController@fetch_featured_publications_sidebar');
Route::get('/endpoint/author/latest/{author_id}/{limit}/{offset}', 'Ajax\EndpointsController@fetch_author_entries');
Route::get('/endpoint/generate_confirmation_code', ['as' => 'generate_confirmation_code', 'uses' => 'Frontend\UserController@send_confirmation_email']);
Route::get('/endpoint/favorites/fetch/posts', ['as' => 'fetch_favorite_posts', 'uses' => 'Ajax\EndpointsController@fetch_favorites_posts']);
Route::get('/endpoint/favorites/post/{id}/{action}', ['as' => 'favorite_post', 'uses' => 'Ajax\EndpointsController@favorite_post']);
Route::get('/endpoint/favorites/fetch/posts/list/{limit}/{offset}', ['as' => 'favorite_post', 'uses' => 'Ajax\EndpointsController@favorite_articles']);
Route::get('/endpoint/issues/fetch/list/{limit}/{offset}/{sort}', ['as' => 'user_issues_ajax', 'uses' => 'Ajax\EndpointsController@user_issues']);
Route::post('/endpoint/validate_email', ['as' => 'email_validation_unique', 'uses' => 'Ajax\EndpointsController@check_email_unique']);
Route::get('/endpoint/issues/fetch/category/{category}/{limit}/{offset}/{sort}', ['as' => 'user_issues_ajax', 'uses' => 'Ajax\EndpointsController@category_issues']);
Route::post('/endpoint/inAppPurchaseClick', ['as' => 'inAppPurchaseClick', 'uses' => 'Ajax\EndpointsController@inAppPurchaseClick']);



/** Deploy */

Route::any('/deploy_application/{token}/{c_user?}', 'Core\LoggiaController@deploy');

