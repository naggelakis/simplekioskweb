<?php

namespace WebKiosk\Http\Controllers\Frontend;

use PayPal\Exception\PayPalConnectionException;
use WebKiosk\Http\Controllers\Core\LoggiaController;
use Illuminate\Http\Request;
use Helpers;
use WebKiosk\ApiModels\Market;
use Gloudemans\Shoppingcart\Facades\Cart;
use Gloudemans\Shoppingcart\CartCollection;
use  WebKiosk\Custom\VivaRedirect;
use PayPal\Rest\ApiContext;
use Illuminate\Support\Facades\Redirect;
use Netshell\Paypal\Facades\Paypal;
use PayPal\Auth\OAuthTokenCredential;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;

class MarketController extends LoggiaController
{
    public $cart_items;
    public $payment_methods=[];
    public $sandbox = true;
    private $_apiContext;
    private $viva;


    public function __construct(){
        parent::__construct();
        $content = Cart::content()->all();
        if(is_array($content) && count($content)>0){
            foreach($content as $row)
            {
                $item = $row->all();
                $item['options'] = $item['options']->all();
                $items[$item['rowid']] = $item;
            }
            $this->cart_items = $items;
        }
        $model = new Market();

        //fetch payment methods
        $payment_methods = $model->getPaymentMethods($this->lang,$this->page_id,8600*7);
        if(is_array($payment_methods) && count($payment_methods)>0){
            foreach($payment_methods as $payment_method):
                $this->payment_methods[$payment_method->id] = $payment_method;

                if($payment_method->sys_payment_id == 1){
                    $this->_apiContext = PayPal::ApiContext(
                        $payment_method->paypal_app_client_id,
                        $payment_method->paypal_app_client_secret
                    );
                }

                if($payment_method->sys_payment_id == 2){
                    $this->viva = new VivaRedirect();
                    $this->viva->_register($payment_method->viva_merchant, $payment_method->viva_api, $this->sandbox);
                }


            endforeach;
        }

        if(is_object($this->_apiContext)){
            $this->_apiContext->setConfig(array(
                'mode' => $this->sandbox == true ? 'sandbox' : 'live',
                'service.EndPoint' => $this->sandbox == true ? 'https://api.sandbox.paypal.com' : 'https://api.paypal.com' ,
                'http.ConnectionTimeOut' => 30,
                'log.LogEnabled' => true,
                'log.FileName' => storage_path('logs/paypal.log'),
                'log.LogLevel' => 'FINE'
            ));
        }
        //Helpers::die_pre($this->payment_methods);
    }

    /**
     * Cart index
    * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request){
        $data = [];
        $data['items'] = $this->cart_items;
        $data['subtotal'] = Cart::total();
        $data['redirect'] = route('cart');
        $model = new Market();
        $data['countries'] = $model->getCountriesList($this->lang,8600*30);
        $data['cities'] = $model->getCitiesList($this->lang,8600*30);
        //Helpers::die_pre($this->cart_items);
        return view('v1/views/market/index',$data);

    }


    /**
     * Remove an item from cart | works with ajax
    * @param Request $request
    * @param $cart_item_id
     */
    public function removeCartItem(Request $request,$cart_item_id){
        $row = Cart::get($cart_item_id);
        if($row){
            $remove = Cart::remove($cart_item_id);
            echo json_encode(true);

        }
    }

    /**
     * Updates quantities of items on cart | works with ajax
    * @param Request $request
     */
    public function updateCart(Request $request){
        $post_data = $request->all();
        if(isset($post_data['data']) && count($post_data['data'])>0){
            foreach($post_data['data'] as $row):
                $update = Cart::update($row['id'], $row['quantity']);
            endforeach;
            echo json_encode(Cart::content());
        }

    }

    /**
     * Cart shipping setup
    * @param Request $request
    * @return \Illuminate\View\View
     */
    public function cart_checkout_shipping(Request $request){
        $data = [];
        $model = new Market();
        $lang = $this->lang;

        $weight = 0;
        foreach($this->cart_items as $item):
            $publication = $model->getPublication($item['id'],$this->lang);
            if(isset($publication) && $publication->weight!=null){
                $weight += $publication->weight*$item['qty'];
            }
        endforeach;



        $data['redirect'] = route('cart');
        $data['payment_methods'] = $this->payment_methods;

        $price = Cart::total();
        $country_id = $request->get('shipping_country') != null ? $request->get('shipping_country') : $request->get('billing_country');
        $city_id = $request->get('shipping_city') != null ? $request->get('shipping_city') : $request->get('billing_city');

        $shipping_methods = $model->getShippingMethods($this->lang,$this->page_id,8600*30);
        if(is_array($shipping_methods) && count($shipping_methods)>0){
            foreach($shipping_methods as $key=>$shipping_method):
                $rate = $model->getShippingRate($lang,$this->page_id,$shipping_method->id,$weight,$country_id,$city_id);

                if($rate){
                    if($rate->rate_type_id == 109){
                        $shipping_method->shipping_rate = $rate->rate_value;
                    }elseif($rate->rate_type_id == 110){
                        $shipping_method->shipping_rate = $rate->rate_value *  ($price->value/100);
                    }
                }else{
                    unset($shipping_methods[$key]);
                }

            endforeach;
        }



            $post_data = $request->all();

            $this->authUser->firstname = $post_data['billing_firstname'];
            $this->authUser->lastname = $post_data['billing_lastname'];

            if( isset($post_data['postal_same']) && $post_data['postal_same'] == 'on'){
                $user_ship_address1 =  $post_data['billing_address'];
                $user_ship_address2 =  $post_data['billing_address2'];
                if($post_data['billing_country'] == 95){
                    $user_ship_address_city = $post_data['billing_city'];
                }else{
                    $user_ship_address_city = $post_data['billing_city_alt'];
                }
                $user_ship_address_country=  $post_data['billing_country'];
                $user_ship_address_zip=  $post_data['billing_zip'];
                $receiver = $post_data['billing_firstname'].' '.$post_data['billing_lastname'];
            }else{
                $user_ship_address1 =  $post_data['shipping_address'];
                $user_ship_address2 =  $post_data['shipping_address2'];
                if($post_data['shipping_country'] == 95){
                    $user_ship_address_city = $post_data['shipping_city'];
                }else{
                    $user_ship_address_city = $post_data['shipping_city_alt'];
                }
                $user_ship_address_country=  $post_data['shipping_country'];
                $user_ship_address_zip=  $post_data['shipping_zip'];
                $receiver = $post_data['shipping_firstname'].' '.$post_data['shipping_lastname'];
            }

            $profile_data = [
                'user_ship_address1' => $user_ship_address1,
                'user_ship_address2' => $user_ship_address2,
                'user_ship_address_city' => $user_ship_address_city,
                'user_ship_address_zip' => $user_ship_address_zip,
                'user_ship_address_country' => $user_ship_address_country,
                'user_bill_address1' => $post_data['billing_address'],
                'user_bill_address2' => $post_data['billing_address2'],
                'user_bill_address_city' => $post_data['billing_country'] == 95 ? $post_data['billing_city'] : $post_data['billing_city_alt'],
                'user_bill_address_zip' => $post_data['billing_zip'],
                'user_bill_address_country' => $post_data['billing_country'],
                'receiver'=>$receiver
            ];

            if(!is_object($this->order)) {
                $order_id = $this->create_order($request, null, null, $profile_data);
                $request->session()->put('items_order', true);
                if (isset($price->value)) {
                    $total = $price->value;
                }

                if ($order_id) {
                    $order =  new \stdClass();
                    $order->subtotal = 0;
                    $order->total = 0;

                    $count = 0;
                    foreach($this->cart_items as $item):
                        $count++;
                        $order->subtotal += $item['qty']*$item['price'];
                        $order->total += $item['qty']*$item['price'];
                        $item = $this->add_order_item($order_id, $item['id'], $item['name'], $item['price'], $item['qty'], null, $item['options']['system_cat_id'], 230, $item['options']['group_id'],null,$count == count($this->cart_items) ? $order : null);

                    endforeach;
                }
            }else{
                //todo edit cart items
            }



            $data['items'] = $this->cart_items;
            $data['price'] = Cart::total();
            $data['shipping_methods'] = $shipping_methods;
            //Helpers::die_pre($shipping_methods);
            return view('v1/views/market/cart_shipping',$data);



    }


    /**
     * Add non electronic item to cart
    * @param Request $request
    * @param $publication_id
    * @param $group_id
    * @param $system_cat_id
     * @return mixed
     */
    public function add_cart(Request $request,$publication_id,$group_id,$system_cat_id){
           // Cart::destroy();
            $model = new Market();
            $lang = $this->lang;
            $publication = $model->getPublication($publication_id,$this->lang);
          //  Helpers::die_pre( $publication->translations->$lang);
            if(!$publication){ return redirect()->route('homepage')->send();  }

            //if electronic proceed to checkout
            if($system_cat_id == 74){
                return redirect()->route('electronic_checkout',[$publication_id,$group_id])->send();
            }


            $price = new \stdClass();


            if(isset($publication->prices) && is_array($publication->prices) && count($publication->prices)>0){ //issue or magazine has prices set
                $pub_prices = $publication->prices;
            }elseif(isset($publication->magazine) && isset($publication->magazine->prices) && is_array($publication->magazine->prices) && count($publication->magazine->prices)>0){ //else if issue, check if issue's parent magazine has prices set
                $pub_prices = $publication->magazine->prices;
            }elseif(isset($publication->base_price ) && $publication->base_price!=null){ // else check if issue or mag has base price
                $price->value =  $publication->base_price;
                $price->vat = 1;
                $price->title = trans('application.cart_item_title_base_price');
            }elseif(isset($publication->magazine) && isset($publication->magazine->base_price ) && $publication->magazine->base_price!=null){ // in case issue, check if issue's magazine has base price
                $price->value = $publication->magazine->base_price;
                $price->vat = 1;
                $price->title = trans('application.cart_item_title_base_price');
            }

            if(isset($pub_prices) && is_array($pub_prices) && count($pub_prices)>0){
                foreach($pub_prices as $pub_price){
                    if($pub_price->system_type_id == $system_cat_id && $pub_price->user_group_id == $group_id){
                        $price->value = $pub_price->price;
                        $price->vat = $pub_price->tax_inclusive;

                        $price->title = isset($publication->translations->$lang->title) ? $publication->translations->$lang->title : current($publication->translations)->title;
                        $price->system_translation = $pub_price->system_type_id_translation;
                        if(isset($pub_price->group_translation)){
                            $price->group_translation = $pub_price->group_translation;
                        }
                        break;
                    }
                }
            }


            if(isset($price->value)){

                $options = array('vat' => $price->vat);
                $options['type'] = 'issue';
                $options['system_translation'] = isset($price->system_translation) ? $price->system_translation : null;
                $options['group_id'] = $group_id;
                $options['system_cat_id'] = $system_cat_id;
                if(isset( $price->group_translation )){
                    $options['group_translation'] = $price->group_translation;
                }
                if(isset($publication->thumb->cdn)){
                    $options['image'] = $publication->thumb->cdn;
                }
                $name = isset($publication->magazine) ? $publication->magazine->translations->$lang->title.': '.$price->title : $price->title;
                Cart::add($publication_id, $name, 1, $price->value, $options);
            }


            return redirect()->route('cart')->send();

    }

    /**
     * Step2 for buying printed publication
    * @param Request $request
    * @param $publication_id
    * @param $recurring_plan_id
    * @param $system_cat_id
    * @param int $group_id
     * @return \Illuminate\View\View
     */
    public function publication_checkout_shipping(Request $request,$publication_id,$recurring_plan_id,$system_cat_id,$group_id=0){
        $data = [];
        $model = new Market();
        $lang = $this->lang;
        $publication = $model->getPublication($publication_id,$this->lang);

        if(!$publication || $publication->parent_id != null ){ return redirect()->route('homepage')->send();  }
        if($system_cat_id == 72 || $system_cat_id == 73) { // mixed or printed

            $data['redirect'] = route('publication_checkout',[$publication_id,$recurring_plan_id,$system_cat_id,$group_id]);
            $data['payment_methods'] = $this->payment_methods;
            $publication->title='';
            if(isset($publication->magazine->translations->$lang->title)){ $publication->title .= $publication->magazine->translations->$lang->title.': ';}
            if(isset($publication->translations->$lang->title)){ $publication->title .= $publication->translations->$lang->title;}
            $price = $this->compute_recurring_price($publication,$recurring_plan_id);
            $price->value = $price->price;
            $country_id = $request->get('shipping_country') != null ? $request->get('shipping_country') : $request->get('billing_country');
            $city_id = $request->get('shipping_city') != null ? $request->get('shipping_city') : $request->get('billing_city');

            $shipping_methods = $model->getShippingMethods($this->lang,$this->page_id,8600*30);
            if(is_array($shipping_methods) && count($shipping_methods)>0){
                foreach($shipping_methods as $key=>$shipping_method):
                    $rate = $model->getShippingRate($lang,$this->page_id,$shipping_method->id,$publication->weight!=null?$publication->weight:0.5,$country_id,$city_id);

                    if($rate){
                        if($rate->rate_type_id == 109){
                            $shipping_method->shipping_rate = $rate->rate_value;
                        }elseif($rate->rate_type_id == 110){
                            $shipping_method->shipping_rate = $rate->rate_value *  ($price->value/100);
                        }
                    }else{
                        unset($shipping_methods[$key]);
                    }

                endforeach;
            }

            if(count($shipping_methods)==0){
                $redirect = route('publication_checkout',[$publication_id,$recurring_plan_id,$system_cat_id,$group_id]).'?error=shipping_unavailable';
                return Redirect::to( $redirect );
            }

            $post_data = $request->all();

            $this->authUser->firstname = $post_data['billing_firstname'];
            $this->authUser->lastname = $post_data['billing_lastname'];

            if( isset($post_data['postal_same']) && $post_data['postal_same'] == 'on'){
                $user_ship_address1 =  $post_data['billing_address'];
                $user_ship_address2 =  $post_data['billing_address2'];
                if($post_data['billing_country'] == 95){
                    $user_ship_address_city = $post_data['billing_city'];
                }else{
                    $user_ship_address_city = $post_data['billing_city_alt'];
                }
                $user_ship_address_country=  $post_data['billing_country'];
                $user_ship_address_zip=  $post_data['billing_zip'];
                $user_ship_address_phone =  $post_data['billing_phone'];
                $receiver = $post_data['billing_firstname'].' '.$post_data['billing_lastname'];
            }else{
                $user_ship_address1 =  $post_data['shipping_address'];
                $user_ship_address2 =  $post_data['shipping_address2'];
                if($post_data['shipping_country'] == 95){
                    $user_ship_address_city = $post_data['shipping_city'];
                }else{
                    $user_ship_address_city = $post_data['shipping_city_alt'];
                }
                $user_ship_address_country=  $post_data['shipping_country'];
                $user_ship_address_zip=  $post_data['shipping_zip'];
                $user_ship_address_phone=  $post_data['shipping_phone'];
                $receiver = $post_data['shipping_firstname'].' '.$post_data['shipping_lastname'];
            }

            $profile_data = [
                'user_ship_address1' => $user_ship_address1,
                'user_ship_address2' => $user_ship_address2,
                'user_ship_address_city' => $user_ship_address_city,
                'user_ship_address_zip' => $user_ship_address_zip,
                'user_ship_address_phone' => $user_ship_address_phone,
                'user_ship_address_country' => $user_ship_address_country,
                'user_bill_address1' => $post_data['billing_address'],
                'user_bill_address2' => $post_data['billing_address2'],
                'user_bill_address_city' => $post_data['billing_country'] == 95 ? $post_data['billing_city'] : $post_data['billing_city_alt'],
                'user_bill_address_zip' => $post_data['billing_zip'],
                'user_bill_address_phone' => $post_data['billing_phone'],
                'user_bill_address_country' => $post_data['billing_country'],
                'receiver'=>$receiver
            ];

            if(!is_object($this->order)) {
                $order_id = $this->create_order($request, null, $recurring_plan_id, $profile_data);
                if (isset($price->value)) {
                    $total = $price->value;
                }

                if ($order_id) {
                    $item = $this->add_order_item($order_id, $publication->id, $publication->title, $total, 1, null, $system_cat_id, 230, $group_id, $price);
                }
            }



            $data['group_id'] = $group_id;
            $data['item'] = $publication;
            $data['price'] = $price;
            $data['recurring_plan_id'] = $recurring_plan_id;
            $data['system_cat_id'] = $system_cat_id;
            $data['shipping_methods'] = $shipping_methods;
            $data['action'] = 'step2';
            //Helpers::die_pre($shipping_methods);
            return view('v1/views/market/checkout_printed_publication',$data);


        }
    }


    public function update_subscription(Request $request,$subscription_id){
        $model = new Market();
        $data = [];
        $subscription = $model->getSubscriptionDetails($subscription_id,$this->page_id,$this->lang);
        if(!$subscription){
            return redirect()->route('user_profile')->send();
        }
        $data['subscription'] = $subscription;
        $data['redirect'] = route('update_subscription');
        $data['payment_methods'] = $this->payment_methods;

        $data['price'] = $subscription->recurring_plan_price->price;
       // Helpers::die_pre($subscription);

        return view('v1/views/market/update_subscription',$data);

    }

    /**
     * Step 1 for printed publication checkout
    * @param Request $request
    * @param $publication_id
    * @param $recurring_plan_id
    * @param $system_cat_id
    * @param int $group_id
     * @return \Illuminate\View\View
     */
    public function checkout_publication(Request $request,$publication_id,$recurring_plan_id,$system_cat_id,$group_id=0){
        $data = [];
        $model = new Market();
        $lang = $this->lang;
        $publication = $model->getPublication($publication_id,$this->lang);

        if(!$publication || $publication->parent_id != null ){ return redirect()->route('homepage')->send();  }

        $error = $request->get('error');
        if($error!=null && $error!=''){
            $data['error'] = $error;
        }

        if($system_cat_id == 72 || $system_cat_id == 73){ // mixed or printed

            $data['redirect'] = route('publication_checkout',[$publication_id,$recurring_plan_id,$system_cat_id,$group_id]);
            $data['payment_methods'] = $this->payment_methods;
            $publication->title='';
            if(isset($publication->magazine->translations->$lang->title)){ $publication->title .= $publication->magazine->translations->$lang->title.': ';}
            if(isset($publication->translations->$lang->title)){ $publication->title .= $publication->translations->$lang->title;}
            $price = $this->compute_recurring_price($publication,$recurring_plan_id);
            $price->value = $price->price;

            $data['group_id'] = $group_id;
            $data['item'] = $publication;
            $data['price'] = $price;
            $data['recurring_plan_id'] = $recurring_plan_id;
            $data['system_cat_id'] = $system_cat_id;
            $data['countries'] = $model->getCountriesList($lang,8600*30);
            $data['cities'] = $model->getCitiesList($lang,8600*30);
            //Helpers::die_pre($data['cities']);
            $data['action'] = 'step1';

            return view('v1/views/market/checkout_printed_publication',$data);

        }elseif($system_cat_id == 74){ // electronic

            $data['redirect'] = route('publication_checkout',[$publication_id,$recurring_plan_id,$system_cat_id,$group_id]);
            $data['payment_methods'] = $this->payment_methods;
            $publication->title='';
            if(isset($publication->magazine->translations->$lang->title)){ $publication->title .= $publication->magazine->translations->$lang->title.': ';}
            if(isset($publication->translations->$lang->title)){ $publication->title .= $publication->translations->$lang->title;}
            $price = $this->compute_recurring_price($publication,$recurring_plan_id);
            $price->value = $price->price;

            $data['group_id'] = $group_id;
            $data['item'] = $publication;
            $data['price'] = $price;
            $data['recurring_plan_id'] = $recurring_plan_id;
            return view('v1/views/market/checkout_electronic',$data);
        }
    }


    /**
     * Electonic checkout, in case goods are in electronic form only
    * @param Request $request
    * @param $publication_id
    * @param $group_id
     * @return \Illuminate\View\View
     */
    public function electronic_checkout(Request $request,$publication_id,$group_id){
        $data = [];
        $model = new Market();
        $lang = $this->lang;
        $publication = $model->getPublication($publication_id,$this->lang);
        if(!$publication){ return redirect()->route('homepage')->send();  }
        //Helpers::die_pre($publication);
        $data['redirect'] = route('electronic_checkout',[$publication_id,$group_id]);
        $data['payment_methods'] = $this->payment_methods;
        $publication->title='';
        if(isset($publication->magazine->translations->$lang->title)){ $publication->title .= $publication->magazine->translations->$lang->title.': ';}
        if(isset($publication->translations->$lang->title)){ $publication->title .= $publication->translations->$lang->title;}


        $price = $this->compute_publication_price($publication,$this->lang,$group_id,74);
        //Helpers::die_pre($price);
        $data['group_id'] = $group_id;
        $data['item'] = $publication;
        $data['price'] = $price;
        return view('v1/views/market/checkout_electronic',$data);
    }




    /**
     * Action after payment authorization has been completed
     *  @param Request $request
     * @param $payment_service
     * @return \Illuminate\View\View
     */
    public function checkout_complete(Request $request,$payment_service){
        $success = false;
        $payment_status = null;
        $model = new Market();

        if(!is_object($this->order) && !Session::has('subscription_update_order')){
            return redirect()->route('user_orders')->send();
        }

        if($payment_service == 'paypal'){
           $transaction =  $this->confirm_paypal_transaction($request->get('paymentId'),$request->get('PayerID'));
           if($transaction){
               $success = true;
               $payment_status = 121;
           }
        }elseif($payment_service == 'viva'){
                $transaction_id = $request->get('t');
                if($transaction_id){
                    $transaction = $this->viva->check_transaction($transaction_id);
                    if(isset($transaction->Transactions[0]) && $transaction->Transactions[0]->StatusId == 'F'){ //success
                        $success = true;
                        $payment_status = 121;
                    }
                }
        }elseif($payment_service == 'bank'){
            $success = true;
            $payment_status = 120;
        }elseif($payment_service == 'delivery'){
            $success = true;
            $payment_status = 120;
        }

        $data = [];
        if($success && is_object($this->order) && !Session::has('subscription_update_order')){
            $model->updateOrder($this->lang,$this->page_id,$this->order->id,array('payment_status'=>$payment_status),isset($this->order->subscription) ? $this->order->subscription : null,$payment_service);
            $check = Session::has('items_order');

            $items_order = false;
            if(isset($check) && $check==1 ){
                $items_order = true;
            }

            if(isset($this->order->items) && count($this->order->items)>0 && $items_order==false) {
                if (count($this->order->items) == 1 && $this->order->items[0]->prod_type_option == 74) { // in case one item, electronic payment checkout
                    $data['product_type'] = $this->order->items[0]->prod_type;
                    $data['product_type_option'] = $this->order->items[0]->prod_type_option;
                    $data['product_id'] = $this->order->items[0]->prod_id;
                    $data['product_title'] = $this->order->items[0]->prod_title;
                    $data['publication'] = $model->getPublication($data['product_id'], $this->lang);
                    $data['order_type'] = 'electronic';
                    if ($data['publication']->system_cat_id == 213) {
                        if ($data['publication']->parent_id == null) {
                            $data['route'] = route('publication_single', [$data['product_id'], Helpers::urlize($data['product_title'])]);
                            $data['route_text'] = trans('application.checkout_success_electronic_simple_route_text_publication');
                        } else {
                            $data['route'] = route('issue_single', [$data['product_id'], Helpers::urlize($data['product_title'])]);
                            $data['route_text'] = trans('application.checkout_success_electronic_simple_route_text_issue');
                        }
                    } else {
                        $data['route'] = route('book_single', [$data['product_id'], Helpers::urlize($data['product_title'])]);
                        $data['route_text'] = trans('application.checkout_success_electronic_simple_route_text_book');
                    }
                }
            }elseif(isset($this->order->items) && count($this->order->items) > 0 && $items_order == 1){

                //todo manipulate the case of cart items
                Cart::destroy();
                $data['order_type'] = 'items';
                $data['order_details'] = $this->order;
                foreach($this->payment_methods as $payment_method):
                    if($this->order->payment_method == $payment_method->id){
                        $data['payment_method'] = $payment_method;
                    }
                endforeach;
                foreach($data['order_details']->items as $item):
                    $publication = $model->getPublication($item->prod_id,$this->lang);
                    if(isset($publication->thumb->cdn)){
                        $item->thumb = $publication->thumb->cdn;
                    }
                    if($publication->system_cat_id == 213){ // publication
                        if ($publication->parent_id == null) {
                            $item->url = route('publication_single', [$publication->id, Helpers::urlize($item->prod_title)]);
                        } else {
                            $item->url = route('issue_single',  [$publication->id, Helpers::urlize($item->prod_title)]);
                        }
                    }elseif($publication->system_cat_id == 212){ //book
                        $item->url = route('book_single', [$publication->id, Helpers::urlize($item->prod_title)]);
                    }

                    if ($publication->parent_id == null) {
                        $data['route'] = route('publication_single',  [$publication->id, Helpers::urlize($item->prod_title)]);
                        $data['route_text'] = trans('application.checkout_success_electronic_simple_route_text_publication');
                    } else {
                        $data['route'] = route('issue_single',  [$publication->id, Helpers::urlize($item->prod_title)]);
                        $data['route_text'] = trans('application.checkout_success_electronic_simple_route_text_issue');
                    }
                endforeach;
               // Helpers::die_pre($this->order);
               Session::forget('items_order');

            }elseif(isset($this->order->subscription)){

                if($this->order->shipping_method != null){

                    $data['order_type'] = 'printed_subscription';
                    $data['order_details'] = $this->order;
                    foreach($this->payment_methods as $payment_method):
                        if($this->order->payment_method == $payment_method->id){
                            $data['payment_method'] = $payment_method;
                        }
                    endforeach;
                }else{
                    $data['order_type'] = 'electronic_subscription';
                }

                $data['route_text'] = trans('application.checkout_success_electronic_simple_route_text_issue');
                $data['product_title'] =  $this->order->subscription->prod_title;
            }

           // Helpers::die_pre($data['payment_method']);
            Session::forget('order_id');
            return view('v1/views/market/checkout_success',$data);
        }elseif(Session::has('subscription_update_order') && $success){

            $subscription = $model->getSubscriptionDetails($this->order->subscription->id,$this->page_id,$this->lang);
            // Helpers::die_pre($subscription);
            //Helpers::die_pre($subscription);
            //$expire_date = $this->updateSubscriptionExecute($subscription->id,$subscription->recurring_plan,$subscription->expires);
            //if($expire_date){
                //$subscription->expires = $expire_date;
                Session::forget('subscription_update_order');
                Session::forget('order_id');
                $data['subscription'] = $subscription;
                return view('v1/views/market/checkout_update_success',$data);
            //}

        }else{

            return view('v1/views/market/checkout_error',$data);
        }

    }

    /**
     * Perform viva checkout
     * @param $request
        * @param $payment_method_id
        * @param $item_id
        * @param $system_id
        * @param $group_id
        * @param string $type
     */
    public function viva_checkout(Request $request, $payment_method_id,$item_id,$system_id,$group_id,$type='electronic',$recurring_plan_id=null){
        if(count($this->payment_methods)>0 && isset($this->payment_methods[$payment_method_id])){

            $method = $this->payment_methods[$payment_method_id];
            $model = new Market();

            if($type =='electronic' || $type == 'printed') {
                $publication = $model->getPublication($item_id, $this->lang);
                $lang = $this->lang;
                if (!$publication) {
                    return redirect()->route('homepage')->send();
                }
                $publication->title = '';
                if (isset($publication->magazine->translations->$lang->title)) {
                    $publication->title .= $publication->magazine->translations->$lang->title . ': ';
                }
                if (isset($publication->translations->$lang->title)) {
                    $publication->title .= $publication->translations->$lang->title;
                }
            }

            if($type == 'electronic'){
                $order_id = $this->create_order($request,$method->id,$recurring_plan_id);
                $system_type_option = 74;
            }


            if($type == 'update_subscription'){
                $subscription = $model->getSubscriptionDetails($item_id,$this->page_id,$this->lang);
                if(!$subscription){
                    return redirect()->route('user_profile')->send();
                }
            }

            if($type=='electronic'){

                if($recurring_plan_id == null) {
                    $price = $this->compute_publication_price($publication, $this->lang, $group_id, $system_id);
                }else{
                    $price = $this->compute_recurring_price($publication,$recurring_plan_id);
                    $price->value = $price->price;
                }
                if(isset($price->value)){ $total = $price->value; }

                if($order_id){
                    $item = $this->add_order_item($order_id,$publication->id,$publication->title,$total,1,null,$system_type_option,230,$group_id);
                }

            }elseif($type=='printed'){

                $shipping_method_selected = $request->get('shipping_selected');
                $rate = $this->find_shipping_rate($shipping_method_selected,$publication);

                $total = $this->order->subscription->subtotal+$rate->rate_value;
                $update_data = array(
                    'subtotal'=>$this->order->subscription->subtotal,
                    'shipping_method'=>$shipping_method_selected,
                    'shipping'=>$rate->rate_value,
                    'total'=> $total,
                    'billing_method'=>$payment_method_id
                );
                $update = $model->updateOrder($this->lang,$this->page_id,$this->order->id,$update_data);
            }elseif($type == 'items'){
                $weight = 0;
                foreach($this->cart_items as $item):
                    $publication = $model->getPublication($item['id'],$this->lang);
                    if(isset($publication) && $publication->weight!=null){
                        $weight += $publication->weight*$item['qty'];
                    }
                endforeach;

                $shipping_method_selected = $request->get('shipping_selected');
                $shipping_methods = $model->getShippingMethods($this->lang,$this->page_id,8600*30);

                $rate = $model->getShippingRate($this->lang,$this->page_id,$shipping_method_selected,$weight,$this->order->user_ship_address_country,$this->order->user_ship_address_city);


                $total = $this->order->subtotal+$rate->rate_value;
                $update_data = array(
                    'subtotal'=>$this->order->subtotal,
                    'shipping_method'=>$shipping_method_selected,
                    'shipping'=>$rate->rate_value,
                    'total'=> $total,
                    'billing_method'=>$payment_method_id
                );
                $update = $model->updateOrder($this->lang,$this->page_id,$this->order->id,$update_data);

            }elseif($type == 'update_subscription'){
                $total = $subscription->recurring_plan_price->price;
                $update = true;
                $order_id = $this->create_order($request,$method->id,$subscription->recurring_plan->id);
                $system_type_option = 74;
                if($order_id){
                    $subscription->recurring_plan->plan_details =  $subscription->recurring_plan;
                    $subscription->recurring_plan->id = $subscription->recurring_plan_price->id;
                    $item = $this->add_order_item($order_id,null,$subscription->prod_title,$total,1,null,$system_type_option,230,$subscription->user_group_id,$subscription->recurring_plan,null,$subscription->expires);
                }
            }


            if(isset($total)) {

                $this->viva->create_order($total * 100, $method->viva_redirect_source_code);
                $order_id = $this->viva->getResponse();

                if ($order_id) {
                        if(isset($update) && $update){
                            $request->session()->put('subscription_update_order', $order_id);
                        }
                        $redirect = $this->viva->send_payment($order_id);
                        return Redirect::to($redirect);

                } else {
                    $referrer =$request->server('HTTP_REFERER');
                    return Redirect::to($referrer);
                    //redirect('booking/checkout?'.http_build_query($args));
                }
            }


        }


    }

    /**
     * Perform paypal checkout
    * @param Request $request
    * @param $payment_method_id
    * @param $item_id
    * @param $system_id
    * @param $group_id
    * @param string $type
     * @return mixed
     */
    public function paypal_checkout(Request $request,$payment_method_id,$item_id,$system_id,$group_id,$type='electronic',$recurring_plan_id=null){
        if(count($this->payment_methods)>0 && isset($this->payment_methods[$payment_method_id])){
            $method = $this->payment_methods[$payment_method_id];
            $model = new Market();

            if($type =='electronic' || $type == 'printed') {
                $publication = $model->getPublication($item_id, $this->lang);
                if (!$publication) {
                    return redirect()->route('homepage')->send();
                }
                $lang = $this->lang;
                $publication->title = '';
                if (isset($publication->magazine->translations->$lang->title)) {
                    $publication->title .= $publication->magazine->translations->$lang->title . ': ';
                }
                if (isset($publication->translations->$lang->title)) {
                    $publication->title .= $publication->translations->$lang->title;
                }
            }
            if($type == 'electronic'){
                $system_type_option = 74;
                $order_id = $this->create_order($request,$method->id,$recurring_plan_id);

            }

            if($type == 'update_subscription'){
                $subscription = $model->getSubscriptionDetails($item_id,$this->page_id,$this->lang);
                if(!$subscription){
                    return redirect()->route('user_profile')->send();
                }
            }

            if($type == 'electronic'){

                if($recurring_plan_id == null) {
                    $price = $this->compute_publication_price($publication, $this->lang, $group_id, $system_id);
                }else{
                    $price = $this->compute_recurring_price($publication,$recurring_plan_id);
                    $price->value = $price->price;
                }

                if(isset($price->value)){ $total = $price->value; }
                if($order_id){
                    $item = $this->add_order_item($order_id,$publication->id,$publication->title,$total,1,null,$system_type_option,230,$group_id);
                }
            }elseif($type=='printed'){

                $shipping_method_selected = $request->get('shipping_selected');
                $rate = $this->find_shipping_rate($shipping_method_selected,$publication);

                $total = $this->order->subscription->subtotal+$rate->rate_value;
                $update_data = array(
                    'subtotal'=>$this->order->subscription->subtotal,
                    'shipping_method'=>$shipping_method_selected,
                    'shipping'=>$rate->rate_value,
                    'total'=> $total,
                    'billing_method'=>$payment_method_id
                );

                $update = $model->updateOrder($this->lang,$this->page_id,$this->order->id,$update_data);
            }elseif($type == 'items'){
                $weight = 0;
                foreach($this->cart_items as $item):
                    $publication = $model->getPublication($item['id'],$this->lang);
                    if(isset($publication) && $publication->weight!=null){
                        $weight += $publication->weight*$item['qty'];
                    }
                endforeach;

                $shipping_method_selected = $request->get('shipping_selected');
                $shipping_methods = $model->getShippingMethods($this->lang,$this->page_id,8600*30);

                $rate = $model->getShippingRate($this->lang,$this->page_id,$shipping_method_selected,$weight,$this->order->user_ship_address_country,$this->order->user_ship_address_city);


                $total = $this->order->subtotal+$rate->rate_value;
                $update_data = array(
                    'subtotal'=>$this->order->subtotal,
                    'shipping_method'=>$shipping_method_selected,
                    'shipping'=>$rate->rate_value,
                    'total'=> $total,
                    'billing_method'=>$payment_method_id
                );
                $update = $model->updateOrder($this->lang,$this->page_id,$this->order->id,$update_data);

            }elseif($type == 'update_subscription'){
                $total = $subscription->recurring_plan_price->price;
                $update = true;
                $order_id = $this->create_order($request,$method->id,$subscription->recurring_plan->id);
                $system_type_option = 74;
                if($order_id){
                    $subscription->recurring_plan->plan_details =  $subscription->recurring_plan;
                    $subscription->recurring_plan->id = $subscription->recurring_plan_price->id;
                    $item = $this->add_order_item($order_id,null,$subscription->prod_title,$total,1,null,$system_type_option,230,$subscription->user_group_id,$subscription->recurring_plan,null,$subscription->expires);
                }
            }

            if(!isset($order_id) && isset($this->order)){
                $order_id = $this->order->id;
            }
            if(isset($total)) {
                if(isset($update) && $update){
                    $request->session()->put('subscription_update_order', $order_id);
                }
                $payer = PayPal::Payer();
                $payer->setPaymentMethod('paypal');

                $amount = PayPal::Amount();
                $amount->setCurrency('EUR');
                $amount->setTotal($total);

                $transaction = PayPal::Transaction();
                $transaction->setAmount($amount);
                if($type == 'update_subscription'){
                    $transaction->setDescription($subscription->prod_title);

                }else{
                    $transaction->setDescription($publication->title);

                }


                $referrer =$request->server('HTTP_REFERER').'/?declined=paypal';
                $redirectUrls = PayPal:: RedirectUrls();
                $redirectUrls->setReturnUrl(route('checkout_success',['paypal']));
                $redirectUrls->setCancelUrl($referrer);

                $payment = PayPal::Payment();
                $payment->setIntent('sale');
                $payment->setPayer($payer);
                $payment->setRedirectUrls($redirectUrls);
                $payment->setTransactions(array($transaction));

                try {
                    $response = $payment->create($this->_apiContext);
                }catch (PayPalConnectionException $e){
                    Helpers::die_pre($e->getMessage());
                }

                $redirectUrl = $response->links[1]->href;

                return Redirect::to( $redirectUrl );

            }
        }


    }

    /**
     * Perform bank checkout
     * @param Request $request
     * @param $payment_method_id
     * @param $item_id
     * @param $system_id
     * @param $group_id
     * @param string $type
     * @return mixed
     */
    public function bank_checkout(Request $request,$payment_method_id,$item_id,$system_id,$group_id,$type='electronic',$recurring_plan_id=null){
        if(count($this->payment_methods)>0 && isset($this->payment_methods[$payment_method_id])){
            $method = $this->payment_methods[$payment_method_id];
            $model = new Market();

            if($type!='items') {
                $publication = $model->getPublication($item_id, $this->lang);
                if (!$publication) {
                    return redirect()->route('homepage')->send();
                }
                $lang = $this->lang;
                $publication->title = '';
                if (isset($publication->magazine->translations->$lang->title)) {
                    $publication->title .= $publication->magazine->translations->$lang->title . ': ';
                }
                if (isset($publication->translations->$lang->title)) {
                    $publication->title .= $publication->translations->$lang->title;
                }
            }
            if($type == 'electronic'){
                $system_type_option = 74;
                $order_id = $this->create_order($request,$method->id,$recurring_plan_id);

            }


            if($type == 'electronic'){

                if($recurring_plan_id == null) {
                    $price = $this->compute_publication_price($publication, $this->lang, $group_id, $system_id);
                }else{
                    $price = $this->compute_recurring_price($publication,$recurring_plan_id);
                    $price->value = $price->price;
                }

                if(isset($price->value)){ $total = $price->value; }
                if($order_id){
                    $item = $this->add_order_item($order_id,$publication->id,$publication->title,$total,1,null,$system_type_option,230,$group_id,$price);
                }
            }elseif($type=='printed'){

                $shipping_method_selected = $request->get('shipping_selected');
                $rate = $this->find_shipping_rate($shipping_method_selected,$publication);

                $total = $this->order->subscription->subtotal+$rate->rate_value;
                $update_data = array(
                    'subtotal'=>$this->order->subscription->subtotal,
                    'shipping_method'=>$shipping_method_selected,
                    'shipping'=>$rate->rate_value,
                    'total'=> $total,
                    'billing_method'=>$payment_method_id
                );

                $update = $model->updateOrder($this->lang,$this->page_id,$this->order->id,$update_data);
            }elseif($type == 'items'){
                $weight = 0;
                foreach($this->cart_items as $item):
                    $publication = $model->getPublication($item['id'],$this->lang);
                    if(isset($publication) && $publication->weight!=null){
                        $weight += $publication->weight*$item['qty'];
                    }
                endforeach;

                $shipping_method_selected = $request->get('shipping_selected');
                $shipping_methods = $model->getShippingMethods($this->lang,$this->page_id,8600*30);

                $rate = $model->getShippingRate($this->lang,$this->page_id,$shipping_method_selected,$weight,$this->order->user_ship_address_country,$this->order->user_ship_address_city);


                $total = $this->order->subtotal+$rate->rate_value;
                $update_data = array(
                    'subtotal'=>$this->order->subtotal,
                    'shipping_method'=>$shipping_method_selected,
                    'shipping'=>$rate->rate_value,
                    'total'=> $total,
                    'billing_method'=>$payment_method_id
                );
                $update = $model->updateOrder($this->lang,$this->page_id,$this->order->id,$update_data);

            }


            if(isset($total)) {

                return redirect()->route('checkout_success',['bank'])->send();

            }
        }


    }


    /**
     * Perform delivery checkout
     * @param Request $request
     * @param $payment_method_id
     * @param $item_id
     * @param $system_id
     * @param $group_id
     * @param string $type
     * @return mixed
     */
    public function delivery_checkout(Request $request,$payment_method_id,$item_id,$system_id,$group_id,$type='electronic',$recurring_plan_id=null){
        if(count($this->payment_methods)>0 && isset($this->payment_methods[$payment_method_id])){
            $method = $this->payment_methods[$payment_method_id];
            $model = new Market();

            if($type!='items') {
                $publication = $model->getPublication($item_id, $this->lang);
                if (!$publication) {
                    return redirect()->route('homepage')->send();
                }
                $lang = $this->lang;
                $publication->title = '';
                if (isset($publication->magazine->translations->$lang->title)) {
                    $publication->title .= $publication->magazine->translations->$lang->title . ': ';
                }
                if (isset($publication->translations->$lang->title)) {
                    $publication->title .= $publication->translations->$lang->title;
                }
            }

            if($type!='items') {

                $shipping_method_selected = $request->get('shipping_selected');
                $rate = $this->find_shipping_rate($shipping_method_selected, $publication);

                $total = $this->order->subscription->subtotal + $rate->rate_value + $method->pay_on_delivery_extra;
                $update_data = array(
                    'subtotal' => $this->order->subscription->subtotal,
                    'shipping_method' => $shipping_method_selected,
                    'shipping' => $rate->rate_value,
                    'total' => $total,
                    'billing_method' => $payment_method_id,
                    'on_delivery' => $method->pay_on_delivery_extra
                );

            }elseif($type == 'items'){
                $weight = 0;
                foreach($this->cart_items as $item):
                    $publication = $model->getPublication($item['id'],$this->lang);
                    if(isset($publication) && $publication->weight!=null){
                        $weight += $publication->weight*$item['qty'];
                    }
                endforeach;

                $shipping_method_selected = $request->get('shipping_selected');
                $shipping_methods = $model->getShippingMethods($this->lang,$this->page_id,8600*30);

                $rate = $model->getShippingRate($this->lang,$this->page_id,$shipping_method_selected,$weight,$this->order->user_ship_address_country,$this->order->user_ship_address_city);


                $total = $this->order->subtotal+$rate->rate_value;
                $update_data = array(
                    'subtotal'=>$this->order->subtotal,
                    'shipping_method'=>$shipping_method_selected,
                    'shipping'=>$rate->rate_value,
                    'total'=> $total,
                    'billing_method'=>$payment_method_id
                );
                $update = $model->updateOrder($this->lang,$this->page_id,$this->order->id,$update_data);

            }
            $update = $model->updateOrder($this->lang,$this->page_id,$this->order->id,$update_data);



            if(isset($total)) {

                return redirect()->route('checkout_success',['delivery'])->send();

            }
        }


    }

    /**
     * Confirm & execute paypal transaction
    * @param $pay_id
    * @param $payer_id
     * @return mixed
     */
    protected function confirm_paypal_transaction($pay_id,$payer_id){

        $payment = PayPal::getById($pay_id, $this->_apiContext);
        $paymentExecution = PayPal::PaymentExecution();

        if($payment->getState() == 'approved'){
            return $payment->getId();
        }

        $paymentExecution->setPayerId($payer_id);
        $executePayment = $payment->execute($paymentExecution, $this->_apiContext);
        if($executePayment->getState() == 'approved'){
            return $executePayment->getId();
        }

    }



    /**
     * Compute Publication price
    * @param $publication
    * @param $lang
    * @param $group_id
    * @param $system_type_id
     * @return \stdClass
     */
    protected function compute_publication_price($publication,$lang,$group_id,$system_type_id){
        $price = new \stdClass();


        if(isset($publication->prices) && is_array($publication->prices) && count($publication->prices)>0){ //issue or magazine has prices set
            $pub_prices = $publication->prices;
        }elseif(isset($publication->magazine) && isset($publication->magazine->prices) && is_array($publication->magazine->prices) && count($publication->magazine->prices)>0){ //else if issue, check if issue's parent magazine has prices set
            $pub_prices = $publication->magazine->prices;
        }elseif(isset($publication->base_price ) && $publication->base_price!=null){ // else check if issue or mag has base price
            $price->value =  $publication->base_price;
            $price->vat = 1;
            $price->title = trans('application.cart_item_title_base_price');
        }elseif(isset($publication->magazine) && isset($publication->magazine->base_price ) && $publication->magazine->base_price!=null){ // in case issue, check if issue's magazine has base price
            $price->value = $publication->magazine->base_price;
            $price->vat = 1;
            $price->title = trans('application.cart_item_title_base_price');
        }

        if(isset($pub_prices) && is_array($pub_prices) && count($pub_prices)>0){
            foreach($pub_prices as $pub_price){

                if($pub_price->system_type_id == $system_type_id && $pub_price->user_group_id == $group_id){
                    $price->value = $pub_price->price;
                    $price->vat = $pub_price->tax_inclusive;
                    $price->title = isset($publication->translations->$lang->title) ? $publication->translations->$lang->title : current($publication->translations)->title;
                    $price->system_translation = $pub_price->system_type_id_translation;
                    if(isset($pub_price->group_translation)){
                        $price->group_translation = $pub_price->group_translation;
                    }
                    break;
                }
            }
        }

        return $price;
    }

    /**
     * Compute publication price
    * @param $publication
    * @param $recurring_id
     * @return mixed
     */
    protected function compute_recurring_price($publication,$recurring_id){
        if(isset($publication->recurring_plans) && is_array($publication->recurring_plans) && count($publication->recurring_plans)>0){
            foreach($publication->recurring_plans as $re):
                if(isset($re->prices) && is_array($re->prices) && count($re->prices)>0){
                    foreach($re->prices as $price):
                            if($price->id == $recurring_id){
                                $price->plan_details = $re;
                                unset($price->plan_details->prices);
                                return $price;
                            }
                    endforeach;
                }
            endforeach;
        }
    }

    /**
     * Create a new order
    * @param Request $request
    * @param null $payment_method
    * @param null $recurring_plan_id
    * @param null $profile_data
     * @return mixed
     */
    protected function create_order(Request $request,$payment_method=null,$recurring_plan_id=null,$profile_data=null){
        $model = new Market();
        $profile = array(
            'user_id'=> $this->authUser->id,
            'ip_address'=> $request->getClientIp(),
            'email' => $this->authUser->email,
            'firstname' => $this->authUser->firstname ,
            'lastname' =>  $this->authUser->lastname ,
        );
        if(is_array($profile_data)){
            $profile = array_merge($profile, $profile_data);
            if(is_object($this->authUser) && !isset($this->authUser->locations) || ( isset($this->authUser->locations) && count($this->authUser->locations) == 0 ) ){
                $this->register_user_location($this->authUser->id,$profile_data);
            }
        }
        $order_id = $model->createOrder($this->lang,$this->page_id,$profile,165,$payment_method,$recurring_plan_id);
        if($order_id){
            $request->session()->put('order_id', $order_id);
            return $order_id;
        }
    }

    protected function register_user_location($user_id,$data){
        $model = new Market();

        $post_data  = [
            'country_id'=>$data['user_bill_address_country'],
            'address1'=>$data['user_bill_address1'],
            'address2'=>$data['user_bill_address2'],
            'zip'=>$data['user_bill_address_zip'],
            'phone'=>$data['user_bill_address_phone'],
            'city_id'=>is_numeric($data['user_bill_address_city']) ? $data['user_bill_address_city'] : null,
            'city_name'=>!is_numeric($data['user_bill_address_city']) ? $data['user_bill_address_city'] : null,
            'is_default'=>1,

        ];

        $location = $model->registerUserLocation($user_id,$this->lang,$post_data);
        if($location){
            return $location;
        }
    }

    /**
     * Add an item in order
    * @param $order_id
    * @param $id
    * @param $title
    * @param $price
    * @param $quantity
    * @param null $order
    * @param null $prod_type_option
    * @param int $prod_type
    * @param null $group_id
    * @param null $recurring_plan
     */
    protected function add_order_item($order_id,$id=null,$title,$price,$quantity,$order=null,$prod_type_option=null,$prod_type=230,$group_id=null,$recurring_plan=null,$cart_order=null,$old_expire=null){
        $model = new Market();

        if($recurring_plan == null) {

            $data = array(
                'product_id' => $id,
                'title' => $title,
                'price' => $price,
                'quantity' => $quantity,
                'subtotal'=>$price*$quantity

            );

            $item = $model->addProductToOrder($order_id, $this->lang, $this->page_id, $data, $prod_type_option, $prod_type, $group_id);

            if ($item) {
                if (is_object($order) && isset($order->subtotal)) {
                    $price = $order->subtotal + $price;
                    $total = $order->total + $price;
                } else {
                    $total = $price;
                }

                $update = $model->updateOrder($this->lang, $this->page_id, $order_id,
                    array(
                        'subtotal' => is_object($cart_order) ? $cart_order->subtotal : $price,
                        'total' => is_object($cart_order) ? $cart_order->total : $total
                    )
                );

            }

        }else{

            $plan_details = $recurring_plan->plan_details;
            if($old_expire == null) {
                $date = new \DateTime(date("Y-m-d h:i:s"));
            }else{
                $date = new \DateTime(date("Y-m-d h:i:s",strtotime($old_expire)));
            }
            if($plan_details->period_id >= 76 && $plan_details->period_id <= 78){
                $date->modify('+ '.$plan_details->duration.' month');
            }elseif($plan_details->period_id == 79){
                $date->modify('+ '.$plan_details->duration.' week');
            }elseif($plan_details->period_id == 80){
                $date->modify('+ '.$plan_details->duration.' day');
            }elseif($plan_details->period_id > 80){
                $date->modify('+ '.$plan_details->duration.' month');
            }
            $expire_date = $date->format("Y-m-d h:i:s");

            $data = [
                'order_id'=>$order_id,
                'plan_id'=>$recurring_plan->id,
                'status_id'=>70,
                'system_type_id'=>$prod_type_option,
                'user_group_id'=>$group_id,
                'prod_title'=>$title,
                'price'=>$price,
                'quantity'=>$quantity,
                'taxes'=>0,
                'expires'=>$expire_date
            ];


            $subscription = $model->createRecurringSubscription($data);
            if($subscription) {
                $update = $model->updateOrder($this->lang, $this->page_id, $order_id,
                    array(
                        'subtotal' => $price,
                        'total' => $price
                    )
                );
            }

        }

    }

    /**
     * Compute shipping rate of order
    * @param $shipping_method_selected
    * @param $publication
     * @return mixed
     */
    protected function find_shipping_rate($shipping_method_selected,$publication){
        $model = new Market();
        $shipping_methods = $model->getShippingMethods($this->lang,$this->page_id,8600*30);
        if(is_array($shipping_methods) && count($shipping_methods)>0){
            foreach($shipping_methods as $key=>$shipping_method):

                if($shipping_method->id == $shipping_method_selected){

                    $rate = $model->getShippingRate($this->lang,$this->page_id,$shipping_method->id,$publication->weight!=null?$publication->weight:0.5,$this->order->user_ship_address_country,$this->order->user_ship_address_city);
                    if($rate){
                        if($rate->rate_type_id == 109){
                            $shipping_method->shipping_rate = $rate->rate_value;
                        }elseif($rate->rate_type_id == 110){
                            $shipping_method->shipping_rate = $rate->rate_value *  ($this->order->subscription->subtotal/100);
                        }

                        return $rate;
                    }
                }
            endforeach;
        }
    }


    protected function updateSubscriptionExecute($subscription_id,$recurring_plan,$old_date){
        $model = new Market();
        $plan_details = $recurring_plan;
        $date = new \DateTime(date("Y-m-d h:i:s",strtotime($old_date)));

        if($plan_details->period_id >= 76 && $plan_details->period_id <= 78){
            $date->modify('+ '.$plan_details->duration.' month');
        }elseif($plan_details->period_id == 79){
            $date->modify('+ '.$plan_details->duration.' week');
        }elseif($plan_details->period_id == 80){
            $date->modify('+ '.$plan_details->duration.' day');
        }elseif($plan_details->period_id > 80){
            $date->modify('+ '.$plan_details->duration.' month');
        }

        $expire_date = $date->format("Y-m-d h:i:s");
        $update = $model->updateSubscription($subscription_id,$expire_date);
        return $expire_date;
    }



    public function connect_subscription(Request $request){
        $model = new Market();
        $recurring_plans = $model->getAllRecurringPlans($this->page_id,$this->lang);

        if( $request->getMethod() == 'POST' ){
            $post_data = $request->all();
            $this->authUser->firstname = $post_data['billing_firstname'];
            $this->authUser->lastname = $post_data['billing_lastname'];

            $the_plan = null;
            foreach($recurring_plans as $recurring_plan){
                if($recurring_plan->id == $post_data['recurring_plan_id']){
                    $the_plan = $recurring_plan;

                }
            }

            $i = clone $the_plan;
            $the_plan->plan_details = $i;
            $user_ship_address1 =  $post_data['billing_address'];
            $user_ship_address2 =  $post_data['billing_address2'];
            if($post_data['billing_country'] == 95){
                $user_ship_address_city = $post_data['billing_city'];
            }else{
                $user_ship_address_city = $post_data['billing_city_alt'];
            }

            $user_ship_address_country=  $post_data['billing_country'];
            $user_ship_address_zip=  $post_data['billing_zip'];
            $user_ship_address_phone =  $post_data['billing_phone'];
            $receiver = $post_data['billing_firstname'].' '.$post_data['billing_lastname'];


            $profile_data = [
                'user_ship_address1' => $user_ship_address1,
                'user_ship_address2' => $user_ship_address2,
                'user_ship_address_city' => $user_ship_address_city,
                'user_ship_address_zip' => $user_ship_address_zip,
                'user_ship_address_phone' => $user_ship_address_phone,
                'user_ship_address_country' => $user_ship_address_country,
                'user_bill_address1' => $post_data['billing_address'],
                'user_bill_address2' => $post_data['billing_address2'],
                'user_bill_address_city' => $post_data['billing_country'] == 95 ? $post_data['billing_city'] : $post_data['billing_city_alt'],
                'user_bill_address_zip' => $post_data['billing_zip'],
                'user_bill_address_phone' => $post_data['billing_phone'],
                'user_bill_address_country' => $post_data['billing_country'],
                'receiver'=>$receiver
            ];
           // Helpers::die_pre($profile_data);

            $order_id = $this->create_order($request, null, $post_data['recurring_plan_id'], $profile_data);
            //Helpers::die_pre($order_id);
            if($order_id) {
                $cookie = Cookie::forever('clickedSubscriptions', 1);
                $request->session()->flash('success', 'subscription_connection_success');
                return redirect()->route('user_subscriptions')->withCookie($cookie);

            }else{
                $request->session()->flash('error', 'subscription_connection_error');
            }
        }
    }

}


