<?php

namespace WebKiosk\Http\Controllers\Frontend;

use WebKiosk\Http\Controllers\Core\LoggiaController;
use Illuminate\Http\Request;
use Helpers;
use WebKiosk\ApiModels\Articles;

class PagesController extends LoggiaController
{
    public function index(Request $request){

        Helpers::die_pre('pages index');
    }

    public function single(Request $request,$id,$slug=null){
        $data = [];
        $cats = [];
        $model = new Articles();
        $data['unlock'] = true;
        $user_id = is_object($this->authUser) && isset($this->authUser->id) ? $this->authUser->id : null;
        $entry = $model->getEntryDetails($this->lang,$id,$user_id,false);

        if(!$entry){
            return redirect()->route('homepage')->send();
        }
        $additional = $model->getEntryAdditional($this->lang,$id);

        if(isset($entry->entry->cat_id) && $entry->entry->cat_id!=null && $entry->entry->cat_id>0){
            $category = $model->getCategoryDetails($this->lang,$entry->entry->cat_id);

            if($category && isset($category->siblings)){
                $data['category_siblings'] = $category->siblings;
            }
        }

        $data['lock_status'] = $this->check_article_permissions($entry->entry->id,$user_id,$entry->permission);
        if(isset($entry->entry->categories) && is_array($entry->entry->categories) && count($entry->entry->categories)>0){
            foreach($entry->entry->categories as $category){
                $cats[$category->id] = $category->title;
                /** Checks if banners have permission to be shown on current category */
                $this->check_banner_category_permissions($category->id,2);
                if(isset($category->parent_id) && $category->parent_id != null){
                    $this->check_banner_category_permissions($category->parent_id,2);
                }
            }
        }
        $posts = $model->getLatestPosts($this->lang,100,0,false,241,'ASC');
        if($posts){
            foreach($posts as $key=>$relative_post):
                if($relative_post->id == $entry->entry->id){
                    unset($posts[$key]);
                }
            endforeach;
            $data['other_posts'] = $posts;
            //Helpers::die_pre($posts);
        }
        $data['cats'] = $cats;
        $data['entry'] = $entry->entry;
        if(isset($additional->views)){
            $data['entry']->views_count = $additional->views;
        }
        if(isset($additional->related)){
            $data['entry']->related = $additional->related;
        }
        if(isset($entry->article_tags) && is_array($entry->article_tags) && count($entry->article_tags)>0){
            $data['tags'] = $entry->article_tags;
        }
        //Helpers::die_pre($data);
        return view('v1/views/pages/single',$data);
    }

    public function category(Request $request,$cat_id,$slug=null){
        $model = new Articles();
        $category = $model->getCategoryDetails($this->lang,$cat_id);

        if(!$category){
            return redirect()->route('homepage')->send();
        }
        /** Checks if banners have permission to be shown on current category */
        $this->check_banner_category_permissions($category->parent_id,2);
        $data = array();
        $data['category'] = $category;
        $data['type'] =241;

        return view('v1/views/pages/list',$data);
    }

}
