<?php

namespace WebKiosk\Http\Controllers\Frontend;

use WebKiosk\Http\Controllers\Core\LoggiaController;
use Helpers;
use Illuminate\Http\Request;
use WebKiosk\ApiModels\Users;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\Input;
use Socialite;
use Mail;
use WebKiosk\Http\Controllers\Frontend\UserController;

class AuthController extends LoggiaController
{

    public function __construct(){
        parent::__construct();
        $user = Session::has('authUser');
        if($user==1 && !Session::has('redirectRoute')){
             redirect()->route('user_profile')->send();
        }
    }

    /**
     * Prepare and redirect url for social login
     * @param Request $request
     * @param $method
     */
    public function social_login(Request $request,$method){

        if(isset($this->socialApps->facebook) && $method == 'facebook'){
            return Socialite::driver('facebook')->redirect();
        }elseif(isset($this->socialApps->twitter) && $method == 'twitter'){
            return Socialite::driver('twitter')->redirect();
        }elseif(isset($this->socialApps->google) && $method == 'google'){
            return Socialite::driver('google')->redirect();
        }

        redirect()->route('login')->send();
    }

    /**
     * Return from social network url and proccess data for register/login user
     * @param Request $request
     * @param $method
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function social_login_user(Request $request,$method){

        if($method == 'facebook') {
            $user = Socialite::driver('facebook')->user();
        }elseif($method == 'twitter'){
            $user = Socialite::driver('twitter')->user();
        }elseif($method == 'google'){
            $user = Socialite::driver('google')->user();
        }else{
            return redirect()->route('login')->send();
        }



        $redirect = $request->get('redirect');
        if($redirect){
            $data['redirect'] = $redirect;
        }
        if(is_object($user)){

            if($method == 'facebook') {
                $user = $user->user;
                $data['id'] = $user['id'];
            }elseif($method == 'twitter'){
                $name = explode(' ',$user->name);
                $data = [
                    'first_name'=>$name[0],
                    'last_name'=>isset($name[1])? $name[1]: $user->nickname,
                    'username'=>$user->nickname,
                    'email'=>$user->nickname.'@twitter.com',
                    'id'=>$user->id
                ];
                $user = $data;
            }elseif($method == 'google'){
                $name = explode(' ',$user->name);
                $data = [
                    'first_name'=>$name[0],
                    'last_name'=>isset($name[1])? $name[1]: $user->nickname,
                    'username'=>isset($user->nickname) && $user->nickname!=null ? $user->nickname : $user->email,
                    'email'=>$user->email,
                    'google_avatar'=>$user->avatar,
                    'id'=>$user->id
                ];
                $user = $data;
            }

            $user['method'] = $method;

            /** This is used on user_profile in order to connect social networks with user profile */
            if(Session::has('redirectRoute')){
                return redirect()->route(Session::get('redirectRoute'),[$data['id'],$method])->send();
            }
            /** end */

            $user = $this->login_social_user($user);
            if($user){
                $request->session()->put('authUser', $user);
                if($redirect){
                    return redirect()->to($redirect)->send();
                }
                return redirect()->route('user_profile')->with('status', 'social_user')->send();
            }
        }

        return redirect()->route('login')->send();


    }


    /**
     * Login user by email
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function login(Request $request){
        $data = [];
        $callback = Session::get('status');

        $redirect = $request->get('redirect');
        if($redirect){
            $data['redirect'] = $redirect;
        }

        if($request->getMethod() == 'POST') {

            $post_data = $request->all();
            $validator = Validator::make($post_data, [
                'email' => 'required|email',
                'password' => 'required|min:6'
            ]);
            if ($validator->fails()) {
                $data['form_errors']['login']['validation'] = $validator->errors();
            }else{
                $model = new Users();
                $user = $model->loginUser($post_data['email'],$post_data['password']);

                if(is_object($user) && isset($user->user_id)){
                    $request->session()->put('authUser', $user);

                    if($redirect){
                        return redirect()->to($redirect)->send();
                    }
                    return redirect()->route('user_profile')->with('status', 'user_loggedin');
                }
                $data['form_errors']['login']['message'] = 'auth_login_credentials_wrong';
                $data['form_errors']['login']['email'] = $post_data['email'];
            }
        }

        if($callback == 'register_failed'){
            $data['form_errors']['register']['message'] = 'auth_register_failed';
            $data['form_errors']['register']['data'] = Input::old();
        }

        //
       // $social = Socialite::driver('facebook')->with(['client_id' => $this->socialApps->facebook->app_id,'client_secret' => $this->socialApps->facebook->app_secret,'redirect' => 'http://news.dev/el/login' ])->redirect();

        return view('v1/views/auth/login',$data);
    }

    /**
     * Register user by email function
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function register(Request $request){
        $data = $request->all();

        $redirect = $request->get('redirect');
        if($redirect){
            $data['redirect'] = $redirect;
        }

        $validator = Validator::make($data, [
            'email' => 'required|email',
            'password' => 'required|min:6',
            'password_confirm' => 'required|min:6|same:password',
            'firstname' => 'required|min:3',
            'lastname' => 'required|min:3',
            'gender'=>'required|integer',
            'terms'=>'required'
        ]);

        if ($validator->fails()) {
            return redirect()->route('login')
                ->withErrors($validator)
                ->withInput();
        }else{
            $model = new Users();
            $birth_date = ( isset($data['birthdate']) && $data['birthdate']!=null && $data['birthdate']!='' && strtotime($data['birthdate'])>0 ) ? $data['birthdate']  : null;
            $user = $model->registerUser($this->lang,$data['email'],$data['password'],$data['firstname'],$data['lastname'],$data['gender'],$birth_date);
            if(is_object($user) && isset($user->user_id)){
                $request->session()->put('authUser', $user);
                $user_ctrl = new UserController();
                $user_ctrl->send_confirmation_email($request);
                if($redirect){
                    return redirect()->to($redirect)->send();
                }
                return redirect()->route('user_profile')->with('status', 'user_created');
            }else{

            }

        }
        return redirect()->route('login')
            ->with('status', 'register_failed')
            ->withInput($data);

    }


    public function forgot_password(Request $request){
        $response = 0;
        if($request->getMethod() == 'POST') {
            $post_data = $request->all();
            $model = new Users();
            $forgot = $model->forgot_password($post_data['email']);

            if($forgot && !is_object($forgot) && $forgot == -1){
                $response = -1; // no user
            }elseif($forgot && is_object($forgot)){
                $response = 1;
                $user = $post_data['email'];
                $url = route('password_reset',[$forgot->user_id,$forgot->forgotten_password_code]);
                Mail::send('emails.forgot_password', ['url' => $url], function ($m) use ($user) {
                    $m->from($this->appEmail, $this->app_details->title);
                    $m->to($user,$user)->subject($this->app_details->title.'-'.trans('application.email_forgot_pass_title'));
                });

            }

        }
        $m = new \stdClass();
        $m->response =  $response;
        echo json_encode($m);

    }

    public function reset_password(Request $request,$id,$code){
        $model = new Users();
        $valid = $model->forgot_code_validate($id,$code);
        $data = [
            'id'=>$id,
            'code'=>$code
        ];
        if($valid){
            $data['valid'] = true;
        }else{
            $data['valid'] = false;
        }

        if($request->getMethod() == 'POST' && $valid) {
            $post_data = $request->all();
            if($post_data['password']){
                $update = $model->new_password($id,$post_data['password']);
                if($update){
                    $data['updated'] = true;
                }
            }
        }

        return view('v1/views/auth/reset_password',$data);
    }

    /**
     * Make the api call to api
     * @param $data
     * @return bool
     */
    protected function login_social_user($data){
        $model = new Users();
        $fb_id = null;
        $twitter_id = null;
        $google_id = null;

        if(isset($data['method']) && $data['method'] == 'facebook'){
            $fb_id = $data['id'];
            $user = $model->social_user_login($this->lang,$data['email'],$data['first_name'],$data['last_name'],$data['email'],$fb_id);
        }elseif(isset($data['method']) && $data['method'] == 'twitter'){
            $twitter_id = $data['id'];
            $user = $model->social_user_login($this->lang,$data['email'],$data['first_name'],$data['last_name'],$data['username'],null,$twitter_id);
        }elseif(isset($data['method']) && $data['method'] == 'google'){
            $google_id = $data['id'];
            $user = $model->social_user_login($this->lang,$data['email'],$data['first_name'],$data['last_name'],$data['username'],null,null,$google_id,$data['google_avatar']);
        }else{
            return false;
        }

        if(is_object($user)){
            return $user;
        }
        return false;
    }

}
