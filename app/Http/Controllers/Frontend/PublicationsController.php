<?php

namespace WebKiosk\Http\Controllers\Frontend;

use WebKiosk\Http\Controllers\Core\LoggiaController;
use Illuminate\Http\Request;
use Helpers;
use WebKiosk\ApiModels\Market;
use Illuminate\Support\Facades\Redirect;

class PublicationsController extends LoggiaController
{
    public function index(Request $request,$cat_id){

        $model = new Market();
        $category = $model->getMarketCategoryDetails($cat_id,$this->page_id,$this->lang,86400*1);
        if(!$category){ return redirect()->route('homepage')->send(); }

        $data = array();
        $data['category'] = $category;
        $data['cat_id'] = $cat_id;
        return view($this->template.'/views/publications/index',$data);
    }

    public function single(Request $request,$id,$slug=null){
        $model = new Market();
        $publication = $model->getPublication($id,$this->lang,false,is_object($this->authUser)?$this->authUser->id:null);
        if(!$publication){ return redirect()->route('404')->send(); }
        $data = [];
        $data['publication'] = $publication;

        ///prices calculation
        if(isset($publication->prices) && is_array($publication->prices) && count($publication->prices)>0){ //issue or magazine has prices set
            $pub_prices = $publication->prices;
        }elseif(isset($publication->magazine) && isset($publication->magazine->prices) && is_array($publication->magazine->prices) && count($publication->magazine->prices)>0){ //else if issue, check if issue's parent magazine has prices set
            $pub_prices = $publication->magazine->prices;
        }elseif(isset($publication->base_price ) && $publication->base_price!=null){ // else check if issue or mag has base price
            $data['base_price'] = $publication->base_price;
        }elseif(isset($publication->magazine) && isset($publication->magazine->base_price ) && $publication->magazine->base_price!=null){ // in case issue, check if issue's magazine has base price
            $data['base_price'] = $publication->magazine->base_price;
        }

        if(isset($pub_prices) && is_array($pub_prices) && count($pub_prices)>0){ /// there are active price plans
            $prices = [];
            foreach($pub_prices as $price):  // we have found prices plans so make manipulate for displaying in html
                if($price->user_group_id !=0 && $price->user_group_id !=null) { //case price plan has user_group set
                    if($price->system_type_id == 74 && $publication->file_name == null){ continue; } // if plan is type electronic version and uploaded file is not set, skip
                    $prices[$price->user_group_id]['title'] = isset($price->group_translation) ? $price->group_translation : trans('application.publication_user_groups_all');
                    $prices[$price->user_group_id]['options'][$price->system_type_id] = $price;
                }elseif($price->user_group_id ==0 || $price->user_group_id ==null) { //case price plan hasn't user_group set
                    if($price->system_type_id == 74 && $publication->file_name == null){ continue; } // if plan is type electronic version and uploaded file is not set, skip
                    $prices[$price->user_group_id]['title'] = isset($price->group_translation) ? $price->group_translation : trans('application.publication_user_groups_all');
                    $prices[$price->user_group_id]['options'][$price->system_type_id] = $price;
                }else{ // there are no plans, use base price if any either issue's or magazine's
                    $data['base_price'] = $price->price;
                }
            endforeach;

            if(count($prices)>0) {
                $data['prices'] = $prices;
            }elseif(!isset($data['base_price'])){
                if(isset($publication->base_price ) && $publication->base_price!=null){
                    $data['base_price'] = $publication->base_price;
                }elseif(isset($publication->magazine) && isset($publication->magazine->base_price ) && $publication->magazine->base_price!=null){
                    $data['base_price'] = $publication->magazine->base_price;
                }
            }
        }

        ///recurring plans of magazine
        if(isset($publication->magazine->recurring_plans) || isset($publication->recurring_plans)){ //if there are recurring plans in case of issue or magazine

            if(isset($publication->magazine->recurring_plans)){
                $recurring_plans = $publication->magazine->recurring_plans;
            }elseif(isset($publication->recurring_plans)){
                $recurring_plans = $publication->recurring_plans;
            }

            $plans = [];
            foreach($recurring_plans as $recurring_plan): //manipulate date and transform to display in html
                    $plans[$recurring_plan->id]['title'] = $recurring_plan->title;
                    $plans[$recurring_plan->id]['period_value'] = $recurring_plan->period_value;
                    $plans[$recurring_plan->id]['period_id'] = $recurring_plan->period_id;
                    $plans[$recurring_plan->id]['duration'] = $recurring_plan->duration;
                    $plans[$recurring_plan->id]['period_translation'] = $recurring_plan->period_translation;
                    if(isset($recurring_plan->prices)){
                        foreach($recurring_plan->prices as $price_plan):
                            $plans[$recurring_plan->id]['prices'][$price_plan->user_group_id]['group_translation'] = isset($price_plan->group_translation) ? $price_plan->group_translation : trans('application.publication_user_groups_all');//$price_plan->group_translation;
                            $plans[$recurring_plan->id]['prices'][$price_plan->user_group_id]['items'][$price_plan->system_type_id] = $price_plan;
                        endforeach;
                    }
            endforeach;

            if(isset($plans) && is_array($plans) && count($plans)>0){ // if there are valid recurring plans make them available for rendering
                $data['plans'] = $plans;
            }

        }

        //get articles connected with publication,in case publication is issue
        if($publication->system_cat_id == 213 && $publication->parent_id!=null) {
            $articles = $model->getPublicationArticles($this->lang, $publication->id, null, null);
            if ($articles && is_array($articles) && count($articles) > 0) {
                $data['articles'] = $articles;
            }
        }

        //Helpers::die_pre($this->authUser);

        if($publication->system_cat_id == 212){ //book
            return view('v1/views/publications/single_book',$data);
        }elseif($publication->system_cat_id == 213 && $publication->parent_id != NULL){ // issue
            return view('v1/views/publications/single_issue',$data);
        }elseif($publication->system_cat_id == 213 && $publication->parent_id == NULL){ // magazine
            return view('v1/views/publications/single_magazine',$data);
        }
    }

    public function publications_index(Request $request){
        Helpers::die_pre('publications list');
    }

    public function viewer(Request $request,$id){
        if(!is_object($this->authUser)){
            return redirect()->route('homepage')->send();
        }
        $model = new Market();
        $publication = $model->getPublication($id,$this->lang,false,is_object($this->authUser)?$this->authUser->id:null);
        if(!$publication){ return redirect()->route('404')->send(); }
        $data = [
            'publication'=>$publication
        ];
       // Helpers::die_pre($publication);
        if(Helpers::check_if_bought($this->authUser,$publication,isset($publication->magazine) ? $publication->magazine : null,74,null)){
            return view('viewer/layout',$data);
        }else{
            return redirect()->route('homepage')->send();
        }

    }
    public function serve_pdf($id){
        $model = new Market();
        $publication = $model->getPublication($id,$this->lang,false,is_object($this->authUser)?$this->authUser->id:null);
        //Helpers::die_pre($publication->file_name);
        //if(!$publication){ return redirect()->route('404')->send(); }
        if(Helpers::check_if_bought($this->authUser,$publication,isset($publication->magazine) ? $publication->magazine : null,74,null)) {
            echo file_get_contents($publication->file_name);
        }
    }

}
