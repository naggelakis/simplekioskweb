<?php

namespace WebKiosk\Http\Controllers\Frontend;

use WebKiosk\ApiModels\Market;
use WebKiosk\Http\Controllers\Core\LoggiaController;
use Helpers;
use Illuminate\Http\Request;
use WebKiosk\ApiModels\Users;
use Illuminate\Support\Facades\Validator;
use Session;
use Mail;
use Socialite;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;

class UserController extends LoggiaController
{

    public function __construct(){
        parent::__construct();
        $user = Session::has('authUser');
        if($user==0){
            redirect()->route('login')->send();
        }
    }

    public function home(){
        $data = [];
        $expiring_subscriptions = [];
        if(isset($this->authUser->subscriptions) && is_array($this->authUser->subscriptions) && count($this->authUser->subscriptions)>0){
            foreach($this->authUser->subscriptions as $subscription):

                $date1 = new \DateTime(date('Y-m-d',strtotime($subscription->expires)));
                $date2 = new \DateTime();
                $interval = $date1->diff($date2);
                if($interval->y == 0 && $interval->m == 0 && $interval->d>=0){
                    $subscription->expiring_in = $interval->d;
                    $expiring_subscriptions[]= $subscription;
                }
            endforeach;
        }
        if(count($expiring_subscriptions) > 0){
            $data['expiring_subscriptions'] = $expiring_subscriptions;
        }

        if(isset($this->socialApps->facebook) && $this->authUser->fb_id==null){
           // return Socialite::driver('facebook')->redirect();
        }
      //  Helpers::die_pre($this->authUser);

        return view('v1/views/user/home',$data);
    }



    public function logout(){
        Session::forget('authUser');
        return redirect()->route('login')->send();
    }


    public function send_confirmation_email(Request $request){
        $model = new Users();
        $code = $model->generate_confirmation_code($this->authUser->id);
        if($code){

            $user = $this->authUser->email;
            $url = route('confirm_email',[$code]);
            Mail::send('emails.confirm_email', ['url' => $url], function ($m) use ($user) {
                $m->from($this->appEmail, $this->app_details->title);
                $m->to($user,$user)->subject($this->app_details->title.'-'.trans('application.confirm_email_pass_title'));
            });

            if($request->ajax()) {
                echo json_encode($code);
            }else{
                return $code;
            }
        }
    }

    public function confirm_email(Request $request,$code){
        $model = new Users();
        $activation = $model->validate_activation_token($this->authUser->id,$code);
        if($activation){
            Session::flash('email_confirmed', 1);
            return redirect()->route('user_profile')->send();
        }
    }

    public function subscriptions(Request $request){
        $data = [];
        $model = new Market();
        $subscriptions = $model->getUserSubscriptions($this->authUser->id,$this->lang);
        //Helpers::die_pre($subscriptions);
        if($subscriptions){
            $data['subscriptions'] = $subscriptions;
        }
        $data['clickedSubscriptions'] = $request->hasCookie('clickedSubscriptions');
       // Helpers::die_pre($request->cookies);
        return view($this->template.'/views/user/subscriptions',$data);
    }


    public function favorites(){
        $data = [];
        return view($this->template.'/views/user/favorites',$data);
    }

    public function issues(Request $request){
        $data = [];
        $model = new Market();
        $magazines = $model->getSubscribedMagazines($this->page_id,$this->lang,$this->authUser->id,100);
        if(isset($magazines)){
            $data['magazines'] = $magazines;
        }
        $data['userIssuesAppPurchases'] = $request->cookie('userIssuesAppPurchases');

        return view($this->template.'/views/user/issues',$data);
    }

    public function orders(Request $request){
        $data = [];
        $model = new Market();
        $orders = $model->getOrders($this->lang,$this->page_id,$this->authUser->id);
        if($orders){
            $data['orders'] = array_reverse($orders);
        }
        //Helpers::die_pre($this->authUser->id);
        return view($this->template.'/views/user/orders',$data);
    }

    public function edit(Request $request){
        $data = [];
       // Helpers::die_pre($this->authUser);
        return view($this->template.'/views/user/profile',$data);
    }

    public function update(Request $request){
        $post_data = $request->all();
        if($post_data['email'] == $this->authUser->email){
            unset($post_data['email']);
        }
        unset($post_data['_token']);
        $model = new Users;
        $update = $model->update_user_profile($this->authUser->id,$this->lang,$post_data);
        if($update){
            $request->session()->flash('update_profile_status', 'success');
        }else{
            $request->session()->flash('update_profile_status', 'error');
        }
        return redirect()->route('user_edit')->send();
    }

    public function update_password(Request $request){
        $post_data = $request->all();
        $model = new Users();
        if(isset($post_data['old_password'])){
            $update = $model->update_password($this->authUser->email,$post_data['old_password'],$post_data['password'],$this->authUser->id);
        }else{
            $update = $model->update_password($this->authUser->email,null,$post_data['password'],$this->authUser->id,1);
        }
        if($update){
            $request->session()->flash('update_password_status', 'success');
        }else{
            $request->session()->flash('update_password_status', 'error');
        }
        return redirect()->route('user_edit')->send();
    }

    public function user_social_connect(Request $request,$media){
            Session::put('redirectRoute','user_social_connect_success');
            if($media == 'facebook'){
                return redirect()->route('social_login',['facebook'])->send();
            }elseif($media == 'twitter'){
                return redirect()->route('social_login',['twitter'])->send();
            }elseif($media == 'google'){
                return redirect()->route('social_login',['google'])->send();

            }
    }

    public function user_social_connect_success(Request $request,$id,$method){
        Session::forget('redirectRoute');

        if($method == 'facebook'){
            $field = 'fb_id';
        }elseif($method == 'twitter'){
            $field = 'twitter_id';
        }elseif($method == 'google'){
            $field = 'google_id';
        }

        $model = new Users();
        $check_user = $model->getSocialUser($id,$this->page_id,$field);
        if(!$check_user){
            $update = $model->connectSocialUser($this->authUser->id,$id,$field);

            if($update && $update == 1){
                $request->session()->flash('success', 'social_connected_'.$method);
            }else{
                $request->session()->flash('error', 'social_connected_failed_'.$method);
            }
        }else{
            $request->session()->flash('error', 'social_already_connected_'.$method);
        }
        return redirect()->route('user_profile')->send();

    }

    public function locations(Request $request){
        $model = new Market();
        $data = [];

        if($request->method() == 'POST'){
            $post_data = $request->all();
            unset($post_data['_token']);
            if(!is_object($this->default_location)){
                $post_data['is_default'] = 1;
            }
            $register_location = $model->registerUserLocation($this->authUser->id,$this->lang,$post_data);
            if($register_location){
                $request->session()->flash('success', 'new_location_item_success');
            }else{
                $request->session()->flash('error', 'new_location_item_error');
            }
            return redirect()->route('user_locations')->send();
        }


        $locations = array();

        $data['countries'] = (object)$model->getCountriesList($this->lang,8600*30);
        $data['cities_e'] = $model->getCitiesList($this->lang,8600*30,true);
        $data['cities'] = $model->getCitiesList($this->lang,8600*30);


        if(is_object($this->default_location)){
            $locations[]=$this->default_location;
        }

        if(isset($this->authUser->locations) && is_array($this->authUser->locations) && count($this->authUser->locations)>0){
            foreach($this->authUser->locations as $location):
                if(is_object($this->default_location) && $this->default_location->id != $location->id){
                    $locations[] = $location;
                }elseif(!is_object($this->default_location)){
                    $locations[] = $location;
                }
            endforeach;
        }
        //Helpers::die_pre($locations);
        $data['locations'] = $locations;
        return view($this->template.'/views/user/locations',$data);
    }


    public function make_location_default(Request $request,$location_id){
        $model = new Market();
        $make_default = $model->setDefaultLocation($this->authUser->id,$location_id);
        if($make_default){
            $request->session()->flash('success', 'location_item_set_default_success');
        }else{
            $request->session()->flash('error', 'location_item_set_default_error');
        }
        return redirect()->route('user_locations')->send();

    }

    public function delete_location(Request $request,$location_id){
        $model = new Market();
        $delete = $model->deleteLocation($this->authUser->id,$location_id);
        if($delete){
            $request->session()->flash('success', 'location_item_delete_success');
        }else{
            $request->session()->flash('error', 'location_item_delete_error');
        }
        return redirect()->route('user_locations')->send();
    }

    public function edit_location(Request $request){
        $post_data = $request->all();
        $model = new Market();

        if(isset($post_data['location_id']) && is_numeric($post_data['location_id'])){
            unset($post_data['_token']);
            $update = $model->updateUserLocation($this->authUser->id,$this->lang,$post_data);
            if($update){
                $request->session()->flash('success', 'location_item_update_success');
            }else{
                $request->session()->flash('error', 'location_item_update_error');
            }
        }
        return redirect()->route('user_locations')->send();

    }

    public function connect_subscription(Request $request){
        if($request->get('noSubscription')){
          //$cookie =  Cookie::forget('clickedSubscriptions');
          $cookie =  Cookie::forever('clickedSubscriptions',1);
          return redirect()->route('user_subscriptions')->withCookie($cookie);
        }

        $model = new Market();
        $data = [];
        $recurring_plans = $model->getAllRecurringPlans($this->page_id,$this->lang);
        $data['recurring_plans'] = $recurring_plans;
        $data['countries'] = $model->getCountriesList($this->lang,8600*30);
        $data['cities'] = $model->getCitiesList($this->lang,8600*30);
        return view($this->template.'/views/user/connect_subscription',$data);
    }


}
