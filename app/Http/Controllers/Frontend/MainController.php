<?php

namespace WebKiosk\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use WebKiosk\Http\Controllers\Core\LoggiaController;
use Helpers;
use Mail;

class MainController extends LoggiaController
{
    public function homepage(){

        return view($this->template.'/views/main/home');
    }

    public function contact(Request $request){
        //Helpers::die_pre($this->basic_app);
        if ($request->method() == 'POST' && $request->ajax())
        {
                $user = $this->appEmail;
                $post_data = $request->all();
                $send = Mail::send('emails.contact_email', ['data' => $post_data], function ($m) use ($user,$post_data) {
                    $m->from($this->appEmail, $this->app_details->title);
                    $m->to($user,$user)->subject('Νέο μήνυμα από '.$post_data['name'].' μέσω φόρμας επικοινωνίας '.$this->app_details->title);
                    $m->replyTo($post_data['email'], $post_data['name']);
                });
                echo json_encode($send);
        }else{
            return view($this->template.'/views/main/contact');
        }

    }

    public function not_found(){
        Helpers::die_pre('Not found');
    }
}
