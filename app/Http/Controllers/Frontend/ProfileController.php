<?php

namespace WebKiosk\Http\Controllers\Frontend;

use WebKiosk\Http\Controllers\Core\LoggiaController;
use Illuminate\Http\Request;
use Helpers;
use WebKiosk\ApiModels\Articles;


class ProfileController extends LoggiaController
{
    public function index(Request $request){
        $model = new Articles();
        $posts = $model->getLatestPosts($this->lang,100,0,86400 * 20,168,'ASC');
        if($posts){
            $post = $posts[0];
            return redirect()->route('profile_single',[$post->id,Helpers::urlize($post->title)])->send();
        }else{
            return redirect()->route('homepage')->send();
        }
    }

    public function single(Request $request,$id,$slug=null){
        $model = new Articles();
        $post = $model->getEntryDetails($this->lang,$id,null,86400*20);

        if($post){
            $data['post'] = $post->entry;
            $posts = $model->getLatestPosts($this->lang,100,0,86400*20,168,'ASC');
            if($posts){
                foreach($posts as $key=>$relative_post):
                    if($relative_post->id == $post->entry->id){
                        unset($posts[$key]);
                    }
                endforeach;
                $data['other_posts'] = $posts;
                //Helpers::die_pre($posts);
            }
            //Helpers::die_pre($post);
            return view('v1/views/profile/single',$data);
        }else{
            return redirect()->route('homepage')->send();
        }
    }
    public function category(Request $request,$cat_id,$slug=null){
        Helpers::die_pre($cat_id);
    }


}
