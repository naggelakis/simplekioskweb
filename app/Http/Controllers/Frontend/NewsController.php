<?php

namespace WebKiosk\Http\Controllers\Frontend;

use Symfony\Component\Console\Helper\Helper;
use WebKiosk\Http\Controllers\Core\LoggiaController;
use Illuminate\Http\Request;
use Helpers;
use WebKiosk\ApiModels\Articles;


class NewsController extends LoggiaController
{
    public function category(Request $request,$cat_id,$slug=null){
        $model = new Articles();
        $category = $model->getCategoryDetails($this->lang,$cat_id,86400);
        if(!$category){
            return redirect()->route('homepage')->send();
        }
        /** Checks if banners have permission to be shown on current category */
        $this->check_banner_category_permissions($category->parent_id,2);

        $showcase_posts = $model->getCategoryEntries($this->lang,$this->page_id,$cat_id,170,13,0,8,'DESC',null,3600);
        if($showcase_posts){
            $data['showcase_posts'] = $showcase_posts;
        }


        $data['type'] = 170;
        $data['category'] = $category;
        $data['ids'] = [];
        //Helpers::die_pre($category);
        return view('v1/views/news/list',$data);
    }

    public function single(Request $request,$id,$slug=null){

        $data = [];
        $cats = [];
        $model = new Articles();
        $data['unlock'] = true;
        $user_id = is_object($this->authUser) && isset($this->authUser->id) ? $this->authUser->id : null;
        $entry = $model->getEntryDetails($this->lang,$id,$user_id,86400*7);
        if(!$entry){
            return redirect()->route('homepage')->send();
        }
        $additional = $model->getEntryAdditional($this->lang,$id);

       // Helpers::die_pre($entry);
        $data['lock_status'] = $this->check_article_permissions($entry->entry->id,$user_id,$entry->permission,isset($entry->plans) ? $entry->plans : null);
        if(isset($entry->entry->categories) && is_array($entry->entry->categories) && count($entry->entry->categories)>0){
            foreach($entry->entry->categories as $category){
                $cats[$category->id] = $category->title;
                /** Checks if banners have permission to be shown on current category */
                $this->check_banner_category_permissions($category->id,2);
                if(isset($category->parent_id) && $category->parent_id != null){
                    $this->check_banner_category_permissions($category->parent_id,2);
                }
            }
        }

        $data['cats'] = $cats;
        $data['entry'] = $entry->entry;
        if(isset($additional->views)){
            $data['entry']->views_count = $additional->views;
        }
        if(isset($additional->related)){
            $data['entry']->related = $additional->related;
        }
        if(isset($entry->article_tags) && is_array($entry->article_tags) && count($entry->article_tags)>0){
            $data['tags'] = $entry->article_tags;
        }
        //Helpers::die_pre($entry->plans);
        if(isset($entry->plans) && is_array($entry->plans) && count($entry->plans)>0) {
            $plans = [];
            foreach ($entry->plans as $recurring_plan): //manipulate date and transform to display in html
                //Helpers::die_pre($recurring_plan);
                $plans[$recurring_plan->id]['title'] = $recurring_plan->title;
               // $plans[$recurring_plan->id]['period_value'] = $recurring_plan->period;
                $plans[$recurring_plan->id]['period_id'] = $recurring_plan->period_id;
              // $plans[$recurring_plan->id]['duration'] = $recurring_plan->duration;
                $plans[$recurring_plan->id]['period_translation'] = $recurring_plan->period;
                if (isset($recurring_plan->prices)) {
                    foreach ($recurring_plan->prices as $dd):
                        foreach($dd as $price_plan):
                        //Helpers::die_pre($price_plan);
                        $plans[$recurring_plan->id]['prices'][$price_plan->user_group_id]['group_translation'] = isset($price_plan->user_group) ? $price_plan->user_group : trans('application.publication_user_groups_all');//$price_plan->group_translation;
                        $plans[$recurring_plan->id]['prices'][$price_plan->user_group_id]['items'][$price_plan->system_type_id] = $price_plan;
                        endforeach;
                    endforeach;
                }
            endforeach;

            if (isset($plans) && is_array($plans) && count($plans) > 0) { // if there are valid recurring plans make them available for rendering
                $data['plans'] = $plans;
            }
        }

       // Helpers::die_pre($data['plans']);

        return view('v1/views/news/single',$data);
    }

    public function feed(Request $request,$from=null,$to=null){

        $data = [];
        $data['from'] = $from;
        $data['to'] = $to;
        $data['only_news'] = true;
        return view('v1/views/news/search_tag',$data);

    }

    public function author(Request $request,$id){
        //Helpers::die_pre($id);
        $data = [];
        $data['author_id'] = $id;
        $model = new Articles();
        $author = $model->getAuthorProfile($this->lang,$id,8600*7);
       // Helpers::die_pre($author);
        if(!$author){ return redirect()->route('homepage')->send(); }
        $data['author'] = $author;
        return view('v1/views/news/author',$data);
    }

    public function tags(Request $request, $tag_id,$tag_slug){
        $data = [];
        $data['tag_slug'] = $tag_slug;
        $data['tag_id'] = $tag_id;
        return view('v1/views/news/search_tag',$data);

    }

    public function search(Request $request,$search=null){
        $data = [];
       // Helpers::die_pre($request->getAll());
        $data['search'] = strip_tags($search);
        //$data['tag_id'] = $tag_id;
        return view('v1/views/news/search_tag',$data);

    }
}
