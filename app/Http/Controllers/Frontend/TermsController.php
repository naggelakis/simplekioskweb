<?php

namespace WebKiosk\Http\Controllers\Frontend;

use WebKiosk\Http\Controllers\Core\LoggiaController;
use Illuminate\Http\Request;
use Helpers;
use WebKiosk\ApiModels\Articles;

class TermsController extends LoggiaController
{

    public function index(Request $request){
        $model = new Articles();
        $posts = $model->getLatestPosts($this->lang,100,0,86400*20,169,'ASC');

        if($posts){
            $post = $posts[0];
            return redirect()->route('terms_single',[$post->id,Helpers::urlize($post->title)])->send();
        }else{
            return redirect()->route('homepage')->send();
        }
    }


    public function single(Request $request,$id,$slug=null){

        $model = new Articles();
        $post = $model->getEntryDetails($this->lang,$id,null,86400*20);
        //Helpers::die_pre($post);

        if($post){
            $data['post'] = $post->entry;
            $posts = $model->getLatestPosts($this->lang,100,0,86400*20,168,'ASC');
            if($posts){
                foreach($posts as $key=>$relative_post):
                    if($relative_post->id == $post->entry->id){
                        unset($posts[$key]);
                    }
                endforeach;
                $data['profile_posts'] = $posts;
                //Helpers::die_pre($posts);
            }

            $terms_posts = $model->getLatestPosts($this->lang,100,0,86400*20,169,'ASC');
            if($terms_posts){
                foreach($terms_posts as $key=>$relative_post):
                    if($relative_post->id == $post->entry->id){
                        unset($terms_posts[$key]);
                    }
                endforeach;
                $data['terms_posts'] = $terms_posts;
                //Helpers::die_pre($data['terms_posts']);
            }

            //Helpers::die_pre($this->basic_app);
            return view('v1/views/terms/single',$data);
        }else{
            return redirect()->route('homepage')->send();
        }
    }

    public function author(Request $request,$id){
        Helpers::die_pre($id);
    }
    public function category(Request $request,$cat_id,$slug=null){
        return redirect()->route('terms')->send();
    }
}
