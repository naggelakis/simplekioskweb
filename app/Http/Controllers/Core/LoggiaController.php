<?php

namespace WebKiosk\Http\Controllers\Core;
use WebKiosk\Custom\RestClient;
use WebKiosk\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use WebKiosk\Custom\Helpers;
use Illuminate\Support\Facades\Lang;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Blade;
use WebKiosk\ApiModels\Articles;
use Session;
use WebKiosk\ApiModels\Market;
//

class LoggiaController extends Controller
{
     public $template;
     public $page_id;
     public $lang;
     protected $RestClient;
     protected $status;
     public $basic_app;
     public $mailchimp;
     public $app_languages;
     public $app_details;
     public $default_currency;
     public $active_currency;
     public $guest_currency;
     public $appAddress;
     public $addresses;
     public $banners;
     public $brands;
     public $offers;
     public $terms;
     public $appEmail;
     public $appPhone;
     public $sublocations;
     public $socialMedia;
     public $appCurrency;
     public $user_profile;
     public $cart_items_from_session;
     public $wishlists;
     public $wishlists_products;
     public $footer_featured_posts;
     public $authUser;
     public $socialApps;
     public $banners_check_subcat;
     public $order;
     public $favorited_articles;
     public $default_location;

    public function __construct(){
        //Helpers::die_pre('1');
        //
        try {
            require app_path() . '/../../loggia_vars.php';
        }catch(\Exception $e){
            //Helpers::die_pre($e);
        }
        $this->RestClient = new RestClient();
        $this->system_setup();
        $this->system_run();

    }

    private function system_setup(){
        Blade::setRawTags('{{{', '}}}');
        $this->lang = Config::get('loggia.default_lang');
        $this->template = Config::get('loggia.default_template');
        $this->page_id = defined('LOGGIA_PAGE') ? LOGGIA_PAGE : Config::get('loggia.page_id');
        $this->lang = strtoupper(Lang::getLocale());
        $this->status = $this->RestClient->get('pages/app_status',null,1440);
        $this->socialApps  = new \stdClass();
        view()->share('lang',$this->lang );
    }

    private function system_run(){
        $app = $this->RestClient->get('publishing/initApplication',array(
            'page_id' => $this->page_id,
            'depth' => true,
            'lang' => $this->lang
        ),1440);

        if(isset($app->basic_app->page_app_details->icons_logo_light)){
            $app->basic_app->icons_logo_light = $app->basic_app->page_app_details->icons_logo_light;
        }
        if(isset($app->basic_app->page_app_details->icons_logo_dark)){
            $app->basic_app->icons_logo_dark = $app->basic_app->page_app_details->icons_logo_dark;
        }
        if (!isset($app)) { //error
            $this->loggia_error();
        } elseif (!isset($app->basic_app)) { //error
            $this->basic_app = $app->basic_app;
            $this->loggia_error('database');
        } else { //all good,procceed
            $this->basic_app= $app->basic_app;
            $this->session_senarios();
            $this->mailchimp_init();
            $this->social_apps_init();

            $this->app_details=$app->basic_app->page_app_details;
            $this->app_languages = isset($app->basic_app->languages)?Helpers::object_to_array($app->basic_app->languages):null;
            $this->default_currency=$this->app_details->retail_currency;
            $this->currency_init();

            if(isset($this->basic_app->location_address) && is_object($this->basic_app->location_address)){
                $this->appAddress = $this->basic_app->location_address;
            }
            $this->languages_init();
            if(isset($app->basic_app->addresses)):
                $this->addresses=$app->basic_app->addresses;
            endif;


            $this->widgets_init($app);

            $this->terms_init();
            $this->fetch_footer_featured();


            if ($this->status == 'active') {
                if (!isset($this->basic_app) && 0) { //error
                    $this->loggia_error('database');
                } else {
                    $user_id = Session::has('authUser');
                    if(isset($user_id) && $user_id>0 ){
                        $this->user_init();
                    }

                    $order_id = Session::has('order_id');
                    if(isset($order_id) && $order_id>0 ){
                        $this->order_init();
                    }

                    //die_pre($this->$user_profile);
                    if($this->app_details->settings_contactform_email && $this->app_details->settings_contactform_email!='') {
                        $this->appEmail_set();
                    }
                    if($this->app_details->settings_contactform_phone && $this->app_details->settings_contactform_phone!='') {
                        $this->appPhone_set();
                    }
                    if(isset($app->basic_app->sublocations) && is_array($app->basic_app->sublocations) && count($app->basic_app->sublocations)>0){
                        $this->sublocations = $app->basic_app->sublocations;
                    }
                    if(isset($this->basic_app->social_media_links) && count($this->basic_app->social_media_links)>0){
                        $this->socialMedia=$this->basic_app->social_media_links;
                    }
                    if(isset($this->basic_app->retail_currency_details) && $this->basic_app->retail_currency_details!=NULL){
                        $this->appCurrency=$this->basic_app->retail_currency_details;
                    }
                    if(!isset($this->app_details->app_theme_base_color) || $this->app_details->app_theme_base_color=='' || $this->app_details->app_theme_base_color==NULL){
                        $this->app_details->app_theme_base_color='999999';
                    }
                    $this->template_set();
                    $this->make_variables_public();


                }
            } elseif ($this->status == 'expired') {
                $this->loggia_error($this->status);
            } elseif($this->status=='deleted') {
                $this->loggia_error($this->status);
            } else {
            }
        }
    }

    protected function social_apps_init(){
        if($this->basic_app->facebook_app_id != null && $this->basic_app->facebook_app_id!='' && $this->basic_app->facebook_secret!=null && $this->basic_app->facebook_secret!=''){
            $facebook = new \stdClass();
            $facebook->app_id = $this->basic_app->facebook_app_id;
            $facebook->app_secret = $this->basic_app->facebook_secret;
            $this->socialApps->facebook = $facebook;
            $data = [
                'client_id'=>$this->basic_app->facebook_app_id,
                'client_secret'=>$this->basic_app->facebook_secret,
                'redirect'=>route('social_login_user',['facebook'])
            ];
            Config::set('services.facebook',$data);
        }

        if($this->basic_app->google_client_id != null && $this->basic_app->google_client_id!='' && $this->basic_app->google_client_secret!=null && $this->basic_app->google_client_secret!=''){
            $google = new \stdClass();
            $google->app_id = $this->basic_app->google_client_id;
            $google->app_secret = $this->basic_app->google_client_secret;
            $this->socialApps->google = $google;
            $data = [
                'client_id'=>$this->basic_app->google_client_id,
                'client_secret'=>$this->basic_app->google_client_secret,
                'redirect'=>route('social_login_user',['google'])
            ];
            Config::set('services.google',$data);
        }

        if($this->basic_app->twitter_app_id != null && $this->basic_app->twitter_app_id!='' && $this->basic_app->twitter_app_secret!=null && $this->basic_app->twitter_app_secret!=''){
            $twitter = new \stdClass();
            $twitter->app_id = $this->basic_app->twitter_app_id;
            $twitter->app_secret = $this->basic_app->twitter_app_secret;
            $this->socialApps->twitter = $twitter;
            $data = [
                'client_id'=>$this->basic_app->twitter_app_id,
                'client_secret'=>$this->basic_app->twitter_app_secret,
                'redirect'=>route('social_login_user',['twitter'])
            ];
            Config::set('services.twitter',$data);
        }
    }
    /**
    * Make variables accessible to views
     */
    protected function make_variables_public(){
        view()->share('basic_app', $this->basic_app);
        view()->share('user_profile', $this->user_profile);
        view()->share('app_details', $this->app_details);
        view()->share('terms', $this->terms);
        view()->share('socialMedia', $this->socialMedia);
        view()->share('addresses', $this->addresses);
        view()->share('appPhone', $this->appPhone);
        view()->share('appEmail', $this->appEmail);
        view()->share('banners', $this->banners);
        view()->share('footer_featured_posts', $this->footer_featured_posts);
        view()->share('authUser', $this->authUser);
        view()->share('socialApps', $this->socialApps);
        view()->share('default_location', $this->default_location);
        //Helpers::die_pre($this->default_location);
    }

    /**
     * Mailchimp settings register
     */
    protected function mailchimp_init(){
        if(isset($this->app_details->mailchimp_active) && $this->app_details->mailchimp_active==1){
            $this->mailchimp=new stdClass();
            $this->mailchimp->active=1;
            $this->mailchimp->api_key=$this->app_details->mailchimp_apikey;
            $this->mailchimp->list_id=$this->app_details->mailchimp_listid;
        }
    }

    /**
     * Session scenarios
     */
    protected function session_senarios(){

        /*if($this->session->userdata('user') && $this->session->userdata('user')>0){

            $this->user=$this->session->userdata('user');

        }
        if($this->session->userdata('user_type') && $this->session->userdata('user_type')>0){

            $this->user_type=$this->session->userdata('user_type');

        }

        /////// cart id set senarios /////////////////////////////////////////
        if($this->session->userdata('cart_id')){

            if($this->session->userdata('cart_id')>0){

                $this->cart_id=$this->session->userdata('cart_id');

            }elseif($this->session->userdata('cart_id')==0){

                $this->cart_id=NULL;
            }

        }
        /////// cart id set senarios /////////////////////////////////////////
        /////// pending payment cart id set senarios /////////////////////////////////////////

        if($this->session->userdata('cart_pending') ){

            if($this->session->userdata('cart_pending')>0){

                $this->cart_pending=$this->session->userdata('cart_pending');


            }elseif($this->session->userdata('cart_pending')==0){

                $this->cart_pending=NULL;
            }
        }
        /////// pending payment cart id set senarios /////////////////////////////////////////
        */
    }

    /**
     * Register Currency
     */
    protected function currency_init(){
        if(isset($this->guest_currency)){
            $currency= $this->RestClient->get('pages/getCurrency',array(
                'page_id' => $this->page_id,
                'id'=> $this->guest_currency
            ),10*1440);

            if($currency->code==200){
                $this->active_currency=$currency->message;
            }
        }elseif(isset($this->basic_app->retail_currency_details)){
            $this->active_currency=$this->basic_app->retail_currency_details;
        }else{
            $currency= $this->RestClient->get('pages/getCurrency',array(
                'page_id' => $this->page_id,
                'id'=> $this->default_currency
            ),10*1440);
            if($currency->code==200){
                $this->active_currency=$currency->message;
            }
        }
    }

    /**
     * Register available languages
     */
    protected function languages_init(){

        if(isset($this->lang) && count($this->app_languages) == 1 && $this->lang != strtoupper($this->app_languages[0]['lang_code'])){
            return $this->redirector->to('/'.strtolower($this->app_languages[0]['lang_code']));
        }
        if ($this->lang == 'EL') {
            setlocale(LC_TIME,'el_GR.UTF-8');
        } else {
            setlocale(LC_TIME,'en_UK.UTF-8');
        }
    }

    /**
     * Set the active theme
     */
    protected function template_set(){
        if(isset($this->basic_app) && isset($this->basic_app->app_theme) && $this->basic_app->app_theme!=NULL && $this->basic_app->app_theme!=''){
            $this->template=$this->basic_app->app_theme;
        }else{
            $this->template='v1';
        }
        view()->share('template', $this->template);
    }


    /**
     * Various widgets registration
     * @param $app
     */
    protected function widgets_init($app){
        if(isset($app->basic_app->app_banners) && count($app->basic_app->app_banners)):
            $this->banners=$app->basic_app->app_banners;
            $this->banners_initialize();
        endif;
        if(isset($app->basic_app->brands) && count($app->basic_app->brands)):
            $this->brands=$app->basic_app->brands;
        endif;
        if(isset($this->basic_app->offers) && count($this->basic_app->offers)>0){
            $this->offers=count($this->basic_app->offers);
        }
    }

    protected function banners_initialize(){
        if(isset($this->banners) && (is_array($this->banners) || is_object($this->banners)) && count($this->banners)>0){
            $output = [];
            foreach($this->banners as $banner):
                $place = Helpers::findBannerPlace($banner);
                foreach($banner->entries as $key=>$single_banner):
                    $check = Helpers::checkBannerCategory($banner,$single_banner);
                    if($check) {
                        if ($check === 100) {
                            $single_banner->place = $place;
                            $single_banner->module = $banner->module;
                            $single_banner->array_position = $key;
                            $this->banners_check_subcat[] = $single_banner;
                        } elseif ($check) {
                            $output[$place][] = $single_banner;
                        }
                    }
                endforeach;

            endforeach;
            $this->banners = $output;

        }
    }


    protected function check_banner_category_permissions($category_parent_id,$module){

        if(is_array($this->banners_check_subcat) && count($this->banners_check_subcat)>0){
            foreach($this->banners_check_subcat as $banner_check):
                if($banner_check->module == $module && isset($category_parent_id) && $category_parent_id != null && $category_parent_id == $banner_check->module_value) {
                    $this->banners[$banner_check->place] = Helpers::array_insert($this->banners[$banner_check->place],$banner_check, $banner_check->array_position);
                }
            endforeach;

            $this->make_variables_public();
        }
    }

    /**
     * User flow if user is logged in
     */
    protected function user_init(){
        $user = Session::get('authUser');
        if(isset($user->user_id) && $user->user_id>0 ) {
            $user_details= $this->RestClient->get('users/user_profile',array(
                'lang' => $this->lang,
                'page_id' => $this->page_id,
                'id' => $user->user_id,
                'wishlist' => 1,
                'cart' => 1
            ));

            //Helpers::die_pre($user_details);
            if ($user_details) {
                $this->authUser = $user_details; // assign the user's profile in a variable
                $this->favorited_articles = $user_details->favorites;
                if(isset($user_details->locations) && is_array($user_details->locations) && count($user_details->locations)>0){
                    foreach($user_details->locations as $location):
                        if($location->is_default == 1){
                            $this->default_location = $location;
                            break;
                        }
                    endforeach;
                }
            }

        }
    }

    /**
     * Register App Emails for easy access
     */
    protected function appEmail_set(){
        if(isset($this->app_details->emails) && count($this->app_details->emails)>0){
            foreach($this->app_details->emails as $email):
                if($email->id==$this->app_details->settings_contactform_email){
                    $this->appEmail=$email->link;
                }
            endforeach;
        }else{
            $email= $this->RestClient->get('pages/app_email',array(
                'page_id' => $this->page_id,
                'email_id'=>$this->app_details->settings_contactform_email,
            ),1450);
            $this->appEmail=$email;
        }
    }
    /**
     * Register App Phone for easy access
     */
    protected function appPhone_set(){
        if(isset($this->app_details->phone) && count($this->app_details->phones)>0){
            foreach($this->app_details->phones as $phone):
                if($phone->id==$this->app_details->settings_contactform_phone){
                    $this->appPhone='+'.$phone->code.'.'.$phone->number;
                }
            endforeach;
        }else{
            $phone= $this->RestClient->get('pages/app_phone',array(
                'page_id' => $this->page_id,
                'phone_id'=>$this->app_details->settings_contactform_phone,
            ),1460);

            if($phone){
                $this->appPhone='+'.$phone->tel_code.'.'.$phone->number;
            }
        }
    }

    /**
     * Loggia errors flow
     * @param null $app_status
     */
    public function loggia_error($app_status = NULL) {
        $app_status = $this->app_status;

        if(isset($this->basic_app) && isset($this->basic_app->app_theme) && $this->basic_app->app_theme!=NULL && $this->basic_app->app_theme!=''){
            $this->theme=$this->basic_app->app_theme;
        }else{
            $this->theme='v1';
        }

        if($this->app_details->settings_contactform_email && $this->app_details->settings_contactform_email!='') {
            $this->appEmail_set();
        }

        if($this->app_details->settings_contactform_phone && $this->app_details->settings_contactform_phone!='') {
            $this->appPhone_set();
        }

        if(isset($this->basic_app->social_media_links) && count($this->basic_app->social_media_links)>0){
            $this->socialMedia=$this->basic_app->social_media_links;
        }

        if(isset($this->basic_app->retail_currency_details) && $this->basic_app->retail_currency_details!=NULL){
            $this->appCurrency=$this->basic_app->retail_currency_details;
        }

        if(!isset($this->app_details->app_theme_base_color) || $this->app_details->app_theme_base_color=='' || $this->app_details->app_theme_base_color==NULL){
            $this->app_details->app_theme_base_color='666666';
        }

        $this->template->set_layout('loggia_default_'.$this->theme);
        $this->template->set_partial('metadata','partials/'.$this->theme.'/error/metadata');
        $this->template->set_partial('header','partials/'.$this->theme.'/error/header');
        $this->template->set_partial('footer','partials/'.$this->theme.'/error/footer');
        $this->template->set_partial('color','partials/'.$this->theme.'/color');

        $data = array();
        switch ($app_status) {
            case 'expired':
                $data['title']=$this->basic_app->page_info->title.'-'.lang('website-offline-title-expired');
                $data['headtitle'] = lang('website-offline-headtitle-expired');
                $data['message'] = lang('website-offline-message-expired');
                break;
            case 'database':
                $data['title']=$this->basic_app->page_info->title.'-'.lang('website-offline-title-database');

                $data['headtitle'] = lang('website-offline-headtitle-database');
                $data['message'] = lang('website-offline-message-database');
                break;
            case 'deleted':
                redirect('http://www.loggia.gr', 'refresh');
                break;
            default:
                $data['title']=$this->basic_app->page_info->title.'-'.lang('website-offline-title-no-exist');
                $data['headtitle'] = lang('website-offline-headtitle-no-exist');
                $data['message'] = lang('website-offline-message-no-exist');
                break;
        }
        $this->template->build('errors/loggia_error_'.$this->theme,$data);
    }

    /**
     * Register terms articles for easy access
     */
    protected function terms_init(){
        $terms = $this->RestClient->get('articles/articles_list',array(
            'lang' => $this->lang,
            'page_id'=>$this->page_id,
        ),1440);
        if(isset($terms) && is_array($terms) && count($terms)>0){
            $this->terms = $terms;
        }
    }

    protected function fetch_footer_featured(){
        $model = new Articles();
        $posts = $model->getShowcasePosts($this->lang, 34, 3, 0,7200);
        if (!$posts) {
            $posts = array();
        } else {
            foreach ($posts as $post):
                if(isset($post->category_title) && $post->category_title!=null && $post->category_title!=''){
                    $post->render_category = Helpers::remove_tones($post->category_title);
                    $post->render_category_url = route('article_category',[$post->category_id,Helpers::urlize($post->category_title)]);
                }
                $post->render_url = Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,Helpers::urlize($post->title));
                $post->render_date = strtoupper(Helpers::remove_tones(Helpers::localeDate($post->created)));
                $post->render_no_image = isset($post->thumb)? null : 'no-image';
                if(isset($post->thumb)){
                    $post->thumb_square = Helpers::squareFilePath($post->thumb);
                }
                if(isset($post->subtitle) && $post->subtitle!=null && $post->subtitle!=''){
                    $post->render_subtitle = Helpers::get_snippet($post->subtitle,20).' [...]';
                }
                if(isset($post->authors) && $post->authors!=null && $post->authors!=''){
                    $post->render_author_name = Helpers::remove_tones($post->authors[0]->profile->firstname.' '.$post->authors[0]->profile->lastname);
                    $post->render_author_url = route('author',[$post->authors[0]->user_id]);
                }
                if(isset($post->permissions) && is_array($post->permissions) && count($post->permissions)>0) {
                    foreach ($post->permissions as $permission):
                        if ($permission->system_permission_id == 57 || $permission->system_permission_id == 58 || $permission->system_permission_id == 59) {
                            $post->render_lock = true;
                        }
                    endforeach;
                }
            endforeach;
            $this->footer_featured_posts = $posts;
        }
    }


    protected function check_article_permissions($id,$user_id=null,$entry_permission,$entry_plans=null){

        if($entry_permission == 'free'){
            return 'open';
        }elseif($entry_permission == 'users'){
            if($user_id==null){
                return 'users';
            }else{
                return 'open';
            }
        }elseif($entry_permission == 'locked'){
            return 'locked';
        }elseif($entry_permission == 'subscribers'){
            if($user_id == null){
                return 'subscribers';
            }else{
                if(isset($entry_plans) && is_array($entry_plans) && count($entry_plans) && is_object($this->authUser) && isset($this->authUser->subscriptions)){
                    foreach($entry_plans as $entry_plan):
                        $prices_group = (array)$entry_plan->prices;
                        if(count($prices_group)>0){
                            foreach($prices_group as $price_group_inner):
                                if(is_array($price_group_inner) && count($price_group_inner)>0){
                                    foreach($price_group_inner as $price):
                                        if(isset($this->authUser->subscriptions->{$price->plan_id}) && $this->authUser->subscriptions->{$price->plan_id}->system_type_id == $price->system_type_id){
                                            return 'open';
                                        }
                                    endforeach;
                                }
                            endforeach;
                        }
                    endforeach;
                }
            }

        }
    }

    protected function order_init(){
        $model = new Market();
        $order_id =  Session::get('order_id');
        $order = $model->getOrder($this->lang,$this->page_id,$order_id);
        if($order){
            $this->order = $order;
        }
    }

    public function deploy($token,$c_user=null){

        if($c_user!=null){
            $LOCAL_ROOT         = "/home/".$c_user."/"; // centos server
            $LOCAL_REPO_NAME    = "public_html";
        }else{
            // Set Variables
            $LOCAL_ROOT         = "/var/www/html"; // centos server
            $LOCAL_REPO_NAME    = "simplekioskweb";
        }

        $LOCAL_REPO         = "{$LOCAL_ROOT}/{$LOCAL_REPO_NAME}";
        $REMOTE_REPO        = "git@bitbucket.org:naggelakis/simplekioskweb.git";
        $BRANCH             = "master";

        echo 'received';
        if ($token = 'owiqmcnbks3w2pD' ) {
            // Only respond to POST requests from Github

            if( file_exists($LOCAL_REPO) ) {
                echo 'exist';
                // If there is already a repo, just run a git pull to grab the latest changes
                echo shell_exec("cd {$LOCAL_REPO} &&  /usr/bin/git pull 2>&1");
                //shell_exec("composer install");
                die("done " . time());
            } else {

                // If the repo does not exist, then clone it into the parent directory
                echo shell_exec("cd {$LOCAL_ROOT} && /usr/bin/git clone {$REMOTE_REPO} 2>&1");
                //shell_exec("composer install");
                die("done " . time());
            }
        }
    }
}
