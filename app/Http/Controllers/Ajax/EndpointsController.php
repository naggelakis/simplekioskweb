<?php

namespace WebKiosk\Http\Controllers\Ajax;

use WebKiosk\Http\Controllers\Core\LoggiaController;
use Illuminate\Http\Request;
use WebKiosk\ApiModels\Articles;
use WebKiosk\ApiModels\Users;
use Helpers;
use ArandiLopez\Feed\Facades\Feed;
use WebKiosk\ApiModels\Market;
use Symfony\Component\HttpFoundation\Cookie;
use Illuminate\Http\Response;

class EndpointsController extends LoggiaController
{

    public function latest_articles(Request $request,$limit=15,$offset=0){
        $model = new Articles();
        $posts = $model->getLatestPosts($this->lang,$limit,$offset);
        if(!$posts){
            $posts = array();
        }else{
            foreach($posts as $post):
                //Helpers::die_pre($post->default_category);
                if(isset($post->default_category->title) && $post->default_category->title!=null && $post->default_category->title!=''){
                    $post->render_category = Helpers::remove_tones($post->default_category->title);
                    $post->render_category_url =  Helpers::getRouteCategoryBySystemCat($post->system_parent_cat_id, $post->default_category->id, Helpers::urlize($post->default_category->title));//route('article_category',[$post->default_category->id,Helpers::urlize($post->default_category->title)]);
                }
                $post->render_url = Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,Helpers::urlize($post->title));
                $post->render_date = strtoupper(Helpers::remove_tones(Helpers::localeDate($post->created)));
                $post->render_no_image = isset($post->thumb)? null : 'no-image';
                if(isset($post->thumb)){
                    $post->thumb_square = Helpers::squareFilePath($post->thumb);
                }
                if(isset($post->subtitle) && $post->subtitle!=null && $post->subtitle!=''){
                    $post->render_subtitle = Helpers::get_snippet($post->subtitle,40).' [...]';
                }
                if(isset($post->author) && $post->author!=null && $post->author!=''){
                    $post->render_author_name = Helpers::remove_tones($post->author[0]->full_name);
                    $post->render_author_url = route('author',[$post->author[0]->user_id]);
                }
                if(is_object($this->authUser)) {
                    if (in_array($post->id,  $this->favorited_articles)){
                        $post->favorited = 1;
                    }else{
                        $post->favorited = 0;
                    }
                }
            endforeach;
        }
        echo json_encode($posts);

    }

    public function latest_publications(Request $request){
        $model = new Articles();
        $publications = $model->getLatestPublications($this->lang,86400);
        if(!$publications){
            $publications = array();
        }
        if(isset($publications->issues) && count($publications->issues) > 0){
            foreach($publications->issues as $issue):
                $issue->render_date = date('d/m/Y',strtotime($issue->publication_date));
                $issue->render_url = route('issue_single',[$issue->id,Helpers::urlize($issue->title)]);
            endforeach;
        }
        if(isset($publications->magazines) && count($publications->magazines) > 0){
            foreach($publications->magazines as $magazine):
                $magazine->render_date = date('d/m/Y',strtotime($magazine->publication_date));
                $magazine->render_url = route('publication_single',[$magazine->id,Helpers::urlize($magazine->title)]);

            endforeach;
        }
        echo json_encode($publications);
    }


    public function month_log(Request $request){
        $model = new Articles();
        $log = $model->getMonthLog($this->page_id,182,$this->lang,3600);
        if(is_object($log)){
            $log = Helpers::object_to_array($log);
        }
        $first  = strtotime('first day this month');
        $last  = strtotime('last day this month');

        $months = array();

        for ($i = 5; $i >= 0; $i--) {
            $object = new \stdClass();
            $string = date('M Y', strtotime("-$i month", $first));
            $object->name = Helpers::localeDate($string,'F Y','M Y');
            $key = (integer)date('m', strtotime("-$i month", $first));
            $object->from = date('m-Y', strtotime("-$i month", $first));
            $object->to = date('t-m-Y', strtotime("-$i month", $first));
            $object->url = route('feed_list',['01-'.$object->from,$object->to]);

            $object->count = isset($log[$key])?$log[$key]:0;
            $months[] = $object;
        }
        $months = array_reverse($months);
        echo json_encode($months);

    }


    public function recent_tabs_homepage()
    {
        $model = new Articles();

        $data = [];
        if (isset($this->basic_app->disqus_forum) && $this->basic_app->disqus_forum != null && $this->basic_app->disqus_forum != ''){
            $feed = Feed::make('http://'.$this->basic_app->disqus_forum.'.disqus.com/latest.rss');
            $json_items = $feed->toJson();
            $comments = json_decode($json_items);
            $data['comments'] = $comments;
        }

        $gallery_posts = $model->getLatestPostsGalleries($this->lang,3,0,3600,241,249);
        if(is_array($gallery_posts) && count($gallery_posts)>0){
            foreach($gallery_posts as $post):
                $post->render_url = Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,$post->slug);
                $post->render_date = strtoupper(Helpers::remove_tones(Helpers::localeDate($post->created)));
                $post->render_no_image = isset($post->thumb)? null : 'no-image';
                if(isset($post->thumb)){
                    $post->thumb_square = Helpers::squareFilePath($post->thumb);
                }
                if(isset($post->subtitle) && $post->subtitle!=null && $post->subtitle!=''){
                    $post->render_subtitle = Helpers::get_snippet($post->subtitle,20).' [...]';
                }
                if(is_object($this->authUser)) {
                    if (in_array($post->id,  $this->favorited_articles)){
                        $post->favorited = 1;
                    }else{
                        $post->favorited = 0;
                    }
                }
            endforeach;
            $data['galleries'] = $gallery_posts;
        }

        echo json_encode($data);
    }


    public function authors_showcase_homepage(){
        $model = new Articles();

        $data = [];
        $authors = $model->getAuthorsWithRecentPost($this->lang);
        if($authors && is_array($authors) && count($authors)>0){
            foreach($authors as $author):
                $author->render_name = Helpers::remove_tones($author->firstname.' '.$author->lastname);
                $author->render_url = route('author',[$author->id]);
                if(isset($author->entries) && is_array($author->entries) && count($author->entries)>0){
                    $entry = $author->entries[0];
                    $entry->url = Helpers::getRouteSingleBySystemCat($entry->system_parent_cat_id,$entry->id,Helpers::urlize($entry->title));
                    $entry->render_date =   Helpers::remove_tones( Helpers::localeDate($entry->created,'d F Y'));
                    $author->entry = $entry;
                }
            endforeach;
            echo json_encode($authors);
        }

    }

    public function showcase_entries($showcase_id,$limit=5,$offset=0)
    {
        $model = new Articles();
        $posts = $model->getShowcasePosts($this->lang, $showcase_id, $limit, $offset);
        if (!$posts) {
            $posts = array();
        } else {
            foreach ($posts as $post):
                if(isset($post->category_title) && $post->category_title!=null && $post->category_title!=''){
                    $post->render_category = Helpers::remove_tones($post->category_title);
                    $post->render_category_url =  Helpers::getRouteCategoryBySystemCat($post->system_parent_cat_id, $post->category_id, Helpers::urlize($post->category_title));
                    //route('article_category',[$post->category_id,Helpers::urlize($post->category_title)]);
                }
                $post->render_url = Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,Helpers::urlize($post->title));
                $post->render_date = strtoupper(Helpers::remove_tones(Helpers::localeDate($post->created)));
                $post->render_no_image = isset($post->thumb)? null : 'no-image';
                if(isset($post->thumb)){
                    $post->thumb_square = Helpers::squareFilePath($post->thumb);
                }
                if(isset($post->subtitle) && $post->subtitle!=null && $post->subtitle!=''){
                    $post->render_subtitle = Helpers::get_snippet($post->subtitle,20).' [...]';
                }
                if(isset($post->authors) && $post->authors!=null && $post->authors!=''){
                    $post->render_author_name = Helpers::remove_tones($post->authors[0]->profile->firstname.' '.$post->authors[0]->profile->lastname);
                    $post->render_author_url = route('author',[$post->authors[0]->user_id]);
                }
                if(isset($post->permissions) && is_array($post->permissions) && count($post->permissions)>0) {
                    foreach ($post->permissions as $permission):
                        if ($permission->system_permission_id == 57 || $permission->system_permission_id == 58 || $permission->system_permission_id == 59) {
                            $post->render_lock = true;
                        }
                    endforeach;
                    // if($post->system_status_id == )
                }
                if(is_object($this->authUser)) {
                    if (in_array($post->id,  $this->favorited_articles)){
                        $post->favorited = 1;
                    }else{
                        $post->favorited = 0;
                    }
                }
            endforeach;
        }
        //Helpers::die_pre($posts);
        echo json_encode($posts);
    }

    public function showcase_categories($showcase_id,$limit=5,$offset=0)
    {
        $model = new Articles();
        $cats = $model->getShowcaseCategories($this->lang, $showcase_id, $limit, $offset);

        if (!$cats) {
            $cats = array();
        } else {
            foreach ($cats as $key=>$cat):
                if(isset($cat->entries) && is_array($cat->entries) && count($cat->entries)>0) {
                    $cat->render_title =  Helpers::remove_tones($cat->title);
                    foreach($cat->entries as $post):
                        if (isset($post->category_title) && $post->category_title != null && $post->category_title != '') {
                            $post->render_category = Helpers::remove_tones($post->category_title);
                            $post->render_category_url = Helpers::getRouteCategoryBySystemCat($post->system_parent_cat_id, $post->category_id, Helpers::urlize($post->category_title));
                                //route('article_category', [$post->category_id, Helpers::urlize($post->category_title)]);
                        }
                        $post->render_url = Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id, $post->id, Helpers::urlize($post->title));
                        $post->render_date = strtoupper(Helpers::remove_tones(Helpers::localeDate($post->created)));
                        $post->render_no_image = isset($post->thumb) ? null : 'no-image';
                        if (isset($post->thumb)) {
                            $post->thumb_square = Helpers::squareFilePath($post->thumb);
                        }
                        if (isset($post->subtitle) && $post->subtitle != null && $post->subtitle != '') {
                            $post->render_subtitle = Helpers::get_snippet($post->subtitle, 20) . ' [...]';
                        }
                        if (isset($post->authors) && $post->authors != null && $post->authors != '') {
                            $post->render_author_name = Helpers::remove_tones($post->authors[0]->profile->firstname . ' ' . $post->authors[0]->profile->lastname);
                            $post->render_author_url = route('author', [$post->authors[0]->user_id]);
                        }
                        if (isset($post->permissions) && is_array($post->permissions) && count($post->permissions) > 0) {
                            foreach ($post->permissions as $permission):
                                if ($permission->system_permission_id == 57 || $permission->system_permission_id == 58 || $permission->system_permission_id == 59) {
                                    $post->render_lock = true;
                                }
                            endforeach;
                            // if($post->system_status_id == )
                        }
                        if(is_object($this->authUser)) {
                            if (in_array($post->id,  $this->favorited_articles)){
                                $post->favorited = 1;
                            }else{
                                $post->favorited = 0;
                            }
                        }
                    endforeach;
                }else{
                    unset($cats[$key]);
                }
            endforeach;
        }
        //Helpers::die_pre($posts);
        echo json_encode($cats);
    }


    public function latest_category_articles(Request $request,$limit=15,$offset=0,$type,$cat_id=null,$ids=null){
        $model = new Articles();

        if($ids!=null && $ids!='null' && $ids!='' && $ids!='undefined'){
            $explode = explode('_',$ids);
            if(count($explode)>0){
                $ids = $explode;
            }else{
                $ids = null;
            }
        }
        $posts = $model->getCategoryEntries($this->lang,$this->page_id,$cat_id,$type,$limit,$offset,null,'DESC',$ids);


        if(!$posts){
            $posts = array();
        }else{
            foreach($posts as $post):
                //Helpers::die_pre($post->default_category);
                if(isset($post->default_category->title) && $post->default_category->title!=null && $post->default_category->title!=''){
                    $post->render_category = Helpers::remove_tones($post->default_category->title);
                    $post->render_category_url =  Helpers::getRouteCategoryBySystemCat($post->system_parent_cat_id, $post->default_category->id, Helpers::urlize($post->default_category->title));//route('article_category',[$post->default_category->id,Helpers::urlize($post->default_category->title)]);
                }
                $post->render_url = Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,Helpers::urlize($post->title));
                $post->render_date = strtoupper(Helpers::remove_tones(Helpers::localeDate($post->created)));
                $post->render_no_image = isset($post->thumb)? null : 'no-image';
                if(isset($post->thumb)){
                    $post->thumb_square = Helpers::squareFilePath($post->thumb);
                }
                if(isset($post->subtitle) && $post->subtitle!=null && $post->subtitle!=''){
                    $post->render_subtitle = Helpers::get_snippet($post->subtitle,40).' [...]';
                }
                if(isset($post->author) && $post->author!=null && $post->author!=''){
                    $post->render_author_name = Helpers::remove_tones($post->author[0]->full_name);
                    $post->render_author_url = route('author',[$post->author[0]->user_id]);
                }
                if(is_object($this->authUser)) {
                    if (in_array($post->id,  $this->favorited_articles)){
                        $post->favorited = 1;
                    }else{
                        $post->favorited = 0;
                    }
                }
            endforeach;
        }


        echo json_encode($posts);

    }


    public function tag_search(Request $request,$limit=15,$offset=0,$only_news=0,$tag_id=null,$search=null,$from=null,$to=null){

        $model = new Articles();
        if($tag_id == 0){ $tag_id = null; }
        if($search == 'undefined'){ $search = null; }
        $posts = $model->searchPosts($this->lang,$limit,$offset,$tag_id,$search,$from,$to,$only_news);



        $output = array();
        if($posts){
            foreach($posts as $key=>$post):
                if(isset($post->id)) {

                    //Helpers::die_pre($post->default_category);
                    if (isset($post->default_category->title) && $post->default_category->title != null && $post->default_category->title != '') {
                        $post->render_category = Helpers::remove_tones($post->default_category->title);
                        $post->render_category_url = Helpers::getRouteCategoryBySystemCat($post->system_parent_cat_id, $post->default_category->id, Helpers::urlize($post->default_category->title));//route('article_category',[$post->default_category->id,Helpers::urlize($post->default_category->title)]);
                    }
                    $post->render_url = Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id, $post->id, Helpers::urlize($post->title));
                    $post->render_date = strtoupper(Helpers::remove_tones(Helpers::localeDate($post->created)));
                    $post->render_no_image = isset($post->thumb) ? null : 'no-image';
                    if (isset($post->thumb)) {
                        $post->thumb_square = Helpers::squareFilePath($post->thumb);
                    }
                    if (isset($post->subtitle) && $post->subtitle != null && $post->subtitle != '') {
                        $post->render_subtitle = Helpers::get_snippet($post->subtitle, 40) . ' [...]';
                    }
                    if (isset($post->author) && $post->author != null && $post->author != '') {
                        $post->render_author_name = Helpers::remove_tones($post->author->firstname . ' ' . $post->author->lastname);
                        $post->render_author_url = route('author', [$post->author->id]);
                    }
                    if(is_object($this->authUser)) {
                        if (in_array($post->id,  $this->favorited_articles)){
                            $post->favorited = 1;
                        }else{
                            $post->favorited = 0;
                        }
                    }
                    $output[] = $post;
                }

            endforeach;
        }

       // Helpers::die_pre($posts);
        echo json_encode($output);

    }

    public function publication_issues(Request $request,$publication_id,$limit = 10,$offset=0){
        $model = new Market();
        $issues = $model->getPublicationIssues($this->lang,$publication_id,$limit,$offset,86400);
        if(!$issues){
            $issues = array();
        }else{
            foreach($issues as $issue):
                $issue->render_url = route('issue_single',[$issue->id,Helpers::urlize($issue->title)]);
                $issue->render_date = Helpers::localeDate($issue->publication_date,'d/m/Y');
                if(isset($issue->cdn)) {
                    $issue->render_image = $issue->cdn;
                }
            endforeach;
        }
        echo json_encode($issues);
    }


    public function market_categories(Request $request){
        $model = new Market();
        $categories = $model->getCategories($this->lang,86400);

        if($categories){
            foreach($categories as $category):
                $category->render_title = $category->title;
                $category->render_url = route('publications_category',[$category->id,Helpers::urlize($category->title)]);
            endforeach;
        }else{
            $categories = array();
        }

        echo json_encode($categories);
    }


    public function fetch_magazines(Request $request,$limit=100){
        $model = new Market();
        $magazines = $model->getPublications($this->lang,$limit,213,0,86400);
        if($magazines){
            foreach($magazines as $magazine):
                $magazine->render_title = $magazine->title;
                $magazine->render_date = Helpers::localeDate($magazine->publication_date,'d/m/Y');
                $magazine->render_url = route('publication_single',[$magazine->id,Helpers::urlize($magazine->title)]);
                if(isset($magazine->cdn)) {
                    $magazine->render_image = Helpers::squareFilePath($magazine->cdn);
                }
            endforeach;
        }else{
            $magazines = array();
        }
        echo json_encode($magazines);

    }

    public function fetch_issues(Request $request,$limit=100){
        $model = new Market();
        $issues = $model->getPublications($this->lang,$limit,213,1,86400);
        if($issues){
            foreach($issues as $issue):
                $issue->render_title = $issue->title;
                $issue->render_date = Helpers::localeDate($issue->publication_date,'d/m/Y');
                $issue->render_url = route('issue_single',[$issue->id,Helpers::urlize($issue->title)]);
                if(isset($issue->cdn)) {
                    $issue->render_image = Helpers::squareFilePath($issue->cdn);
                }
            endforeach;
        }else{
            $issues = array();
        }
        echo json_encode($issues);

    }


    public function fetch_featured_publications_sidebar(Request $request){
        $model = new Market();
        $publications = $model->getMarketShowcasePublications($this->lang,9);
        $output = array();
        if($publications){
            foreach($publications as $publication):
                $item = new \stdClass();
                if($publication->system_cat_id == 213){ // publication
                    if($publication->parent_id == null){
                        $item->render_url = route('publication_single',[$publication->id,Helpers::urlize($publication->slug)]);
                    }else{
                        $item->render_url = route('issue_single',[$publication->id,Helpers::urlize($publication->slug)]);
                    }
                }elseif($publication->system_cat_id == 212){ //book
                    $item->render_url = route('book_single',[$publication->id,Helpers::urlize($publication->slug)]);
                }

                $item->render_title = isset($publication->translations->{$this->lang}) ? $publication->translations->{$this->lang}->title : current($publication->translations)->title;
                if(isset($publication->thumb->cdn)){
                    $item->render_image = $publication->thumb->cdn;
                }
                if(isset($publication->min_price)) {
                    $item->render_price = $publication->min_price;
                }
                $output[] = $item;
            endforeach;
        }
        echo json_encode($output);
    }

    public function fetch_author_entries(Request $request,$author_id,$limit=10,$offset=0){
        $model = new Articles();
        $posts = $model->getLatestPosts($this->lang,$limit,$offset,false,182,null,$author_id,1);
        if(!$posts){
            $posts = array();
        }else{
            foreach($posts as $post):
                //Helpers::die_pre($post->default_category);
                if(isset($post->default_category->title) && $post->default_category->title!=null && $post->default_category->title!=''){
                    $post->render_category = Helpers::remove_tones($post->default_category->title);
                    $post->render_category_url =  Helpers::getRouteCategoryBySystemCat($post->system_parent_cat_id, $post->default_category->id, Helpers::urlize($post->default_category->title));//route('article_category',[$post->default_category->id,Helpers::urlize($post->default_category->title)]);
                }
                $post->render_url = Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,Helpers::urlize($post->title));
                $post->render_date = strtoupper(Helpers::remove_tones(Helpers::localeDate($post->created)));
                $post->render_no_image = isset($post->thumb)? null : 'no-image';
                if(isset($post->thumb)){
                    $post->thumb_square = Helpers::squareFilePath($post->thumb);
                }
                if(isset($post->subtitle) && $post->subtitle!=null && $post->subtitle!=''){
                    $post->render_subtitle = Helpers::get_snippet($post->subtitle,40).' [...]';
                }
                if(isset($post->author) && $post->author!=null && $post->author!=''){
                    $post->render_author_name = Helpers::remove_tones($post->author[0]->full_name);
                    $post->render_author_url = route('author',[$post->author[0]->user_id]);
                }
                if(is_object($this->authUser)) {
                    if (in_array($post->id,  $this->favorited_articles)){
                        $post->favorited = 1;
                    }else{
                        $post->favorited = 0;
                    }
                }
            endforeach;
        }
        echo json_encode($posts);
    }


    public function fetch_favorites_posts(Request $request){
        if(is_object($this->authUser)){
            echo json_encode($this->favorited_articles);
        }
    }

    public function favorite_post(Request $request,$id,$action){
        if(is_object($this->authUser)) {
            $model = new Articles();
            if ($action == 'like') {
                $result = $model->favoritePost($this->authUser->id, $id);
            } elseif ($action == 'unlike') {
                $result = $model->unfavoritePost($this->authUser->id, $id);
            }

            if($result){
                echo json_encode($result);
            }
        }
    }

    public function favorite_articles(Request $request,$limit=15,$offset=0){
        $model = new Articles();
        $posts = $model->getLatestPosts($this->lang,$limit,$offset,false,182,null,null,null,$this->authUser->id);
        if(!$posts){
            $posts = array();
        }else{
            foreach($posts as $post):
                //Helpers::die_pre($post->default_category);
                if(isset($post->default_category->title) && $post->default_category->title!=null && $post->default_category->title!=''){
                    $post->render_category = Helpers::remove_tones($post->default_category->title);
                    $post->render_category_url =  Helpers::getRouteCategoryBySystemCat($post->system_parent_cat_id, $post->default_category->id, Helpers::urlize($post->default_category->title));//route('article_category',[$post->default_category->id,Helpers::urlize($post->default_category->title)]);
                }
                $post->render_url = Helpers::getRouteSingleBySystemCat($post->system_parent_cat_id,$post->id,Helpers::urlize($post->title));
                $post->render_date = strtoupper(Helpers::remove_tones(Helpers::localeDate($post->created)));
                $post->render_no_image = isset($post->thumb)? null : 'no-image';
                if(isset($post->thumb)){
                    $post->thumb_square = Helpers::squareFilePath($post->thumb);
                }
                if(isset($post->subtitle) && $post->subtitle!=null && $post->subtitle!=''){
                    $post->render_subtitle = Helpers::get_snippet($post->subtitle,40).' [...]';
                }
                if(isset($post->author) && $post->author!=null && $post->author!=''){
                    $post->render_author_name = Helpers::remove_tones($post->author[0]->full_name);
                    $post->render_author_url = route('author',[$post->author[0]->user_id]);
                }
                if(is_object($this->authUser)) {
                    if (in_array($post->id,  $this->favorited_articles)){
                        $post->favorited = 1;
                    }else{
                        $post->favorited = 0;
                    }
                }
            endforeach;
        }
        echo json_encode($posts);

    }

    public function user_issues(Request $request,$limit=15,$offset=0,$sort=0){
        $model = new Market();
        $issues = $model->getSubscribedIssues($this->page_id,$this->lang,$this->authUser->id,$limit,$offset,$sort);
        if(!$issues){
            $issues = array();
        }else{

            foreach($issues as $issue):
                $issue->render_title = $issue->title;
                $issue->render_date = Helpers::localeDate($issue->publication_date,'d/m/Y');
                $issue->render_url = route('issue_single',[$issue->id,Helpers::urlize($issue->title)]);
                if(isset($issue->thumb) && isset($issue->thumb->cdn)) {
                    $issue->render_image = Helpers::squareFilePath($issue->thumb->cdn);
                }
                if($issue->parent_id != null){
                    $issue->render_magazine_url = route('publication_single',[$issue->magazine->id,Helpers::urlize($issue->magazine->title)]);
                }
            endforeach;
        }

        echo json_encode($issues);
    }

    public function check_email_unique(Request $request){
        if($request->getMethod() == 'POST') {
            $post_data = $request->all();
            if(isset($post_data['email']) && $post_data['email'] != $this->authUser->email){
                $model = new Users();
                $check = $model->check_email_unique($this->page_id,$post_data['email']);
                echo json_encode($check);
            }else{
                echo json_encode(0);
            }

        }
    }


    public function category_issues(Request $request,$category_id,$limit=15,$offset=0,$sort=0){
        $model = new Market();
        if($sort > 0){
            if($sort == 1){
                $sort = array('title'=>'asc');
            }elseif($sort == 2){
                $sort = array('base_price'=>'asc');
            }elseif($sort == 3){
                $sort = array('base_price'=>'desc');
            }
        }

        $issues = $model->getPublicationsByCat($category_id,$this->page_id,$this->lang,$limit,$offset,$sort,86400*1);

        if(!$issues){
            $issues = array();
        }else{
            foreach($issues as $issue):
                $issue->render_title = $issue->title;
                $issue->render_date = Helpers::localeDate($issue->publication_date,'d/m/Y');
                $issue->render_url = route('issue_single',[$issue->id,Helpers::urlize($issue->title)]);
                if( isset($issue->last_issue_cover) && isset($issue->last_issue_cover->cdn) ) {
                   // Helpers::die_pre($issue);
                    $issue->render_image = $issue->last_issue_cover->cdn;
                }elseif( isset($issue->thumb) && isset($issue->thumb->cdn) ) {
                    $issue->render_image =$issue->thumb->cdn;
                }
                if($issue->parent_id != null) {
                    $issue->render_magazine_url = route('publication_single', [$issue->magazine->id, Helpers::urlize($issue->magazine->title)]);
                }
            endforeach;
        }

        echo json_encode($issues);
    }

    public function inAppPurchaseClick(Request $request){
        $response = new Response('i clicked');

        $response->withCookie(cookie()->forever('userIssuesAppPurchases', 1));

        return $response;
    }
}
