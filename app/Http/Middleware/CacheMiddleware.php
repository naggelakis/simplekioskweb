<?php

namespace WebKiosk\Http\Middleware;

use Closure;
use Cache;
use Helpers;

class CacheMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->input('eraseCache')) {
           Cache::flush();
        }
        return $next($request);
    }
}
