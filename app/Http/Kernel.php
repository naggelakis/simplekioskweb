<?php

namespace WebKiosk\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        'WebKiosk\Http\Middleware\CacheMiddleware',
        'WebKiosk\Http\Middleware\Language',
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \WebKiosk\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \WebKiosk\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \WebKiosk\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \WebKiosk\Http\Middleware\RedirectIfAuthenticated::class,
    ];
}
