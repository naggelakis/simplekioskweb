<?php
namespace WebKiosk\ApiModels;
use WebKiosk\Custom\RestClient;
use Helpers;
use Illuminate\Support\Facades\Config;

class Market {
    private $restClient;
    private $page_id;

    public function __construct(){
        $this->restClient = new RestClient();
        $this->page_id =  $this->page_id = defined('LOGGIA_PAGE') ? LOGGIA_PAGE : Config::get('loggia.page_id');

    }

    public function getPublication($id,$lang='EL',$cache=false,$user_id=null){
        $data = array(
            'id'=>$id,
            'lang'=>$lang
        );
        if($user_id!=null){
            $data['user_id'] = $user_id;
        }
        $publication = $this->restClient->get('market/getPublication',
            $data
            ,
            $cache
        );

        if(isset($publication) && is_object($publication)){
            return $publication;
        }
    }

    public function getPublicationIssues($lang='EL',$id,$limit=10,$offset=0,$cache=false){
        $data = array(
            'id'=>$id,
            'limit'=>$limit,
            'offset'=>$offset,
            'lang'=>$lang
        );

        $issues = $this->restClient->get('market/getPublicationIssues',
            $data
            ,
            $cache
        );
        if(isset($issues) && is_array($issues)){
            return $issues;
        }
    }


    public function getCategories($lang='EL',$cache=false){
        $data = array(
            'lang'=>$lang
        );

        $categories = $this->restClient->get('market/getCategories',
            $data
            ,
            $cache
        );
        if(isset($categories->message) && is_array($categories->message)){
            return $categories->message;
        }
    }

    public function getPublications($lang='EL',$limit=10,$system_cat_id=null,$issues_only=null,$cache=false){
        $data = array(
            'lang'=>$lang,
            'system_cat_id'=>$system_cat_id,
            'issues_only'=>$issues_only,
            'limit'=>$limit
        );

        $publications = $this->restClient->get('market/getPublications',
            $data
            ,
            $cache
        );

        if(isset($publications) && is_array($publications)){
            return $publications;
        }
    }


    public function getMarketShowcasePublications($lang,$showcase_id,$cache=false){
        $data = array(
            'lang'=>$lang,
            'showcase_id'=>$showcase_id
        );

        $publications = $this->restClient->get('market/getShowcasePublications',
            $data
            ,
            $cache
        );
        if($publications && is_array($publications) && count($publications)>0){
            return $publications;
        }

    }


    public function getPublicationArticles($lang,$publication_id,$limit=null,$offset=null,$cache=false){
        $data = array(
            'lang'=>$lang,
            'id'=>$publication_id,
            'limit'=>$limit,
            'offset'=>$offset
        );

        $articles = $this->restClient->get('market/getPublicationArticles',
            $data
            ,
            $cache
        );
        if($articles && is_array($articles) && count($articles)>0){
            return $articles;
        }
    }


    public function getPaymentMethods($lang,$page_id,$cache=false){

        $data = array(
            'lang'=>$lang,
            'page_id'=>$page_id
        );

        $payment_methods = $this->restClient->get('market/getPaymentMethods',
            $data
            ,
            $cache
        );
        if(isset($payment_methods->code) && $payment_methods->code==200){
            return $payment_methods->message;
        }

    }


    public function createOrder($lang,$page_id,$profile,$class_type = 165,$payment_method=null,$recurring_plan_id=null){

        $data = array(
            'lang' => $lang,
            'page_id'=>$page_id,
            'profile'=>$profile,
            'class_type'=>$class_type,
            'pay_method' => $payment_method,
            'status_id'=>111
        );


        if($recurring_plan_id!=null){
            $data['has_subscription'] = $recurring_plan_id;
        }
        $response = $this->restClient->post('market/createCart',
            $data,
        false);

        if($response->code==200){
            $cart_id=$response->message;
            return $cart_id;
        }

    }

    public function addProductToOrder($cart_id,$lang,$page_id,$data,$prod_type_option,$prod_type,$group_id=null){

        $item = $this->restClient->get('market/addProductToCart',array(
            'lang' => $lang,
            'page_id'=>$page_id,
            'cart_id'=>$cart_id,
            'product'=>$data,
            'prod_type_option'=>$prod_type_option,
            'prod_type'=>$prod_type,
            'prod_group_id' =>$group_id
        ));
        //Helpers::die_pre($this->restClient->last_get);
        if(isset($item->code) && $item->code == 200){
            return $item->message;
        }
    }


    public function updateOrder($lang,$page_id,$cart_id,$update_data=array(),$subscription = null){

        $data = array(
            'lang' => $lang,
            'page_id'=> $page_id,
            'cart_id'=> $cart_id,
        );


        if(is_array($update_data) && count($update_data)>0){

            foreach($update_data as $field=>$value):
                $data[$field] = $value;
            endforeach;

            $update_cart = $this->restClient->get('market/updateCart',
                $data
            );

            if($subscription!=null){
                $update_subscription = $this->restClient->get('market/updateSubscription',
                    array(
                        'id'=>$subscription->id,
                        'status'=> 69
                    )
                );
            }

            if(isset($update_cart->code) && $update_cart->code == 200){
                return $update_cart->message;
            }
        }

    }

    public function getOrder($lang,$page_id,$order_id){

        $order = $this->restClient->get('market/getCart',array(
            'lang' => $lang,
            'page_id'=> $page_id,
            'cart_id'=> $order_id,
        ));

        if(isset($order->code) && $order->code == 200){
            return $order->message;
        }
    }


    public function getOrders($lang,$page_id,$user_id){

        $orders = $this->restClient->get('market/getOrders',array(
            'lang' => $lang,
            'page_id'=> $page_id,
            'user_id'=> $user_id,
            'class_type'=>165
        ));
        if(isset($orders->code) && $orders->code == 200){
            return $orders->message;
        }
    }

    public function createRecurringSubscription($data){

        $order = $this->restClient->post('market/createRecurringSubscription',$data);
        if(isset($order->code) && $order->code == 200){
            return $order->message;
        }
    }


    public function getCountriesList($lang,$cache=false){
        $countries = $this->restClient->get('settings/countries',
            array(
                'lang' => $lang
            ),
            $cache
        );

        if(is_object($countries)){
            return (array)$countries;
        }
    }


    public function getCitiesList($lang,$cache=false,$proccess=false){

        $cities = $this->restClient->get('settings/cities',
            array(
                'lang' => $lang
            ),
            $cache
        );

        if(is_object($cities)){

            $cities = (array)$cities;
            if($proccess) {
                $output = [];
                foreach($cities as $county):
                    $county = (array)$county;
                    if(count($county)>0){
                        foreach($county as $city_id=>$city_name):
                            $output[$city_id] = $city_name;
                        endforeach;
                    }
                endforeach;
            }else {
                $output = $cities;
            }
            return $output;
        }
    }

    public function getShippingMethods($lang,$page_id,$cache=false){

        $shippingMethods = $this->restClient->get('market/getShippingMethods',
            array(
                'lang'=>$lang,
                'page_id'=>$page_id
            ),
            $cache
        );

        if($shippingMethods && isset($shippingMethods->code) && $shippingMethods->code == 200 ){
            return $shippingMethods->message;
        }
    }

    public function getShippingRate($lang,$page_id,$shipping_method,$weight,$country,$city=null){

        $shippingRate = $this->restClient->get('market/getShippingRate',
            array(
                'lang'=>$lang,
                'page_id'=>$page_id,
                'shipping_method'=>$shipping_method,
                'weight'=>$weight,
                'country'=>$country,
                'city'=>$city
            )
        );

        if($shippingRate && isset($shippingRate->code) && $shippingRate->code == 200 ){
            return $shippingRate->message;
        }
    }

    public function getSubscriptionDetails($subscription_id,$page_id,$lang){
        $subscription = $this->restClient->get('market/getSubscriptionDetails',
            array(
                'subscription_id'=>$subscription_id,
                'page_id'=>$page_id,
                'lang'=>$lang
            )
        );

        if($subscription && isset($subscription->code) && $subscription->code == 200 ){
            return $subscription->message;
       }
    }

    public function updateSubscription($subscription_id,$expire_date){
        $update = $this->restClient->post('market/updateSubscription',
            array(
                'id'=>$subscription_id,
                'expire_date'=>$expire_date
            )
        );
        if($update){
            return $update;
        }
    }

    public function getUserSubscriptions($user_id,$lang){
        $subscriptions = $this->restClient->get('market/getUserSubscriptions',
            array(
                'user_id'=>$user_id,
            )
        );

        if($subscriptions && isset($subscriptions->code) && $subscriptions->code == 200 ){
            return (array)$subscriptions->message;
        }
    }

    public function getSubscribedIssues($page_id,$lang,$user_id,$limit = 15,$offset=0,$sort=0){
        $issues = $this->restClient->get('market/getUserSubscribedIssues',
            array(
                'user_id'=>$user_id,
                'limit'=>$limit,
                'offset'=>$offset,
                'page_id'=>$page_id,
                'lang'=>$lang,
                'sort'=>$sort
            )
        );

        if($issues && isset($issues->code) && $issues->code == 200 ){
            return (array)$issues->message;
        }
    }

    public function getSubscribedMagazines($page_id,$lang,$user_id,$limit = 15,$offset=0){
        $magazines = $this->restClient->get('market/getUserSubscribedMagazines',
            array(
                'user_id'=>$user_id,
                'limit'=>$limit,
                'offset'=>$offset,
                'page_id'=>$page_id,
                'lang'=>$lang
            )
        );
        //Helpers::die_pre($this->restClient->last_get);
        if($magazines && isset($magazines->code) && $magazines->code == 200 ){
            return (array)$magazines->message;
        }
    }


    public function getPublicationsByCat($cat_id,$page_id,$lang,$limit = 15,$offset=0,$sort=0,$cache=false){
        $issues = $this->restClient->get('market/getPublicationsByCategory',
            array(
                'cat_id'=>$cat_id,
                'limit'=>$limit,
                'offset'=>$offset,
                'page_id'=>$page_id,
                'lang'=>$lang,
                'sort'=>$sort
            ),
            $cache
        );

        if($issues && isset($issues->code) && $issues->code == 200 ){
            return (array)$issues->message;
        }
    }


    public function getMarketCategoryDetails($cat_id,$page_id,$lang,$cache=false){
        $category = $this->restClient->get('market/categoryDetails',
            array(
                'cat_id'=>$cat_id,
                'page_id'=>$page_id,
                'lang'=>$lang
            ),
            $cache
        );
        if($category && isset($category->code) && $category->code == 200 ){
            return $category->message;
        }
    }

    public function registerUserLocation($user_id,$lang,$data){
        $data['user_id'] = $user_id;
        $data['lang'] = $lang;

        $location = $this->restClient->post('users/register_user_location',
            $data
        );

        if($location && isset($location->code) && $location->code == 200 ){
            return $location->message;
        }
    }

    public function updateUserLocation($user_id,$lang,$data){
        $data['user_id'] = $user_id;
        $data['lang'] = $lang;

        $location = $this->restClient->post('users/update_user_location',
            $data
        );

        if($location && isset($location->code) && $location->code == 200 ){
            return $location->message;
        }
    }


    public function setDefaultLocation($user_id,$location_id){
        $location = $this->restClient->get('users/set_default_location',
            [
                'user_id'=>$user_id,
                'location_id'=>$location_id
            ]
        );


        if($location && isset($location->code) && $location->code == 200 ){
            return $location->message;
        }
    }

    public function deleteLocation($user_id,$location_id){
        $location = $this->restClient->post('users/delete_location',
            [
                'user_id'=>$user_id,
                'location_id'=>$location_id
            ]
        );


        if($location && isset($location->code) && $location->code == 200 ){
            return $location->message;
        }
    }

    public function getAllRecurringPlans($page_id,$lang){

         $recurring_plans = $this->restClient->get('market/getRecurringPlans',
             [
                 'page_id'=>$page_id,
                 'lang'=>$lang
             ]
         );

        if($recurring_plans && isset($recurring_plans->code) && $recurring_plans->code == 200 ){
            return $recurring_plans->message;
        }
    }
}