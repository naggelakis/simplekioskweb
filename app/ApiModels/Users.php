<?php
namespace WebKiosk\ApiModels;
use WebKiosk\Custom\RestClient;
use Helpers;
use Illuminate\Support\Facades\Config;

class Users {
    private $restClient;
    private $page_id;

    public function __construct(){
        $this->restClient = new RestClient();
        $this->page_id =  $this->page_id = defined('LOGGIA_PAGE') ? LOGGIA_PAGE : Config::get('loggia.page_id');

    }

    public function registerUser($lang='EL',$email,$password,$firstname,$lastname,$gender,$birthdate=null,$cache=false){

        $data = array(
            'lang' => $lang,
            'page_id'=>$this->page_id,
            'email'=>$email,
            'password'=>$password,
            'firstname'=>$firstname,
            'lastname'=>$lastname,
            'gender'=>$gender
        );

        if($birthdate!=null){
            $data['birth_date'] = strtotime($birthdate);
        }

        $user = $this->restClient->post('auth/create_user',
            $data
        );
        if($user){
            return $user;
        }
    }

    public function loginUser($email,$password){

        $data = array(
            'email' => $email,
            'password'=> $password
        );

        $user = $this->restClient->post('auth/login',
            $data
        );

        if($user && is_object($user)){
            return $user;
        }

    }

    public function social_user_login($lang='EL',$email,$firstname,$lastname,$username,$fb_id=null,$twitter_id=null,$google_id=null,$google_avatar=null){
        $data = [
            'lang'=>$lang,
            'email'=>$email,
            'first_name'=>$firstname,
            'last_name'=>$lastname,
            'username'=>$username,
        ];

        if($fb_id!=null){
            $data['fb_id'] = $fb_id;
        }

        if($twitter_id!=null){
            $data['twitter_id'] = $twitter_id;
        }

        if($google_id!=null){
            $data['google_id'] = $google_id;
        }
        if($google_avatar!=null){
            $data['google_image'] = $google_avatar;
        }
        $user = $this->restClient->post('auth/mobile_application_login',
            $data
            );

        if(is_object($user) && isset($user->user->id)){
            $user->user->user_id = $user->user->id;
            return  $user->user;
        }

    }

    public function forgot_password($email){
        $request = $this->restClient->post('auth/forgot_password',
                array(
                    'email'=>$email
                )
            );
        if(is_object($request) && isset($request->status)){
            if($request->status == -1){
                return -1;
            }elseif($request->status == 1){
                $user = json_decode($request->user);
                return $user;
            }
        }
    }

    public function forgot_code_validate($id,$code,$hours=24){
        $request = $this->restClient->post('auth/forgotten_password',
            array(
                'user_id'=>$id,
                'code'=>$code
            )
        );

        if(is_object($request) && isset($request->user)){
            if( $request->user->forgotten_password_time + ($hours * 3600) >= time() ){
                return true;
            }
        }
    }

    public function new_password($id,$password){
        $request = $this->restClient->post('auth/new_password',
            array(
                'id'=>$id,
                'password'=>$password
            )
        );

        if(is_object($request) && isset($request->message) && $request->message == 'success'){
           return true;
        }
    }

    public function generate_confirmation_code($id){
        $request = $this->restClient->get('auth/generate_confirmation_code',
            array(
                'user_id'=>$id
            )
        );
        if(is_object($request) && isset($request->code) && $request->code == 200){
            return $request->message;
        }
    }

    public function validate_activation_token($id,$code){
        $request = $this->restClient->get('auth/validate_confirmation_code',
            array(
                'user_id'=>$id,
                'code'=>$code
            )
        );
        if(is_object($request) && isset($request->code) && $request->code == 200){
            return $request->message;
        }
    }


    public function check_email_unique($page_id,$email){
        $request = $this->restClient->post('auth/check_email',
            array(
                'page_id'=>$page_id,
                'email'=>$email
            )
        );
        if(is_object($request) && isset($request->code) && $request->code == 200){
            return $request->message;
        }
    }

    public function update_user_profile($user_id,$lang,$data){
        $post_data = array(
            'id'=>$user_id,
            'lang'=>$lang,
            'gender'=>$data['gender'],
            'birth_date'=>strtotime(str_replace('/','-',$data['birthdate'])),
            'firstname'=>$data['firstname'],
            'lastname'=>$data['lastname'],
        );
        if(isset($data['email'])){
            $post_data['email'] = $data['email'];
        }
        //Helpers::die_pre($post_data);
        $request = $this->restClient->post('users/update_user_profile',
            $post_data
        );

        if(is_object($request) ){
            return $request;
        }
    }

    public function update_password($email,$old_password=null,$password,$user_id,$social=null){
        $post_data = [
            'email'=>$email,
            'new_password'=>$password,
            'old_password'=>$old_password,
            'user_id'=>$user_id,
            'create_new' => $social
        ];

        $request = $this->restClient->post('auth/change_password',
            $post_data
        );
        if(is_object($request) && isset($request->code) && $request->code == 200){
            return true;
        }
    }

    public function getSocialUser($social_id,$page_id,$social_field){

        $post_data = array(
            'social_id'=>$social_id,
            'page_id'=>$page_id,
            'social_field'=>$social_field,
        );

        $request = $this->restClient->get('auth/getSocialUser',
            $post_data
        );

        if(is_object($request) && isset($request->code) && $request->code == 200 ){
            return $request->data;
        }
    }

    public function connectSocialUser($user_id,$social_id,$social_field){

        $post_data = array(
            'user_id'=>$user_id,
            'social_id'=>$social_id,
            'social_field'=>$social_field,
        );

        $request = $this->restClient->get('auth/connectSocialUser',
            $post_data
        );

        if(is_object($request) && isset($request->code) && $request->code == 200 ){
            return $request->data;
        }
    }
}