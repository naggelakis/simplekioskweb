<?php
namespace WebKiosk\ApiModels;
use WebKiosk\Custom\RestClient;
use Helpers;
use Illuminate\Support\Facades\Config;

class Articles {
    private $restClient;
    private $page_id;

    public function __construct(){
        $this->restClient = new RestClient();
        $this->page_id = defined('LOGGIA_PAGE') ? LOGGIA_PAGE : Config::get('loggia.page_id');
    }

    public function getLatestPosts($lang='EL',$limit=15,$offset=0,$cache=false,$type=182,$sort=null,$author=null,$disable_type=null,$favorites=null){
        $data = array(
            'lang' => $lang,
            'page_id'=>$this->page_id,
            'type'=>$type,
            'limit'=>$limit,
            'offset'=>$offset
        );
        if($sort!=null){
            $data['sort'] = $sort;
        }
        if($disable_type!=null){
            $data['disable_type'] = 1;
        }
        if($author!=null){
            $data['author'] = $author;
        }
        if($favorites!=null){
            $data['favorites'] = $favorites;
        }
        $posts = $this->restClient->get('articles/articles_list',
            $data
            ,
            $cache
        );
       //Helpers::die_pre($this->restClient->last_get);
        if(is_array($posts) && count($posts)>0){
            return $posts;
        }
    }

    public function getLatestPostsWithCustomGalleries($lang='EL',$limit=3,$offset=0,$cache=false,$type=null){
        $data = array(
            'lang' => $lang,
            'page_id'=>$this->page_id,
            'disable_type'=>1,
            'has_custom_gallery'=>1,
            'limit'=>$limit,
            'offset'=>$offset,

        );
        if($type!=null){
            $data['type'] = $type;
        }
        $posts = $this->restClient->get('articles/articles_list',
            $data,
            $cache
        );

        if(is_array($posts) && count($posts)>0){
            return $posts;
        }
    }

    public function getLatestPostsGalleries($lang='EL',$limit=3,$offset=0,$cache=false,$type=null,$article_type=null){
        $data = array(
            'lang' => $lang,
            'page_id'=>$this->page_id,
            'disable_type'=>1,
            'limit'=>$limit,
            'offset'=>$offset,

        );
        if($type!=null){
            $data['type'] = $type;
        }
        if($article_type!=null){
            $data['article_type'] = $article_type;
        }
        $posts = $this->restClient->get('articles/articles_list',
            $data,
            $cache
        );

        if(is_array($posts) && count($posts)>0){
            return $posts;
        }
    }


    public function getLatestPublications($lang='EL',$cache=false){
        $publications = $this->restClient->get('market/latest_publications',
            array(
                'lang' => $lang,
                'magazines_limit'=>3,
                'sort_magazines'=>1
            ),
            $cache
        );
        if(is_object($publications)){
            return $publications;
        }
    }

    public function getMonthLog($page_id,$type,$lang='EL',$cache=false){
        $log = $this->restClient->get('articles/getMonthLogCount',
            array(
                'page_id'=>$page_id,
                'system_cat_id'=>$type,
                'lang' => $lang
            ),
            $cache
        );
        if(is_object($log)){
            return $log;
        }
    }

    public function getAuthorsWithRecentPost($lang,$cache=false){
        $authors = $this->restClient->get('articles/authors_latest_entries',
            array(
                'limit'=>1,
                'lang' => $lang
            ),
            $cache
        );
        if(is_array($authors)){
            return $authors;
        }
    }


    public function getShowcasePosts($lang,$showcase_id,$limit=5,$offset=0,$cache=false){
        $posts = $this->restClient->get('articles/showcase_entries',
            array(
                'lang' => $lang,
                'limit'=>$limit,
                'id'=>$showcase_id,
                'show_locked'=>1
            ),
            $cache
        );

        if(is_array($posts) && count($posts)>0){
            return $posts;
        }
    }


    public function getShowcaseCategories($lang,$showcase_id,$limit=5,$offset=0,$cache=false){
        $cats = $this->restClient->get('articles/showcase_categories',
            array(
                'lang' => $lang,
                'limit'=>$limit,
                'offset'=>$offset,
                'id'=>$showcase_id,
                'show_locked'=>1
            ),
            $cache
        );

        if(is_array($cats) && count($cats)>0){
            return $cats;
        }
    }


    public function getCategoryDetails($lang,$cat_id,$cache=false){
         $category = $this->restClient->get('articles/category',
            array(
            'lang' => $lang,
            'id'=>$cat_id
            ),
            $cache
        );

        if(is_object($category)){
            return $category;
        }
    }


    public function getCategoryEntries($lang,$page_id,$cat_id,$type,$limit=5,$offset=0,$showcase_id=null,$order_by='DESC',$ids=null,$cache=false){
        $data = array(
            'lang' => $lang,
            'page_id'=>$page_id,
            'category_id'=>$cat_id,
            'id'=>$cat_id,
            'type'=>$type,
            'order_by'=>$order_by,
            'limit'=>$limit,
            'offset'=>$offset,
            'skip_permissions'=>1
        );

        if($showcase_id != null){
            $data['showcase_id'] = $showcase_id;
        }
        if($ids != null){
            $data['skip_ids'] = $ids;
        }
        $entries = $this->restClient->get('articles/category_articles_list',
            $data,
            $cache
        );
        //Helpers::die_pre($this->restClient->last_get);
        if(is_array($entries) && count($entries)>0){
            return $entries;
        }
    }


    public function getEntryDetails($lang,$entry_id,$user_id=null,$cache=false){
        $data = array(
            'lang' => $lang,
            'id'=>$entry_id
        );

        $entry = $this->restClient->get('articles/entry',
            $data,
            $cache
        );
        //Helpers::die_pre($this->restClient->last_get);
        if(is_object($entry) && isset($entry->entry->id)){
            return $entry;
        }
    }


    public function getEntryAdditional($lang,$entry_id,$cache=false){
        $data = array(
            'lang' => $lang,
            'id'=>$entry_id
        );

        $response = $this->restClient->get('articles/entry_additional',
            $data,
            $cache
        );
        if(is_object($response) && isset($response->views)){
            return $response;
        }
    }

    public function searchPosts($lang,$limit=15,$offset=0,$tag_id=null,$search=null,$from=null,$to=null,$only_news=0,$cache=false){
        $data = [
            'lang'=>$lang,
            'limit'=>$limit,
            'offset'=>$offset,
            'only_news'=>$only_news
        ];

        if($tag_id!=null && is_numeric($tag_id) && $tag_id > 0){
            $data['tag'] = $tag_id;
        }

        if($search!=null && $search!=''){
            $data['search'] = $search;
        }

        if($from!=null && $from!=0){
            $data['from'] = $from;
        }
        if($to!=null && $to!=''){
            $data['to'] = $to;
        }

        $response = $this->restClient->get('articles/search_tags',
            $data,
            $cache
        );
        //Helpers::die_pre($this->restClient->last_get);

        if($response && is_array($response) && count($response)>0 ){
            return $response;
        }

    }


    public function getAuthorProfile($lang,$author_id,$cache=false){
        $data = [
            'lang'=>$lang,
            'author_id'=>$author_id
        ];

        $response = $this->restClient->get('articles/getAuthorProfile',
            $data,
            $cache
        );


        if($response && is_object($response) ){
            return $response;
        }
    }

    public function favoritePost($user_id,$id){
        $data = [
            'user_id'=>$user_id,
            'post_id'=>$id
        ];

        $response = $this->restClient->get('articles/favoriteArticle',
            $data
        );


        if($response && is_object($response) && isset($response->code) && $response->code == 200 ){
            return $response->message;
        }
    }

    public function unfavoritePost($user_id,$id){
        $data = [
            'user_id'=>$user_id,
            'post_id'=>$id
        ];

        $response = $this->restClient->get('articles/unfavoriteArticle',
            $data
        );


        if($response && is_object($response) && isset($response->code) && $response->code == 200 ){
            return $response->message;
        }
    }
}