<?php

namespace WebKiosk\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\Lang;
use Helpers;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Blade::directive('break', function() {
            return "<?php break; ?>";
        });
        //
        Blade::directive('continue', function() {
            return "<?php continue; ?>";
        });

        Blade::directive('javascript',function($file){
            return "<script type='text/javascript' src='".preg_replace('/^\(|\)$/', '', $file)."' ></script>";
        });

        Blade::directive('css',function($file){
            return "<link rel='tylesheet href='".preg_replace('/^\(|\)$/', '', $file)."' >";
        });
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
