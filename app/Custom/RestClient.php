<?php
namespace WebKiosk\Custom;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Helpers;
use Cache;
use GuzzleHttp\Exception\RequestException;

class RestClient {

    private $client;
    private $api_key;
    private $format='json';
    private $host;
    private $cache_active = true;
    public $last_get;
    /**
     * Construct the client
     */
    public function __construct(){
        $this->api_key = defined('LOGGIA_API_KEY') ? LOGGIA_API_KEY : Config::get('loggia.api_key');
        $this->client = new Client([
            'timeout'  => 25.0,
        ]);
        $this->host = $_SERVER['HTTP_HOST'] == 'news.dev' ? 'http://loggia.dev/api/' : 'http://my.loggia.gr/api/';

    }

    /**
     * Check/set default values for request(api_key,format)
     * @param array $parameters
     * @return array
     */
    private function check_defaults($parameters = null){
        if(is_array($parameters)){
            if(!isset($parameters['api_key'])){
                $parameters['api_key'] = $this->api_key;
            }
            if(!isset($parameters['format'])){
                $parameters['format'] = $this->format;
            }
        }else{
            $parameters = array();
            $parameters['api_key'] = $this->api_key;
            $parameters['format'] = $this->format;

        }
        return $parameters;
    }

    /**
     * Execute a get request to remote server
     * @param $uri
     * @param null $parameters
     * @param bool $cache
     * @return mixed
     */
    public function get($uri,$parameters=null,$cache=false ){
        $parameters = $this->check_defaults($parameters);
        $url = $this->host.$uri.'?'.http_build_query($parameters);
        $this->last_get = $url;
        if($cache != false && $this->cache_active == true){
            if (Cache::has(md5($url))) {
                $response = Cache::get(md5($url));
            }
        }
        if(!isset($response)){
            try {
                $request = $this->client->get($url);
            } catch (RequestException $e) {
                return false;
            }
            $response = json_decode($request->getBody()->getContents());


            if($cache != false && $this->cache_active == true && isset($response)) {
                Cache::put(md5($url), $response, $cache);
            }
        }
        return $response;
    }


    /**
     * Execute a get request to remote server
     * @param $uri
     * @param null $parameters
     * @return mixed
     */
    public function post($uri,$parameters=null ){
        $parameters = $this->check_defaults($parameters);
        $url = $this->host.$uri.'?api_key='.$this->api_key;

        try {
            $request = $this->client->post($url,['form_params'=>$parameters]);
        } catch (RequestException $e) {
          //  Helpers::die_pre(  $e->getMessage() );
            return false;
        }
        $response = json_decode($request->getBody()->getContents());

        return $response;
    }
}