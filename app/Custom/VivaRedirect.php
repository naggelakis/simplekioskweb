<?php

namespace WebKiosk\Custom;

use Symfony\Component\Console\Helper\Helper;
use Illuminate\Support\Facades\Redirect;

class VivaRedirect
{

    protected  $request;
    protected  $MerchantId;
    protected  $amount;
    protected  $apikey;
    protected  $allowReccuring;
    protected  $requestLang;
    protected  $source;
    protected  $postargs;
    protected  $sandbox;
    protected  $endpoint;
    protected  $response;

    public function _register($merchant_id,$apikey,$sandbox=false){
        if($sandbox == true){
            $this->endpoint = 'http://demo.vivapayments.com';
        }else{
            $this->endpoint = 'https://www.vivapayments.com';
        }
        $this->MerchantId = $merchant_id;
        $this->apikey = $apikey;
        $this->allowReccuring; // This flag will prompt the customer to accept recurring payments in tbe future.
        $this->requestLang = 'en-US'; //This will display the payment page in English (default language is Greek)
        $this->source = 'Default';
    }

    public function create_order($amount,$source){
        $this->request = $this->endpoint.'/api/orders';
        $this->amount = $amount;
        $this->source = $source;
        $args = 'Amount='.urlencode($amount).'&AllowRecurring='.$this->allowReccuring.'&RequestLang='.$this->requestLang.'&SourceCode='.$this->source;
        $response = $this->_execute($args);
        if(is_object($response)){
            $this->response = $response;
        }else{
            $this->response =  $response;
        }
    }

    public function check_transaction($transaction_id){
        $this->request = $this->endpoint.'/api/transactions/'.$transaction_id;
        $response = $this->_execute();

        if(is_object($response)){
            $this->response = $response;
        }else{
            $this->response =  $response;
        }
        return $response;
    }
    public function getResponse(){
        return $this->response;
    }

    public function send_payment($order_id){

        if($order_id){
            $redirect = $this->endpoint.'/web/checkout?ref='.$order_id;
            return $redirect;
        }
    }



    protected function _execute($postargs=null)
    {
        $output = new \stdClass();

        // Get the curl session object
        $session = curl_init($this->request);
        // Set the POST options.
        if($postargs!=null) {
            curl_setopt($session, CURLOPT_POST, true);
            curl_setopt($session, CURLOPT_POSTFIELDS, $postargs);
        }
        curl_setopt($session, CURLOPT_HEADER, true);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_USERPWD, $this->MerchantId . ':' . $this->apikey);
        curl_setopt($session, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');

        // Do the POST and then close the session
        $response = curl_exec($session);

        // Separate Header from Body
        $header_len = curl_getinfo($session, CURLINFO_HEADER_SIZE);
        $resHeader = substr($response, 0, $header_len);
        $resBody = substr($response, $header_len);

        curl_close($session);

        // Parse the JSON response
        try {
            $resBody = json_decode($resBody);

            if (is_object($resBody)) {
                $resultObj = $resBody;
            } else {
                preg_match('#^HTTP/1.(?:0|1) [\d]{3} (.*)$#m', $resHeader, $match);
                $output->status = 'error';
                $output->message = "API Call failed! The error was: " . trim($match[1]);
            }

        } catch
        (Exception $e) {
            $output->status = 'error';
            $output->message = $e->getMessage();
        }


        if ($resultObj->ErrorCode == 0) {    //success when ErrorCode = 0
            if(isset($resultObj->OrderCode)) {

                $orderId = $resultObj->OrderCode;
                return $orderId;
            }else{
                return $resultObj;
            }
        } else {
            $output->status = 'error';
            $output->message = $resultObj->ErrorText;
        }

        return $output;
    }

}
?>