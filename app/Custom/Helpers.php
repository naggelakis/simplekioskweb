<?php
 namespace WebKiosk\Custom;
 use Jenssegers\Date\Date;
 use Illuminate\Support\Facades\Lang;
 use Illuminate\Support\Facades\Route;

 class Helpers {

     /**
      * Debugger
      * @param null $data
      */
     public static function die_pre($data=null){
         echo '<pre>';
         print_r($data);
         echo '</pre>';
         exit;
     }

     /**
      * Converts an object to an array
      * @param $var
      * @return array
      */
     public static function object_to_array($var) {
         $result = array();
         $references = array();
         // loop over elements/properties

         foreach ($var as $key => $value) {
             // recursively convert objects
             if (is_object($value) || is_array($value)) {
                 // but prevent cycles
                 if (!in_array($value,$references)) {
                     $result[$key] = Helpers::object_to_array($value);
                     $references[] = $value;
                 }
             } else {
                 // simple values are untouched
                 $result[$key] = $value;
             }
         }
         return $result;
     }

     /**
      * Check if user can view item
      * @param $user_groups
      * @param $user_profile
      * @return bool
      */
     public static function check_group_permission($user_groups,$user_profile){
         if($user_groups == null){
             return true;
         }else{
             $groups = explode(',',$user_groups);
             if(isset($user_profile->group) && $user_profile->group!=null  && in_array($user_profile->group,$groups)){
                 return true;
             }
         }
         return false;

     }

     /**
      * Get square image path
      * @param $file_path
      * @return string
      */
     public static function squareFilePath($file_path) {
         $parts=pathinfo($file_path);
         $image =  str_replace('%C6%B7','3',$parts["dirname"].'/'.$parts['filename'].'_square'.'.'.$parts["extension"]);
         $image =  str_replace('Ʒ','3',$image);
         return $image;

     }

     /**
      * Get portrait image path
      * @param $file_path
      * @return string
      */
     public static function portraitFilePath($file_path) {
         $parts=pathinfo($file_path);
         $image =  str_replace('%C6%B7','3',$parts["dirname"].'/'.$parts['filename'].'_portrait'.'.'.$parts["extension"]);
         $image =  str_replace('Ʒ','3',$image);
         return $image;
     }

     /**
      * Get square image name
      * @param $file_name
      * @return string
      */
     public static function squareFile($file_name) {
         $parts=pathinfo($file_name);
         return $parts['filename'].'_square'.'.'.$parts["extension"];
     }

     /**
      * Return the slug of given text
      * @param $text
      * @return mixed
      */
     public static function urlize($text) {
         $greek_chars = array(
         'α','ά','β','γ','δ','ε','έ','ζ','η','ή','θ','ι','ί','ϊ','κ','λ','μ','ν','ξ',
         'ο','ό','π','ρ','σ','τ','υ','ύ','ϋ','φ','χ','ψ','ω','ώ','ς','Α','Ά','Β','Γ',
         'Δ','Ε','Έ','Ζ','Η','Ή','Θ','Ι','Ί','Ϊ','Κ','Λ','Μ','Ν','Ξ','Ο','Ό','Π','Ρ',
         'Σ','Τ','Υ','Ύ','Ϋ','Φ','Χ','Ψ','Ω','Ώ',' ',"\t",'_','`','~','!','@','#','$','%',
         '^','&','*','(',')','=','+','[','{',']','}','\\','|',';',':','\'','"',',',
         '<','.','>','/','?'
         );

         $greek_chars_replace = array(
         'a','a','v','g','d','e','e','z','i','i','th','i','i','i','k','l','m','n','ks',
         'o','o','p','r','s','t','y','y','y','f','x','ps','w','w','s','A','A','V','G',
         'D','E','E','Z','H','H','TH','I','I','I','K','L','M','N','KS','O','O','P','R',
         'S','T','Y','Y','Y','F','X','PS','W','W','-','-'
         );

         $output = str_replace($greek_chars,$greek_chars_replace,$text);
         $output = preg_replace("/-{2,}/","-",$output);
         return strtolower($output);
     }

     /**
      * Remove tones of text
      * @param $text
      * @return mixed
      */
    public static function remove_tones($text) {
         $special = array('ά','Ά','έ','Έ','ή','Ή','ί','Ί','ΐ','΅Ι','ό','Ό','ύ','Ύ','ϋ','΅Υ','ώ',"¨Ω");
         $simple = array('α','Α','ε','Ε','η','Η','ι','Ι','ϊ','Ϊ','ο','Ο','υ','Υ','υ','Υ','ω',"Ω");
         $output = str_replace($special,$simple,$text);
         return $output;
     }

     public static function getRouteSingleBySystemCat($system_cat,$id,$slug=null){
          $route='#';
            if($system_cat == 167){
                $route = route('article_single',[$id,Helpers::urlize($slug)]);
            }elseif($system_cat == 168){
                $route = route('profile_single',[$id,Helpers::urlize($slug)]);
            }elseif($system_cat == 169){
                $route = route('terms_single',[$id,Helpers::urlize($slug)]);
            }elseif($system_cat == 170){
                $route = route('news_single',[$id,Helpers::urlize($slug)]);
            }elseif($system_cat == 172){
                $route = route('pages_single',[$id,Helpers::urlize($slug)]);
            }elseif($system_cat == 241){
                $route = route('pages_single',[$id,Helpers::urlize($slug)]);
            }
         return $route;
     }

     public static function getRouteCategoryBySystemCat($system_cat,$id,$slug=null){
         $route='#';
         if($system_cat == 167){
             $route = route('article_category',[$id,Helpers::urlize($slug)]);
         }elseif($system_cat == 168){
             $route = route('profile_category',[$id,Helpers::urlize($slug)]);
         }elseif($system_cat == 169){
             $route = route('terms_category',[$id,Helpers::urlize($slug)]);
         }elseif($system_cat == 170){
             $route = route('news_category',[$id,Helpers::urlize($slug)]);
         }elseif($system_cat == 172){
             $route = route('pages_category',[$id,Helpers::urlize($slug)]);
         }elseif($system_cat == 241){
             $route = route('pages_category',[$id,Helpers::urlize($slug)]);
         }
         return $route;
     }

     public static function localeDate($date_string,$format='F d, Y',$string_format='Y-M-d h:i:s'){
         Date::setLocale(Lang::getLocale());
         $date = new Date($date_string);
         return $date->format($format);
     }

     public static function get_snippet( $str, $wordCount = 8 ) {
         return implode(
             '',
             array_slice(
                 preg_split(
                     '/([\s,\.;\?\!]+)/',
                     $str,
                     $wordCount*2+1,
                     PREG_SPLIT_DELIM_CAPTURE
                 ),
                 0,
                 $wordCount*2-1
             )
         );
     }


     /**
      * @param $file_path
      * @param $extension
      * @return string
      */
     public static function extendedFilePath($file_path,$extension) {
         $parts=pathinfo($file_path);
         return $parts["dirname"].'/'.$parts['filename'].$extension.'.'.$parts["extension"];
     }

     /**
      * Find display place for banner
      * @param $banner
      * @return null|string
      */
     public static function findBannerPlace($banner){
         $place = null;
         if($banner->id == 1){
             $place = 'sidebar_top';
         }elseif($banner->id == 2){
             $place = 'list_top';
         }elseif($banner->id == 3){
             $place = 'sidebar_top';
         }elseif($banner->id == 4){
             $place = 'list_bottom';
         }elseif($banner->id == 5){
             $place = 'header_top';
         }elseif($banner->id == 8){
             $place = 'sidebar_mid';
         }elseif($banner->id == 10){
             $place = 'list_mid';
         }elseif($banner->id == 12){
             $place = 'list_bottom';
         }elseif($banner->id == 14){
             $place = 'header_top';
         }elseif($banner->id == 17){
             $place = 'sidebar_bottom';
         }elseif($banner->id == 18){
             $place = 'sidebar_bottom';
         }elseif($banner->id == 45){
             $place = 'list_top';
         }elseif($banner->id == 46){
             $place = 'list_mid';
         }elseif($banner->id == 47){
             $place = 'sidebar_mid';
         }elseif($banner->id == 50){
             $place = 'featured_long';
         }elseif($banner->id == 51){
             $place = 'featured_small';
         }elseif($banner->id == 52){
             $place = 'featured_long';
         }elseif($banner->id == 53){
             $place = 'featured_small';
         }

         return $place;
     }

     /**
      * Check if banner is valid for current route
      * @param $banner
      * @param $single_banner
      * @return bool
      */
     public static function checkBannerCategory($banner,$single_banner){
         $route = Route::currentRouteName();
         $params = Route::current()->parameters();
         if($banner->module == 2){
             if($single_banner->module_value == null || $single_banner->module_value == 0){
                 return true;
             }elseif($single_banner->module_value>0){
                 if($route == 'news_category' || $route=='profile_category' || $route=='article_category' || $route=='pages_category' || $route=='terms_category'){
                     if(isset($params['cat_id']) && $params['cat_id'] == $single_banner->module_value){
                         return true;
                     }elseif($single_banner->module_include_all == 1){
                         return 100;
                     }
                 }
             }elseif($single_banner->module_value == -1 && $route == 'homepage'){
                 return true;
             }
         }elseif($banner->module == 6){
             if($single_banner->module_value == null || $single_banner->module_value == 0){
                 return true;
             }elseif($single_banner->module_value>0){
                 if($route == 'market_category'){
                     if(isset($params['cat_id']) && $params['cat_id'] == $single_banner->module_value){
                         return true;
                     }elseif($single_banner->module_include_all == 1){
                         return 100;
                     }
                 }
             }elseif($single_banner->module_value == -1 && $route == 'homepage'){
                 return true;
             }
         }elseif($banner->module == null){
             return true;
         }
         return false;

     }

     /**
      * Insert an item in a spesific position of the array
      * @param $arr
      * @param $insert
      * @param $position
      * @return mixed
      */
     public static function array_insert($arr, $insert, $position) {
         $i = 0;
         foreach ($arr as $key => $value) {
             if ($i == $position) {
                 $current = $arr[$key];
                 $ret[$key] = $insert;
                 $ret[$key+1] = $current;
                 $i = $i+1;
                 continue;
             }else{
                 $ret[$key] = $value;
             }
             $i++;
         }
         return $ret;
     }


     public static function check_if_bought($authUser=null,$publication=null,$magazine=null,$system_type=null,$group_id=null){
         if($authUser == null || $publication == null || $system_type == 73) { return false; }

         if(is_object($authUser) && isset($authUser->issues) && isset($publication->id) && isset($authUser->issues->{$system_type}->{$publication->id})){
             return true;
         }

         if(is_object($authUser) && isset($authUser->magazine_subscriptions) && isset($magazine->id) && isset($authUser->magazine_subscriptions->{$system_type}->{$magazine->id})){
             return true;
         }

         if($system_type == 74){
             if(is_object($authUser) && isset($authUser->magazine_subscriptions) && isset($magazine->id) && isset($authUser->magazine_subscriptions->{72}->{$magazine->id})){
                 return true;
             }
         }

         return false;
     }

 }