var app = angular.module('testApp', [ 'ngPDFViewer','ngAnimate', 'ngTouch','ngSanitize' ]);

app.controller('TestController', [ '$scope', 'PDFViewerService','$sce', function($scope, pdf,$sce) {
	console.log('TestController: new instance');
    $scope.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
    };

    $scope.id;
    $scope.lang;
    $scope.loaded = 0;

    $scope.initUrl = function() {
        $scope.pdfURL = '/'+$scope.lang+'/publications/view/pdf/'+$scope.id;
    };
	$scope.scale = 1;

	$scope.pages = 0;

	$scope.instance = pdf.Instance("viewer");

	$scope.nextPage = function() {
		$scope.instance.nextPage();
	};

	$scope.prevPage = function() {
		$scope.instance.prevPage();
	};

	$scope.gotoPage = function(page) {
		$scope.instance.gotoPage(page);
	};

	$scope.setScale = function(v) {
		$scope.instance.setScale(v);
	};

	$scope.pageLoaded = function(curPage, totalPages) {
		$scope.currentPage = curPage;
		$scope.totalPages = totalPages;
	};
	
	$scope.callFunction = function(eventNew) {
 	 	if (eventNew.which==39){
  	  		$scope.nextPage();
  	  	}else if (eventNew.which==37){
		  	$scope.prevPage();
  	  	}else if (eventNew.which==38){
	  	  	$scope.nextPage();
  	  	}else if (eventNew.which==40){
  	  		$scope.prevPage();
  	  	}
	};

	$scope.loadProgress = function(loaded, total, state) {
        $scope.loaded = 1;
		console.log('loaded =', loaded, 'total =', total, 'state =', state);
	};
}]);
