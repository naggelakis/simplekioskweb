

var myApp = angular.module('mainApp',[]);
myApp.filter('htmlToPlaintext', function() {
        return function(text) {
            return angular.element(text).text();
        }
    }
);
myApp.directive('img', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            // show an image-missing image
            element.error(function () {
                var w = element.width();
                var h = element.height();
                // using 20 here because it seems even a missing image will have ~18px width
                // after this error function has been called

                var url = element.attr('src');
                url = url.replace(new RegExp('Ʒ', 'g'),'3');
                url = url.replace(new RegExp('%C6%B7', 'g'),'3');
                element.prop('src', url);
            });
        }
    }
});
myApp.filter('chunk', function() {
    var cache = {}; // holds old arrays for difference repeat scopes
    var filter = function(newArr, size, scope) {
        var i,
            oldLength = 0,
            newLength = 0,
            arr = [],
            id = scope.$id,
            currentArr = cache[id];
        if (!newArr) return;

        if (currentArr) {
            for (i = 0; i < currentArr.length; i++) {
                oldLength += currentArr[i].length;
            }
        }
        if (newArr.length == oldLength) {
            return currentArr; // so we keep the old object and prevent rebuild (it blurs inputs)
        } else {
            for (i = 0; i < newArr.length; i += size) {
                arr.push(newArr.slice(i, i + size));
            }
            cache[id] = arr;
            return arr;
        }
    };
    return filter;

});

myApp.service('endpoints', function($http,$q){
    return {

        getLatestPosts: function(lang,limit,offset) {

            var promise = $http.get('/'+lang+'/endpoint/news/latest/'+limit+'/'+offset).then(function (response) {
                return response.data;
            });
            return promise;
        },
        getLatestPublications: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/latest_publications').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getMonthLog: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/month_log').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getSidebarTabs: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/recent_homepage_tabs').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getAuthors: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/authors_showcase').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getShowcaseEntries: function(lang,id,limit,offset) {
            var promise = $http.get('/'+lang+'/endpoint/news/showcase_entries/'+id+'/'+limit+'/'+offset).then(function (response) {
                return response.data;
            });
            return promise;
        },
        getShowcaseCategories: function(lang,id,limit,offset) {
            var promise = $http.get('/'+lang+'/endpoint/news/showcase_categories/'+id+'/'+limit+'/'+offset).then(function (response) {
                return response.data;
            });
            return promise;
        }

    };
});

myApp.controller(
    'sidebarCtrl',
    function($scope, endpoints){


        $scope.limit = 15; // 500;
        $scope.offset = 0;
        $scope.lang;
        $scope.month_log = [];
        $scope.comment_posts = [];
        $scope.gallery_posts = [];



        $scope.log_month_fetch = function(){
            endpoints.getMonthLog($scope.lang).then(function(d) {
                if (d.length > 0){
                    for(var i=0; i< d.length;i++){
                        $scope.month_log.push(d[i]);
                    }
                }
            });
        };

        $scope.sidebar_tabs = function(){
            endpoints.getSidebarTabs($scope.lang).then(function(data) {
                if (data.comments && data.comments.length > 0){
                    for(var i=0; i< data.comments.length;i++){
                        $scope.comment_posts.push(data.comments[i]);
                    }
                }
                if (data.galleries && data.galleries.length > 0){
                    for(var i=0; i< data.galleries.length;i++){
                        $scope.gallery_posts.push(data.galleries[i]);
                    }
                }
            });
        };



    }

);
