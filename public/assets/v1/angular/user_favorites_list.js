/**
 * Created by naggelakis on 26/8/15.
 */


var myApp = angular.module('mainApp',[]);
myApp.filter('htmlToPlaintext', function() {
        return function(text) {
            return angular.element(text).text();
        }
    }
);

myApp.filter('chunk', function() {
    var cache = {}; // holds old arrays for difference repeat scopes
    var filter = function(newArr, size, scope) {
        var i,
            oldLength = 0,
            newLength = 0,
            arr = [],
            id = scope.$id,
            currentArr = cache[id];
        if (!newArr) return;

        if (currentArr) {
            for (i = 0; i < currentArr.length; i++) {
                oldLength += currentArr[i].length;
            }
        }
        if (newArr.length == oldLength) {
            return currentArr; // so we keep the old object and prevent rebuild (it blurs inputs)
        } else {
            for (i = 0; i < newArr.length; i += size) {
                arr.push(newArr.slice(i, i + size));
            }
            cache[id] = arr;
            return arr;
        }
    };
    return filter;

});
myApp.directive('img', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            // show an image-missing image
            element.error(function () {
                var w = element.width();
                var h = element.height();
                // using 20 here because it seems even a missing image will have ~18px width
                // after this error function has been called

                var url = element.attr('src');
                url = url.replace(new RegExp('Ʒ', 'g'),'3');
                url = url.replace(new RegExp('%C6%B7', 'g'),'3');
                element.prop('src', url);
            });
        }
    }
});
myApp.service('endpoints', function($http,$q){
    return {


        favoritePost: function(lang,id,action){
            var promise = $http.get('/'+lang+'/endpoint/favorites/post/'+id+'/'+action).then(function (response) {
                return response.data;
            });
            return promise;
        },
        fetchFavorites: function(lang){
            var promise = $http.get('/'+lang+'/endpoint/favorites/fetch/posts').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getPosts: function(lang,limit,offset) {

            var promise = $http.get('/'+lang+'/endpoint/favorites/fetch/posts/list/'+limit+'/'+offset).then(function (response) {
                return response.data;
            });
            return promise;
        }

    };
});

myApp.controller(
    'userFavoritesListController',
    function($scope, endpoints){

        $scope.limit = 15; // 500;
        $scope.offset = 0;
        $scope.lang;
        $scope.spinner = 0;
        $scope.show_load_more = 1;
        $scope.posts = [];
        $scope.favorite_posts = [];
        $scope.running = 0;

        $scope.favorite_posts_fetch = function(){
            endpoints.fetchFavorites($scope.lang).then(function(data){
                if(data.length > 0){
                    for(var i=0; i<data.length; i++){
                        $scope.favorite_posts.push(data[i]);
                    }
                }
            });
        };

        $scope.favorite_post = function(id){
            if($scope.running == 0) {

                for (var i = 0; i < $scope.posts.length; i++) {
                    var post = $scope.posts[i];
                    if (post.id == id) {
                        $scope.posts.splice(i, 1);
                    }
                }

                $scope.running = 1;
                if ($scope.favorite_posts.indexOf(id) > -1) {
                    var action = 'unlike';
                } else {
                    var action = 'like';
                }

                endpoints.favoritePost($scope.lang, id, action).then(function (data) {
                    if (action == 'like') {
                        $scope.favorite_posts.push(id);
                    } else if (action == 'unlike') {
                        $scope.favorite_posts.splice($scope.favorite_posts.indexOf(id), 1);
                    }
                    $scope.running = 0;
                });
            }
        };

        $scope.favorite_posts_fetch();

        $scope.posts_fetch = function(){

            $scope.spinner = 1;

            endpoints.getPosts($scope.lang,$scope.limit,$scope.offset).then(function(d) {

                if (d.length > 0){
                    for(var i=0; i< d.length;i++){
                        $scope.posts.push(d[i]);
                    }
                    $scope.offset += d.length;

                }else{
                    $scope.show_load_more = 0;
                }
                $scope.spinner = 0;
                console.log($scope.posts);
            });
        };




    }

);
