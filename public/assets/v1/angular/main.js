

var myApp = angular.module('mainApp',[]);
myApp.filter('htmlToPlaintext', function() {
        return function(text) {
            return angular.element(text).text();
        }
    }
);
myApp.directive('img', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            // show an image-missing image
            element.error(function () {
                var w = element.width();
                var h = element.height();
                // using 20 here because it seems even a missing image will have ~18px width
                // after this error function has been called

                var url = element.attr('src');
                url = url.replace(new RegExp('Ʒ', 'g'),'3');
                url = url.replace(new RegExp('%C6%B7', 'g'),'3');
                element.prop('src', url);
            });
        }
    }
});

myApp.filter('chunk', function() {
    var cache = {}; // holds old arrays for difference repeat scopes
    var filter = function(newArr, size, scope) {
        var i,
            oldLength = 0,
            newLength = 0,
            arr = [],
            id = scope.$id,
            currentArr = cache[id];
        if (!newArr) return;

        if (currentArr) {
            for (i = 0; i < currentArr.length; i++) {
                oldLength += currentArr[i].length;
            }
        }
        if (newArr.length == oldLength) {
            return currentArr; // so we keep the old object and prevent rebuild (it blurs inputs)
        } else {
            for (i = 0; i < newArr.length; i += size) {
                arr.push(newArr.slice(i, i + size));
            }
            cache[id] = arr;
            return arr;
        }
    };
    return filter;

});

myApp.service('endpoints', function($http,$q){
    return {

        getLatestPosts: function(lang,limit,offset) {

            var promise = $http.get('/'+lang+'/endpoint/news/latest/'+limit+'/'+offset).then(function (response) {
                return response.data;
            });
            return promise;
        },
        getLatestPublications: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/latest_publications').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getMonthLog: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/month_log').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getSidebarTabs: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/recent_homepage_tabs').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getAuthors: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/authors_showcase').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getShowcaseEntries: function(lang,id,limit,offset) {
            var promise = $http.get('/'+lang+'/endpoint/news/showcase_entries/'+id+'/'+limit+'/'+offset).then(function (response) {
                return response.data;
            });
            return promise;
        },
        getShowcaseCategories: function(lang,id,limit,offset) {
           // alert('/'+lang+'/endpoint/news/showcase_categories/'+id+'/'+limit+'/'+offset);
            var promise = $http.get('/'+lang+'/endpoint/news/showcase_categories/'+id+'/'+limit+'/'+offset).then(function (response) {
                return response.data;
            });
            return promise;
        },
        favoritePost: function(lang,id,action){
            var promise = $http.get('/'+lang+'/endpoint/favorites/post/'+id+'/'+action).then(function (response) {
                return response.data;
            });
            return promise;
        },
        fetchFavorites: function(lang){
            var promise = $http.get('/'+lang+'/endpoint/favorites/fetch/posts').then(function (response) {
                return response.data;
            });
            return promise;
        }

    };
});

myApp.controller(
    'HomepageCtrl',
    function($scope, endpoints,$timeout){

        $scope.limit = 10; // 500;
        $scope.offset = 0;
        $scope.lang;
        $scope.spinner = 0;
        $scope.show_load_more = 1;
        $scope.latest_posts = [];
        $scope.latest_issues = [];
        $scope.latest_magazines = [];
        $scope.authors = [];
        $scope.slider_posts = [];
        $scope.featured_posts = [];
        $scope.showcase_categories = [];
        $scope.latest_books = [];
        $scope.favorite_posts = [];


        $scope.favorite_posts_fetch = function(){
            endpoints.fetchFavorites($scope.lang).then(function(data){
                if(data.length > 0){
                    for(var i=0; i<data.length; i++){
                        $scope.favorite_posts.push(data[i]);
                    }
                    console.log($scope.favorite_posts);
                }
            });
        };

        $scope.favorite_post = function(id){

            if($scope.favorite_posts.indexOf(id) > -1){
                var action = 'unlike';
            }else{
                var action = 'like';
            }

            endpoints.favoritePost($scope.lang,id,action).then(function(data){
                console.log(data);
                if(action == 'like'){
                    $scope.favorite_posts.push(id);
                }else if(action == 'unlike'){
                    $scope.favorite_posts.splice($scope.favorite_posts.indexOf(id),1);
                }
            });
        };

        $scope.latest_posts_fetch = function(){
            $scope.spinner = 1;
            endpoints.getLatestPosts($scope.lang,$scope.limit,$scope.offset).then(function(d) {
                if (d.length > 0){
                    for(var i=0; i< d.length;i++){
                        $scope.latest_posts.push(d[i]);
                    }
                    $scope.offset += d.length;

                }else{
                    $scope.show_load_more = 0;
                }
                $scope.spinner = 0;
            });
        };

        $scope.latest_publications_fetch = function(){

            endpoints.getLatestPublications($scope.lang).then(function(data) {
                if ( data.issues || data.magazines){

                    if(data.issues && data.issues.length > 0){
                        for(var i = 0 ; i< data.issues.length; i++)
                        $scope.latest_issues.push(data.issues[i]);
                    }
                    console.log($scope.latest_issues);
                    if(data.magazines && data.magazines.length > 0){
                        for(var i = 0 ; i< data.magazines.length; i++)
                            $scope.latest_magazines.push(data.magazines[i]);
                    }

                    if(data.books && data.books.length > 0){
                        for(var i = 0 ; i< data.books.length; i++)
                            $scope.latest_books.push(data.books[i]);
                    }
                }
            });
        };



        $scope.fetch_authors = function(){
            endpoints.getAuthors($scope.lang).then(function(data) {
                if (data.length > 0){
                    for(var i=0; i< data.length;i++){
                        $scope.authors.push(data[i]);
                    }
                }
            });
        };




        $scope.fetch_slider = function(){
            endpoints.getShowcaseEntries($scope.lang,1,7,0).then(function(data) {
                if (data.length > 0){
                    for(var i=0; i< data.length;i++){
                        $scope.slider_posts.push(data[i]);
                        $timeout(function(){
                            $(".gallery-single").flexslider({smoothHeight : true, controlNav: "thumbnails"});
                        },100);
                    }
                }
            });
        };

        $scope.fetch_featured = function(){
            endpoints.getShowcaseEntries($scope.lang,2,12,0).then(function(data) {
                if (data.length > 0){
                    for(var i=0; i< data.length;i++){
                        $scope.featured_posts.push(data[i]);
                    }
                }
            });
        };

        $scope.fetch_showcase_categories = function(){
            endpoints.getShowcaseCategories($scope.lang,33,8,0).then(function(data) {
                console.log(data);
                if (data.length > 0){
                    for(var i=0; i< data.length;i++){
                        $scope.showcase_categories.push(data[i]);
                    }
                }
            });
        };

        $scope.favorite_posts_fetch();

    }

);
