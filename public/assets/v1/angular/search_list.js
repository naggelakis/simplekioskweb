/**
 * Created by naggelakis on 26/8/15.
 */


var myApp = angular.module('mainApp',[]);
myApp.filter('htmlToPlaintext', function() {
        return function(text) {
            return angular.element(text).text();
        }
    }
);

myApp.directive('img', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            // show an image-missing image
            element.error(function () {
                var w = element.width();
                var h = element.height();
                // using 20 here because it seems even a missing image will have ~18px width
                // after this error function has been called

                var url = element.attr('src');
                url = url.replace(new RegExp('Ʒ', 'g'),'3');
                url = url.replace(new RegExp('%C6%B7', 'g'),'3');
                element.prop('src', url);
            });
        }
    }
});

myApp.filter('chunk', function() {
    var cache = {}; // holds old arrays for difference repeat scopes
    var filter = function(newArr, size, scope) {
        var i,
            oldLength = 0,
            newLength = 0,
            arr = [],
            id = scope.$id,
            currentArr = cache[id];
        if (!newArr) return;

        if (currentArr) {
            for (i = 0; i < currentArr.length; i++) {
                oldLength += currentArr[i].length;
            }
        }
        if (newArr.length == oldLength) {
            return currentArr; // so we keep the old object and prevent rebuild (it blurs inputs)
        } else {
            for (i = 0; i < newArr.length; i += size) {
                arr.push(newArr.slice(i, i + size));
            }
            cache[id] = arr;
            return arr;
        }
    };
    return filter;

});

myApp.service('endpoints', function($http,$q){
    return {

        searchPosts: function(lang,only_news,limit,offset,tag_id,search,from,to) {
            if(search == '' || search == null){
                search = 'undefined';
            }
            if(from == '' || from == null){
                from = 0;
            }else{
                from = from.split('/');
                from = from.join('-');
            }


            if(to == '' || to == null){
                to = 0;
            }else{
                to = to.split('/');
                to = to.join('-');
            }
            var promise = $http.get('/'+lang+'/endpoint/news/tag_search/'+limit+'/'+offset+'/'+only_news+'/'+tag_id+'/'+search+'/'+from+'/'+to).then(function (response) {

                return response.data;
            });
            return promise;
        }
        ,
        favoritePost: function(lang,id,action){
            var promise = $http.get('/'+lang+'/endpoint/favorites/post/'+id+'/'+action).then(function (response) {
                return response.data;
            });
            return promise;
        },
        fetchFavorites: function(lang){
            var promise = $http.get('/'+lang+'/endpoint/favorites/fetch/posts').then(function (response) {
                return response.data;
            });
            return promise;
        }

    };
});

myApp.controller(
    'searchListController',
    function($scope, endpoints){

        $scope.limit = 15; // 500;
        $scope.offset = 0;
        $scope.lang;
        $scope.spinner = 0;
        $scope.show_load_more = 1;
        $scope.posts = [];
        $scope.search = '';
        $scope.tag_id=0;
        $scope.from;
        $scope.to;
        $scope.only_news=0;

        $scope.favorite_posts = [];


        $scope.favorite_posts_fetch = function(){
            endpoints.fetchFavorites($scope.lang).then(function(data){
                if(data.length > 0){
                    for(var i=0; i<data.length; i++){
                        $scope.favorite_posts.push(data[i]);
                    }
                }
            });
        };

        $scope.favorite_post = function(id){

            if($scope.favorite_posts.indexOf(id) > -1){
                var action = 'unlike';
            }else{
                var action = 'like';
            }

            endpoints.favoritePost($scope.lang,id,action).then(function(data){
                if(action == 'like'){
                    $scope.favorite_posts.push(id);
                }else if(action == 'unlike'){
                    $scope.favorite_posts.splice($scope.favorite_posts.indexOf(id),1);
                }
            });
        };

        $scope.favorite_posts_fetch();

        $scope.posts_fetch = function(){

           console.log($scope.from);
           console.log($scope.to);
           $scope.spinner = 1;

            endpoints.searchPosts($scope.lang,$scope.only_news,$scope.limit,$scope.offset,$scope.tag_id,$scope.search,$scope.from,$scope.to).then(function(d) {

                if (d.length > 0){
                    for(var i=0; i< d.length;i++){
                        $scope.posts.push(d[i]);
                    }
                    $scope.offset += d.length;

                }else{
                    $scope.show_load_more = 0;
                }
                $scope.spinner = 0;
            });
        };

        $scope.submit_form = function(){
            $scope.posts = [];
            $scope.offset = 0;
            $scope.posts_fetch();
        }


    }

);
