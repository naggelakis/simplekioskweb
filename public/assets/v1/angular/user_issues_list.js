/**
 * Created by naggelakis on 26/8/15.
 */


var myApp = angular.module('mainApp',[]);
myApp.filter('htmlToPlaintext', function() {
        return function(text) {
            return angular.element(text).text();
        }
    }
);
myApp.directive('img', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            // show an image-missing image
            element.error(function () {
                var w = element.width();
                var h = element.height();
                // using 20 here because it seems even a missing image will have ~18px width
                // after this error function has been called

                var url = element.attr('src');
                url = url.replace(new RegExp('Ʒ', 'g'),'3');
                url = url.replace(new RegExp('%C6%B7', 'g'),'3');
                element.prop('src', url);
            });
        }
    }
});
myApp.filter('chunk', function() {
    var cache = {}; // holds old arrays for difference repeat scopes
    var filter = function(newArr, size, scope) {
        var i,
            oldLength = 0,
            newLength = 0,
            arr = [],
            id = scope.$id,
            currentArr = cache[id];
        if (!newArr) return;

        if (currentArr) {
            for (i = 0; i < currentArr.length; i++) {
                oldLength += currentArr[i].length;
            }
        }
        if (newArr.length == oldLength) {
            return currentArr; // so we keep the old object and prevent rebuild (it blurs inputs)
        } else {
            for (i = 0; i < newArr.length; i += size) {
                arr.push(newArr.slice(i, i + size));
            }
            cache[id] = arr;
            return arr;
        }
    };
    return filter;

});

myApp.service('endpoints', function($http,$q){
    return {



        getIssues: function(lang,limit,offset,sort) {
            //console.log('/'+lang+'/endpoint/issues/fetch/list/'+limit+'/'+offset+'/'+sort);
            var promise = $http.get('/'+lang+'/endpoint/issues/fetch/list/'+limit+'/'+offset+'/'+sort).then(function (response) {
                return response.data;
            });
            return promise;
        },
        inAppPurchaseClick: function(lang) {
            //console.log('/'+lang+'/endpoint/issues/fetch/list/'+limit+'/'+offset+'/'+sort);
            var promise = $http.post('/'+lang+'/endpoint/inAppPurchaseClick').then(function (response) {
                return response.data;
            });
            return promise;
        }

    };
});

myApp.controller(
    'userIssuesListController',
    function($scope, endpoints){

        $scope.limit = 12; // 500;
        $scope.offset = 0;
        $scope.lang;
        $scope.spinner = 0;
        $scope.show_load_more = 1;
        $scope.posts = [];
        $scope.running = 0;
        $scope.sort = 0;
        $scope.message_hide = 0;

        $scope.sort_fetch = function(){
            $scope.offset = 0;
            $scope.posts.length = 0;
            $scope.fetchIssues();
        };

        $scope.fetchIssues = function(){

            $scope.spinner = 1;

            endpoints.getIssues($scope.lang,$scope.limit,$scope.offset,$scope.sort).then(function(d) {

                if (d.length > 0){
                    for(var i=0; i< d.length;i++){
                        $scope.posts.push(d[i]);
                    }
                    $scope.offset += d.length;

                }else{
                    $scope.show_load_more = 0;
                }
                $scope.spinner = 0;
                console.log($scope.posts);
            });
        };

        $scope.hideMessage = function(){
            $scope.message_hide = 1;
            endpoints.inAppPurchaseClick($scope.lang).then(function(){

            });
        };


    }

);
