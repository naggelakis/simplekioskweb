
myApp.service('apis', function($http,$q){
    return {

        getCategories: function(lang,limit) {
            var promise = $http.get('/'+lang+'/endpoint/market/categories/fetch').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getLatestMagazines: function(lang,limit) {
            var promise = $http.get('/'+lang+'/endpoint/fetch/latest_magazines/'+limit).then(function (response) {
                return response.data;
            });
            return promise;
        },
        getLatestIssues: function(lang,limit) {
            var promise = $http.get('/'+lang+'/endpoint/fetch/latest_issues/'+limit).then(function (response) {
                return response.data;
            });
            return promise;
        },
        getFeaturedPublications: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/fetch/featured_publications').then(function (response) {
                return response.data;
            });
            return promise;
        }

    };
});
myApp.directive('img', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            // show an image-missing image
            element.error(function () {
                var w = element.width();
                var h = element.height();
                // using 20 here because it seems even a missing image will have ~18px width
                // after this error function has been called

                var url = element.attr('src');
                url = url.replace(new RegExp('Ʒ', 'g'),'3');
                url = url.replace(new RegExp('%C6%B7', 'g'),'3');
                element.prop('src', url);
            });
        }
    }
});
myApp.controller(
    'sidebarCtrl',
    function($scope, apis){

        $scope.limit = 15; // 500;
        $scope.offset = 0;
        $scope.lang;
        $scope.categories =[];
        $scope.latest_magazines = [];
        $scope.latest_issues = [];
        $scope.latest_featured = [];

        $scope.setVars = function(lang){
          $scope.lang = lang;
          $scope.fetch_categories();
          $scope.fetch_magazines();
          $scope.fetch_issues();
          $scope.fetch_featured();
        };

        $scope.fetch_categories = function(){
            apis.getCategories($scope.lang,100).then(function(data){
                if (data.length > 0){
                    for(var i=0; i< data.length;i++){
                        $scope.categories.push(data[i]);
                    }
                }
            });
        };

        $scope.fetch_magazines = function(){
            apis.getLatestMagazines($scope.lang,3).then(function(data){
                if (data.length > 0){
                    for(var i=0; i< data.length;i++){
                        $scope.latest_magazines.push(data[i]);
                    }
                }
            });
        };

        $scope.fetch_issues = function(){
            apis.getLatestIssues($scope.lang,3).then(function(data){
                if (data.length > 0){
                    for(var i=0; i< data.length;i++){
                        $scope.latest_issues.push(data[i]);
                    }
                }
            });
        };

        $scope.fetch_featured = function(){
            apis.getFeaturedPublications($scope.lang).then(function(data){
                if (data.length > 0){
                    for(var i=0; i< data.length;i++){
                        $scope.latest_featured.push(data[i]);
                    }
                }
            });
        };



    }

);
