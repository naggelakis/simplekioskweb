myApp.directive('img', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            // show an image-missing image
            element.error(function () {
                var w = element.width();
                var h = element.height();
                // using 20 here because it seems even a missing image will have ~18px width
                // after this error function has been called

                var url = element.attr('src');
                url = url.replace(new RegExp('Ʒ', 'g'),'3');
                url = url.replace(new RegExp('%C6%B7', 'g'),'3');
                element.prop('src', url);
            });
        }
    }
});
myApp.service('apis', function($http,$q){
    return {

        getLatestPosts: function(lang,limit,offset) {

            var promise = $http.get('/'+lang+'/endpoint/news/latest/'+limit+'/'+offset).then(function (response) {
                return response.data;
            });
            return promise;
        },
        getLatestPublications: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/latest_publications').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getMonthLog: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/month_log').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getSidebarTabs: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/recent_homepage_tabs').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getAuthors: function(lang) {
            var promise = $http.get('/'+lang+'/endpoint/news/authors_showcase').then(function (response) {
                return response.data;
            });
            return promise;
        },
        getShowcaseEntries: function(lang,id,limit,offset) {
            var promise = $http.get('/'+lang+'/endpoint/news/showcase_entries/'+id+'/'+limit+'/'+offset).then(function (response) {
                return response.data;
            });
            return promise;
        },
        getShowcaseCategories: function(lang,id,limit,offset) {
            var promise = $http.get('/'+lang+'/endpoint/news/showcase_categories/'+id+'/'+limit+'/'+offset).then(function (response) {
                return response.data;
            });
            return promise;
        }

    };
});

myApp.controller(
    'sidebarCtrl',
    function($scope, apis){

        $scope.limit = 15; // 500;
        $scope.offset = 0;
        $scope.lang;
        $scope.month_log = [];
        $scope.comment_posts = [];
        $scope.gallery_posts = [];


        $scope.log_month_fetch = function(){

            apis.getMonthLog($scope.lang).then(function(d) {

                if (d.length > 0){
                    for(var i=0; i< d.length;i++){
                        $scope.month_log.push(d[i]);
                    }
                }
            });
        };

        $scope.sidebar_tabs = function(){
            apis.getSidebarTabs($scope.lang).then(function(data) {
                if (data.comments && data.comments.length > 0){
                    for(var i=0; i< data.comments.length;i++){
                        $scope.comment_posts.push(data.comments[i]);
                    }
                }
                if (data.galleries && data.galleries.length > 0){
                    for(var i=0; i< data.galleries.length;i++){
                        $scope.gallery_posts.push(data.galleries[i]);
                    }
                }
            });
        };



    }

);
