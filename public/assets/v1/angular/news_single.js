

var myApp = angular.module('mainApp',[]);

myApp.filter('htmlToPlaintext', function() {
        return function(text) {
            return angular.element(text).text();
        }
    }
);
myApp.directive('img', function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            // show an image-missing image
            element.error(function () {
                var w = element.width();
                var h = element.height();
                // using 20 here because it seems even a missing image will have ~18px width
                // after this error function has been called

                var url = element.attr('src');
                url = url.replace(new RegExp('Ʒ', 'g'),'3');
                url = url.replace(new RegExp('%C6%B7', 'g'),'3');
                element.prop('src', url);
            });
        }
    }
});
myApp.service('endpoints', function($http,$q){
    return {

        favoritePost: function(lang,id,action){
            var promise = $http.get('/'+lang+'/endpoint/favorites/post/'+id+'/'+action).then(function (response) {
                return response.data;
            });
            return promise;
        },
        fetchFavorites: function(lang){
            var promise = $http.get('/'+lang+'/endpoint/favorites/fetch/posts').then(function (response) {
                return response.data;
            });
            return promise;
        }

    };
});

myApp.controller(
    'newsSingleCtrl',
    function($scope, endpoints){

        $scope.lang;
        $scope.favorite_posts = [];


        $scope.favorite_posts_fetch = function(){
            endpoints.fetchFavorites($scope.lang).then(function(data){
                if(data.length > 0){
                    for(var i=0; i<data.length; i++){
                        $scope.favorite_posts.push(data[i]);
                    }
                    console.log($scope.favorite_posts);
                }
            });
        };

        $scope.favorite_post = function(id){

            if($scope.favorite_posts.indexOf(id) > -1){
                var action = 'unlike';
            }else{
                var action = 'like';
            }

            endpoints.favoritePost($scope.lang,id,action).then(function(data){
                if(action == 'like'){
                    $scope.favorite_posts.push(id);
                }else if(action == 'unlike'){
                    $scope.favorite_posts.splice($scope.favorite_posts.indexOf(id),1);
                }
            });
        };

        $scope.favorite_posts_fetch();




    }

);
